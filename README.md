## Installation

To install and run this project, first make sure the requirements are met and then follow the below steps.

### Requirements
* PHP 8.1 with the following extensions:
  * cli
  * curl
  * fpm (recommended for production, not strictly needed)
  * gd
  * imap
  * intl
  * mbstring
  * mysql
  * opcache
  * xml
  * zip
* composer
* symfony-cli
* yarn
* MariaDB 10
* a web server of your choice (e.g. nginx)
* Mercure Hub (if you want to use live voting and guestlist features)
* pdftk (if you want to print volunteer member forms)
* wkhtmltopdf (if you want to print shift tables)

### Steps
* Clone the repository to where you want to install this webinterface
* `cd` into the directory
* Copy `.env` to `.env.local` and fill in all appropriate values (see below)
* Run `symfony composer install`
* Check all requirements are met with `symfony check:req`
* Run `symfony run yarn install`
* Run `symfony run yarn run build`
* Create a database and user with all access rights to that database
* Run `symfony console doctrine:migrations:migrate`
* Make sure all files are accessible by the web server process and the `var` directory is writeable
  * Best practice is to set the owner of the directory (recursively) to the web user (e.g. `www-data`)
* Point your webserver document root to the `public` directory inside the project directory

### Updating

* Update the source files
  * `git pull`
* Update PHP dependencies
  * `symfony composer update`
* Update JS dependencies if `package.json` has changed
  * `symfony run yarn install`
* Rebuild JS files if they have changed
  * `symfony run yarn run build`
* Migrate database if necessary
  * `symfony console doctrine:migrations:migrate`
* Clear cache
  * `symfony console cache:clear`
* Make sure file permissions are still set correctly

## Configuration
All secret environment variables should be defined in `.env.local`. Never use `.env` for sensitive values, as this file is committed to git!

### Symfony
* Set `APP_ENV` to `prod` or `dev`
* Set `APP_SECRET` to a strong random string. This secret is used to encrypt remember_me cookies and more
 
### Doctrine
Set `DATABASE_URL`:

    DATABASE_URL=mysql://user:password@host:port/database?serverVersion=mariadb-10.x.x

### Mailer
Set `MAILER_URL`:

    MAILER_URL=smtp://myprovider.tld:port?encryption=tls&auth_mode=login&username=myuser%40mydomain.tld&password=mypassword

Note that the `@` character in the user varibale is URL-encoded as `%40`. Make sure to encode all non-alphanumeric characters in values.

Set default from address and staff from address:

    MAILER_FROM_SYSTEM=noreply@mydomain.de
    MAILER_FROM_STAFF=staff@mydomain.de

### Mercure

Set mercure values

    # See https://symfony.com/doc/current/mercure.html#configuration
    # The URL of the Mercure hub, used by the app to publish updates (can be a local URL)
    MERCURE_URL=http://localhost:3000/.well-known/mercure
    # The public URL of the Mercure hub, used by the browser to connect
    MERCURE_PUBLIC_URL=http://localhost:3000/.well-known/mercure
    # the key to sign the JWT
    MERCURE_JWT_SECRET=!ChangeMe!
    # the URI used to create topics
    MERCURE_TOPIC_URI=https://localhost

### wkhtmltopdf
Set the wkhtmltopdf binary path if

    ###> knplabs/knp-snappy-bundle ###
    WKHTMLTOPDF_PATH=/usr/bin/wkhtmltopdf
    WKHTMLTOIMAGE_PATH=/usr/bin/wkhtmltoimage
    ###< knplabs/knp-snappy-bundle ###

### Globals
Global variables are defined in the `src/System/Application/ConfigService.php` file

## Spotify Integration

The application can display Spotify follower counts for artist profiles. This feature is optional and requires Spotify API credentials.

### Setup

1. Go to [Spotify Developer Dashboard](https://developer.spotify.com/dashboard)
2. Log in with your Spotify account
3. Create a new application and select the Web API
4. Get the Client ID and Client Secret
5. Add these credentials to your `.env.local`:
   ```ini
   SPOTIFY_CLIENT_ID=your_client_id_here
   SPOTIFY_CLIENT_SECRET=your_client_secret_here
   ```
