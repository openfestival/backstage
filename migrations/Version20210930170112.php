<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210930170112 extends AbstractMigration {
    public function getDescription(): string {
        return '';
    }

    public function up(Schema $schema): void {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE INDEX IDX_55BDEA305E237E06 ON inventory_item (name)');
        $this->addSql('CREATE INDEX IDX_55BDEA30CF60E67C ON inventory_item (owner)');
        $this->addSql('CREATE INDEX IDX_F65507A147CC8C92 ON inventory_log (action)');
        $this->addSql('ALTER TABLE volunteer CHANGE applied_at applied_at DATETIME DEFAULT \'2019-01-01 00:00:00\' NOT NULL, CHANGE status status VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema): void {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX IDX_55BDEA305E237E06 ON inventory_item');
        $this->addSql('DROP INDEX IDX_55BDEA30CF60E67C ON inventory_item');
        $this->addSql('DROP INDEX IDX_F65507A147CC8C92 ON inventory_log');
        $this->addSql('ALTER TABLE volunteer CHANGE applied_at applied_at DATETIME NOT NULL, CHANGE status status VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'pending\' NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
