<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190924153217 extends AbstractMigration {
    public function getDescription(): string {
        return '';
    }

    public function up(Schema $schema): void {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE band_application (id INT AUTO_INCREMENT NOT NULL, history_id INT NOT NULL, festival_id INT NOT NULL, name VARCHAR(255) NOT NULL, contact_name VARCHAR(255) NOT NULL, contact_email VARCHAR(255) NOT NULL, contact_phone VARCHAR(255) DEFAULT NULL, location VARCHAR(255) NOT NULL, website VARCHAR(255) DEFAULT NULL, facebook VARCHAR(255) DEFAULT NULL, instagram VARCHAR(255) DEFAULT NULL, youtube VARCHAR(255) DEFAULT NULL, spotify VARCHAR(255) DEFAULT NULL, twitter VARCHAR(255) DEFAULT NULL, application LONGTEXT NOT NULL, members INT NOT NULL, fee VARCHAR(255) DEFAULT NULL, technician TINYINT(1) NOT NULL, accommodation TINYINT(1) NOT NULL, applied_at DATETIME DEFAULT NULL, INDEX IDX_A1A0BE8B1E058452 (history_id), INDEX IDX_A1A0BE8B8AEBAF57 (festival_id), UNIQUE INDEX UNIQ_A1A0BE8B1E0584528AEBAF57 (history_id, festival_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE band_application_genre (band_application_id INT NOT NULL, genre_id INT NOT NULL, INDEX IDX_4739A35827B1112E (band_application_id), INDEX IDX_4739A3584296D31F (genre_id), PRIMARY KEY(band_application_id, genre_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE band_application_history (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_79B29B8FA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE genre (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE band_application ADD CONSTRAINT FK_A1A0BE8B1E058452 FOREIGN KEY (history_id) REFERENCES band_application_history (id)');
        $this->addSql('ALTER TABLE band_application ADD CONSTRAINT FK_A1A0BE8B8AEBAF57 FOREIGN KEY (festival_id) REFERENCES festival (id)');
        $this->addSql('ALTER TABLE band_application_genre ADD CONSTRAINT FK_4739A35827B1112E FOREIGN KEY (band_application_id) REFERENCES band_application (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE band_application_genre ADD CONSTRAINT FK_4739A3584296D31F FOREIGN KEY (genre_id) REFERENCES genre (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE band_application_history ADD CONSTRAINT FK_79B29B8FA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE festival ADD band_application_start DATETIME NOT NULL DEFAULT "2000-01-01 00:00:00", ADD band_application_end DATETIME NOT NULL DEFAULT "2000-01-01 00:00:00", ADD band_rating_end DATETIME NOT NULL DEFAULT "2000-01-01 00:00:00", ADD volunteer_application_start DATETIME NOT NULL DEFAULT "2000-01-01 00:00:00", ADD volunteer_application_end DATETIME NOT NULL DEFAULT "2000-01-01 00:00:00"');
    }

    public function down(Schema $schema): void {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE band_application_genre DROP FOREIGN KEY FK_4739A35827B1112E');
        $this->addSql('ALTER TABLE band_application DROP FOREIGN KEY FK_A1A0BE8B1E058452');
        $this->addSql('ALTER TABLE band_application_genre DROP FOREIGN KEY FK_4739A3584296D31F');
        $this->addSql('DROP TABLE band_application');
        $this->addSql('DROP TABLE band_application_genre');
        $this->addSql('DROP TABLE band_application_history');
        $this->addSql('DROP TABLE genre');
        $this->addSql('ALTER TABLE festival DROP band_application_start, DROP band_application_end, DROP band_rating_end, DROP volunteer_application_start, DROP volunteer_application_end');
    }
}
