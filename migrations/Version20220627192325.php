<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220627192325 extends AbstractMigration {
    public function getDescription(): string {
        return '';
    }

    public function up(Schema $schema): void {
        $this->addSql('ALTER TABLE travel_party DROP INDEX UNIQ_2139C95649ABEB17, ADD INDEX IDX_2139C95649ABEB17 (band_id)');
        $this->addSql('ALTER TABLE travel_party CHANGE start_time stage_time DATETIME NOT NULL, ADD checked_in TINYINT(1) NOT NULL');
        $this->addSql('CREATE TABLE travel_party_guestlist (travel_party_id INT NOT NULL, guestlist_id INT NOT NULL, INDEX IDX_19E7826E4B4D731C (travel_party_id), INDEX IDX_19E7826EDE52DBE2 (guestlist_id), PRIMARY KEY(travel_party_id, guestlist_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE travel_party_guestlist ADD CONSTRAINT FK_19E7826E4B4D731C FOREIGN KEY (travel_party_id) REFERENCES travel_party (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE travel_party_guestlist ADD CONSTRAINT FK_19E7826EDE52DBE2 FOREIGN KEY (guestlist_id) REFERENCES guestlist (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE band CHANGE contact_name contact_name VARCHAR(255) DEFAULT NULL, CHANGE contact_email contact_email VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE band_member DROP CONSTRAINT FK_89A1C7A94B4D731C');
        $this->addSql('ALTER TABLE band_member DROP INDEX IDX_89A1C7A94B4D731C');
    }

    public function down(Schema $schema): void {
        $this->addSql('DROP TABLE travel_party_guestlist');
        $this->addSql('ALTER TABLE travel_party DROP INDEX IDX_2139C95649ABEB17, ADD UNIQUE INDEX UNIQ_2139C95649ABEB17 (band_id)');
        $this->addSql('ALTER TABLE travel_party CHANGE stage_time start_time DATETIME NOT NULL, DROP checked_in');
        $this->addSql('ALTER TABLE band CHANGE contact_name contact_name VARCHAR(255) NOT NULL, CHANGE contact_email contact_email VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE band_member ADD CONSTRAINT FK_89A1C7A94B4D731C FOREIGN KEY (travel_party_id) REFERENCES travel_party (id)');
        $this->addSql('ALTER TABLE band_member ADD INDEX IDX_89A1C7A94B4D731C (travel_party_id)');
    }
}
