<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230209182627 extends AbstractMigration {
    public function getDescription(): string {
        return '';
    }

    public function up(Schema $schema): void {
        $this->addSql('ALTER TABLE festival ADD band_application_good DOUBLE PRECISION NOT NULL DEFAULT 2.0, ADD band_application_bad DOUBLE PRECISION NOT NULL DEFAULT 4.0');
    }

    public function down(Schema $schema): void {
        $this->addSql('ALTER TABLE festival DROP band_application_good, DROP band_application_bad');
    }
}
