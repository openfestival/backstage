<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240414124809 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add type and state columns to guestlist table';
    }

    public function up(Schema $schema): void
    {
        $this->addSql(<<<SQL
            CREATE TABLE `guestlist_inviter` (
                `user_id` INT NOT NULL,
                `name` VARCHAR(255) NOT NULL,
                CONSTRAINT FOREIGN KEY fk_user_id (`user_id`) REFERENCES `user` (id) ON DELETE CASCADE ON UPDATE CASCADE,
                PRIMARY KEY(`user_id`)
            );
            SQL);
        $this->addSql(<<<SQL
            ALTER TABLE `guestlist`
                ADD `token` VARCHAR(255) NULL DEFAULT NULL AFTER `id`,
                ADD `email` VARCHAR(255) NULL AFTER `name`,
                ADD `inviter_id` INT NULL DEFAULT NULL AFTER `email`,
                ADD `type` VARCHAR(255) NULL DEFAULT NULL AFTER `email`,
                ADD `state` VARCHAR(255) NULL DEFAULT NULL AFTER `type`,
                ADD CONSTRAINT FOREIGN KEY `fk_guestlist_inviter` (`inviter_id`) REFERENCES `guestlist_inviter` (user_id),
                ADD UNIQUE INDEX `guestlist_uindex_token` (`token`),
                ADD UNIQUE INDEX `guestlist_uindex_festival_email` (`festival_id`, `email`);
            SQL);
        $this->addSql('UPDATE `guestlist` SET `type` = "friend" WHERE `type` IS NULL;');
        $this->addSql('UPDATE `guestlist` SET `state` = "accepted" WHERE `state` IS NULL;');
        $this->addSql('ALTER TABLE `guestlist` MODIFY `type` VARCHAR(255) NOT NULL, MODIFY `state` VARCHAR(255) NOT NULL;');
        $this->addSql(<<<SQL
            ALTER TABLE `email_record`
                MODIFY `sender_id` INT NULL,
                MODIFY `receiver_id` INT NULL,
                ADD `from_email` VARCHAR(255) NULL AFTER `sender_id`,
                ADD `to_email` VARCHAR(255) NULL AFTER `receiver_id`;
            SQL);
        $this->addSql(<<<SQL
            UPDATE `email_record`
                JOIN `user` AS `sender` ON `email_record`.`sender_id` = `sender`.`id`
                JOIN `user` AS `receiver` ON `email_record`.`receiver_id` = `receiver`.`id`
            SET `email_record`.`from_email` = `sender`.`email`,
                `email_record`.`to_email` = `receiver`.`email`;
            SQL);
        $this->addSql(<<<SQL
            ALTER TABLE `email_record`
                MODIFY `from_email` VARCHAR(255) NOT NULL,
                MODIFY `to_email` VARCHAR(255) NOT NULL;
            SQL);
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `guestlist` DROP INDEX `UNIQUE.festival.email`');
        $this->addSql('ALTER TABLE `guestlist` DROP `email`, DROP `type`, DROP `state`;');
        $this->addSql('ALTER TABLE `email_record` DROP `to_email`, DROP `from_email`;');
    }
}
