<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190412114726 extends AbstractMigration {
    public function getDescription(): string {
        return '';
    }

    public function up(Schema $schema): void {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE configuration (id VARCHAR(255) NOT NULL COLLATE utf8_bin, value VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE shift (id INT AUTO_INCREMENT NOT NULL, area_id INT NOT NULL, start DATETIME NOT NULL, end DATETIME NOT NULL, name VARCHAR(255) NOT NULL, description VARCHAR(255) DEFAULT NULL, INDEX IDX_A50B3B45BD0F409C (area_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE shift_area (id INT AUTO_INCREMENT NOT NULL, supervisor_id INT NOT NULL, name VARCHAR(255) NOT NULL, preferable TINYINT(1) NOT NULL, INDEX IDX_E2AD42A719E9AC5F (supervisor_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE shift_volunteer (shift_id INT NOT NULL, volunteer_id INT NOT NULL, start DATETIME DEFAULT NULL, end DATETIME DEFAULT NULL, INDEX IDX_80D20DB2BB70BC0E (shift_id), INDEX IDX_80D20DB28EFAB6B1 (volunteer_id), PRIMARY KEY(shift_id, volunteer_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, name VARCHAR(255) DEFAULT NULL, password VARCHAR(255) NOT NULL, activation_token VARCHAR(255) NOT NULL, registered_at DATETIME NOT NULL, activated_at DATETIME DEFAULT NULL, last_login DATETIME NOT NULL, password_reset VARCHAR(255) DEFAULT NULL, password_reset_timeout DATETIME DEFAULT NULL, force_reset TINYINT(1) NOT NULL, lang VARCHAR(2) NOT NULL, roles LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:simple_array)\', UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE volunteer (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, firstname VARCHAR(255) NOT NULL, surname VARCHAR(255) NOT NULL, gender VARCHAR(255) NOT NULL, birthday DATETIME NOT NULL, phone VARCHAR(255) DEFAULT NULL, shirtsize VARCHAR(255) DEFAULT NULL, friday TINYINT(1) NOT NULL, saturday TINYINT(1) NOT NULL, preferred_areas LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:simple_array)\', experience LONGTEXT DEFAULT NULL, comments LONGTEXT DEFAULT NULL, applied_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_5140DEDBA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE shift ADD CONSTRAINT FK_A50B3B45BD0F409C FOREIGN KEY (area_id) REFERENCES shift_area (id)');
        $this->addSql('ALTER TABLE shift_area ADD CONSTRAINT FK_E2AD42A719E9AC5F FOREIGN KEY (supervisor_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE shift_volunteer ADD CONSTRAINT FK_80D20DB2BB70BC0E FOREIGN KEY (shift_id) REFERENCES shift (id)');
        $this->addSql('ALTER TABLE shift_volunteer ADD CONSTRAINT FK_80D20DB28EFAB6B1 FOREIGN KEY (volunteer_id) REFERENCES volunteer (id)');
        $this->addSql('ALTER TABLE volunteer ADD CONSTRAINT FK_5140DEDBA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
    }

    public function down(Schema $schema): void {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE shift_volunteer DROP FOREIGN KEY FK_80D20DB2BB70BC0E');
        $this->addSql('ALTER TABLE shift DROP FOREIGN KEY FK_A50B3B45BD0F409C');
        $this->addSql('ALTER TABLE shift_area DROP FOREIGN KEY FK_E2AD42A719E9AC5F');
        $this->addSql('ALTER TABLE volunteer DROP FOREIGN KEY FK_5140DEDBA76ED395');
        $this->addSql('ALTER TABLE shift_volunteer DROP FOREIGN KEY FK_80D20DB28EFAB6B1');
        $this->addSql('DROP TABLE configuration');
        $this->addSql('DROP TABLE shift');
        $this->addSql('DROP TABLE shift_area');
        $this->addSql('DROP TABLE shift_volunteer');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE volunteer');
    }
}
