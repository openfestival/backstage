<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use App\Volunteer\Enum\ShiftAreaType;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220513140341 extends AbstractMigration {
    public function getDescription(): string {
        return '';
    }

    public function up(Schema $schema): void {
        $this->addSql('ALTER TABLE shift_area ADD type VARCHAR(255) NOT NULL DEFAULT "' . ShiftAreaType::FESTIVAL->value . '" AFTER name');
        $this->addSql('UPDATE shift_area SET type = "' . ShiftAreaType::CONSTRUCTION->value . '" WHERE name = "Aufbau"');
        $this->addSql('UPDATE shift_area SET type = "' . ShiftAreaType::DISMANTLING->value . '" WHERE name = "Abbau"');
    }

    public function down(Schema $schema): void {
        $this->addSql('ALTER TABLE shift_area DROP type');
    }
}
