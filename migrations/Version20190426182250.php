<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190426182250 extends AbstractMigration {
    public function getDescription(): string {
        return '';
    }

    public function up(Schema $schema): void {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE permission (permission VARCHAR(255) NOT NULL COLLATE utf8_bin, permission_group_id INT NOT NULL, INDEX IDX_E04992AAB6C0CF1 (permission_group_id), PRIMARY KEY(permission_group_id, permission)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE permission_group (id INT AUTO_INCREMENT NOT NULL, parent_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_BB4729B6727ACA70 (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE permission_inheritance (user_id INT NOT NULL, permission_group_id INT NOT NULL, INDEX IDX_F9BA902A76ED395 (user_id), INDEX IDX_F9BA902B6C0CF1 (permission_group_id), PRIMARY KEY(user_id, permission_group_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE permission ADD CONSTRAINT FK_E04992AAB6C0CF1 FOREIGN KEY (permission_group_id) REFERENCES permission_group (id)');
        $this->addSql('ALTER TABLE permission_group ADD CONSTRAINT FK_BB4729B6727ACA70 FOREIGN KEY (parent_id) REFERENCES permission_group (id)');
        $this->addSql('ALTER TABLE permission_inheritance ADD CONSTRAINT FK_F9BA902A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE permission_inheritance ADD CONSTRAINT FK_F9BA902B6C0CF1 FOREIGN KEY (permission_group_id) REFERENCES permission_group (id)');
        $this->addSql('ALTER TABLE user DROP roles');
        $this->addSql('INSERT INTO permission_group (id, parent_id, name) VALUES (1, null, "admin");');
        $this->addSql('INSERT INTO permission (permission, permission_group_id) VALUES ("*", 1);');
    }

    public function down(Schema $schema): void {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE permission DROP FOREIGN KEY FK_E04992AAB6C0CF1');
        $this->addSql('ALTER TABLE permission_group DROP FOREIGN KEY FK_BB4729B6727ACA70');
        $this->addSql('ALTER TABLE permission_inheritance DROP FOREIGN KEY FK_F9BA902B6C0CF1');
        $this->addSql('DROP TABLE permission');
        $this->addSql('DROP TABLE permission_group');
        $this->addSql('DROP TABLE permission_inheritance');
        $this->addSql('ALTER TABLE user ADD roles LONGTEXT DEFAULT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:simple_array)\'');
    }
}
