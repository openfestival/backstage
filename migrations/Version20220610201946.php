<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220610201946 extends AbstractMigration {
    public function getDescription(): string {
        return '';
    }

    public function up(Schema $schema): void {
        $this->addSql('CREATE TABLE guestlist_day (guestlist_id INT NOT NULL, day_id INT NOT NULL, backstage TINYINT(1) NOT NULL, INDEX IDX_C0AE908CDE52DBE2 (guestlist_id), INDEX IDX_C0AE908C9C24126 (day_id), PRIMARY KEY(guestlist_id, day_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE guestlist_day ADD CONSTRAINT FK_C0AE908CDE52DBE2 FOREIGN KEY (guestlist_id) REFERENCES guestlist (id)');
        $this->addSql('ALTER TABLE guestlist_day ADD CONSTRAINT FK_C0AE908C9C24126 FOREIGN KEY (day_id) REFERENCES festival_day (id)');
        // insert days as friday and saturday by hardcoded ID...
        $this->addSql('INSERT INTO guestlist_day (guestlist_id, day_id, backstage) (SELECT id, 7, backstage_checked_in FROM guestlist WHERE friday = 1)');
        $this->addSql('INSERT INTO guestlist_day (guestlist_id, day_id, backstage) (SELECT id, 8, backstage_checked_in FROM guestlist WHERE saturday = 1)');
        $this->addSql('ALTER TABLE guestlist ADD festival_id INT NOT NULL AFTER id, CHANGE plus1 plus SMALLINT DEFAULT NULL, DROP friday, DROP saturday');
        $this->addSql('UPDATE guestlist SET festival_id = (SELECT id FROM festival ORDER BY number LIMIT 1);');
        $this->addSql('CREATE INDEX IDX_75D1A118AEBAF57 ON guestlist (festival_id)');
        $this->addSql('ALTER TABLE guestlist ADD CONSTRAINT FK_75D1A118AEBAF57 FOREIGN KEY (festival_id) REFERENCES festival (id)');

        // fix old stuff
        $this->addSql('ALTER TABLE band_final_score CHANGE opendoor opendoor TINYINT(1) NOT NULL, CHANGE aftershow aftershow TINYINT(1) NOT NULL, CHANGE budget budget TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE inventory_item CHANGE notes notes LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE shift_area CHANGE type type VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE volunteer CHANGE street street VARCHAR(255) NOT NULL, CHANGE zip zip VARCHAR(255) NOT NULL, CHANGE city city VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema): void {
        $this->addSql('ALTER TABLE guestlist DROP FOREIGN KEY FK_75D1A118AEBAF57');
        $this->addSql('DROP INDEX IDX_75D1A118AEBAF57 ON guestlist');
        $this->addSql('ALTER TABLE guestlist ADD friday TINYINT(1) NOT NULL, ADD saturday TINYINT(1) NOT NULL, CHANGE plus plus1 INT DEFAULT NULL, DROP festival_id');
        $this->addSql('UPDATE guestlist g LEFT JOIN guestlist_day d ON g.id = d.guestlist_id AND d.day_id = 7 SET friday = d.day_id IS NOT NULL');
        $this->addSql('UPDATE guestlist g LEFT JOIN guestlist_day d ON g.id = d.guestlist_id AND d.day_id = 8 SET saturday = d.day_id IS NOT NULL');
        $this->addSql('DROP TABLE guestlist_day');

        // restore old stuff
        $this->addSql('ALTER TABLE band_final_score CHANGE opendoor opendoor TINYINT(1) DEFAULT 0 NOT NULL, CHANGE aftershow aftershow TINYINT(1) DEFAULT 0 NOT NULL, CHANGE budget budget TINYINT(1) DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE inventory_item CHANGE notes notes TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE shift_area CHANGE type type VARCHAR(255) DEFAULT \'festival\' NOT NULL');
        $this->addSql('ALTER TABLE volunteer CHANGE street street VARCHAR(255) DEFAULT \'\' NOT NULL, CHANGE zip zip VARCHAR(255) DEFAULT \'\' NOT NULL, CHANGE city city VARCHAR(255) DEFAULT \'\' NOT NULL');
    }
}
