<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190926195135 extends AbstractMigration {
    public function getDescription(): string {
        return '';
    }

    public function up(Schema $schema): void {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE band_application_comment (id INT AUTO_INCREMENT NOT NULL, application_id INT NOT NULL, author_id INT NOT NULL, comment LONGTEXT NOT NULL, created_at DATETIME NOT NULL, edited_at DATETIME DEFAULT NULL, INDEX IDX_CA7CB9A83E030ACD (application_id), INDEX IDX_CA7CB9A8F675F31B (author_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE band_application_favorite (application_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_F492809B3E030ACD (application_id), INDEX IDX_F492809BA76ED395 (user_id), PRIMARY KEY(application_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE band_application_score (application_id INT NOT NULL, user_id INT NOT NULL, score INT NOT NULL, INDEX IDX_F6F0A7F13E030ACD (application_id), INDEX IDX_F6F0A7F1A76ED395 (user_id), PRIMARY KEY(application_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE band_application_comment ADD CONSTRAINT FK_CA7CB9A83E030ACD FOREIGN KEY (application_id) REFERENCES band_application (id)');
        $this->addSql('ALTER TABLE band_application_comment ADD CONSTRAINT FK_CA7CB9A8F675F31B FOREIGN KEY (author_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE band_application_favorite ADD CONSTRAINT FK_F492809B3E030ACD FOREIGN KEY (application_id) REFERENCES band_application (id)');
        $this->addSql('ALTER TABLE band_application_favorite ADD CONSTRAINT FK_F492809BA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE band_application_score ADD CONSTRAINT FK_F6F0A7F13E030ACD FOREIGN KEY (application_id) REFERENCES band_application (id)');
        $this->addSql('ALTER TABLE band_application_score ADD CONSTRAINT FK_F6F0A7F1A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
    }

    public function down(Schema $schema): void {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE band_application_comment');
        $this->addSql('DROP TABLE band_application_favorite');
        $this->addSql('DROP TABLE band_application_score');
    }
}
