<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221201165309 extends AbstractMigration {
    public function getDescription(): string {
        return '';
    }

    public function up(Schema $schema): void {
        $this->addSql('ALTER TABLE festival ADD name VARCHAR(255) NOT NULL AFTER id');
    }

    public function down(Schema $schema): void {
        $this->addSql('ALTER TABLE festival DROP name');
    }
}
