<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220528141059 extends AbstractMigration {
    public function getDescription(): string {
        return '';
    }

    public function up(Schema $schema): void {
        // create new inventory type table and insert existing items
        $this->addSql('CREATE TABLE inventory_item_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, owner VARCHAR(255) NOT NULL, INDEX IDX_C352FABA5E237E06 (name), INDEX IDX_C352FABACF60E67C (owner), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('INSERT INTO inventory_item_type (id, name, description, owner) SELECT id, name, description, owner FROM inventory_item');

        // add reference to new inventory type to item table and update values
        $this->addSql('ALTER TABLE inventory_item ADD type_id INT NOT NULL AFTER id');
        $this->addSql('CREATE INDEX IDX_55BDEA30C54C8C93 ON inventory_item (type_id)');
        $this->addSql('UPDATE inventory_item SET type_id = id');
        $this->addSql('ALTER TABLE inventory_item ADD CONSTRAINT FK_55BDEA30C54C8C93 FOREIGN KEY (type_id) REFERENCES inventory_item_type (id) ON DELETE CASCADE');

        // move category references to new type table
        $this->addSql('ALTER TABLE inventory_item_category DROP FOREIGN KEY FK_94C12FE536BF4A2');
        $this->addSql('ALTER TABLE inventory_item_category ADD CONSTRAINT FK_94C12FE536BF4A2 FOREIGN KEY (inventory_item_id) REFERENCES inventory_item_type (id) ON DELETE CASCADE');

        // remove old values from items and add new columns
        $this->addSql('DROP INDEX IDX_55BDEA305E237E06 ON inventory_item');
        $this->addSql('DROP INDEX IDX_55BDEA30CF60E67C ON inventory_item');
        $this->addSql('ALTER TABLE inventory_item ADD number INT DEFAULT NULL AFTER type_id, ADD borrowed_for VARCHAR(255) NULL AFTER borrowed_id, ADD notes TEXT NULL AFTER count, DROP name, DROP description, DROP owner');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_55BDEA3096901F54 ON inventory_item (number)');
        $this->addSql('CREATE INDEX IDX_55BDEA30C7AC3438 ON inventory_item (borrowed_for)');

        // change images to item types
        $this->addSql('ALTER TABLE inventory_item_image DROP FOREIGN KEY FK_AC744A0C126F525E, DROP INDEX IDX_AC744A0C126F525E');
        $this->addSql('ALTER TABLE inventory_item_image RENAME inventory_item_type_image, CHANGE item_id item_type_id INT NOT NULL, CHANGE image_name file_name VARCHAR(255) NULL');
        $this->addSql('CREATE INDEX IDX_AC744A0C126F525E ON inventory_item_type_image (item_type_id)');
        $this->addSql('ALTER TABLE inventory_item_type_image ADD CONSTRAINT FK_AC744A0C126F525E FOREIGN KEY (item_type_id) REFERENCES inventory_item_type (id) ON DELETE CASCADE');

        // add item type to log
        $this->addSql('ALTER TABLE inventory_log ADD item_type_id INT NOT NULL AFTER id, CHANGE item_id item_id INT DEFAULT NULL');
        $this->addSql('UPDATE inventory_log SET item_type_id = item_id');
        $this->addSql('ALTER TABLE inventory_log ADD CONSTRAINT FK_F65507A1CE11AAC7 FOREIGN KEY (item_type_id) REFERENCES inventory_item_type (id)');
        $this->addSql('CREATE INDEX IDX_F65507A1CE11AAC7 ON inventory_log (item_type_id)');
    }

    public function down(Schema $schema): void {
        // add old columns back to items, remove number column, insert values
        $this->addSql('DROP INDEX UNIQ_55BDEA3096901F54 ON inventory_item');
        $this->addSql('DROP INDEX IDX_55BDEA30C7AC3438 ON inventory_item');
        $this->addSql('ALTER TABLE inventory_item ADD name VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, ADD description LONGTEXT CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, ADD owner VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, DROP number, DROP borrowed_for, DROP notes');
        $this->addSql('UPDATE inventory_item i INNER JOIN inventory_item_type t ON i.type_id = t.id SET i.name = t.name, i.description = t.description, i.owner = t.owner');
        $this->addSql('CREATE INDEX IDX_55BDEA305E237E06 ON inventory_item (name)');
        $this->addSql('CREATE INDEX IDX_55BDEA30CF60E67C ON inventory_item (owner)');

        // move category references to old table
        $this->addSql('ALTER TABLE inventory_item_category DROP FOREIGN KEY FK_94C12FE536BF4A2');
        $this->addSql('ALTER TABLE inventory_item_category ADD CONSTRAINT FK_94C12FE536BF4A2 FOREIGN KEY (inventory_item_id) REFERENCES inventory_item (id) ON DELETE CASCADE');

        // change images to items
        $this->addSql('ALTER TABLE inventory_item_type_image DROP FOREIGN KEY FK_AC744A0C126F525E, DROP INDEX IDX_AC744A0C126F525E');
        $this->addSql('ALTER TABLE inventory_item_type_image RENAME inventory_item_image, CHANGE item_type_id item_id INT NOT NULL, CHANGE file_name image_name VARCHAR(255) NOT NULL');
        $this->addSql('CREATE INDEX IDX_AC744A0C126F525E ON inventory_item_image (item_id)');
        $this->addSql('ALTER TABLE inventory_item_image ADD CONSTRAINT FK_AC744A0C126F525E FOREIGN KEY (item_id) REFERENCES inventory_item (id) ON DELETE CASCADE');

        // remove type from logs (this deletes entries)
        $this->addSql('DELETE FROM inventory_log WHERE item_id IS NULL');
        $this->addSql('ALTER TABLE inventory_log DROP FOREIGN KEY FK_F65507A1CE11AAC7');
        $this->addSql('DROP INDEX IDX_F65507A1CE11AAC7 ON inventory_log');
        $this->addSql('ALTER TABLE inventory_log DROP item_type_id');

        // remove reference and drop type table
        $this->addSql('ALTER TABLE inventory_item DROP FOREIGN KEY FK_55BDEA30C54C8C93, DROP type_id');
        $this->addSql('DROP TABLE inventory_item_type');
    }
}
