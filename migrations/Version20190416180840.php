<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190416180840 extends AbstractMigration {
    public function getDescription(): string {
        return '';
    }

    public function up(Schema $schema): void {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE festival (id INT AUTO_INCREMENT NOT NULL, start DATETIME NOT NULL, end DATETIME NOT NULL, number INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE festival_day (id INT AUTO_INCREMENT NOT NULL, festival_id INT NOT NULL, date DATETIME NOT NULL, INDEX IDX_7BF16EFB8AEBAF57 (festival_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE festival_day ADD CONSTRAINT FK_7BF16EFB8AEBAF57 FOREIGN KEY (festival_id) REFERENCES festival (id)');
        $this->addSql('INSERT INTO festival (start, end, number) VALUES (\'2019-07-19 00:00:00\', \'2019-07-20 00:00:00\', 19)');
        $this->addSql('INSERT INTO festival_day (festival_id, date) VALUES (1, \'2019-07-13 00:00:00\')');
        $this->addSql('INSERT INTO festival_day (festival_id, date) VALUES (1, \'2019-07-14 00:00:00\')');
        $this->addSql('INSERT INTO festival_day (festival_id, date) VALUES (1, \'2019-07-15 00:00:00\')');
        $this->addSql('INSERT INTO festival_day (festival_id, date) VALUES (1, \'2019-07-16 00:00:00\')');
        $this->addSql('INSERT INTO festival_day (festival_id, date) VALUES (1, \'2019-07-17 00:00:00\')');
        $this->addSql('INSERT INTO festival_day (festival_id, date) VALUES (1, \'2019-07-18 00:00:00\')');
        $this->addSql('INSERT INTO festival_day (festival_id, date) VALUES (1, \'2019-07-19 00:00:00\')');
        $this->addSql('INSERT INTO festival_day (festival_id, date) VALUES (1, \'2019-07-20 00:00:00\')');
        $this->addSql('INSERT INTO festival_day (festival_id, date) VALUES (1, \'2019-07-21 00:00:00\')');
        $this->addSql('INSERT INTO festival_day (festival_id, date) VALUES (1, \'2019-07-22 00:00:00\')');
        $this->addSql('INSERT INTO festival_day (festival_id, date) VALUES (1, \'2019-07-23 00:00:00\')');
        $this->addSql('INSERT INTO festival_day (festival_id, date) VALUES (1, \'2019-07-24 00:00:00\')');
        $this->addSql('INSERT INTO festival_day (festival_id, date) VALUES (1, \'2019-07-25 00:00:00\')');
        $this->addSql('INSERT INTO festival_day (festival_id, date) VALUES (1, \'2019-07-26 00:00:00\')');
        $this->addSql('ALTER TABLE shift ADD day_id INT NOT NULL DEFAULT 1');
        $this->addSql('ALTER TABLE shift ADD CONSTRAINT FK_A50B3B459C24126 FOREIGN KEY (day_id) REFERENCES festival_day (id)');
        $this->addSql('CREATE INDEX IDX_A50B3B459C24126 ON shift (day_id)');
        $this->addSql('ALTER TABLE volunteer ADD days LONGTEXT NOT NULL COMMENT \'(DC2Type:simple_array)\', DROP friday, DROP saturday');
        $this->addSql('INSERT INTO configuration (id, value) VALUES (\'current_festival\', \'1\')');
    }

    public function down(Schema $schema): void {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE festival_day DROP FOREIGN KEY FK_7BF16EFB8AEBAF57');
        $this->addSql('ALTER TABLE shift DROP FOREIGN KEY FK_A50B3B459C24126');
        $this->addSql('DROP TABLE festival');
        $this->addSql('DROP TABLE festival_day');
        $this->addSql('DROP INDEX IDX_A50B3B459C24126 ON shift');
        $this->addSql('ALTER TABLE shift DROP day_id');
        $this->addSql('ALTER TABLE volunteer ADD friday TINYINT(1) NOT NULL, ADD saturday TINYINT(1) NOT NULL, DROP days');
    }
}
