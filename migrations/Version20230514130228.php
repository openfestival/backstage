<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230514130228 extends AbstractMigration {
    public function getDescription(): string {
        return '';
    }

    public function up(Schema $schema): void {
        $this->addSql('ALTER TABLE volunteer ADD preferred_shifts INT NOT NULL DEFAULT 2, ADD food VARCHAR(255) NOT NULL DEFAULT "all"');
        $this->addSql('ALTER TABLE volunteer_preferred_areas ADD preference INT NOT NULL DEFAULT 1');
        $this->addSql('UPDATE volunteer_preferred_areas JOIN (SELECT @pref := 0) p SET preference = @pref:=@pref+1');
        $this->addSql('CREATE UNIQUE INDEX preference_unique ON volunteer_preferred_areas (volunteer_id, preference)');
    }

    public function down(Schema $schema): void {
        $this->addSql('ALTER TABLE volunteer DROP preferred_shifts, DROP food');
        $this->addSql('DROP INDEX preference_unique ON volunteer_preferred_areas');
        $this->addSql('ALTER TABLE volunteer_preferred_areas DROP preference');
    }
}
