<?php

namespace App\Tests\System\Application;

use App\System\Application\MapperService;
use App\Tests\AppTestCase;

class MapperServiceTest extends AppTestCase {

    public function testMapToArraySimple() {
        $container = static::getContainer();
        $mapper = $container->get(MapperService::class);

        $data = new MapperServiceTestObject('test', 42, 13.37, null);

        $expected = [
            'stringValue' => $data->stringValue,
            'intValue' => $data->intValue,
            'floatValue' => $data->floatValue,
            'nullValue' => $data->nullValue
        ];

        $result = $mapper->mapToArray($data, [
            'stringValue',
            'intValue',
            'floatValue',
            'nullValue'
        ]);

        $this->assertEquals($expected, $result);
    }

    public function testMapToArrayNested() {
        $container = static::getContainer();
        $mapper = $container->get(MapperService::class);

        $data = new class {
            public object $nestedValue;

            public function __construct() {
                $this->nestedValue = new class {
                    public int $value = 42;
                };
            }
        };

        $expected = [
            'nestedValue' => [
                'value' => 42
            ]
        ];

        $result = $mapper->mapToArray($data, ['nestedValue' => ['value']]);

        $this->assertEquals($expected, $result);
    }

    public function testMapToArrays() {
        $container = static::getContainer();
        $mapper = $container->get(MapperService::class);

        $data = [];
        $expected = [];

        for ($i = 0; $i < 10; $i++) {
            $data[] = new MapperServiceTestObject("$i", $i, (float)$i / 10, null);
            $expected[] = [
                'stringValue' => "$i",
                'intValue' => $i,
                'floatValue' => (float)$i / 10,
                'nullValue' => null,
            ];
        }

        $result = $mapper->mapToArrays($data, [
            'stringValue',
            'intValue',
            'floatValue',
            'nullValue'
        ]);

        $this->assertEquals($expected, $result);
    }

    public function testMapToObject() {
        $container = static::getContainer();
        $mapper = $container->get(MapperService::class);

        $data = [
            'stringValue' => 'test',
            'intValue' => 42,
            'floatValue' => 13.37,
            'nullValue' => null,
        ];

        $expected = new MapperServiceTestObject(
            $data['stringValue'],
            $data['intValue'],
            $data['floatValue'],
            $data['nullValue']
        );

        $result = $mapper->mapToObject($data, MapperServiceTestObject::class);

        $this->assertEquals($expected, $result);
    }

    public function testMapToObjects() {
        $container = static::getContainer();
        $mapper = $container->get(MapperService::class);

        $data = [];
        $expected = [];

        for ($i = 0; $i < 10; $i++) {
            $data[] = [
                'stringValue' => "$i",
                'intValue' => $i,
                'floatValue' => (float)$i / 10,
                'nullValue' => null,
            ];

            $expected[] = new MapperServiceTestObject("$i", $i, (float)$i / 10, null);
        }

        $result = $mapper->mapToObjects($data, MapperServiceTestObject::class);

        $this->assertEquals($expected, $result);
    }

}

class MapperServiceTestObject {
    public string $stringValue;
    public int $intValue;
    public float $floatValue;
    public mixed $nullValue;
    public int $unsedValue;             // not mapped

    public function __construct(string $stringValue, int $intValue, float $floatValue, mixed $nullValue) {
        $this->stringValue = $stringValue;
        $this->intValue = $intValue;
        $this->floatValue = $floatValue;
        $this->nullValue = $nullValue;
        $this->unsedValue = 0;
    }
}
