<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class AppTestCase extends KernelTestCase {
    /**
     * Reboot the kernel before every test method invocation
     */
    protected function setUp(): void {
        $this->bootKernel();
    }
}
