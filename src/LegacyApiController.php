<?php

namespace App;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @deprecated use {@link ApiControllerTrait} instead
 */
abstract class LegacyApiController extends AbstractController {

    protected function resolveBody(Request $request, array $properties) {
        $data = json_decode($request->getContent());

        if ($data === null)
            return false;
        foreach ($properties as $name => $type) {
            if (str_starts_with($type, '?')) {
                // property is optional
                $type = substr($type, 1);
                // if property exists check type
                if (property_exists($data, $name) && !is_null($data->$name) && !call_user_func('is_' . $type, $data->$name))
                    return false;
            } else {
                // property is required
                if (!property_exists($data, $name) || !call_user_func('is_' . $type, $data->$name))
                    return false;
            }
        }
        return $data;
    }

    protected function responseOk($data = ''): Response {
        return new Response(json_encode($data), Response::HTTP_OK, ['Content-Type' => 'application/json']);
    }

    protected function responseBadRequest($message = ''): Response {
        return new Response($message, Response::HTTP_BAD_REQUEST);
    }

    protected function responseNotFound($message = ''): Response {
        return new Response($message, Response::HTTP_NOT_FOUND);
    }

    protected function responseForbidden($message = ''): Response {
        return new Response($message, Response::HTTP_FORBIDDEN);
    }
}
