<?php

namespace App\Festival\Entity;

use App\Festival\Repository\FestivalDayRepository;
use App\Guestlist\Entity\TravelParty;
use App\Volunteer\Entity\Shift;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

#[ORM\Entity(repositoryClass: FestivalDayRepository::class)]
class FestivalDay implements JsonSerializable {
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\JoinColumn(nullable: false)]
    #[ORM\ManyToOne(targetEntity: Festival::class, inversedBy: 'days')]
    private ?Festival $festival = null;

    #[ORM\Column(type: 'datetime_immutable')]
    private ?DateTimeImmutable $date = null;

    #[ORM\OneToMany(mappedBy: 'day', targetEntity: Shift::class, orphanRemoval: true)]
    private Collection $shifts;

    #[ORM\OneToMany(mappedBy: 'day', targetEntity: TravelParty::class)]
    private Collection $travelParties;

    public function __construct() {
        $this->shifts = new ArrayCollection();
        $this->travelParties = new ArrayCollection();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getFestival(): ?Festival {
        return $this->festival;
    }

    public function setFestival(?Festival $festival): self {
        $this->festival = $festival;

        return $this;
    }

    public function getDate(): ?DateTimeImmutable {
        return $this->date;
    }

    public function setDate(DateTimeImmutable $date): self {
        $this->date = $date;

        return $this;
    }

    /**
     * @return Collection|Shift[]
     */
    public function getShifts(): Collection {
        return $this->shifts;
    }

    public function addShift(Shift $shift): self {
        if (!$this->shifts->contains($shift)) {
            $this->shifts[] = $shift;
            $shift->setDay($this);
        }

        return $this;
    }

    public function removeShift(Shift $shift): self {
        if ($this->shifts->contains($shift)) {
            $this->shifts->removeElement($shift);
            // set the owning side to null (unless already changed)
            if ($shift->getDay() === $this) {
                $shift->setDay(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TravelParty[]
     */
    public function getTravelParties(): Collection {
        return $this->travelParties;
    }

    public function addTravelParty(TravelParty $travelParty): self {
        if (!$this->travelParties->contains($travelParty)) {
            $this->travelParties[] = $travelParty;
            $travelParty->setDay($this);
        }

        return $this;
    }

    public function removeTravelParty(TravelParty $travelParty): self {
        if ($this->travelParties->contains($travelParty)) {
            $this->travelParties->removeElement($travelParty);
            // set the owning side to null (unless already changed)
            if ($travelParty->getDay() === $this) {
                $travelParty->setDay(null);
            }
        }

        return $this;
    }

    public function jsonSerialize(): array {
        return [
            'id' => $this->id,
            'festivalId' => $this->festival->getId(),
            'date' => $this->date->getTimestamp(),
        ];
    }
}
