<?php

namespace App\Festival\Entity;

use App\Band\Entity\Application;
use App\Band\Enum\BandFinalsStatus;
use App\Festival\Repository\FestivalRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;
use JsonSerializable;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: FestivalRepository::class)]
class Festival implements JsonSerializable {
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $name = null;

    #[ORM\Column(type: 'datetime_immutable')]
    private ?DateTimeImmutable $start = null;

    #[ORM\Column(type: 'datetime_immutable')]
    private ?DateTimeImmutable $end = null;

    #[ORM\Column(type: 'integer')]
    private ?int $number = null;

    #[ORM\OneToMany(mappedBy: 'festival', targetEntity: FestivalDay::class, orphanRemoval: true)]
    private Collection $days;

    #[ORM\OneToMany(mappedBy: 'festival', targetEntity: Application::class, orphanRemoval: true)]
    private Collection $bandApplications;

    #[ORM\Column(type: 'datetime_immutable')]
    private ?DateTimeImmutable $bandApplicationStart = null;

    #[ORM\Column(type: 'datetime_immutable')]
    private ?DateTimeImmutable $bandApplicationEnd = null;

    #[ORM\Column(type: 'datetime_immutable')]
    private ?DateTimeImmutable $bandRatingEnd = null;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank]
    private ?string $bandFinalStatus = null;

    #[ORM\ManyToOne(targetEntity: Application::class)]
    private ?Application $bandFinalCurrentVote = null;

    #[ORM\Column(type: 'float')]
    #[Assert\GreaterThanOrEqual(1)]
    #[Assert\LessThan(propertyPath: 'bandApplicationBad')]
    #[Assert\LessThanOrEqual(6)]
    private float $bandApplicationGood = 2;

    #[ORM\Column(type: 'float')]
    #[Assert\GreaterThanOrEqual(1)]
    #[Assert\GreaterThan(propertyPath: 'bandApplicationGood')]
    #[Assert\LessThanOrEqual(6)]
    private float $bandApplicationBad = 4;

    #[ORM\Column(type: 'datetime_immutable')]
    private ?DateTimeImmutable $volunteerApplicationStart = null;

    #[ORM\Column(type: 'datetime_immutable')]
    private ?DateTimeImmutable $volunteerApplicationEnd = null;

    public function __construct() {
        $this->days = new ArrayCollection();
        $this->bandApplications = new ArrayCollection();
        $this->bandFinalStatus = BandFinalsStatus::CLOSED;
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getName(): ?string {
        return $this->name;
    }

    public function setName(?string $name): self {
        $this->name = $name;

        return $this;
    }

    public function getStart(): ?DateTimeImmutable {
        return $this->start;
    }

    public function setStart(DateTimeImmutable $start): self {
        $this->start = $start;

        return $this;
    }

    public function getEnd(): ?DateTimeImmutable {
        return $this->end;
    }

    public function setEnd(DateTimeImmutable $end): self {
        $this->end = $end;

        return $this;
    }

    public function getNumber(): ?int {
        return $this->number;
    }

    public function setNumber(int $number): self {
        $this->number = $number;

        return $this;
    }

    /**
     * @return Collection|FestivalDay[]
     */
    public function getDays(): Collection {
        return $this->days;
    }

    public function addDay(FestivalDay $day): self {
        if (!$this->days->contains($day)) {
            $this->days[] = $day;
            $day->setFestival($this);
        }

        return $this;
    }

    public function removeDay(FestivalDay $day): self {
        if ($this->days->contains($day)) {
            $this->days->removeElement($day);
            // set the owning side to null (unless already changed)
            if ($day->getFestival() === $this) {
                $day->setFestival(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Application[]
     */
    public function getBandApplications(): Collection {
        return $this->bandApplications;
    }

    public function addBandApplication(Application $bandApplication): self {
        if (!$this->bandApplications->contains($bandApplication)) {
            $this->bandApplications[] = $bandApplication;
            $bandApplication->setFestival($this);
        }

        return $this;
    }

    public function removeBandApplication(Application $bandApplication): self {
        if ($this->bandApplications->contains($bandApplication)) {
            $this->bandApplications->removeElement($bandApplication);
            // set the owning side to null (unless already changed)
            if ($bandApplication->getFestival() === $this) {
                $bandApplication->setFestival(null);
            }
        }

        return $this;
    }

    public function getBandApplicationStart(): ?DateTimeImmutable {
        return $this->bandApplicationStart;
    }

    public function setBandApplicationStart(DateTimeImmutable $bandApplicationStart): self {
        $this->bandApplicationStart = $bandApplicationStart;

        return $this;
    }

    public function getBandApplicationEnd(): ?DateTimeImmutable {
        return $this->bandApplicationEnd;
    }

    public function setBandApplicationEnd(DateTimeImmutable $bandApplicationEnd): self {
        $this->bandApplicationEnd = $bandApplicationEnd;

        return $this;
    }

    public function getBandRatingEnd(): ?DateTimeImmutable {
        return $this->bandRatingEnd;
    }

    public function setBandRatingEnd(DateTimeImmutable $bandRatingEnd): self {
        $this->bandRatingEnd = $bandRatingEnd;

        return $this;
    }

    public function getBandFinalStatus(): ?string {
        return $this->bandFinalStatus;
    }

    public function setBandFinalStatus(string $bandFinalStatus): self {
        if (!in_array($bandFinalStatus, BandFinalsStatus::getAvailableTypes()))
            throw new InvalidArgumentException("Invalid status");
        $this->bandFinalStatus = $bandFinalStatus;

        return $this;
    }

    public function getBandFinalCurrentVote(): ?Application {
        return $this->bandFinalCurrentVote;
    }

    public function setBandFinalCurrentVote(?Application $bandFinalCurrentVote): self {
        $this->bandFinalCurrentVote = $bandFinalCurrentVote;

        return $this;
    }

    public function getBandApplicationGood(): float {
        return $this->bandApplicationGood;
    }

    public function setBandApplicationGood(float $bandApplicationGood): self {
        $this->bandApplicationGood = $bandApplicationGood;

        return $this;
    }

    public function getBandApplicationBad(): float {
        return $this->bandApplicationBad;
    }

    public function setBandApplicationBad(float $bandApplicationBad): self {
        $this->bandApplicationBad = $bandApplicationBad;

        return $this;
    }

    public function getVolunteerApplicationStart(): ?DateTimeImmutable {
        return $this->volunteerApplicationStart;
    }

    public function setVolunteerApplicationStart(DateTimeImmutable $volunteerApplicationStart): self {
        $this->volunteerApplicationStart = $volunteerApplicationStart;

        return $this;
    }

    public function getVolunteerApplicationEnd(): ?DateTimeImmutable {
        return $this->volunteerApplicationEnd;
    }

    public function setVolunteerApplicationEnd(DateTimeImmutable $volunteerApplicationEnd): self {
        $this->volunteerApplicationEnd = $volunteerApplicationEnd;

        return $this;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'start' => $this->start->getTimestamp(),
            'end' => $this->end->getTimestamp(),
            'number' => $this->number,
            'days' => $this->days->toArray(),
        ];
    }
}
