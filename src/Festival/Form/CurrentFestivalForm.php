<?php

namespace App\Festival\Form;

use App\Festival\Entity\Festival;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class CurrentFestivalForm extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('festival', EntityType::class, [
                'class' => Festival::class,
                'choice_label' => function (Festival $festival) {
                    return $festival->getName() . ' #' . $festival->getNumber() . ' (' . $festival->getStart()->format("d.m.Y") . ' - ' . $festival->getEnd()->format('d.m.Y') . ')';
                }
            ])
            ->add('submit', SubmitType::class);
    }
}