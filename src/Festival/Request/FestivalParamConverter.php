<?php

namespace App\Festival\Request;

use App\Festival\Entity\Festival;
use App\Festival\Repository\FestivalRepository;
use App\System\Application\ConfigService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Automatically fetches a {@link Festival} entity via doctrine. If no festival was given as request parameter,
 * the current festival as configured is fetched via the {@link ConfigService}.
 */
class FestivalParamConverter implements ParamConverterInterface {
    private ConfigService $configService;
    private FestivalRepository $festivalRepository;

    /**
     * @param ConfigService $configService
     * @param FestivalRepository $festivalRepository
     */
    public function __construct(ConfigService $configService, FestivalRepository $festivalRepository) {
        $this->configService = $configService;
        $this->festivalRepository = $festivalRepository;
    }

    public function apply(Request $request, ParamConverter $configuration): bool {
        $name = $configuration->getName();
        $festival = $request->attributes->get($name);

        // if parameter is not set return the current festival
        if (is_null($festival)) {
            $request->attributes->set($name, $this->configService->getCurrentFestival());
            return true;
        }

        // if parameter is already the correct entity, return
        if ($festival instanceof Festival)
            return true;
        // if parameter is not an int abort
        if (!is_int($festival))
            return false;
        // else fetch the corresponding festival by id
        $request->attributes->set($name, $this->festivalRepository->find($festival));
        return true;
    }

    public function supports(ParamConverter $configuration): bool {
        return $configuration->getClass() === 'App\Festival\Entity\Festival';
    }
}
