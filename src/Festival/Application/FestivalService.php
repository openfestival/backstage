<?php

namespace App\Festival\Application;

use App\Festival\Entity\Festival;
use App\Festival\Entity\FestivalDay;
use App\Festival\Repository\FestivalDayRepository;
use App\Festival\Repository\FestivalRepository;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;

class FestivalService
{
    public function __construct(
        private readonly EntityManagerInterface $doctrine,
        private readonly FestivalRepository $festivalRepository,
        private readonly FestivalDayRepository $festivalDayRepository,
    ) {}

    public function getFestival(int $id): ?Festival
    {
        return $this->festivalRepository->find($id);
    }

    public function getFestivalDay(int $id): ?FestivalDay
    {
        return $this->festivalDayRepository->find($id);
    }

    public function getOtherFestivals(Festival $except): array
    {
        return $this->festivalRepository->findOtherFestivals($except);
    }

    public function getAllFestivals(): array
    {
        return $this->festivalRepository->findAllOrderedByNumber();
    }

    public function saveFestival(Festival $festival): void
    {
        $this->doctrine->persist($festival);
        $this->doctrine->flush();
    }

    public function getFestivalDaysArray(Festival $festival): array
    {
        return $this->festivalDayRepository->findAllWhereFestivalAsArray($festival);
    }

    public function saveFestivalDay(FestivalDay $day)
    {
        $this->doctrine->persist($day);
        $this->doctrine->flush();
    }

    public function deleteFestivalDay(FestivalDay $day): bool
    {
        try {
            $this->doctrine->remove($day);
            $this->doctrine->flush();
            return true;
        } catch (ForeignKeyConstraintViolationException) {
            return false;
        }
    }

}
