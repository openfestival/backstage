<?php

namespace App\Festival\Controller;

use App\Festival\Application\FestivalService;
use App\Festival\Entity\Festival;
use App\Festival\Form\CurrentFestivalForm;
use App\Festival\Form\FestivalForm;
use App\System\Application\ConfigService;
use App\System\Application\MapperService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Contracts\Translation\TranslatorInterface;

class AdminController extends AbstractController {
    private FestivalService $service;
    private MapperService $mapper;
    private TranslatorInterface $trans;
    private ConfigService $config;

    public function __construct(FestivalService $service, MapperService $mapper, TranslatorInterface $trans, ConfigService $config) {
        $this->service = $service;
        $this->mapper = $mapper;
        $this->trans = $trans;
        $this->config = $config;
    }

    #[Route(path: '/admin/festival', name: 'admin_festival')]
    public function index(Request $request): Response {
        $this->denyAccessUnlessGranted('admin.festival');

        // new festival form
        $festival = new Festival();
        $newFestivalForm = $this->createForm(FestivalForm::class, $festival);
        $newFestivalForm->handleRequest($request);
        if ($newFestivalForm->isSubmitted() && $newFestivalForm->isValid()) {
            $this->service->saveFestival($festival);
            $this->addFlash('success', $this->trans->trans('changes.saved'));
            return $this->redirectToRoute('admin_festival_edit', ['festival' => $festival->getId()]);
        }

        // active festival form
        $currentFestivalForm = $this->createForm(CurrentFestivalForm::class);
        // set current festival to be selected
        $currentFestivalForm->get('festival')->setData($this->config->getCurrentFestival());
        $currentFestivalForm->handleRequest($request);
        if ($currentFestivalForm->isSubmitted() && $currentFestivalForm->isValid()) {
            $this->config->setCurrentFestival($currentFestivalForm->get('festival')->getData());
            $this->addFlash('success', $this->trans->trans('festival.current.changed'));
        }

        // existing festivals
        $festivals = $this->service->getAllFestivals();

        return $this->render('festival/admin.html.twig', [
            'festivals' => $festivals,
            'newFestivalForm' => $newFestivalForm->createView(),
            'currentFestivalForm' => $currentFestivalForm->createView()
        ]);
    }

    #[Route(path: '/admin/festival/{festival<\d+>}', name: 'admin_festival_edit')]
    public function edit(Festival $festival, Request $request): Response {
        $this->denyAccessUnlessGranted('admin.festival');

        $form = $this->createForm(FestivalForm::class, $festival);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->service->saveFestival($festival);
            $this->addFlash('success', $this->trans->trans('changes.saved'));
            return $this->redirectToRoute('admin_festival');
        }

        return $this->render('festival/edit.html.twig', [
            'festival' => $festival,
            'days' => $this->mapper->serializeToJson($this->service->getFestivalDaysArray($festival), [
                AbstractNormalizer::CALLBACKS => [
                    'festival' => function ($innerObject) {
                        return $innerObject instanceof Festival ? $innerObject->getId() : '';
                    }
                ]
            ]),
            'form' => $form->createView()
        ]);
    }
}
