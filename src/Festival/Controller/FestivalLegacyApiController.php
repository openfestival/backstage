<?php

namespace App\Festival\Controller;

use App\LegacyApiController;
use App\Festival\Application\FestivalService;
use App\Festival\Entity\Festival;
use App\Festival\Entity\FestivalDay;
use App\System\Application\MapperService;
use DateTimeImmutable;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

/**
 * @deprecated Use FestivalApiController instead
 */
#[Route(path: '/api/festival')]
class FestivalLegacyApiController extends LegacyApiController {
    private FestivalService $service;
    private MapperService $mapper;

    public function __construct(FestivalService $service, MapperService $mapper) {
        $this->service = $service;
        $this->mapper = $mapper;
    }

    #[Route(path: '/day', methods: ['POST'])]
    public function postDay(Request $request): Response {
        if (!$this->isGranted('admin.festival'))
            return $this->responseForbidden();

        $data = $this->resolveBody($request, [
            'festival' => 'integer',
            'date' => 'string'
        ]);
        if ($data === false)
            return $this->responseBadRequest();

        $festival = $this->service->getFestival($data->festival);
        if ($festival == null)
            return $this->responseBadRequest();
        try {
            $date = new DateTimeImmutable($data->date);
        } catch (Exception) {
            return $this->responseBadRequest();
        }

        $day = new FestivalDay();
        $day->setFestival($festival);
        $day->setDate($date);
        $this->service->saveFestivalDay($day);

        return $this->responseOk($this->mapper->serializeToJson($day, [
            AbstractNormalizer::CALLBACKS => [
                'festival' => function ($innerObject, $outerObject, string $attributeName, string $format = null, array $context = []) {
                    return $innerObject instanceof Festival ? $innerObject->getId() : '';
                }
            ]
        ]));
    }

    #[Route(path: '/day/{day<\d+>}', methods: ['DELETE'])]
    public function deleteDay(FestivalDay $day): Response {
        if (!$this->isGranted('admin.festival'))
            return $this->responseForbidden();

        if ($this->service->deleteFestivalDay($day))
            return $this->responseOk();
        else return $this->responseBadRequest();
    }
}
