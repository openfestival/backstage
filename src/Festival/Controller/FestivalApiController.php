<?php
declare(strict_types=1);

namespace App\Festival\Controller;

use App\Festival\Entity\Festival;
use App\Festival\Repository\FestivalDayRepository;
use App\System\Application\ConfigService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route(path: '/api/v1/festival')]
class FestivalApiController extends AbstractController
{
    public function __construct(
        private readonly ConfigService $configService,
        private readonly FestivalDayRepository $festivalDayRepository,
    ) {}

    #[Route(path: '/current', methods: ['GET'])]
    public function getCurrentFestival(): Response
    {
        return $this->json($this->configService->getCurrentFestival());
    }

    #[Route(path: '/{festival<\d+>}', methods: ['GET'])]
    public function getFestival(Festival $festival): Response
    {
        return $this->json($festival);
    }

    #[Route(path: '/{festival<\d+>}/days', methods: ['GET'])]
    public function getDays(Festival $festival): Response
    {
        $days = $this->festivalDayRepository->findBy([
            'festival' => $festival,
        ]);
        return $this->json($days);
    }
}
