<?php

namespace App\Festival\Repository;

use App\Festival\Entity\Festival;
use App\Festival\Entity\FestivalDay;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FestivalDay|null find($id, $lockMode = null, $lockVersion = null)
 * @method FestivalDay|null findOneBy(array $criteria, array $orderBy = null)
 * @method FestivalDay[]    findAll()
 * @method Collection<FestivalDay>    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FestivalDayRepository extends ServiceEntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, FestivalDay::class);
    }

    /**
     * Finds all FestivalDays that are between start and end of the festival
     *
     * @param Festival $festival
     * @return FestivalDay[]
     */
    public function findAllDuringFestival(Festival $festival): array {
        return $this->createQueryBuilder('d')
            ->where('d.festival = :festival', 'd.date >= :start', 'd.date <= :end')
            ->orderBy('d.date')
            ->setParameters([
                'festival' => $festival,
                'start' => $festival->getStart(),
                'end' => $festival->getEnd()
            ])
            ->getQuery()
            ->getResult();
    }

    public function findAllWhereFestivalAsArray(Festival $festival): array {
        return $this->createQueryBuilder('d')
            ->select('d.id', 'd.date')
            ->where('d.festival = :festival')
            ->orderBy('d.date')
            ->setParameter('festival', $festival)
            ->getQuery()
            ->getArrayResult();
    }
}
