<?php

namespace App\Festival\Repository;

use App\Festival\Entity\Festival;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Festival|null find($id, $lockMode = null, $lockVersion = null)
 * @method Festival|null findOneBy(array $criteria, array $orderBy = null)
 * @method Festival[]    findAll()
 * @method Festival[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FestivalRepository extends ServiceEntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Festival::class);
    }

    /**
     * Finds all festivals except the given one.
     *
     * @param Festival $except festival to exclude
     * @return Festival[] the other festivals in descending order
     */
    public function findOtherFestivals(Festival $except): array {
        return $this->createQueryBuilder('f')
            ->where('f != :except')
            ->orderBy('f.number', 'DESC')
            ->setParameter('except', $except)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Festival[] all festivals, ordered by number
     */
    public function findAllOrderedByNumber(): array {
        return $this->createQueryBuilder('f')
            ->orderBy('f.number')
            ->getQuery()
            ->getResult();
    }
}
