<?php

namespace App\System\Repository;

use App\Security\Entity\User;
use App\System\Entity\EmailRecord;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EmailRecord|null find($id, $lockMode = null, $lockVersion = null)
 * @method EmailRecord|null findOneBy(array $criteria, array $orderBy = null)
 * @method EmailRecord[]    findAll()
 * @method EmailRecord[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EmailRecordRepository extends ServiceEntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, EmailRecord::class);
    }

    /**
     * Finds all email records sent to the given user sorted by date descending
     *
     * @param User $user receiver of the email records
     * @return array all found email records sent to the given user
     */
    public function findAllWhereReceivedOrderBySentAtDescending(User $user): array {
        return $this->createQueryBuilder('e')
            ->where('e.receiver = :receiver')
            ->orderBy('e.sent_at', 'DESC')
            ->setParameter('receiver', $user)
            ->getQuery()
            ->getResult();
    }
}
