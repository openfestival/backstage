<?php

namespace App\System\EventSubscriber;

use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;

class SetLocalTimezoneSubscriber implements EventSubscriberInterface
{
    public function __construct(
        #[Autowire(env: 'APP_TIMEZONE')]
        private readonly string $timezone,
    ) {}

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => ['setDefaultTimezone', 1024],
        ];
    }

    public function setDefaultTimezone(): void
    {
        date_default_timezone_set($this->timezone);
    }
}
