<?php

namespace App\System\EventSubscriber;

use App\Security\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Http\FirewallMapInterface;

/**
 * Updates the preferred language if the `lang` parameter is set in the request. Defaults to the session value.
 */
class LanguageRequestSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private readonly RequestStack $requestStack,
        private readonly Security $security,
        private readonly EntityManagerInterface $doctrine,
        private readonly FirewallMapInterface $firewallMap,
    ) {}

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => ['onKernelRequest', 20],
            KernelEvents::CONTROLLER => ['onKernelController', 10],
        ];
    }

    /**
     * Sets the locale for the request. Uses the `lang` query parameter if it is present, otherwise
     * the current session value is used.
     */
    public function onKernelRequest(RequestEvent $event): void
    {
        $request = $event->getRequest();
        # We cannot use the request attribute '_stateless' yet as this event is processed before the firewall resolves
        if ($this->firewallMap->getFirewallConfig($request)?->isStateless()) {
            return;
        }
        $lang = $request->get('lang');
        if ($lang === 'de' || $lang === 'en') {
            $this->requestStack->getSession()->set('_locale', $lang);
        } else {
            $lang = $this->requestStack->getSession()->get('_locale', 'de');
        }
        $request->setLocale($lang);
        switch ($lang) {
            case 'de':
                setlocale(LC_ALL, 'de', 'de_DE', 'de_DE.utf8');
                break;
            case 'en':
                setlocale(LC_ALL, 'en', 'en_GB', 'en_GB.utf8');
                break;
        }
    }

    /**
     * Saves the locale in the user entity if it does not match the current value
     */
    public function onKernelController(ControllerEvent $event): void
    {
        if ($this->security->isGranted('IS_AUTHENTICATED')) {
            $locale = $event->getRequest()->getLocale();
            /** @var User $user */
            $user = $this->security->getUser();
            if ($locale !== $user->getLang()) {
                $user->setLang($locale);
                $this->doctrine->flush();
            }
        }
    }
}
