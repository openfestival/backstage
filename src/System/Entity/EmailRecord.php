<?php

namespace App\System\Entity;

use App\Security\Entity\User;
use App\System\Repository\EmailRecordRepository;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: EmailRecordRepository::class)]
class EmailRecord {
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\Email]
    private ?string $fromEmail = null;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\Email]
    private ?string $toEmail = null;

    #[ORM\JoinColumn(nullable: true)]
    #[ORM\ManyToOne(targetEntity: User::class)]
    private ?User $sender = null;

    #[ORM\JoinColumn(nullable: true)]
    #[ORM\ManyToOne(targetEntity: User::class)]
    private ?User $receiver = null;

    #[ORM\Column(type: 'datetime_immutable')]
    private ?DateTimeImmutable $sent_at = null;

    #[ORM\Column(type: 'text')]
    private ?string $subject = null;

    #[ORM\Column(type: 'text')]
    private ?string $text = null;

    public function getId(): ?int {
        return $this->id;
    }

    public function getFromEmail(): ?string
    {
        return $this->fromEmail;
    }

    public function setFromEmail(?string $fromEmail): self
    {
        $this->fromEmail = $fromEmail;

        return $this;
    }

    public function getToEmail(): ?string
    {
        return $this->toEmail;
    }

    public function setToEmail(?string $toEmail): self
    {
        $this->toEmail = $toEmail;

        return $this;
    }

    public function getSender(): ?User {
        return $this->sender;
    }

    public function setSender(?User $sender): self {
        $this->sender = $sender;

        return $this;
    }

    public function getReceiver(): ?User {
        return $this->receiver;
    }

    public function setReceiver(?User $receiver): self {
        $this->receiver = $receiver;

        return $this;
    }

    public function getSentAt(): ?DateTimeImmutable {
        return $this->sent_at;
    }

    public function setSentAt(DateTimeImmutable $sent_at): self {
        $this->sent_at = $sent_at;

        return $this;
    }

    public function getSubject(): ?string {
        return $this->subject;
    }

    public function setSubject(string $subject): self {
        $this->subject = $subject;

        return $this;
    }

    public function getText(): ?string {
        return $this->text;
    }

    public function setText(string $text): self {
        $this->text = $text;

        return $this;
    }
}
