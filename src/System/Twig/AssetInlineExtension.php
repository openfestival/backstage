<?php

namespace App\System\Twig;

use App\System\Exception\InternalServerErrorHttpException;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Mime\MimeTypes;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class AssetInlineExtension extends AbstractExtension {
    private MimeTypes $mimeTypes;

    public function __construct(
        #[Autowire(param: 'kernel.project_dir')]
        private readonly string $projectDir,
        private readonly Filesystem $filesystem
    ) {
        $this->mimeTypes = new MimeTypes();
    }

    public function getFunctions(): array {
        return [
            new TwigFunction('asset_inline', [$this, 'loadFileAndEncode'])
        ];
    }

    /**
     * Loads a file from the `assets` directory and encodes it using {@link base64_encode()} to include in the `src`
     * tag of images or for similar applications.
     *
     * Automatically inserts required mime type based on the file extension if able.
     *
     * @param string $path path of asset to load
     * @param string|null $mimeType mime type of asset. Tries to guess this from the file extension if null.
     * @return string the base64 encoded file
     */
    public function loadFileAndEncode(string $path, ?string $mimeType = null): string {
        // load path from assets directory in project dir
        $path = $this->projectDir . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . $path;
        // check file exists
        if (!$this->filesystem->exists($path))
            throw new InternalServerErrorHttpException('Cannot find file at path: ' . $path);

        // encode file contents
        $encoded = base64_encode(file_get_contents($path));
        // find mime type
        if ($mimeType == null) {
            $mimeTypes = $this->mimeTypes->getMimeTypes(pathinfo($path, PATHINFO_EXTENSION));
            if (empty($mimeTypes))
                throw new InternalServerErrorHttpException('Unable to find mime type from file extension, please specify directly.');
            $mimeType = $mimeTypes[0];
        }

        return 'data:' . $mimeType . ';base64,' . $encoded;
    }

    public function getName(): string {
        return 'asset_inline';
    }
}
