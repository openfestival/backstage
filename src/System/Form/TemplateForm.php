<?php

namespace App\System\Form;

use App\System\Entity\Template;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TemplateForm extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('id', TextType::class, [
                'empty_data' => ''
            ])
            ->add('description', TextType::class)
            ->add('subject', TextType::class)
            ->add('text', TextareaType::class, [
                'attr' => ['rows' => 20],
            ])
            ->add('variables', CollectionType::class, [
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true
            ])
            ->add('submit', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Template::class
        ]);
    }
}
