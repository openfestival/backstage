<?php

namespace App\System\Application;

use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mercure\Authorization;
use Symfony\Component\Mercure\Discovery;
use Symfony\Component\Mercure\HubInterface;
use Symfony\Component\Mercure\Update;

class MercureService
{

    public function __construct(
        private readonly HubInterface $hub,
        private readonly Discovery $discovery,
        private readonly Authorization $authorization,
        #[Autowire(param: 'app.mercure_topic_uri')]
        private readonly string $uri,
    ) {}

    /**
     * Publishes a private update to the Mercure Hub.
     *
     * @param string $topic path without leading slash
     * @param mixed $data the data to publish. Will be encoded using json_encode
     */
    public function publish(string $topic, mixed $data): void
    {
        $update = new Update($this->getTopicURI($topic), json_encode($data, JSON_THROW_ON_ERROR), true);
        $this->hub->publish($update);
    }

    /**
     * Sets the cookie required by the client to authorize the subscriptions at the mercure hub for the given topics.
     *
     * @param Request $request the request to authorize
     * @param string[] $subscribe topics to include in the authorization cookie (without leading slash)
     */
    public function authorizeSubscription(Request $request, array $subscribe): void
    {
        $this->authorization->setCookie($request, array_map(fn($path): string => $this->getTopicURI($path), $subscribe));
    }

    /**
     * Adds a Link HTTP header to the given request containing the URL of the mercure hub.
     *
     * @param Request $request the request to add the link to
     */
    public function addDiscovery(Request $request): void
    {
        $this->discovery->addLink($request);
    }

    /**
     * Adds the mercure topic uri as an additional HTTP header to the given response. The client uses this to create
     * the full topic URIs to subscribe to.
     *
     * @param Response $response the response to add the header to
     */
    public function finalizeResponse(Response $response): void
    {
        $response->headers->set('X-Mercure-Topic-Uri', $this->uri);
    }

    /**
     * Creates the full URI for a topic.
     *
     * @param string $path path without leading slash
     * @return string full URI describing the given path
     */
    private function getTopicURI(string $path): string
    {
        return $this->uri . '/' . $path;
    }
}
