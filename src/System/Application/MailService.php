<?php

namespace App\System\Application;

use App\Band\Entity\Application;
use App\Security\Entity\User;
use App\System\Entity\EmailRecord;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class MailService
{

    public function __construct(
        private readonly MailerInterface $mailer,
        private readonly UrlGeneratorInterface $urlGenerator,
        private readonly TranslatorInterface $translator,
        private readonly EntityManagerInterface $entityManager,
        #[Autowire(param: 'app.system_email')]
        private readonly string $systemAddress,
    ) {}

    public function getSystemAddress(): string
    {
        return $this->systemAddress;
    }

    public function sendEmail(EmailRecord $record): void
    {
        if ($record->getFromEmail() === null) {
            $record->setFromEmail($this->systemAddress);
        }
        $record->setSentAt(new DateTimeImmutable());
        $this->entityManager->persist($record);
        $this->entityManager->flush();

        $email = (new TemplatedEmail())
            ->from($record->getFromEmail())
            ->to($record->getToEmail())
            ->subject($record->getSubject())
            ->text($record->getText())
            ->htmlTemplate('email/empty_markdown.html.twig')
            ->context(['content' => $record->getText()]);
        $this->mailer->send($email);
    }

    public function sendRegisterEmail(User $user): void
    {
        $activationLink = $this->urlGenerator->generate('app_activate', [
            'user' => $user->getId(),
            'token' => $user->getActivationToken(),
        ], UrlGeneratorInterface::ABSOLUTE_URL);

        $email = (new TemplatedEmail())
            ->to($user->getEmail())
            ->subject($this->translator->trans('email.register.title'))
            ->textTemplate('email/registration.txt.twig')
            ->htmlTemplate('email/registration.html.twig')
            ->context(['link' => $activationLink]);
        $this->mailer->send($email);
    }

    public function sendChangeaddressEmail(User $user): void
    {
        $activationLink = $this->urlGenerator->generate('app_activate', [
            'user' => $user->getId(),
            'token' => $user->getActivationToken(),
        ], UrlGeneratorInterface::ABSOLUTE_URL);

        $email = (new TemplatedEmail())
            ->to($user->getEmail())
            ->subject($this->translator->trans('email.changeaddress.title'))
            ->textTemplate('email/changeaddress.txt.twig')
            ->htmlTemplate('email/changeaddress.html.twig')
            ->context(['link' => $activationLink]);

        $this->mailer->send($email);
    }

    public function sendResetEmail(User $user, string $token): void
    {
        $resetLink = $this->urlGenerator->generate('app_reset_new', [
            'userid' => $user->getId(),
            'token' => $token,
        ], UrlGeneratorInterface::ABSOLUTE_URL);

        $email = (new TemplatedEmail())
            ->to($user->getEmail())
            ->subject($this->translator->trans('email.reset.title'))
            ->textTemplate('email/reset.txt.twig')
            ->htmlTemplate('email/reset.html.twig')
            ->context(['link' => $resetLink]);

        $this->mailer->send($email);
    }

    public function sendBandApplicationEmail(Application $application): void
    {
        $applicationLink = $this->urlGenerator->generate('app_band_apply', [
            'history' => $application->getHistory()->getId(),
            'application' => $application->getId(),
        ], UrlGeneratorInterface::ABSOLUTE_URL);

        $email = (new TemplatedEmail())
            ->to($application->getContactEmail())
            ->subject($this->translator->trans('band.apply.email.subject'))
            ->textTemplate('email/application.txt.twig')
            ->htmlTemplate('email/application.html.twig')
            ->context(['link' => $applicationLink]);

        $this->mailer->send($email);
    }

    public function sendStaffEmail(EmailRecord $record): void
    {
        $email = (new TemplatedEmail())
            ->from($_ENV['MAILER_FROM_STAFF'])
            ->to($record->getToEmail())
            ->subject($record->getSubject())
            ->text($record->getText())
            ->htmlTemplate('email/empty_markdown.html.twig')
            ->context(['content' => $record->getText()]);
        $this->mailer->send($email);
    }

    public function sendVolunteerDigest(string $to, array $volunteers): void
    {
        $email = (new TemplatedEmail())
            ->to($to)
            ->subject("TJOA Helferanmeldungen")
            ->textTemplate('email/volunteer_digest.txt.twig')
            ->htmlTemplate('email/volunteer_digest.html.twig')
            ->context([
                'volunteers' => $volunteers,
            ]);
        $this->mailer->send($email);
    }
}
