<?php

namespace App\System\Application;

use App\Festival\Entity\Festival;
use App\Festival\Repository\FestivalRepository;
use App\System\Repository\ConfigurationRepository;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ConfigService
{
    public const PASSWORD_MIN_LENGTH = 6;
    public const PASSWORD_MAX_LENGTH = 51;
    public const PASSWORD_HASH_STRENGTH = 12;
    public const EMAIL_REGEX = "^[a-zA-Z0-9\._%+-]+@[a-zA-Z0-9\.-]+\.[a-zA-Z]{2,4}$";
    public const URL_REGEX = "https?://[-_./a-z0-9]+\.[a-z]{2,4}[-_./?&=%#a-z0-9]*";
    public const TOKEN_KEYSPACE = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_-";
    public const PAGE_SIZE = 50;

    public const KEY_CURRENT_FESTIVAL = 'current_festival';

    private EntityManagerInterface $doctrine;
    private ConfigurationRepository $repository;
    private FestivalRepository $festivalRepository;
    private ?array $values;

    private ?Festival $currentFestival = null;

    public function __construct(EntityManagerInterface $doctrine, ConfigurationRepository $repository, FestivalRepository $festivalRepository)
    {
        $this->doctrine = $doctrine;
        $this->repository = $repository;
        $this->festivalRepository = $festivalRepository;
        $this->values = null;
    }

    private function fetch(): void
    {
        $entries = $this->repository->findAll();
        $this->values = array();
        foreach ($entries as $entry) {
            $this->values[$entry->getId()] = $entry->getValue();
        }
    }

    /**
     * Finds a config entry
     *
     * @param string $key the key to search for
     * @param ?string $default default value in case no entry with the given key exists
     * @return string the value for the corresponding key
     * @throws HttpException if no entry ith the given key exists and no default value is given
     */
    public function get(string $key, ?string $default = null): string
    {
        if ($this->values === null)
            $this->fetch();
        if (empty($this->values[$key])) {
            if ($default === null)
                throw new HttpException(
                    Response::HTTP_INTERNAL_SERVER_ERROR,
                    sprintf('Unknown config value "%s"', $key),
                );
            return $default;
        }
        return $this->values[$key];
    }

    /**
     * Updates an existing config entry
     *
     * @param string $key the key to update
     * @param string $value the value to set
     */
    public function update(string $key, string $value): void
    {
        $entry = $this->repository->find($key);
        if ($entry == null)
            throw new HttpException(
                Response::HTTP_INTERNAL_SERVER_ERROR,
                sprintf('Unknown config value "%s"', $key),
            );
        $entry->setValue($value);
        $this->doctrine->flush();
        if ($this->values !== null)
            $this->values[$key] = $value;
    }

    /**
     * Gets the value of the given key as a {@link DateTimeImmutable} object.
     *
     * @param string $key The key to look up
     * @return DateTimeImmutable parsed datetime object
     */
    public function getDateTime(string $key): DateTimeImmutable
    {
        $value = $this->get($key);
        try {
            return new DateTimeImmutable($value);
        } catch (Exception $e) {
            throw new HttpException(
                Response::HTTP_INTERNAL_SERVER_ERROR,
                sprintf(
                    'Cannot parse config value for key "%s" as DateTimeImmutable object: %s',
                    $key,
                    $value
                ),
                $e,
            );
        }
    }

    /**
     * Gets the current festival as an object.
     *
     * @return Festival the current festival
     */
    public function getCurrentFestival(): Festival
    {
        if ($this->currentFestival === null) {
            $id = (int)$this->get(self::KEY_CURRENT_FESTIVAL);
            if (empty($id))
                throw new HttpException(Response::HTTP_INTERNAL_SERVER_ERROR, 'Current festival is not configured');
            $festival = $this->festivalRepository->find($id);
            if ($festival == null)
                throw new NotFoundHttpException('Cannot find festival with id ' . $id);
            $this->currentFestival = $festival;
        }
        return $this->currentFestival;
    }

    /**
     * Sets the current festival.
     *
     * @param Festival $festival the festival to set as the current festival
     */
    public function setCurrentFestival(Festival $festival): void
    {
        $this->update(self::KEY_CURRENT_FESTIVAL, $festival->getId());
        $this->currentFestival = $festival;
    }
}
