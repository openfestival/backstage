<?php

namespace App\System\Application;

use App\System\Exception\InternalServerErrorHttpException;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * This service uses the symfony serializer component to map from associative arrays to DTOs.
 */
class MapperService {
    private Serializer $serializer;

    public function __construct() {
        $normalizers = [new DateTimeNormalizer(), new ObjectNormalizer()];
        $encoders = [new JsonEncoder()];
        $this->serializer = new Serializer($normalizers, $encoders);
    }

    public function serializeToJson(mixed $data, array $context): string {
        return $this->serializer->serialize($data, 'json', $context);
    }

    /**
     * Converts an object to an associative array. The object field names must match the attributes given.
     *
     * @param object $data object to map
     * @param array $attributes names of fields to include in the result. Can contain nested arrays for nested objects.
     * @return array associative array representing the given object
     */
    public function mapToArray(object $data, array $attributes): array {
        try {
            return $this->serializer->normalize($data, null, [AbstractNormalizer::ATTRIBUTES => $attributes]);
        } catch (ExceptionInterface $e) {
            throw new InternalServerErrorHttpException('Error while normalizing data', $e);
        }
    }

    /**
     * Converts an array of objects to an array of associative arrays. The object field names must match the
     * attributes given. See {@link mapToArray} for details on the mapping process.
     *
     * @param array $data array of objects to map
     * @param array $attributes names of fields to include in the result. Can contain nested arrays for nested objects.
     * @return array array of associative arrays representing the given objects
     */
    public function mapToArrays(array $data, array $attributes): array {
        return array_map(fn($v) => $this->mapToArray($v, $attributes), $data);
    }

    /**
     * Converts a normal indexed array of associative arrays to an associative array of arrays. The inner arrays must
     * contain a unique entry with the given key as name.
     *
     * For example the array `[['a' => 1], ['a' => 2]]` will be mapped to `[1 => ['a' => 1], 2 => ['a' => 2]]`
     * when `$key = 'a'`.
     *
     * @param array $data array to map
     * @param string $key key of inner arrays to use as key for new associative array
     * @return array associative array of arrays
     */
    public function mapToAssociativeArray(array $data, string $key): array {
        return array_reduce($data, function ($result, $item) use ($key) {
            $result[$item[$key]] = $item;
            return $result;
        }, []);
    }

    /**
     * Converts an associative array to an object. The object field names must match the array keys.
     *
     * @param array $data the array representing the object
     * @param string $type the type of the desired object
     * @return object object of the given type containing the given values
     */
    public function mapToObject(array $data, string $type): object {
        try {
            return $this->serializer->denormalize($data, $type);
        } catch (ExceptionInterface $e) {
            throw new InternalServerErrorHttpException('Error while denormalizing data', $e);
        }
    }

    /**
     * Converts an array of associative arrays to an array of objects. See {@link mapToObject} for details on the
     * mapping process.
     *
     * @param array $data the array of arrays representing the objects.
     * @param string $type the type of the desired object.
     * @return array array of objects of the given type containing the given values.
     */
    public function mapToObjects(array $data, string $type): array {
        return array_map(fn($v) => $this->mapToObject($v, $type), $data);
    }

}
