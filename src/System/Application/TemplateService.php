<?php

namespace App\System\Application;

use App\System\Entity\Template;
use App\System\Repository\TemplateRepository;
use Psr\Log\LoggerInterface;

/**
 * Service to load templates from the database. Is available in all twig templates with the `templates` variable.
 */
class TemplateService {
    private TemplateRepository $repository;
    private LoggerInterface $logger;

    public function __construct(TemplateRepository $repository, LoggerInterface $logger) {
        $this->repository = $repository;
        $this->logger = $logger;
    }

    /**
     * Finds a template by id. If no template was found an error is logged.
     *
     * @param string $id the id of the required template
     * @return Template|null the template or null
     */
    public function getTemplate(string $id): ?Template {
        $template = $this->repository->find($id);
        if (is_null($template))
            $this->logger->error('Cannot find template with id \'' . $id . '\'!');
        return $template;
    }

    /**
     * Finds the text of a template by id. If no template was found an error is logged.
     *
     * @param string $id the id of the required template
     * @return string the text of the template or the given id if the template was not found
     */
    public function getText(string $id): string {
        return $this->getTemplate($id)?->getText() ?: $id;
    }
}
