<?php

namespace App\System\Controller;

use App\System\Entity\Template;
use App\System\Form\TemplateForm;
use App\System\Repository\TemplateRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route(path: '/template')]
class TemplateController extends AbstractController {

    #[Route(path: '/', name: 'app_templates')]
    public function index(EntityManagerInterface $doctrine): Response {
        $this->denyAccessUnlessGranted('template.list');

        /** @var TemplateRepository $repo */
        $repo = $doctrine->getRepository(Template::class);

        $templates = $repo->findAll();

        return $this->render('system/templates/index.html.twig', [
            'templates' => $templates
        ]);
    }

    #[Route(path: '/new', name: 'app_templates_new')]
    public function new(Request $request, EntityManagerInterface $doctrine): RedirectResponse|Response {
        $this->denyAccessUnlessGranted('template.create');

        $template = new Template();
        $form = $this->createForm(TemplateForm::class, $template);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $template->setId($form->get('id')->getData());
            // remove empty variables
            $template->setVariables(array_filter($template->getVariables()));
            $doctrine->persist($template);
            $doctrine->flush();
            return $this->redirectToRoute('app_templates_edit', [
                'id' => $template->getId()
            ]);
        }

        return $this->render('system/templates/new.html.twig', [
            'form' => $form->createView()
        ]);
    }

    #[Route(path: '/edit/{id}', name: 'app_templates_edit')]
    public function edit(string $id, Request $request, EntityManagerInterface $doctrine): Response {
        $this->denyAccessUnlessGranted('template.edit.' . $id);

        /** @var Template $template */
        $template = $doctrine->find(Template::class, $id);

        if ($template === null)
            throw $this->createNotFoundException('The template does not exist');

        $form = $this->createForm(TemplateForm::class, $template);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $template->setId($id);
            // remove empty variables
            $template->setVariables(array_filter($template->getVariables()));
            $doctrine->flush();
        }

        return $this->render('system/templates/edit.html.twig', [
            'template' => $template,
            'form' => $form->createView()
        ]);
    }

}
