<?php

namespace App\System\Controller;

use App\Security\Entity\User;
use App\System\Application\ConfigService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class MainController extends AbstractController {

    #[Route(path: '/', name: 'index')]
    public function index(ConfigService $config): Response {
        /** @var User $user */
        $user = $this->getUser();
        return $this->render('system/main/index.html.twig', [
            'festival' => $config->getCurrentFestival(),
            'volunteer' => $user !== null && $user->getVolunteer() !== null
        ]);
    }

}
