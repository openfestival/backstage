<?php

namespace App\System\Controller;

use App\LegacyApiController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Attribute\Route;

/**
 * Used to save session variables from javascript
 */
class SessionApiController extends LegacyApiController {

    #[Route(path: '/api/session', methods: ['PUT'])]
    public function putSessionVariable(Request $request, SessionInterface $session): Response {
        // we only now about the following variables
        $data = $this->resolveBody($request, [
            'sidebarCollapsed' => '?bool',
        ]);
        if ($data === false)
            return $this->responseBadRequest();

        // simply save the variables as-is, verification was done above
        foreach ($data as $key => $value) {
            $session->set($key, $value);
        }

        return $this->responseOk();
    }
}
