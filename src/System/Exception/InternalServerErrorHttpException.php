<?php

namespace App\System\Exception;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Throwable;

/**
 * Signals an internal server error. Displays a clean message to the client.
 * @deprecated use HttpException directly
 */
class InternalServerErrorHttpException extends HttpException {
    public function __construct(string $message = 'Internal Server Error', Throwable $previous = null) {
        parent::__construct(Response::HTTP_INTERNAL_SERVER_ERROR, $message, $previous);
    }
}
