<?php

namespace App\Band\Application;

use App\Band\Entity\Application;
use App\Band\Entity\FinalScore;
use App\Band\Enum\BandFinalsAction;
use App\Band\Enum\BandFinalsStatus;
use App\Band\Repository\ApplicationRepository;
use App\Band\Repository\FinalScoreRepository;
use App\Festival\Entity\Festival;
use App\Security\Entity\User;
use App\System\Application\MercureService;
use Doctrine\ORM\EntityManagerInterface;
use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\Pure;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class BandFinalsService {
    private EntityManagerInterface $doctrine;
    private Security $security;
    private MercureService $mercure;
    private ApplicationRepository $applicationRepository;
    private FinalScoreRepository $scoreRepository;

    public function __construct(EntityManagerInterface $doctrine, Security $security, MercureService $mercure, FinalScoreRepository $scoreRepository, ApplicationRepository $applicationRepository) {
        $this->doctrine = $doctrine;
        $this->security = $security;
        $this->mercure = $mercure;
        $this->scoreRepository = $scoreRepository;
        $this->applicationRepository = $applicationRepository;
    }

    /**
     * Starts the voting process for the given festival and application.
     *
     * @param Festival $festival
     * @param int $applicationId
     */
    public function startVote(Festival $festival, int $applicationId): void {
        if ($festival->getBandFinalStatus() !== BandFinalsStatus::OPEN)
            throw new BadRequestHttpException('Band finals voting is not open for this festival.');

        $application = $this->applicationRepository->find($applicationId);
        if ($application == null)
            throw new NotFoundHttpException('Unknown application');

        if ($festival->getBandFinalCurrentVote() !== $application) {
            $festival->setBandFinalCurrentVote($application);
            $this->doctrine->flush();

            // update clients
            $this->mercure->publish('band/finals/' . $festival->getId(), $this->getStatusResponse($festival));
        }
    }

    /**
     * Sets the status of the band finals. Resets the currently voted application.
     *
     * @param Festival $festival the festival to set the status for
     * @param string $status the status of the finals. Must be of type {@link BandFinalsStatus}.
     */
    public function setStatus(Festival $festival, string $status): void {
        $festival->setBandFinalCurrentVote(null);
        $festival->setBandFinalStatus($status);
        $this->doctrine->flush();

        // update clients
        $this->mercure->publish('band/finals/' . $festival->getId(), $this->getStatusResponse($festival));
    }

    /**
     * Sets the score for the band currently being voted on for the given festival. Updates the control panel.
     *
     * @param Festival $festival The festival to get the current band from
     * @param int $value the score from 1 to 6
     */
    public function setScore(Festival $festival, int $value): void {
        $application = $festival->getBandFinalCurrentVote();
        /** @var User $user */
        $user = $this->security->getUser();
        $score = $this->scoreRepository->findOneBy([
            'application' => $application,
            'user' => $user
        ]);

        if ($score == null) {
            $score = new FinalScore();
            $score->setUser($user);
            $score->setApplication($application);
            $score->setScore($value);
            $score->setOpendoor(false);
            $score->setAftershow(false);
            $score->setBudget(false);
            $this->doctrine->persist($score);
        } else {
            $score->setScore($value);
        }
        $this->doctrine->flush();

        // update clients
        $this->updateControl($festival, $application);
    }

    /**
     * Sets a score action for the band currently being voted on for the given festival. Updates the control panel.
     *
     * @param Festival $festival The festival to get the current band from
     * @param string $action the action to set. Must be of type {@link BandFinalsAction}.
     * @param bool $value the value to set the action to
     */
    public function setAction(Festival $festival, string $action, bool $value): void {
        $application = $festival->getBandFinalCurrentVote();
        /** @var User $user */
        $user = $this->security->getUser();
        $score = $this->scoreRepository->findOneBy([
            'application' => $application,
            'user' => $user
        ]);

        if ($score == null)
            throw new BadRequestHttpException('Cannot set action before score.');

        switch ($action) {
            case BandFinalsAction::OPENDOOR:
                $score->setOpendoor($value);
                break;
            case BandFinalsAction::AFTERSHOW:
                $score->setAftershow($value);
                break;
            case BandFinalsAction::BUDGET:
                $score->setBudget($value);
                break;
        }
        $this->doctrine->flush();

        // update clients
        $this->updateControl($festival, $application);
    }

    private function updateControl(Festival $festival, Application $application): void
    {
        // find scores
        $result = $this->scoreRepository->findApplicationResults($application);

        // find genres
        $genres = $this->applicationRepository->findApplicationGenres($application->getId());
        $result['genres'] = $genres;

        // update control
        $this->mercure->publish('band/finals/control/' . $festival->getId(), [
            'status' => $festival->getBandFinalStatus(),
            'currentVote' => $this->getCurrentVoteResponse($application),
            'score' => $result
        ]);
    }

    /**
     * Creates an array representing the current status of the band finals for a given festival.
     * Includes score results if the status of the band finals is not closed, otherwise an empty array.
     *
     * @param Festival $festival the festival to create the status for
     * @return array the status for the band finals of the given festival
     */
    #[ArrayShape([
        'status' => 'string',
        'currentVote' => 'array|null',
        'scores' => '\App\Band\Entity\FinalScore[]'
    ])]
    public function getStatusResponse(Festival $festival): array {
        // find scores
        $scores = [];
        if ($festival->getBandFinalStatus() != BandFinalsStatus::CLOSED)
            $scores = $this->scoreRepository->findByFestival($festival);

        // find genres
        $applications = [];
        foreach ($scores as $score)
            $applications[] = $score['id'];
        $genres = $this->applicationRepository->findAllApplicationGenres($applications);
        foreach ($scores as &$score)
            $score['genres'] = $genres[$score['id']];

        return [
            'status' => $festival->getBandFinalStatus(),
            'currentVote' => $this->getCurrentVoteResponse($festival->getBandFinalCurrentVote()),
            'scores' => $scores
        ];
    }

    #[Pure]
    private function getCurrentVoteResponse(?Application $application): ?array {
        if ($application == null)
            return null;
        return [
            'id' => $application->getId(),
            'name' => $application->getName()
        ];
    }
}
