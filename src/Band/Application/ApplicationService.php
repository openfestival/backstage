<?php

namespace App\Band\Application;

use App\Band\Entity\Application;
use App\Band\Entity\ApplicationComment;
use App\Band\Entity\ApplicationFavorite;
use App\Band\Entity\ApplicationHistory;
use App\Band\Entity\ApplicationScore;
use App\Band\Entity\Genre;
use App\Band\Repository\ApplicationCommentRepository;
use App\Band\Repository\ApplicationFavoriteRepository;
use App\Band\Repository\ApplicationHistoryRepository;
use App\Band\Repository\ApplicationRepository;
use App\Band\Repository\ApplicationScoreRepository;
use App\Band\Repository\GenreRepository;
use App\Festival\Entity\Festival;
use App\Security\Entity\User;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use JetBrains\PhpStorm\ArrayShape;

/**
 * This service acts as a bridge between the controllers and the database repositories.
 */
class ApplicationService {
    private EntityManagerInterface $doctrine;

    private ApplicationRepository $applicationRepository;
    private ApplicationHistoryRepository $historyRepository;
    private ApplicationScoreRepository $scoreRepository;
    private ApplicationFavoriteRepository $favoriteRepository;
    private ApplicationCommentRepository $commentRepository;
    private GenreRepository $genreRepository;

    public function __construct(
        EntityManagerInterface        $doctrine,
        ApplicationRepository         $applicationRepository,
        ApplicationHistoryRepository  $historyRepository,
        ApplicationScoreRepository    $scoreRepository,
        ApplicationFavoriteRepository $favoriteRepository,
        ApplicationCommentRepository  $commentRepository,
        GenreRepository               $genreRepository
    ) {
        $this->doctrine = $doctrine;

        $this->applicationRepository = $applicationRepository;
        $this->historyRepository = $historyRepository;
        $this->scoreRepository = $scoreRepository;
        $this->favoriteRepository = $favoriteRepository;
        $this->commentRepository = $commentRepository;
        $this->genreRepository = $genreRepository;
    }

    /**
     * Fetches an application from the database.
     */
    public function getApplication(int $id): ?Application {
        return $this->applicationRepository->find($id);
    }

    /**
     * Finds all application histories by a user.
     *
     * @param User $user the user to search for
     * @return ApplicationHistory[] all application histories of the given user
     */
    public function getHistories(User $user): array {
        return $this->historyRepository->findByUser($user);
    }

    /**
     * Creates a new application history, including the first application of that history.
     *
     * @param User $user The user to create the history and application for
     * @param Festival $festival the festival to create the application for
     * @return ApplicationHistory the new application history
     */
    public function createNewHistory(User $user, Festival $festival): ApplicationHistory {
        $history = new ApplicationHistory();
        $history->setUser($user);
        $application = new Application();
        $application->setFestival($festival);
        $history->addApplication($application);
        if ($user->getName() != null)
            $application->setContactName($user->getName());
        $application->setContactEmail($user->getEmail());
        return $history;
    }

    /**
     * Creates a new application for a given history and festival. If possible, prefills the application with data from the most recent application of that history.
     *
     * @param ApplicationHistory $history the history to create a new application for
     * @param Festival $festival the festival to create a new application for
     * @return Application the new application
     */
    public function createNewApplication(ApplicationHistory $history, Festival $festival): Application {
        $application = new Application();
        $application->setFestival($festival);
        $application->setName($history->getName());
        // prefill with existing data
        if (!$history->getApplications()->isEmpty()) {
            $previous = $history->getFirstApplication();
            $application->setLocation($previous->getLocation());
            $application->setContactName($previous->getContactName());
            $application->setContactEmail($previous->getContactEmail());
            $application->setContactPhone($previous->getContactPhone());
            $application->setWebsite($previous->getWebsite());
            $application->setFacebook($previous->getFacebook());
            $application->setInstagram($previous->getInstagram());
            $application->setYoutube($previous->getYoutube());
            $application->setSpotify($previous->getSpotify());
            $application->setTwitter($previous->getTwitter());
            $application->setApplication($previous->getApplication());
            $application->setMembers($previous->getMembers());
            $application->setGenders($previous->getGenders());
            $application->setFee($previous->getFee());
            foreach ($previous->getGenres() as $genre)
                $application->addGenre($genre);
        }
        $history->addApplication($application);
        $user = $history->getUser();
        // sets the contact name and email if not already done so by the previous application
        if ($application->getContactName() == null && $user->getName() != null)
            $application->setContactName($user->getName());
        if ($application->getContactEmail() == null)
            $application->setContactEmail($user->getEmail());
        return $application;
    }

    /**
     * Finds the total amount of applications for a given festival.
     *
     * @param Festival $festival the festival to search for
     * @return int the amount of applications for the given festival
     */
    public function getTotalApplicationCount(Festival $festival): int {
        return $this->applicationRepository->findTotalAppliedCount($festival);
    }

    /**
     * Finds the amount of applications matching a set of criteria for a given festival.
     *
     * @param Festival $festival the festival to search for
     * @param array $genres genres which must be included in the applications
     * @param string $searchName name which will be searched for in the application band name
     * @return int the amount of applications matching the given criteria
     */
    public function getApplicationCount(Festival $festival, array $genres, string $searchName): int {
        return $this->applicationRepository->findAllAppliedCount($festival, $genres, $searchName);
    }

    /**
     * Finds all applications matching the given criteria. Returns a summary for the current user including personal
     * score and favorite status.
     *
     * @param User $user
     * @param Festival $festival
     * @param array $genres genres which must be included in the applications
     * @param string $name name which will be searched for in the application band name
     * @param string $sort sort result by this attribute
     * @param string $order sort order
     * @param int $page page to return
     * @return array the matching applications
     */
    public function getApplicationOverview(User $user, Festival $festival, array $genres, string $name, string $sort, string $order, int $page): array {
        return $this->applicationRepository->findAllApplied($user, $festival, $genres, $name, $sort, $order, $page);
    }

    /**
     * Finds an application and corresponding stats.
     *
     * @param int $id The application id
     * @param User $user The user to relate stats to
     * @return array application
     */
    public function getApplicationWithStats(int $id, User $user): array {
        return $this->applicationRepository->findWithStats($id, $user);
    }

    /**
     * Finds all other applications from the given history
     *
     * @param int $history history id
     * @param int $exclude application to exclude
     * @return array the applications
     */
    public function getApplicationsByHistory(int $history, int $exclude): array {
        return $this->applicationRepository->findOtherByHistory($history, $exclude);
    }

    /**
     * Finds a random application for the given festival the user has not scored yet.
     *
     * @param int $festival festival to search for
     * @param User $user current user
     * @return int|null the id of the application or null if the user has scored all applications of the given festival.
     */
    public function getApplicationRandomly(int $festival, User $user): ?int {
        return $this->applicationRepository->findRandomApplication($festival, $user);
    }

    /**
     * Finds all genres sorted by name.
     *
     * @return Genre[] all genres sorted by name
     */
    public function getGenresSortedByName(): array {
        return $this->genreRepository->findBy([], ['name' => 'ASC']);
    }

    /**
     * Creates or updates any existing {@link ApplicationScore}.
     *
     * @param Application $application application to score
     * @param User $user user who scores
     * @param int $value value of score
     */
    public function saveScore(Application $application, User $user, int $value): void {
        $score = $this->scoreRepository->findOneBy([
            'application' => $application,
            'user' => $user
        ]);
        if ($score == null) {
            $score = new ApplicationScore();
            $score->setUser($user);
            $score->setApplication($application);
            $score->setScore($value);

            $this->doctrine->persist($score);
        } else {
            $score->setScore($value);
        }

        $this->doctrine->flush();

    }

    /**
     * Finds the current score for an application.
     *
     * @param int $applicationId The application ID
     * @param User $user the user
     * @return ?ApplicationScore the score if found
     */
    public function getScore(int $applicationId, User $user): ?ApplicationScore {
        return $this->scoreRepository->findOneBy([
            'application' => $applicationId,
            'user' => $user
        ]);
    }

    #[ArrayShape(['score' => "float", 'count' => "int"])]
    public function getApplicationScoreSummary(Application $application): array {
        return $this->scoreRepository->findScoreSummary($application);
    }

    /**
     * Marks an application as favorite.
     *
     * @param Application $application application to mark as favorite
     * @param User $user user whose favorite it is
     */
    public function saveFavorite(Application $application, User $user): void {
        $favorite = $this->favoriteRepository->findOneBy([
            'application' => $application,
            'user' => $user
        ]);

        if ($favorite == null) {
            $favorite = new ApplicationFavorite();
            $favorite->setApplication($application);
            $favorite->setUser($user);

            $this->doctrine->persist($favorite);
            $this->doctrine->flush();
        }
    }

    /**
     * Deletes an existing application favorite.
     *
     * @param Application $application
     * @param User $user
     */
    public function deleteFavorite(Application $application, User $user): void {
        $favorite = $this->favoriteRepository->findOneBy([
            'application' => $application,
            'user' => $user
        ]);
        if ($favorite != null) {
            $this->doctrine->remove($favorite);
            $this->doctrine->flush();
        }
    }

    /**
     * Finds all comments for an application
     *
     * @param int $applicationId the id of the application to search comments for
     * @return array all existing comments
     */
    public function getComments(int $applicationId): array {
        return $this->commentRepository->findByApplication($applicationId);
    }

    /**
     * Saves a new comment for an application.
     *
     * @param Application $application The application to comment
     * @param User $user the user who comments
     * @param string $text the text commented
     * @return ApplicationComment the new comment
     */
    public function saveComment(Application $application, User $user, string $text): ApplicationComment {
        $comment = new ApplicationComment();
        $comment->setApplication($application);
        $comment->setAuthor($user);
        $comment->setComment($text);
        $comment->setCreatedAt(new DateTimeImmutable());

        $this->doctrine->persist($comment);
        $this->doctrine->flush();

        return $comment;
    }

    /**
     * Finds a specific comment.
     *
     * @param int $id The id of the comment
     * @return ApplicationComment|null the comment if found
     */
    public function getComment(int $id): ?ApplicationComment {
        return $this->commentRepository->find($id);
    }

    /**
     * Updates an existing comment. Sets the edited_at field of the comment to now.
     *
     * @param ApplicationComment $comment The comment to update.
     * @param string $text the new text
     */
    public function updateComment(ApplicationComment $comment, string $text): void {
        $comment->setComment($text);
        $comment->setEditedAt(new DateTimeImmutable());

        $this->doctrine->flush();
    }
}
