<?php

namespace App\Band\Application;

use Exception;
use Symfony\Component\HttpClient\Exception\ClientException;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class SpotifyApiService
{
    public function __construct(
        private HttpClientInterface $httpClient,
    ) {}

    public function getSpotifyStats(string $spotifyUrl): ?array
    {
        // If Spotify is not configured, return null
        if (empty($_ENV['SPOTIFY_CLIENT_ID']) || empty($_ENV['SPOTIFY_CLIENT_SECRET'])) {
            return null;
        }

        $clientId = $_ENV['SPOTIFY_CLIENT_ID'];
        $clientSecret = $_ENV['SPOTIFY_CLIENT_SECRET'];

        // Extract artist ID from Spotify URL
        $artistId = $this->extractSpotifyArtistId($spotifyUrl);
        if ($artistId === null) {
            return ['error' => 'Invalid Spotify URL'];
        }

        // Get access token
        try {
            $response = $this->httpClient->request('POST', 'https://accounts.spotify.com/api/token', [
                'headers' => [
                    'Authorization' => 'Basic ' . base64_encode($clientId . ':' . $clientSecret),
                ],
                'body' => [
                    'grant_type' => 'client_credentials'
                ]
            ]);

            $tokenData = $response->toArray();
            if (!isset($tokenData['access_token'])) {
                return ['error' => 'Invalid Spotify credentials'];
            }

            $token = $tokenData['access_token'];
        } catch (ClientException $e) {
            if ($e->getResponse()->getStatusCode() === 401) {
                return ['error' => 'Invalid Spotify credentials'];
            }
            // Log the error but don't expose internal details
            error_log('Spotify API Error: ' . $e->getMessage());
            return ['error' => 'Could authenticate with the Shopify API'];
        }

        try {
            // Get artist data
            $response = $this->httpClient->request('GET', "https://api.spotify.com/v1/artists/{$artistId}", [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token
                ]
            ]);

            $data = $response->toArray();
            return [
                'followers' => $data['followers']['total'] ?? 0
            ];
        } catch (Exception $e) {
            if ($e->getResponse()->getStatusCode() === 404) {
                return ['error' => 'Artist not found'];
            }
            // Log the error but don't expose internal details
            error_log('Spotify API Error: ' . $e->getMessage());
            return ['error' => 'Could not fetch Spotify data'];
        }
    }

    public function extractSpotifyArtistId(string $spotifyUrl): ?string
    {
        // Handle different Spotify URL formats
        if (empty($spotifyUrl)) {
            return null;
        }

        // Handle full URLs
        if (preg_match('#^(?:https?://)?open\.spotify\.com/artist/([a-zA-Z0-9]+)(?:\?|$)#', $spotifyUrl, $matches)) {
            return $matches[1];
        }

        // Handle spotify:artist:id format
        if (preg_match('#^spotify:artist:([a-zA-Z0-9]+)$#', $spotifyUrl, $matches)) {
            return $matches[1];
        }

        // Handle direct IDs
        if (preg_match('#^[a-zA-Z0-9]{22}$#', $spotifyUrl)) {
            return $spotifyUrl;
        }

        return null;
    }
} 