<?php

namespace App\Band\Form;

use App\Band\Entity\Application;
use App\Band\Entity\Genre;
use App\Band\Enum\BandGender;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Positive;

class BandApplicationForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'band.apply.name.label',
                'help' => 'band.apply.name.help',
                'disabled' => !$options['new_history'],
            ])
            ->add('location', TextType::class, [
                'label' => 'band.apply.location.label',
                'help' => 'band.apply.location.help',
            ])
            ->add('contact_name', TextType::class, [
                'help' => 'band.apply.contact.name',
            ])
            ->add('contact_email', EmailType::class, [
                'help' => 'band.apply.contact.email',
            ])
            ->add('contact_phone', TelType::class, [
                'help' => 'optional',
                'required' => false,
            ])
            ->add('website', SocialmediaType::class, [
                'icon' => 'fas fa-link',
                'placeholder' => 'Website',
                'media_type' => 'website',
                'error_message' => 'band.apply.help.website',
            ])
            ->add('facebook', SocialmediaType::class, [
                'icon' => 'fab fa-facebook',
                'placeholder' => 'Facebook',
                'media_type' => 'facebook',
                'error_message' => 'band.apply.help.facebook',
            ])
            ->add('instagram', SocialmediaType::class, [
                'icon' => 'fab fa-instagram',
                'placeholder' => 'Instagram',
                'media_type' => 'instagram',
                'error_message' => 'band.apply.help.instagram',
            ])
            ->add('youtube', SocialmediaType::class, [
                'icon' => 'fab fa-youtube',
                'placeholder' => 'YouTube Channel',
                'media_type' => 'youtube',
                'error_message' => 'band.apply.help.youtube',
            ])
            ->add('spotify', SocialmediaType::class, [
                'icon' => 'fab fa-spotify',
                'placeholder' => 'Spotify Artist',
                'media_type' => 'spotify',
                'error_message' => 'band.apply.help.spotify',
            ])
            ->add('twitter', SocialmediaType::class, [
                'icon' => 'fab fa-x-twitter',
                'placeholder' => 'Twitter',
                'media_type' => 'twitter',
                'error_message' => 'band.apply.help.twitter',
            ])
            ->add('genres', EntityType::class, [
                'class' => Genre::class,
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('g')
                        ->orderBy('g.name', 'ASC');
                },
                'choice_label' => 'name',
                'label' => 'genre',
                'help' => 'band.apply.genre',
                'multiple' => true,
                'required' => true,
            ])
            ->add('application', TextareaType::class, [
                'label' => 'band.apply.application',
            ])
            ->add('members', IntegerType::class, [
                'label' => 'band.apply.members',
                'constraints' => [new Positive()],
            ])
            ->add('genders', ChoiceType::class, [
                'label' => 'band.apply.genders.label',
                'required' => false,
                'choices' => BandGender::getAvailableTypes(),
                'choice_label' => function($value) {
                    return 'band.apply.genders.' . $value;
                },
                'placeholder' => 'band.apply.genders.placeholder',
            ])
            ->add('fee', TextType::class, [
                'label' => 'fee',
                'required' => false,
            ])
            ->add('technician', ChoiceType::class, [
                'label' => 'band.apply.technician',
                'choices' => [
                    'no' => false,
                    'yes' => true,
                ],
            ])
            ->add('accommodation', ChoiceType::class, [
                'label' => 'band.apply.accommodation',
                'choices' => [
                    'no' => false,
                    'yes' => true,
                ],
            ])
            ->add('save', SubmitType::class, [
                'label' => 'save',
            ])
            ->add('send', SubmitType::class, [
                'label' => 'send',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefined('new_history');
        $resolver->setAllowedTypes('new_history', 'bool');
        $resolver->setDefaults([
            'new_history' => false,
            'data_class' => Application::class,
        ]);
    }
}
