<?php


namespace App\Band\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class SocialmediaType extends AbstractType
{
    private TranslatorInterface $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);
        $resolver->setDefined([
            'placeholder',
            'media_type',
            'icon',
            'error_message',
        ]);
        $resolver->setAllowedTypes('placeholder', 'string');
        $resolver->setAllowedTypes('media_type', 'string');
        $resolver->setAllowedTypes('icon', 'string');
        $resolver->setRequired([
            'media_type',
            'icon',
            'error_message',
        ]);
        $resolver->setDefaults([
            'required' => false,
            'empty_data' => null,
        ]);
    }

    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        $view->vars['icon'] = $options['icon'];
        $view->vars['media_type'] = $options['media_type'];
        $view->vars['placeholder'] = $options['placeholder'];
        $view->vars['error_message'] = $this->translator->trans($options['error_message']);
    }

    public function getParent(): string
    {
        return TextType::class;
    }
}
