<?php

namespace App\Band\Enum;

class BandFinalsStatus {
    const CLOSED = "closed";
    const OPEN = "open";
    const FINISHED = "finished";

    public static function getAvailableTypes(): array {
        return [
            self::CLOSED,
            self::OPEN,
            self::FINISHED
        ];
    }
}
