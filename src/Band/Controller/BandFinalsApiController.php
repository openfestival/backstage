<?php

namespace App\Band\Controller;

use App\LegacyApiController;
use App\Band\Application\BandFinalsService;
use App\Band\Enum\BandFinalsAction;
use App\Band\Enum\BandFinalsStatus;
use App\Festival\Entity\Festival;
use App\System\Application\MercureService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route(path: '/api/band/finals')]
class BandFinalsApiController extends LegacyApiController {
    private BandFinalsService $service;

    public function __construct(BandFinalsService $service) {
        $this->service = $service;
    }

    /**
     * Returns the current status and scores. Allows for discovery of mercure for continuous updates.
     */
    #[Route(path: '/{festival<\d+>}/status', methods: ['GET'])]
    public function apiGetFinalsStatus(Festival $festival, MercureService $mercure, Request $request): Response {
        $this->denyAccessUnlessGranted('band.finals');

        $mercure->addDiscovery($request);
        $response = $this->responseOk($this->service->getStatusResponse($festival));
        $mercure->finalizeResponse($response);
        return $response;
    }

    /**
     * Gets called when the control panel updates the status
     */
    #[Route(path: '/{festival<\d+>}/update', methods: ['PUT'])]
    public function apiFinalsUpdate(Festival $festival, Request $request): Response {
        $this->denyAccessUnlessGranted('band.finals.control');

        $data = $this->resolveBody($request, [
            'status' => 'string'
        ]);
        if ($data === false || !in_array($data->status, BandFinalsStatus::getAvailableTypes()))
            return $this->responseBadRequest();

        $this->service->setStatus($festival, $data->status);

        return $this->responseOk();
    }

    /**
     * Gets called when a client votes for a specific band
     */
    #[Route(path: '/{festival<\d+>}/score', methods: ['PUT'])]
    public function apiFinalsScore(Festival $festival, Request $request): Response {
        $this->denyAccessUnlessGranted('band.finals');

        $data = $this->resolveBody($request, [
            'application' => 'int',
            'score' => 'int'
        ]);
        if ($data === false)
            return $this->responseBadRequest();
        $value = (int)$data->score;
        if ($value < 1 || $value > 6)
            return $this->responseBadRequest();

        if ($festival->getBandFinalCurrentVote() == null || $festival->getBandFinalCurrentVote()->getId() !== $data->application)
            return $this->responseBadRequest();

        $this->service->setScore($festival, $value);

        return $this->responseOk();
    }

    /**
     * Gets called when a client presses an action button
     */
    #[Route(path: '/{festival<\d+>}/action', methods: ['PUT'])]
    public function apiFinalsAction(Festival $festival, Request $request): Response {
        $this->denyAccessUnlessGranted('band.finals');

        $data = $this->resolveBody($request, [
            'application' => 'int',
            'action' => 'string',
            'state' => 'bool'
        ]);
        if ($data === false)
            return $this->responseBadRequest();
        $action = $data->action;
        if (!in_array($action, BandFinalsAction::getAvailableTypes()))
            return $this->responseBadRequest('Invalid action: ' . $action);

        if ($festival->getBandFinalCurrentVote() == null || $festival->getBandFinalCurrentVote()->getId() !== $data->application)
            return $this->responseBadRequest();

        $this->service->setAction($festival, $data->action, $data->state);

        return $this->responseOk();
    }
}
