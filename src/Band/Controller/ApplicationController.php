<?php

namespace App\Band\Controller;

use App\Band\Application\ApplicationService;
use App\Festival\Application\FestivalService;
use App\Festival\Entity\Festival;
use App\Security\Entity\User;
use App\System\Application\ConfigService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use App\Band\Entity\ApplicationScore;

/**
 * Displays and manages all applications.
 */
#[Route(path: '/band')]
class ApplicationController extends AbstractController {
    const IMAGE_REGEX = '~https?://([-_a-z0-9]+\.)+[a-z]{2,4}[-_./a-z0-9]+\.(jpg|jpeg|png|gif|svg)~i';
    const YOUTUBE_REGEX = '~https://(?:www\.youtube\.com/watch\?v=|youtu.be/)([-_a-z0-9]+)((&t=[a-z0-9]+|&start=[0-9]+|&end=[0-9]+)*)~i';
    const SPOTIFY_REGEX = '~https://open\.spotify\.com/([a-z]+)/([a-z0-9]+)~i';

    private ApplicationService $applicationService;
    private FestivalService $festivalService;

    public function __construct(ApplicationService $applicationService, FestivalService $festivalService) {
        $this->applicationService = $applicationService;
        $this->festivalService = $festivalService;
    }

    /**
     * Lists all applications for a festival.
     *
     * @param Festival $festival the festival to list applications for. Defaults to the current festival.
     */
    #[Route(path: '/applications/{festival<\d+>?}', name: 'app_band_applications')]
    public function applications(Festival $festival, Request $request, EntityManagerInterface $doctrine, ConfigService $config): Response {
        $this->denyAccessUnlessGranted('band.applications');

        /** @var User $user */
        $user = $this->getUser();

        $festivals = $this->festivalService->getOtherFestivals($festival);
        $allGenres = $this->applicationService->getGenresSortedByName();

        $genres = $request->get('genres');
        if ($genres == null)
            $genres = [];

        $name = $request->get('name');
        if ($name == null)
            $name = '';
        if (!empty($name))
            $searchName = '%' . preg_replace('/\s+/', '%', preg_replace('/%/', '\\%', trim($name))) . '%';
        else $searchName = '';

        $applicationCount = $this->applicationService->getApplicationCount($festival, $genres, $searchName);
        $pageSize = $config->get('pagination.size', 50);
        if ($applicationCount == 0)
            $pages = 1;
        else $pages = (int)($applicationCount % $pageSize == 0 ? $applicationCount / $pageSize : ($applicationCount / $pageSize) + 1);

        $sort = $request->get('sort');
        if ($sort == null)
            $sort = 'name';
        $order = strtoupper($request->get('order'));
        if ($order !== 'DESC')     // prevent sql injection
            $order = 'ASC';

        $page = $request->get('page');
        if ($page == null)
            $page = 1;
        if ($page > $pages)
            $page = $pages;

        $applications = $this->applicationService->getApplicationOverview($user, $festival, $genres, $searchName, $sort, $order, $page - 1);
        // TODO: do this in a better way (use ApplicationRepository.findApplicationGenres())
        $genreQuery = $doctrine->getConnection()->prepare('SELECT g.id, g.name FROM band_application_genre a JOIN genre g on a.genre_id = g.id WHERE a.band_application_id = :id');
        foreach ($applications as &$app) {
            $app['genres'] = $genreQuery->executeQuery(['id' => $app['id']])->fetchAllAssociative();
        }

        // statistics
        // TODO: move to service or similar
        $scoreRepo = $doctrine->getRepository(ApplicationScore::class);
        $totalScoreCount = $scoreRepo->findTotalScoreCount($festival);
        $userScoreCount = $scoreRepo->findUserScoreCount($festival, $user);
        $mostScores = $scoreRepo->findMostScores($festival);

        return $this->render('band/applications.html.twig', [
            'festival' => $festival,
            'festivals' => $festivals,
            'all_genres' => $allGenres,
            'applications' => $applications,
            'genres' => $genres,
            'name' => $name,
            'sort' => $sort,
            'order' => $order,
            'pages' => $pages,
            'page' => $page,
            'application_count' => empty($genres) && empty($searchName) ? $applicationCount : $this->applicationService->getTotalApplicationCount($festival),
            'total_scores' => $totalScoreCount,
            'user_scores' => $userScoreCount,
            'most_scores' => $mostScores
        ]);
    }

    /**
     * Displays a single application.
     *
     * @param int $id the id of the application
     */
    #[Route(path: '/application/{id<\d+>}', name: 'app_band_application')]
    public function application(int $id, Request $request): Response {
        $this->denyAccessUnlessGranted('band.applications');

        /** @var User $user */
        $user = $this->getUser();

        $application = $this->applicationService->getApplicationWithStats($id, $user);
        if ($application == null)
            throw $this->createNotFoundException();
        else $festival = $this->festivalService->getFestival($application['festival_id']);

        $applications = $this->applicationService->getApplicationsByHistory($application['history'], $application['id']);

        // find score of previous year
        if (!empty($applications) && $applications[0]['start'] < $festival->getStart())
            $lastScore = $this->applicationService->getScore($applications[0]['id'], $user);
        else $lastScore = null;

        // remove all html entities, replace newlines with <br>
        $application['application'] = preg_replace('/\n/', '<br>', htmlentities($application['application']));

        // find links to images, replace with <a> tags to image carusel
        $i = 0;
        $images = array();
        preg_match_all(self::IMAGE_REGEX, $application['application'], $images);
        if (!empty($images)) {
            foreach ($images[0] as $image) {
                $application['application'] = str_replace($image, '<a href="#carousel-image" data-slide-to="' . $i . '">' . $image . '</a>', $application['application']);
                $i++;
            }
        }

        // find links to youtube, replace with <a> tags to image carusel
        $videos = array();
        preg_match_all(self::YOUTUBE_REGEX, $application['application'], $videos);
        if (!empty($videos)) {
            foreach ($videos[0] as $video) {
                $application['application'] = str_replace($video, '<a href="#carousel-image" data-slide-to="' . $i . '">' . $video . '</a>', $application['application']);
                $i++;
            }
        }

        // find links to spotify, replace with <a> tags to spotify preview
        $spotify = array();
        preg_match_all(self::SPOTIFY_REGEX, $application['application'], $spotify);
        if (!empty($spotify)) {
            foreach ($spotify[0] as $song) {
                $application['application'] = str_replace($song, '<a href="#spotify-preview">' . $song . '</a>', $application['application']);
            }
        }
        // add spotify artist to preview if it was set
        if ($application['spotify'] != null) {
            array_unshift($spotify[0], 'https://open.spotify.com/artist/' . $application['spotify']);
            array_unshift($spotify[1], 'artist');
            array_unshift($spotify[2], $application['spotify']);
        }

        return $this->render('band/application.html.twig', [
            'application' => $application,
            'festival' => $festival,
            'applications' => $applications,
            'last_score' => $lastScore,
            'user' => $user,
            'images' => empty($images) ? [] : $images[0],
            'videos' => $videos,
            'spotify' => $spotify,
            'genres' => $request->get('genres'),
            'name' => $request->get('name'),
            'sort' => $request->get('sort'),
            'order' => $request->get('order'),
            'page' => $request->get('page')
        ]);
    }

    /**
     * Redirects to a random application for the given festival which was not yet scored by the current user.
     * Redirects to the list of applications if no unscored application remains.
     *
     * @param int $id the id of the festival
     */
    #[Route(path: '/application/random/{festival<\d+>}', name: 'app_band_application_random')]
    public function random(int $festival): Response {
        $this->denyAccessUnlessGranted('band.applications');

        /** @var User $user */
        $user = $this->getUser();
        $application = $this->applicationService->getApplicationRandomly($festival, $user);

        if ($application == null) {
            $this->addFlash('success', 'Du hast alle Bands bewertet! :)');
            return $this->redirectToRoute('app_band_applications', ['festival' => $festival]);
        } else {
            return $this->redirectToRoute('app_band_application', ['id' => $application]);
        }
    }
}
