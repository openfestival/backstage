<?php

namespace App\Band\Controller;

use App\LegacyApiController;
use App\Band\Application\ApplicationService;
use App\Security\Entity\User;
use DateTimeImmutable;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route(path: '/api/band')]
class ApiController extends LegacyApiController {
    private ApplicationService $service;

    public function __construct(ApplicationService $service) {
        $this->service = $service;
    }

    /**
     * Sets the application score for the given band.
     */
    #[Route(path: '/score', methods: ['PUT'])]
    public function putScore(Request $request): Response {
        $this->denyAccessUnlessGranted('band.applications');

        $data = $this->resolveBody($request, [
            'application' => 'int',
            'score' => 'int',
        ]);
        if ($data === false)
            return $this->responseBadRequest();
        $value = (int)$data->score;
        if ($value < 1 || $value > 6)
            return $this->responseBadRequest();
        $application = $this->service->getApplication($data->application);
        if ($application == null)
            return $this->responseBadRequest();

        $festival = $application->getFestival();
        $now = new DateTimeImmutable();
        if ($now > $festival->getBandRatingEnd())
            return $this->responseBadRequest();

        /** @var User $user */
        $user = $this->getUser();
        $this->service->saveScore($application, $user, $value);

        $summary = $this->service->getApplicationScoreSummary($application);

        return $this->responseOk($summary);
    }

    /**
     * Marks an application as favorite.
     *
     * @param int $id the application id
     */
    #[Route(path: '/favorite/{id<\d+>}', methods: ['PUT'])]
    public function putFavorite(int $id): Response {
        $this->denyAccessUnlessGranted('band.applications');

        $application = $this->service->getApplication($id);
        if ($application == null)
            return $this->responseBadRequest();

        $festival = $application->getFestival();
        $now = new DateTimeImmutable();
        if ($now > $festival->getBandRatingEnd())
            return $this->responseBadRequest();

        /** @var User $user */
        $user = $this->getUser();
        $this->service->saveFavorite($application, $user);

        return $this->responseOk();
    }

    /**
     * Removes an application from the favorites.
     *
     * @param int $id the application id
     */
    #[Route(path: '/favorite/{id<\d+>}', methods: ['DELETE'])]
    public function deleteFavorite(int $id): Response {
        $this->denyAccessUnlessGranted('band.applications');

        $application = $this->service->getApplication($id);
        if ($application == null)
            return $this->responseBadRequest();

        /** @var User $user */
        $user = $this->getUser();

        $festival = $application->getFestival();
        $now = new DateTimeImmutable();
        if ($now > $festival->getBandRatingEnd())
            return $this->responseBadRequest();

        $this->service->deleteFavorite($application, $user);

        return $this->responseOk();
    }

    /**
     * Retrieves all comments for an application
     */
    #[Route(path: '/comments/{id<\d+>}', methods: ['GET'])]
    public function getComments(int $id): Response {
        $this->denyAccessUnlessGranted('band.applications');

        $comments = $this->service->getComments($id);

        $data = [];
        foreach ($comments as $comment) {
            $data[] = [
                'id' => $comment['id'],
                'author_id' => $comment['author_id'],
                'author_name' => $comment['author_name'],
                'author_email' => $comment['author_email'],
                'comment' => $comment['comment'],
                'created_at' => $comment['createdAt']->format('d.m.y H:i'),
                'edited_at' => is_null($comment['editedAt']) ? null : $comment['editedAt']->format('d.m.y H:i'),
            ];
        }
        return $this->responseOk($data);
    }

    /**
     * Creates a new comment for an application
     */
    #[Route(path: '/comments', methods: ['POST'])]
    public function postComment(Request $request): Response {
        $this->denyAccessUnlessGranted('band.applications');

        $data = $this->resolveBody($request, [
            'application' => 'int',
            'comment' => 'string'
        ]);
        if ($data === false)
            return $this->responseBadRequest();

        $application = $this->service->getApplication($data->application);
        if ($application == null)
            return $this->responseBadRequest();

        /** @var User $user */
        $user = $this->getUser();
        $comment = $this->service->saveComment($application, $user, $data->comment);

        return $this->responseOk([
            'id' => $comment->getId()
        ]);
    }

    /**
     * Updates a comment for an application.
     *
     * @param int $id the application id
     */
    #[Route(path: '/comments/{id<\d+>}', methods: ['PUT'])]
    public function putComment(int $id, Request $request): Response {
        $this->denyAccessUnlessGranted('band.applications');

        $data = $this->resolveBody($request, [
            'comment' => 'string'
        ]);
        if ($data === false)
            return $this->responseBadRequest();

        $comment = $this->service->getComment($id);
        if ($comment == null)
            return $this->responseBadRequest();
        if ($comment->getAuthor() !== $this->getUser())
            return $this->responseForbidden();

        $this->service->updateComment($comment, $data->comment);

        return $this->responseOk();
    }
}
