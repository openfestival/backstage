<?php

namespace App\Band\Controller;

use App\Band\Application\BandFinalsService;
use App\Festival\Entity\Festival;
use App\System\Application\MercureService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route(path: '/band/finals')]
class BandFinalsController extends AbstractController {
    private BandFinalsService $service;

    public function __construct(BandFinalsService $service) {
        $this->service = $service;
    }

    /**
     * Displays the band finals voting page
     */
    #[Route(path: '/{festival<\d+>?}', name: 'app_band_finals')]
    public function finals(Festival $festival, MercureService $mercure, Request $request): Response {
        $this->denyAccessUnlessGranted('band.finals');

        $mercure->authorizeSubscription($request, [
            'band/finals/' . $festival->getId()
        ]);
        // empty placeholder page, remaining functions get loaded via javascript
        return $this->render('band/finals/index.html.twig', [
            'festival' => $festival
        ]);
    }

    /**
     * Displays the band finals control panel
     */
    #[Route(path: '/control/{festival<\d+>?}', name: 'app_band_finals_control')]
    public function finalsControl(Festival $festival, Request $request, MercureService $mercure): Response {
        $this->denyAccessUnlessGranted('band.finals.control');

        // start vote if query parameter is present
        if ($request->query->has('vote'))
            $this->service->startVote($festival, $request->query->getInt('vote'));

        // set silent session variable
        if ($request->query->has('silent'))
            $request->getSession()->set('band-finals-silent', $request->query->getBoolean('silent'));

        $mercure->authorizeSubscription($request, [
            'band/finals/' . $festival->getId(),
            'band/finals/control/' . $festival->getId()
        ]);
        // empty placeholder page, remaining functions get loaded via javascript
        return $this->render('band/finals/control.html.twig', [
            'festival' => $festival,
            'silent' => $request->getSession()->get('band-finals-silent', false)
        ]);
    }
}
