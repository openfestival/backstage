<?php

namespace App\Band\Controller;

use App\Band\Application\ApplicationService;
use App\Band\Entity\Application;
use App\Band\Entity\ApplicationHistory;
use App\Band\Form\BandApplicationForm;
use App\Security\Entity\User;
use App\System\Application\ConfigService;
use App\System\Application\MailService;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Displays and manages own applications.
 */
#[Route(path: '/band')]
class ApplyController extends AbstractController
{
    public function __construct(
        private readonly ConfigService $config,
        private readonly ApplicationService $service,
        private readonly MailService $mailService,
    ) {}

    /**
     * Displays and saves band applications. Defaults to the first history if not given or creates a new history for the
     * user if none exist yet.
     *
     * @param ?int $his application history id
     * @param ?int $app application id
     */
    #[Route(path: '/apply/{history<\d+>?}/{application<\d+>?}', name: 'app_band_apply')]
    public function apply(
        ?ApplicationHistory $history,
        ?Application $application,
        Request $request,
        EntityManagerInterface $doctrine,
        TranslatorInterface $trans,
    ): RedirectResponse|Response {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');

        /** @var User $user */
        $user = $this->getUser();
        $festival = $this->config->getCurrentFestival();
        // all existing histories for this user
        $histories = $this->service->getHistories($user);
        $newApplication = false;

        // create new history if parameter was set
        if ($request->query->has('new')) {
            $newApplication = true;
            $history = $this->service->createNewHistory($user, $festival);
            $application = $history->getFirstApplication();
        } else if (is_null($application)) {
            if (is_null($history)) {
                // use first existing history or create a new one
                if (empty($histories)) {
                    $newApplication = true;
                    $history = $this->service->createNewHistory($user, $festival);
                } else {
                    $history = $histories[0];
                }
            }
            // use first application of the current history
            $application = $history->getFirstApplication();
            // if the application is not for the current festival, create one
            if ($application->getFestival() !== $festival) {
                $newApplication = true;
                $application = $this->service->createNewApplication($history, $festival);
            }
        }
        // check this application actually belongs to the given history
        if ($application->getHistory() !== $history)
            throw $this->createNotFoundException();

        // check this history belongs to the current user
        if ($history->getUser() !== $user)
            throw $this->createNotFoundException();

        $now = new DateTimeImmutable();
        $newHistory = $history->getId() == null || $history->getApplications()->count() == 1;
        $form = $this->createForm(BandApplicationForm::class, $application, [
            'new_history' => $newHistory,
            'disabled' => !$this->isGranted('band.apply.always')
                && (
                    $application->getAppliedAt() != null
                    || $application->getFestival()->getBandApplicationStart() >= $now
                    || $application->getFestival()->getBandApplicationEnd() <= $now
                ),
        ]);
        $form->handleRequest($request);

        // process form
        if ($form->isSubmitted() && $this->checkValidity($form, $application, $trans)) {
            if ($newHistory)                                    // set name of history for new histories
                $history->setName($application->getName());
            else $application->setName($history->getName());    // or reset name for old histories

            if ($form->get('send')->isClicked()) {      // if send button was used, set applied_at field
                if ($user->getActivatedAt() == null) {
                    $this->addFlash('danger', $trans->trans('band.apply.error.notactivated'));
                } else {
                    $application->setAppliedAt($now);
                    $this->addFlash('success', $trans->trans('band.apply.sent'));
                    $this->mailService->sendBandApplicationEmail($application);
                    // disable form now
                    $form->getConfig()->getOptions()['disabled'] = !$this->isGranted('band.apply.always');
                }
            } else {
                $this->addFlash('success', $trans->trans('band.apply.saved'));
            }
            $doctrine->persist($application);
            $doctrine->flush();
            // if this was a new application, redirect to that now
            if ($newApplication) {
                return $this->redirectToRoute('app_band_apply', [
                    'history' => $application->getHistory()->getId(),
                    'application' => $application->getId(),
                ]);
            }
        }

        $images = array();
        preg_match_all(ApplicationController::IMAGE_REGEX, $application->getApplication(), $images);
        $videos = array();
        preg_match_all(ApplicationController::YOUTUBE_REGEX, $application->getApplication(), $videos);
        $spotify = array();
        preg_match_all(ApplicationController::SPOTIFY_REGEX, $application->getApplication(), $spotify);
        // add spotify artist to preview if it was set
        if ($application->getSpotify() != null) {
            array_unshift($spotify[0], 'https://open.spotify.com/artist/' . $application->getSpotify());
            array_unshift($spotify[1], 'artist');
            array_unshift($spotify[2], $application->getSpotify());
        }

        return $this->render('band/apply.html.twig', [
            'form' => $form->createView(),
            'festival' => $festival,
            'histories' => $histories,
            'history' => $history,
            'application' => $application,
            'scoreSummary' => $this->service->getApplicationScoreSummary($application),
            'new' => $newHistory,
            'images' => empty($images) ? [] : $images[0],
            'videos' => $videos,
            'spotify' => $spotify,
        ]);
    }

    private function checkValidity(FormInterface $form, Application $application, TranslatorInterface $trans): bool
    {
        if (!$form->isValid()) {
            $this->addFlash('danger', $trans->trans('band.apply.error.requirements'));
            return false;
        }
        // skip for debug
        if ($this->isGranted('band.apply.always'))
            return true;

        // check application wasn't sent before
        if ($application->getAppliedAt() != null) {
            $this->addFlash('danger', $trans->trans('band.apply.error.applied'));
            return false;
        }
        // check application time limits
        $now = new DateTimeImmutable();
        if ($application->getFestival()->getBandApplicationStart() > $now) {
            $this->addFlash('danger', $trans->trans('band.apply.error.early'));
            return false;
        }
        if ($application->getFestival()->getBandApplicationEnd() < $now) {
            $this->addFlash('danger', $trans->trans('band.apply.error.late'));
            return false;
        }
        return true;
    }

    /**
     * Deletes an application.
     *
     * @param int $id the id of the application
     */
    #[Route(path: '/delete/{id<\d+>}', name: 'app_band_delete')]
    public function delete(
        int $id,
        EntityManagerInterface $doctrine,
        TranslatorInterface $trans,
    ): RedirectResponse {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');

        /** @var User $user */
        $user = $this->getUser();
        $application = $doctrine->find(Application::class, $id);
        if ($application == null || $application->getHistory()->getUser() !== $user) {
            $this->addFlash('danger', $trans->trans('band.apply.delete.unknown'));
            return $this->redirectToRoute('app_band_apply');
        }
        if ($application->getAppliedAt() != null) {
            $this->addFlash('danger', $trans->trans('band.apply.delete.applied'));
            return $this->redirectToRoute('app_band_apply', [
                'history' => $application->getHistory()->getId(),
                'application' => $application->getId(),
            ]);
        }
        $now = new DateTimeImmutable();
        if ($application->getFestival()->getBandApplicationEnd() < $now) {
            $this->addFlash('danger', $trans->trans('band.apply.delete.end'));
            return $this->redirectToRoute('app_band_apply', [
                'history' => $application->getHistory()->getId(),
                'application' => $application->getId(),
            ]);
        }

        $redirect = [];

        $history = $application->getHistory();
        $history->removeApplication($application);
        $doctrine->remove($application);
        if ($history->getApplications()->isEmpty()) {
            // if this was the only application of the history, delete it as well
            $doctrine->remove($history);
        } else {
            // otherwise redirect to the history
            $redirect['history'] = $history->getId();
        }
        $doctrine->flush();
        $this->addFlash('success', $trans->trans('band.apply.deleted', [
            '%name%' => $application->getName(),
            '%year%' => $application->getFestival()->getStart()->format('Y'),
        ]));

        return $this->redirectToRoute('app_band_apply', $redirect);
    }

    #[Route(path: '/info', name: 'app_band_info')]
    public function info(): Response
    {
        return $this->render('band/info.html.twig', [
            'festival' => $this->config->getCurrentFestival(),
        ]);
    }
}
