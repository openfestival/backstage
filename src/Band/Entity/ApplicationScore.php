<?php

namespace App\Band\Entity;

use App\Band\Repository\ApplicationScoreRepository;
use App\Security\Entity\User;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'band_application_score')]
#[ORM\Entity(repositoryClass: ApplicationScoreRepository::class)]
class ApplicationScore {
    #[ORM\JoinColumn(nullable: false)]
    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: Application::class, inversedBy: 'scores')]
    private ?Application $application;

    #[ORM\JoinColumn(nullable: false)]
    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'bandApplicationScores')]
    private ?User $user;

    #[ORM\Column(type: 'integer')]
    private ?int $score;

    public function getApplication(): ?Application {
        return $this->application;
    }

    public function setApplication(?Application $application): self {
        $this->application = $application;

        return $this;
    }

    public function getUser(): ?User {
        return $this->user;
    }

    public function setUser(?User $user): self {
        $this->user = $user;

        return $this;
    }

    public function getScore(): ?int {
        return $this->score;
    }

    public function setScore(int $score): self {
        $this->score = $score;

        return $this;
    }
}
