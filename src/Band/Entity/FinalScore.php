<?php

namespace App\Band\Entity;

use App\Band\Repository\FinalScoreRepository;
use App\Security\Entity\User;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'band_final_score')]
#[ORM\Entity(repositoryClass: FinalScoreRepository::class)]
class FinalScore {
    #[ORM\JoinColumn(nullable: false)]
    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: Application::class, inversedBy: 'finalScores')]
    private ?Application $application = null;

    #[ORM\JoinColumn(nullable: false)]
    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'bandFinalScores')]
    private ?User $user = null;

    #[ORM\Column(type: 'integer')]
    private ?int $score = null;

    #[ORM\Column(type: 'boolean')]
    private ?bool $opendoor = null;

    #[ORM\Column(type: 'boolean')]
    private ?bool $aftershow = null;

    #[ORM\Column(type: 'boolean')]
    private ?bool $budget = null;

    public function getApplication(): ?Application {
        return $this->application;
    }

    public function setApplication(?Application $application): self {
        $this->application = $application;

        return $this;
    }

    public function getUser(): ?User {
        return $this->user;
    }

    public function setUser(?User $user): self {
        $this->user = $user;

        return $this;
    }

    public function getScore(): ?int {
        return $this->score;
    }

    public function setScore(int $score): self {
        $this->score = $score;

        return $this;
    }

    public function getOpendoor(): ?bool {
        return $this->opendoor;
    }

    public function setOpendoor(?bool $opendoor): self {
        $this->opendoor = $opendoor;

        return $this;
    }

    public function getAftershow(): ?bool {
        return $this->aftershow;
    }

    public function setAftershow(?bool $aftershow): self {
        $this->aftershow = $aftershow;

        return $this;
    }

    public function getBudget(): ?bool {
        return $this->budget;
    }

    public function setBudget(?bool $budget): self {
        $this->budget = $budget;

        return $this;
    }
}
