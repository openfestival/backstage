<?php

namespace App\Band\Entity;

use App\Band\Repository\ApplicationRepository;
use App\Festival\Entity\Festival;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;

#[ORM\Table(name: 'band_application')]
#[ORM\UniqueConstraint(columns: ['history_id', 'festival_id'])]
#[ORM\Entity(repositoryClass: ApplicationRepository::class)]
class Application {
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\JoinColumn(nullable: false)]
    #[ORM\ManyToOne(targetEntity: ApplicationHistory::class, cascade: ['PERSIST'], inversedBy: 'applications')]
    private ?ApplicationHistory $history = null;

    #[ORM\JoinColumn(nullable: false)]
    #[ORM\ManyToOne(targetEntity: Festival::class, inversedBy: 'bandApplications')]
    private ?Festival $festival = null;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $name = null;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $contact_name = null;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $contact_email = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $contact_phone = null;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $location = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $website = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $facebook = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $instagram = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $youtube = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $spotify = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $twitter = null;

    #[ORM\Column(type: 'text')]
    private ?string $application = null;

    #[ORM\Column(type: 'integer')]
    private ?int $members = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $genders = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $fee = null;

    #[ORM\Column(type: 'boolean')]
    private ?bool $technician = null;

    #[ORM\Column(type: 'boolean')]
    private ?bool $accommodation = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private ?DateTimeImmutable $applied_at = null;

    #[ORM\JoinTable(name: 'band_application_genre')]
    #[ORM\JoinColumn(name: 'band_application_id', referencedColumnName: 'id', onDelete: 'CASCADE')]
    #[ORM\ManyToMany(targetEntity: Genre::class, fetch: 'EAGER')]
    private Collection $genres;

    #[ORM\OneToMany(mappedBy: 'application', targetEntity: ApplicationScore::class, orphanRemoval: true)]
    private Collection $scores;

    #[ORM\OneToMany(mappedBy: 'application', targetEntity: ApplicationFavorite::class, orphanRemoval: true)]
    private Collection $favorites;

    #[ORM\OneToMany(mappedBy: 'application', targetEntity: ApplicationComment::class, orphanRemoval: true)]
    private Collection $comments;

    #[ORM\OneToMany(mappedBy: 'application', targetEntity: FinalScore::class, orphanRemoval: true)]
    private Collection $finalScores;

    #[Pure] public function __construct() {
        $this->genres = new ArrayCollection();
        $this->scores = new ArrayCollection();
        $this->favorites = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->finalScores = new ArrayCollection();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getHistory(): ?ApplicationHistory {
        return $this->history;
    }

    public function setHistory(?ApplicationHistory $history): self {
        $this->history = $history;

        return $this;
    }

    public function getFestival(): ?Festival {
        return $this->festival;
    }

    public function setFestival(?Festival $festival): self {
        $this->festival = $festival;

        return $this;
    }

    public function getName(): ?string {
        return $this->name;
    }

    public function setName(string $name): self {
        $this->name = $name;

        return $this;
    }

    public function getContactName(): ?string {
        return $this->contact_name;
    }

    public function setContactName(string $contact_name): self {
        $this->contact_name = $contact_name;

        return $this;
    }

    public function getContactEmail(): ?string {
        return $this->contact_email;
    }

    public function setContactEmail(string $contact_email): self {
        $this->contact_email = $contact_email;

        return $this;
    }

    public function getContactPhone(): ?string {
        return $this->contact_phone;
    }

    public function setContactPhone(?string $contact_phone): self {
        $this->contact_phone = $contact_phone;

        return $this;
    }

    public function getLocation(): ?string {
        return $this->location;
    }

    public function setLocation(string $location): self {
        $this->location = $location;

        return $this;
    }

    public function getWebsite(): ?string {
        return $this->website;
    }

    public function setWebsite(?string $website): self {
        $this->website = $website;

        return $this;
    }

    public function getFacebook(): ?string {
        return $this->facebook;
    }

    public function setFacebook(?string $facebook): self {
        $this->facebook = $facebook;

        return $this;
    }

    public function getInstagram(): ?string {
        return $this->instagram;
    }

    public function setInstagram(?string $instagram): self {
        $this->instagram = $instagram;

        return $this;
    }

    public function getTwitter(): ?string {
        return $this->twitter;
    }

    public function setTwitter(?string $twitter): self {
        $this->twitter = $twitter;

        return $this;
    }

    public function getYoutube(): ?string {
        return $this->youtube;
    }

    public function setYoutube(?string $youtube): self {
        $this->youtube = $youtube;

        return $this;
    }

    public function getSpotify(): ?string {
        return $this->spotify;
    }

    public function setSpotify(?string $spotify): self {
        $this->spotify = $spotify;

        return $this;
    }

    public function getApplication(): ?string {
        return $this->application;
    }

    public function setApplication(string $application): self {
        $this->application = $application;

        return $this;
    }

    public function getMembers(): ?int {
        return $this->members;
    }

    public function setMembers(int $members): self {
        $this->members = $members;

        return $this;
    }

    public function getGenders(): ?string {
        return $this->genders;
    }

    public function setGenders(?string $genders): self {
        $this->genders = $genders;

        return $this;
    }

    public function getFee(): ?string {
        return $this->fee;
    }

    public function setFee(?string $fee): self {
        $this->fee = $fee;

        return $this;
    }

    public function getTechnician(): ?bool {
        return $this->technician;
    }

    public function setTechnician(bool $technician): self {
        $this->technician = $technician;

        return $this;
    }

    public function getAccommodation(): ?bool {
        return $this->accommodation;
    }

    public function setAccommodation(bool $accommodation): self {
        $this->accommodation = $accommodation;

        return $this;
    }

    public function getAppliedAt(): ?DateTimeImmutable {
        return $this->applied_at;
    }

    public function setAppliedAt(?DateTimeImmutable $applied_at): self {
        $this->applied_at = $applied_at;

        return $this;
    }

    /**
     * @return Collection|Genre[]
     */
    public function getGenres(): Collection {
        return $this->genres;
    }

    public function addGenre(Genre $genre): self {
        if (!$this->genres->contains($genre)) {
            $this->genres[] = $genre;
        }

        return $this;
    }

    public function removeGenre(Genre $genre): self {
        if ($this->genres->contains($genre)) {
            $this->genres->removeElement($genre);
        }

        return $this;
    }

    /**
     * @return Collection|ApplicationScore[]
     */
    public function getScores(): Collection {
        return $this->scores;
    }

    public function addScore(ApplicationScore $score): self {
        if (!$this->scores->contains($score)) {
            $this->scores[] = $score;
            $score->setApplication($this);
        }

        return $this;
    }

    public function removeScore(ApplicationScore $score): self {
        if ($this->scores->contains($score)) {
            $this->scores->removeElement($score);
            // set the owning side to null (unless already changed)
            if ($score->getApplication() === $this) {
                $score->setApplication(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ApplicationFavorite[]
     */
    public function getFavorites(): Collection {
        return $this->favorites;
    }

    public function addFavorite(ApplicationFavorite $favorite): self {
        if (!$this->favorites->contains($favorite)) {
            $this->favorites[] = $favorite;
            $favorite->setApplication($this);
        }

        return $this;
    }

    public function removeFavorite(ApplicationFavorite $favorite): self {
        if ($this->favorites->contains($favorite)) {
            $this->favorites->removeElement($favorite);
            // set the owning side to null (unless already changed)
            if ($favorite->getApplication() === $this) {
                $favorite->setApplication(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ApplicationComment[]
     */
    public function getComments(): Collection {
        return $this->comments;
    }

    public function addComment(ApplicationComment $comment): self {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setApplication($this);
        }

        return $this;
    }

    public function removeComment(ApplicationComment $comment): self {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getApplication() === $this) {
                $comment->setApplication(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|FinalScore[]
     */
    public function getFinalScores(): Collection {
        return $this->scores;
    }

    public function addFinalScore(FinalScore $score): self {
        if (!$this->finalScores->contains($score)) {
            $this->finalScores[] = $score;
            $score->setApplication($this);
        }

        return $this;
    }

    public function removeFinalScore(FinalScore $score): self {
        if ($this->finalScores->contains($score)) {
            $this->finalScores->removeElement($score);
            // set the owning side to null (unless already changed)
            if ($score->getApplication() === $this) {
                $score->setApplication(null);
            }
        }

        return $this;
    }
}
