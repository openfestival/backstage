<?php

namespace App\Band\Entity;

use App\Band\Repository\ApplicationCommentRepository;
use App\Security\Entity\User;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'band_application_comment')]
#[ORM\Entity(repositoryClass: ApplicationCommentRepository::class)]
class ApplicationComment {
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\JoinColumn(nullable: false)]
    #[ORM\ManyToOne(targetEntity: Application::class, inversedBy: 'comments')]
    private ?Application $application = null;

    #[ORM\JoinColumn(nullable: false)]
    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'bandApplicationComments')]
    private ?User $author = null;

    #[ORM\Column(type: 'text')]
    private ?string $comment = null;

    #[ORM\Column(type: 'datetime_immutable')]
    private ?DateTimeImmutable $createdAt = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private ?DateTimeImmutable $editedAt = null;

    public function getId(): ?int {
        return $this->id;
    }

    public function getApplication(): ?Application {
        return $this->application;
    }

    public function setApplication(?Application $application): self {
        $this->application = $application;

        return $this;
    }

    public function getAuthor(): ?User {
        return $this->author;
    }

    public function setAuthor(?User $author): self {
        $this->author = $author;

        return $this;
    }

    public function getComment(): ?string {
        return $this->comment;
    }

    public function setComment(string $comment): self {
        $this->comment = $comment;

        return $this;
    }

    public function getCreatedAt(): ?DateTimeImmutable {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeImmutable $createdAt): self {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getEditedAt(): ?DateTimeImmutable {
        return $this->editedAt;
    }

    public function setEditedAt(?DateTimeImmutable $editedAt): self {
        $this->editedAt = $editedAt;

        return $this;
    }
}
