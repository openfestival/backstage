<?php

namespace App\Band\Entity;

use App\Band\Repository\ApplicationFavoriteRepository;
use App\Security\Entity\User;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'band_application_favorite')]
#[ORM\Entity(repositoryClass: ApplicationFavoriteRepository::class)]
class ApplicationFavorite {
    #[ORM\JoinColumn(nullable: false)]
    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: Application::class, inversedBy: 'favorites')]
    private ?Application $application;

    #[ORM\JoinColumn(nullable: false)]
    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'bandApplicationFavorites')]
    private ?User $user;

    public function getApplication(): ?Application {
        return $this->application;
    }

    public function setApplication(?Application $application): self {
        $this->application = $application;

        return $this;
    }

    public function getUser(): ?User {
        return $this->user;
    }

    public function setUser(?User $user): self {
        $this->user = $user;

        return $this;
    }
}
