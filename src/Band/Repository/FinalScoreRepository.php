<?php

namespace App\Band\Repository;

use App\Band\Entity\Application;
use App\Band\Entity\FinalScore;
use App\Festival\Entity\Festival;
use App\System\Exception\InternalServerErrorHttpException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query\Expr;
use Doctrine\Persistence\ManagerRegistry;
use JetBrains\PhpStorm\ArrayShape;

/**
 * @method FinalScore|null find($id, $lockMode = null, $lockVersion = null)
 * @method FinalScore|null findOneBy(array $criteria, array $orderBy = null)
 * @method FinalScore[]    findAll()
 * @method FinalScore[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FinalScoreRepository extends ServiceEntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, FinalScore::class);
    }

    /**
     * Finds all scores for applications of the given festival
     *
     * @param Festival $festival
     * @return array all scores as an array of arrays
     */
    #[ArrayShape([
        'id' => 'int',
        'name' => 'string',
        'location' => 'string',
        'fee' => 'string',
        'score' => 'double',
        'count' => 'int',
        'opendoor' => 'int',
        'aftershow' => 'int',
        'budget' => 'int'
    ])]
    public function findByFestival(Festival $festival): array {
        return $this->createQueryBuilder('s')
            ->select('a.id', 'a.name', 'a.location', 'a.fee',
                'AVG(s.score) AS score', 'COUNT(s.score) AS count',
                'SUM(s.opendoor) AS opendoor', 'SUM(s.aftershow) AS aftershow', 'SUM(s.budget) AS budget')
            ->join(Application::class, 'a', Expr\Join::WITH, 's.application = a.id')
            ->where('a.festival = :festival')
            ->groupBy('a.id')
            ->orderBy('score', 'ASC')
            ->setParameter('festival', $festival)
            ->getQuery()
            ->getArrayResult();
    }

    /**
     * Finds the current score for a specific application.
     *
     * @param Application $application the application to find scores for
     * @return array The current score as an array
     */
    #[ArrayShape([
        'id' => 'int',
        'name' => 'string',
        'location' => 'string',
        'fee' => 'string',
        'score' => 'double',
        'count' => 'int',
        'opendoor' => 'int',
        'aftershow' => 'int',
        'budget' => 'int'
    ])]
    public function findApplicationResults(Application $application): array {
        try {
            return $this->createQueryBuilder('s')
                ->select('a.id', 'a.name', 'a.location', 'a.fee',
                    'AVG(s.score) AS score', 'COUNT(s.score) AS count',
                    'SUM(s.opendoor) AS opendoor', 'SUM(s.aftershow) AS aftershow', 'SUM(s.budget) AS budget')
                ->join(Application::class, 'a', Expr\Join::WITH, 's.application = a.id')
                ->where('s.application = :application')
                ->groupBy('a.id')
                ->setParameter('application', $application)
                ->getQuery()
                ->getSingleResult();
        } catch (NoResultException|NonUniqueResultException $e) {
            throw new InternalServerErrorHttpException(previous: $e);
        }
    }
}
