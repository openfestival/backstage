<?php

namespace App\Band\Repository;

use App\Band\Entity\Application;
use App\Band\Entity\ApplicationScore;
use App\Festival\Entity\Festival;
use App\Security\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\Persistence\ManagerRegistry;
use JetBrains\PhpStorm\ArrayShape;

/**
 * @method ApplicationScore|null find($id, $lockMode = null, $lockVersion = null)
 * @method ApplicationScore|null findOneBy(array $criteria, array $orderBy = null)
 * @method ApplicationScore[]    findAll()
 * @method ApplicationScore[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ApplicationScoreRepository extends ServiceEntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, ApplicationScore::class);
    }

    #[ArrayShape([
        'score' => 'float',
        'count' => 'int'
    ])]
    public function findScoreSummary(Application $application): array {
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('AVG(score)', 'score', 'float');
        $rsm->addScalarResult('COUNT(score)', 'count', 'integer');

        $query = $this->getEntityManager()->createNativeQuery(
            'SELECT AVG(score), COUNT(score) FROM band_application_score WHERE application_id = :app;',
            $rsm
        );
        $query->setParameter('app', $application->getId());
        return $query->getResult()[0];
    }

    public function findTotalScoreCount(Festival $festival): int {
        $query = $this->getEntityManager()->getConnection()->prepare(
            'SELECT COUNT(*) FROM (SELECT s.application_id FROM band_application_score s ' .
            'JOIN band_application a ON s.application_id = a.id WHERE a.festival_id = :festival ' .
            'GROUP BY s.application_id) x'
        );
        return $query->executeQuery(['festival' => $festival->getId()])->fetchFirstColumn()[0];
    }

    public function findUserScoreCount(Festival $festival, User $user): float|bool|int|string|null
    {
        return $this->createQueryBuilder('s')
            ->select('COUNT(s.score)')
            ->join('s.application', 'a')
            ->andWhere('a.festival = :festival')
            ->andWhere('s.user = :user')
            ->setParameter('festival', $festival)
            ->setParameter('user', $user)
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function findMostScores(Festival $festival) {
        return $this->createQueryBuilder('s')
            ->select('u.name', 'u.email', 'COUNT(s.user) AS count')
            ->join('s.user', 'u')
            ->join('s.application', 'a')
            ->where('a.festival = :festival')
            ->groupBy('s.user')
            ->orderBy('count', 'DESC')
            ->setParameter('festival', $festival)
            ->getQuery()
            ->getResult();
    }
}
