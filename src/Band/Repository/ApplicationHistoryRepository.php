<?php

namespace App\Band\Repository;

use App\Band\Entity\ApplicationHistory;
use App\Security\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ApplicationHistory|null find($id, $lockMode = null, $lockVersion = null)
 * @method ApplicationHistory|null findOneBy(array $criteria, array $orderBy = null)
 * @method ApplicationHistory[]    findAll()
 * @method ApplicationHistory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ApplicationHistoryRepository extends ServiceEntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, ApplicationHistory::class);
    }

    public function findByUser(User $user) {
        return $this->createQueryBuilder('h')
            ->join('h.applications', 'a', Expr\Join::WITH, 'a.history = h')
            ->where('h.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();
    }

    public function findById($id) {
        $histories = $this->createQueryBuilder('h')
            ->join('h.applications', 'a', Expr\Join::WITH, 'a.history = h')
            ->where('h.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();
        if (empty($histories))
            return null;
        return $histories[0];
    }

    // /**
    //  * @return ApplicationHistory[] Returns an array of ApplicationHistory objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ApplicationHistory
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
