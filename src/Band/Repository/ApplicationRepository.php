<?php

namespace App\Band\Repository;

use App\Band\Entity\Application;
use App\Festival\Entity\Festival;
use App\Security\Entity\User;
use App\System\Application\ConfigService;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\ArrayParameterType;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\Persistence\ManagerRegistry;
use JetBrains\PhpStorm\ArrayShape;
use App\Band\Entity\ApplicationFavorite;
use App\Band\Entity\ApplicationScore;

/**
 * @method Application|null find($id, $lockMode = null, $lockVersion = null)
 * @method Application|null findOneBy(array $criteria, array $orderBy = null)
 * @method Application[]    findAll()
 * @method Application[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ApplicationRepository extends ServiceEntityRepository
{
    private string $pageSize;

    public function __construct(ManagerRegistry $registry, ConfigService $config)
    {
        parent::__construct($registry, Application::class);
        $this->pageSize = $config->get('pagination.size', 50);
    }

    public function findAllApplied(User $user, Festival $festival, array $genres, string $name, string $sort, string $order, int $page)
    {
        switch (strtolower($sort)) {
            case 'name':
                $sort = 'a.name';
                break;
            case 'favorite':
                $sort = 'f.user';
                $order = $order === 'ASC' ? 'DESC' : 'ASC';
                break;
            case 'score':
                $sort = 'personalScore';
                break;
            case 'total':
                $sort = 'score';
                break;
            default:
                $sort = 'a.name';
        }

        $query = $this->createQueryBuilder('a')
            ->select('a.id', 'a.name', 'a.website', 'a.facebook', 'a.instagram', 'a.youtube', 'a.spotify', 'a.twitter',
                'AVG(s.score) AS score', 'COUNT(s.score) AS scoreCount', 's2.score AS personalScore', 'f AS favorite')
            ->leftJoin('a.scores', 's')
            ->leftJoin(ApplicationScore::class, 's2', Expr\Join::WITH, 's2.user = :user AND s2.application = a')
            ->leftJoin(ApplicationFavorite::class, 'f', Expr\Join::WITH, 'f.user = :user AND f.application = a')
            ->andWhere('a.festival = :festival')
            ->andWhere('a.applied_at IS NOT NULL')
            ->groupBy('a.id')
            ->orderBy($sort, $order)
            ->setParameter('user', $user)
            ->setParameter('festival', $festival)
            ->setMaxResults($this->pageSize)
            ->setFirstResult($page * $this->pageSize);

        if (!empty($genres)) {
            $query = $query
                ->leftJoin('a.genres', 'g')
                ->andWhere('g.id IN (:genres)')
                ->setParameter('genres', $genres);
        }

        $scoreLimitMatches = [];
        if (preg_match('/#score(<|<=|>|>=)((\d+)(\.\d+)?)/i', $name, $scoreLimitMatches)) {
            $name = '';
            $scoreLimitCompare = $scoreLimitMatches[1];
            $scoreLimit = (float) $scoreLimitMatches[2];
            $query = $query
                ->andHaving('score ' . $scoreLimitCompare . ' :scoreLimit')
                ->setParameter('scoreLimit', $scoreLimit);
        }

        if (!empty($name)) {
            $query = $query
                ->andWhere('a.name LIKE :name')
                ->setParameter('name', $name);
        }

        return $query->getQuery()->getResult();
    }

    public function findTotalAppliedCount(Festival $festival): int
    {
        return $this->createQueryBuilder('a')
            ->select('COUNT(a.id)')
            ->andWhere('a.festival = :festival')
            ->andWhere('a.applied_at IS NOT NULL')
            ->setParameter('festival', $festival)
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function findAllAppliedCount(Festival $festival, array $genres, string $name): int
    {
        $subquery = $this->createQueryBuilder('a')
            ->leftJoin('a.scores', 's')
            ->andWhere('a.festival = :festival')
            ->andWhere('a.applied_at IS NOT NULL')
            ->groupBy('a.id');

        if (!empty($genres)) {
            $subquery = $subquery
                ->leftJoin('a.genres', 'g')
                ->andWhere('g.id IN (:genres)');
        }

        $scoreLimitMatches = [];
        $scoreLimitParameter = null;
        if (preg_match('/#score(<|<=|>|>=)((\d+)(\.\d+)?)/i', $name, $scoreLimitMatches)) {
            $name = '';
            $scoreLimitCompare = $scoreLimitMatches[1];
            $scoreLimitParameter = (float) $scoreLimitMatches[2];
            $subquery = $subquery
                ->andHaving('AVG(s.score) ' . $scoreLimitCompare . ' :scoreLimit');
        }

        if (!empty($name)) {
            $subquery = $subquery
                ->andWhere('a.name LIKE :name');
        }

        $query = $this->createQueryBuilder('x')
            ->select('COUNT(x.id)')
            ->where($this->getEntityManager()->getExpressionBuilder()->in('x.id', $subquery->getDQL()))
            ->setParameter('festival', $festival);

        if (!empty($genres))
            $query = $query->setParameter('genres', $genres);
        if (!empty($name))
            $query->setParameter('name', $name);
        if ($scoreLimitParameter !== null)
            $query->setParameter('scoreLimit', $scoreLimitParameter);

        return (int) $query->getQuery()->getSingleScalarResult();
    }

    // TODO: not used anymore?
    public function findBestApplied(Festival $festival, int $page)
    {
        return $this->createQueryBuilder('a')
            ->select('a.id', 'a.name', 'AVG(s.score) AS score', 'COUNT(s.score) AS scoreCount')
            ->join('a.scores', 's')
            ->andWhere('a.festival = :festival')
            ->andWhere('a.applied_at IS NOT NULL')
            ->groupBy('a.id')
            ->orderBy('score', 'DESC')
            ->setParameter('festival', $festival)
            ->setMaxResults($this->pageSize)
            ->setFirstResult($page * $this->pageSize)
            ->getQuery()
            ->getResult();
    }

    // TODO: not used anymore?
    public function findBestAppliedCount(Festival $festival): float|bool|int|string|null
    {
        return $this->createQueryBuilder('a')
            ->select('COUNT(a.id)')
            ->andWhere('a.festival = :festival')
            ->andWhere('a.applied_at IS NOT NULL')
            ->setParameter('festival', $festival)
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function findWithStats(int $id, User $user)
    {
        $application = $this->createQueryBuilder('a')
            ->select(
                'a.id', 'h.id AS history', 'a.name', 'a.contact_name', 'a.contact_email', 'a.contact_phone', 'a.location',
                'a.website', 'a.facebook', 'a.youtube', 'a.instagram', 'a.spotify', 'a.twitter', 'a.application',
                'a.members', 'a.genders', 'a.fee', 'a.technician', 'a.accommodation', 'a.applied_at',
                'f.id AS festival_id', 'fa AS favorite', 's2.score AS score', 'AVG(s.score) AS total', 'COUNT(s.score) AS score_count')
            ->join('a.festival', 'f')
            ->join('a.history', 'h')
            ->leftJoin('a.scores', 's')
            ->leftJoin(ApplicationScore::class, 's2', Expr\Join::WITH, 's2.application = a AND s2.user = :user')
            ->leftJoin(ApplicationFavorite::class, 'fa', Expr\Join::WITH, 'fa.application = a AND fa.user = :user')
            ->where('a.id = :id')
            ->groupBy('a.id')
            ->setParameters([
                'id' => $id,
                'user' => $user,
            ])
            ->getQuery()
            ->getResult();
        if (empty($application))
            return null;
        $application = $application[0];
        $query = $this->getEntityManager()->getConnection()
            ->prepare('SELECT g.* FROM band_application_genre a JOIN genre g ON a.genre_id = g.id WHERE band_application_id = :id');
        $application['genres'] = $query->executeQuery(['id' => $application['id']])->fetchAllAssociative();
        return $application;
    }

    public function findOtherByHistory(int $history, int $first)
    {
        return $this->createQueryBuilder('a')
            ->select('a.id', 'f.start')
            ->join('a.festival', 'f')
            ->andWhere('a.history = :history')
            ->andWhere('a.id != :id')
            ->orderBy('f.number', 'DESC')
            ->setParameters([
                'history' => $history,
                'id' => $first,
            ])
            ->getQuery()
            ->getResult();
    }

    public function findRandomApplication(int $festival, User $user): ?int
    {
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('id', 'id', 'integer');

        $query = $this->getEntityManager()->createNativeQuery(
            'SELECT a.id, COUNT(s.score) AS count FROM band_application a ' .
            'LEFT JOIN band_application_score s ON a.id = s.application_id ' .
            'WHERE a.applied_at IS NOT NULL AND a.festival_id = :festival AND a.id NOT IN (' .
            'SELECT x.id FROM band_application x JOIN band_application_score y ON x.id = y.application_id ' .
            'WHERE y.user_id = :user AND x.festival_id = :festival' .
            ') GROUP BY a.id ORDER BY count, RAND() LIMIT 1;',
            $rsm,
        );
        $query->setParameter('user', $user->getId());
        $query->setParameter('festival', $festival);

        $result = $query->getResult();
        if (empty($result))
            return null;
        else return $result[0]['id'];
    }

    /**
     * Finds the genres for multiple applications at once.
     *
     * @param int[] $applicationIds IDs of all applications to find the genres for
     * @return array the genres as an associative array mapping the application ID to an array of genre names
     */
    #[ArrayShape(['int' => 'string[]'])]
    public function findAllApplicationGenres(array $applicationIds): array
    {
        $query = 'SELECT ag.band_application_id AS application, g.name AS genre FROM band_application_genre ag JOIN genre g on ag.genre_id = g.id WHERE ag.band_application_id IN (?);';
        $result = $this->getEntityManager()->getConnection()
            ->executeQuery($query, [$applicationIds], [ArrayParameterType::INTEGER])
            ->fetchAllAssociative();

        // merge resulting array into new array mapping the application ID to an array of genre names
        $genres = [];
        foreach ($result as $r)
            $genres[$r['application']][] = $r['genre'];
        return $genres;
    }

    /**
     * Finds the genres for a single application
     *
     * @param int[] $applicationId ID of the application to find the genres for.
     * @return string[] the genre names for the given application
     */
    public function findApplicationGenres(int $applicationId): array
    {
        $query = 'SELECT g.name AS genre FROM band_application_genre ag JOIN genre g on ag.genre_id = g.id WHERE ag.band_application_id = ?;';
        return $this->getEntityManager()->getConnection()
            ->executeQuery($query, [$applicationId])
            ->fetchFirstColumn();
    }
}
