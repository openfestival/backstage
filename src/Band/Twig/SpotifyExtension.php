<?php

namespace App\Band\Twig;

use App\Band\Application\SpotifyApiService;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class SpotifyExtension extends AbstractExtension
{
    public function __construct(
        private SpotifyApiService $spotifyApiService,
    ) {}

    public function getFunctions(): array
    {
        return [
            new TwigFunction('get_spotify_stats', [$this, 'getSpotifyStats']),
            new TwigFunction('get_spotify_artist_id', [$this, 'getSpotifyArtistId']),
        ];
    }

    public function getSpotifyStats(string $spotifyUrl): ?array
    {
        return $this->spotifyApiService->getSpotifyStats($spotifyUrl);
    }

    public function getSpotifyArtistId(string $spotifyUrl): ?string
    {
        return $this->spotifyApiService->extractSpotifyArtistId($spotifyUrl);
    }
} 