<?php

namespace App\Security\Form;

use App\Security\Entity\User;
use App\System\Application\ConfigService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;

class RegistrationForm extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class, [
                'required' => true,
                'label' => 'form.email',
                'invalid_message' => 'email.invalid'
            ])
            ->add('password', PasswordType::class, [
                'mapped' => false,
                'required' => true,
                'label' => 'form.password',
                'invalid_message' => 'password.weak',
                'constraints' => [
                    new Length([
                        'min' => ConfigService::PASSWORD_MIN_LENGTH,
                        'max' => ConfigService::PASSWORD_MAX_LENGTH
                    ])
                ]
            ])
            ->add('passwordConfirm', PasswordType::class, [
                'mapped' => false,
                'required' => true,
                'label' => 'password.confirm'
            ])
            ->add('termsAccepted', CheckboxType::class, [
                'mapped' => false,
                'constraints' => new IsTrue()
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class
        ]);
    }
}
