<?php

namespace App\Security\Form;

use App\Security\Entity\User;
use App\System\Application\ConfigService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;

class AccountForm extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class, [
                'label' => 'form.email',
                'help' => 'account.email.help',
                'invalid_message' => 'email.invalid',
            ])
            ->add('name', TextType::class, [
                'required' => false,
                'label' => 'name',
                'help' => 'account.name'
            ])
            ->add('password', PasswordType::class, [
                'required' => false,
                'mapped' => false,
                'label' => 'account.password.new.label',
                'help' => 'account.password.new.help',
                'invalid_message' => 'password.weak',
                'constraints' => [
                    new Length([
                        'min' => ConfigService::PASSWORD_MIN_LENGTH,
                        'max' => ConfigService::PASSWORD_MAX_LENGTH
                    ])
                ]
            ])
            ->add('passwordConfirm', PasswordType::class, [
                'required' => false,
                'mapped' => false,
                'label' => 'account.password.confirm.label',
                'help' => 'account.password.confirm.help',
                'invalid_message' => 'password.nomatch'
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'save'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class
        ]);
    }
}
