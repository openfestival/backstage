<?php

namespace App\Security\Application;

use App\Security\Entity\PermissionGroup;
use App\Security\Entity\User;
use App\Security\Exception\DuplicateUserException;
use App\System\Application\MailService;
use App\System\Application\TokenGeneratorService;
use DateTimeImmutable;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class AccountService {
    public function __construct(
        private readonly UserPasswordHasherInterface $passwordHasher,
        private readonly TokenGeneratorService       $tokenGenerator,
        private readonly RequestStack                $requestStack,
        private readonly EntityManagerInterface      $doctrine,
        private readonly MailService                 $mailService
    ) {}

    /**
     * Creates a new user account.
     *
     * @param string $email the email address for the new user
     * @param string $password the password to set.
     * @return User the new user account
     * @throws DuplicateUserException if there exists another user with the given email address
     */
    public function createAccount(string $email, ?string $password = null): User {
        $user = new User();
        $user->setEmail($email);
        $user->setPassword($this->passwordHasher->hashPassword($user, $password));
        $user->setActivationToken($this->tokenGenerator->generateToken(16));
        $now = new DateTimeImmutable();
        $user->setRegisteredAt($now);
        $user->setActivationEmailSentAt($now);
        $user->setLastLogin($user->getRegisteredAt());
        $user->setForceReset(false);
        $user->setLang($this->requestStack->getSession()->get('_locale', 'de'));

        try {
            $this->doctrine->persist($user);
            $this->doctrine->flush();
        } catch (UniqueConstraintViolationException $e) {
            throw new DuplicateUserException($e);
        }

        $this->mailService->sendRegisterEmail($user);

        return $user;
    }

    public function getPermissions(UserInterface $user): array {
        if (!$user instanceof User) {
            throw new HttpException(Response::HTTP_INTERNAL_SERVER_ERROR, sprintf(
                'class %s does not support permission inheritance.',
                $user::class,
            ));
        }

        $permissions = [];
        foreach ($user->getPermissionInheritances() as $group)
            $this->collectPermissions($group->getPermissionGroup(), $permissions);

        return $permissions;
    }

    private function collectPermissions(PermissionGroup $group, array &$permissions): void {
        if ($group->getParent() !== null)
            $this->collectPermissions($group->getParent(), $permissions);
        foreach ($group->getPermissions() as $permission)
            $permissions[] = $permission->getPermission();
    }
}
