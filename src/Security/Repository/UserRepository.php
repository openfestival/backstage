<?php

namespace App\Security\Repository;

use App\Security\Entity\User;
use App\System\Application\ConfigService;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, User::class);
    }

    public function findCount($where = '', $parameters = []): float|bool|int|string|null
    {
        $query = $this->createQueryBuilder('u')
            ->select('COUNT(u.id)');
        if (!empty($where))
            $query->where($where)->setParameters($parameters);
        return $query->getQuery()->getSingleScalarResult();
    }

    public function findPage($page, $where = '', $parameters = []) {
        $query = $this->createQueryBuilder('u')
            ->select('u.id', 'u.email', ' u.name', ' u.registeredAt', ' u.activatedAt', ' u.lastLogin', ' u.forceReset', ' u.lang');
        if (!empty($where))
            $query->where($where)->setParameters($parameters);
        return $query
            ->orderBy('u.email')
            ->setFirstResult(($page - 1) * ConfigService::PAGE_SIZE)
            ->setMaxResults(ConfigService::PAGE_SIZE)
            ->getQuery()
            ->getResult();
    }

    public function upgradePassword(PasswordAuthenticatedUserInterface $user, string $newHashedPassword): void
    {
        if (!$user instanceof User)
            return;
        $user->setPassword($newHashedPassword);
        $this->getEntityManager()->flush();
    }
}
