<?php

namespace App\Security\Repository;

use App\Security\Entity\PermissionGroup;
use App\Security\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;
use JetBrains\PhpStorm\ArrayShape;

/**
 * @method PermissionGroup|null find($id, $lockMode = null, $lockVersion = null)
 * @method PermissionGroup|null findOneBy(array $criteria, array $orderBy = null)
 * @method PermissionGroup[]    findAll()
 * @method PermissionGroup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PermissionGroupRepository extends ServiceEntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, PermissionGroup::class);
    }

    /**
     * Finds all groups including their parents as an associative array
     *
     * @return array all groups
     */
    #[ArrayShape([
        'id' => 'int',
        'name' => 'string',
        'parent_id' => '?int',
        'parent_name' => '?string'
    ])]
    public function findAllAsArray(): array {
        return $this->createQueryBuilder('g')
            ->select('g.id', 'g.name', 'p.id AS parent_id', 'p.name AS parent_name')
            ->leftJoin('g.parent', 'p', Join::WITH, 'g.parent = p.id')
            ->orderBy('g.id')
            ->getQuery()
            ->getArrayResult();
    }

    public function findAllByUserAsArray(User $user): array {
        return $this->createQueryBuilder('g')
            ->select('g.id', 'g.name')
            ->join('g.users', 'u', Join::WITH, 'g.id = u.permissionGroup')
            ->where('u.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getArrayResult();
    }
}
