<?php

namespace App\Security\Repository;

use App\Security\Entity\PermissionInheritance;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PermissionInheritance|null find($id, $lockMode = null, $lockVersion = null)
 * @method PermissionInheritance|null findOneBy(array $criteria, array $orderBy = null)
 * @method PermissionInheritance[]    findAll()
 * @method PermissionInheritance[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PermissionInheritanceRepository extends ServiceEntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, PermissionInheritance::class);
    }

    /**
     * @return PermissionInheritance[]
     */
    public function findUserGroups(int $user): array {
        return $this->createQueryBuilder('i')
            ->select('i')
            ->where('i.user = :user')
            ->setParameters([
                'user' => $user
            ])
            ->getQuery()
            ->getResult();
    }

    public function deleteInheritance(int $user, int $group): void
    {
        $this->createQueryBuilder('i')
            ->delete('i')
            ->where('i.user_id = :user AND i.permission_group_id = :group')
            ->setParameters([
                'user' => $user,
                'group' => $group
            ])
            ->getQuery()
            ->execute();
    }
}
