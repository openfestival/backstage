<?php

namespace App\Security\Controller;

use App\Security\Application\AccountService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/api/v1/security')]
class ApiController extends AbstractController {
    public function __construct(private readonly AccountService $accountService) {}

    #[Route('/permissions', methods: ['GET'], format: 'json')]
    #[IsGranted('IS_AUTHENTICATED')]
    public function getIsGranted(): Response {
        return $this->json($this->accountService->getPermissions($this->getUser()));
    }
}
