<?php

namespace App\Security\Controller;

use App\LegacyApiController;
use App\Security\Repository\PermissionGroupRepository;
use App\Security\Repository\UserRepository;
use App\System\Application\ConfigService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use App\Security\Entity\User;

#[Route(path: '/admin')]
class AdminController extends LegacyApiController {

    #[Route(path: '', name: 'admin_index')]
    public function index(): Response {
        $this->denyAccessUnlessGranted('admin.index');
        return $this->render('security/admin/index.html.twig');
    }

    #[Route(path: '/users/{page<\d+>?1}', name: 'admin_users')]
    public function users($page, Request $request, EntityManagerInterface $doctrine): Response {
        $this->denyAccessUnlessGranted('admin.users');

        /** @var UserRepository $userRepo */
        $userRepo = $doctrine->getRepository(User::class);
        $where = '';
        $parameters = [];

        $form = $this->createFormBuilder([])
            ->add('email', SearchType::class, [
                'required' => false
            ])
            ->add('search', SubmitType::class, [
                'label' => 'search'
            ])
            ->setMethod('GET')
            ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $where = 'u.email LIKE :email';
            $parameters['email'] = '%' . $form->getData()['email'] . '%';
        }

        $count = $userRepo->findCount($where, $parameters);
        $users = $userRepo->findPage($page, $where, $parameters);

        return $this->render('security/admin/users.html.twig', [
            'form' => $form->createView(),
            'page' => $page,
            'pageCount' => ceil($count / ConfigService::PAGE_SIZE),
            'users' => $users
        ]);
    }

    #[Route(path: '/user/{user<\d+>}', name: 'admin_user')]
    public function user($user, UserRepository $userRepository, PermissionGroupRepository $groupRepository): Response {
        $this->denyAccessUnlessGranted('admin.users');

        $user = $userRepository->find($user);

        return $this->render('security/admin/user.html.twig', [
            'user' => $user,
        ]);
    }

    #[Route(path: '/groups', name: 'admin_groups')]
    public function groups(PermissionGroupRepository $repository): Response {
        $this->denyAccessUnlessGranted('admin.groups');

        return $this->render('security/admin/groups.html.twig', [
            'groups' => $repository->findAllAsArray()
        ]);
    }
}
