<?php

namespace App\Security\Controller;

use App\LegacyApiController;
use App\Security\Entity\Permission;
use App\Security\Entity\PermissionGroup;
use App\Security\Entity\PermissionInheritance;
use App\Security\Entity\User;
use App\Security\Repository\PermissionGroupRepository;
use App\Security\Repository\PermissionInheritanceRepository;
use App\Security\Repository\PermissionRepository;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route(path: '/api/admin')]
class LegacyAdminApiController extends LegacyApiController {

    public function __construct(
        private readonly PermissionGroupRepository $permissionGroupRepository,
    ) {}

    #[Route(path: '/permissions/{group<\d+>}', methods: ['GET'])]
    public function getPermissions($group, EntityManagerInterface $doctrine): Response {
        if (!$this->isGranted('admin.groups'))
            return $this->responseForbidden();

        /** @var PermissionRepository $repo */
        $repo = $doctrine->getRepository(Permission::class);
        $permissions = array_map(function ($p) {
            return $p['permission'];
        }, $repo->findPermissions($group));

        return $this->responseOk($permissions);
    }

    #[Route(path: '/permissions', methods: ['PUT'])]
    public function putPermission(Request $request, EntityManagerInterface $doctrine): Response {
        if (!$this->isGranted('admin.groups'))
            return $this->responseForbidden();

        $data = $this->resolveBody($request, [
            'group' => 'int',
            'permission' => 'string'
        ]);
        if ($data === false)
            return $this->responseBadRequest();

        /** @var PermissionGroupRepository $repo */
        $repo = $doctrine->getRepository(PermissionGroup::class);
        $group = $repo->find($data->group);
        if ($group === null)
            return $this->responseBadRequest();

        $permission = new Permission();
        $permission->setPermission($data->permission);
        $permission->setPermissionGroup($group);
        $doctrine->persist($permission);
        try {
            $doctrine->flush();
        } catch (UniqueConstraintViolationException $e) {
            return $this->responseBadRequest('Permission already granted');
        }

        return $this->responseOk();
    }

    #[Route(path: '/permissions', methods: ['DELETE'])]
    public function deletePermission(Request $request, EntityManagerInterface $doctrine): Response {
        if (!$this->isGranted('admin.groups'))
            return $this->responseForbidden();

        $data = $this->resolveBody($request, [
            'group' => 'int',
            'permission' => 'string'
        ]);
        if ($data === false)
            return $this->responseBadRequest();

        /** @var PermissionRepository $repo */
        $repo = $doctrine->getRepository(Permission::class);
        $permission = $repo->findOneBy([
            'permission' => $data->permission,
            'permissionGroup' => $data->group
        ]);
        if ($permission === null)
            return $this->responseBadRequest();

        $doctrine->remove($permission);
        $doctrine->flush();

        return $this->responseOk();
    }

    #[Route(path: '/usergroups/{userId<\d+>}', name: 'api.usergroups.get', methods: ['GET'])]
    public function getUserGroups(int $userId, EntityManagerInterface $doctrine): Response {
        if (!$this->isGranted('admin.users'))
            return $this->responseForbidden();

        /** @var PermissionGroupRepository $groupRepo */
        $groupRepo = $doctrine->getRepository(PermissionGroup::class);
        /** @var PermissionInheritanceRepository $inheritanceRepo */
        $inheritanceRepo = $doctrine->getRepository(PermissionInheritance::class);
        $inheritances = [];
        foreach ($inheritanceRepo->findUserGroups($userId) as $inheritance) {
            $inheritances[] = $inheritance->getPermissionGroup()->getId();
        }

        $groups = [];
        foreach ($groupRepo->findAll() as $group) {
            $groups[] = [
                'id' => $group->getId(),
                'name' => $group->getName(),
                'member' => in_array($group->getId(), $inheritances)
            ];
        }
        return $this->responseOk($groups);
    }

    #[Route(path: '/usergroups', methods: ['PUT'])]
    public function putUserGroup(Request $request, EntityManagerInterface $doctrine): Response {
        if (!$this->isGranted('admin.users'))
            return $this->responseForbidden();

        $data = $this->resolveBody($request, [
            'user' => 'int',
            'group' => 'int'
        ]);
        if ($data === false)
            return $this->responseBadRequest();

        /** @var User $user */
        $user = $doctrine->find(User::class, $data->user);
        $group = $doctrine->find(PermissionGroup::class, $data->group);
        if ($user === null || $group === null)
            return $this->responseBadRequest();

        $inheritance = new PermissionInheritance();
        $inheritance->setUser($user);
        $inheritance->setPermissionGroup($group);
        $doctrine->persist($inheritance);
        $doctrine->flush();

        return $this->responseOk();
    }

    #[Route(path: '/usergroups', methods: ['DELETE'])]
    public function deleteUserGroup(Request $request, EntityManagerInterface $doctrine): Response {
        if (!$this->isGranted('admin.users'))
            return $this->responseForbidden();

        $data = $this->resolveBody($request, [
            'user' => 'int',
            'group' => 'int'
        ]);
        if ($data === false)
            return $this->responseBadRequest();

        $inheritance = $doctrine->getRepository(PermissionInheritance::class)->findOneBy([
            'user' => $data->user,
            'permissionGroup' => $data->group
        ]);
        $doctrine->remove($inheritance);
        $doctrine->flush();

        return $this->responseOk();
    }

    #[Route(path: '/groups', name: 'api.groups.get', methods: ['GET'])]
    public function getGroups(): Response
    {
        if (!$this->isGranted('admin.groups'))
            return $this->responseForbidden();

        return $this->responseOk($this->permissionGroupRepository->findAllAsArray());
    }

    #[Route(path: '/groups', methods: ['POST'])]
    public function postGroup(Request $request, EntityManagerInterface $doctrine): Response {
        if (!$this->isGranted('admin.groups'))
            return $this->responseForbidden();

        $data = $this->resolveBody($request, [
            'name' => 'string',
            'parent' => '?int'
        ]);
        if ($data === false)
            return $this->responseBadRequest();

        $group = new PermissionGroup();
        $group->setName($data->name);
        if (isset($data->parent)) {
            /** @var PermissionGroup $parent */
            $parent = $doctrine->find(PermissionGroup::class, $data->parent);
            if ($parent === null)
                return $this->responseBadRequest();
            $group->setParent($parent);
        }
        $doctrine->persist($group);
        $doctrine->flush();

        return $this->responseOk($group->getId());
    }

    #[Route(path: '/groups/{id<\d+>}', methods: ['PUT'])]
    public function putGroup(int $id, Request $request, EntityManagerInterface $doctrine): Response {
        if (!$this->isGranted('admin.groups'))
            return $this->responseForbidden();

        $data = $this->resolveBody($request, [
            'name' => 'string',
            'parent' => '?int'
        ]);
        if ($data === false || $id === $data->parent)
            return $this->responseBadRequest();

        /** @var PermissionGroup $group */
        $group = $doctrine->find(PermissionGroup::class, $id);
        if ($group === null)
            return $this->responseBadRequest();

        $group->setName($data->name);
        if (!empty($data->parent)) {
            /** @var PermissionGroup $parent */
            $parent = $doctrine->find(PermissionGroup::class, $data->parent);
            if ($parent === null)
                return $this->responseBadRequest();
            $group->setParent($parent);
        } else {
            $group->setParent(null);
        }
        $doctrine->persist($group);
        $doctrine->flush();

        return $this->responseOk();
    }

    #[Route(path: '/groups/{id<\d+>}', methods: ['DELETE'])]
    public function delGroup($id, EntityManagerInterface $doctrine): Response {
        if (!$this->isGranted('admin.groups'))
            return $this->responseForbidden();

        /** @var PermissionGroup $group */
        $group = $doctrine->find(PermissionGroup::class, $id);
        if ($group === null)
            return $this->responseBadRequest();

        $doctrine->remove($group);
        $doctrine->flush();

        return $this->responseOk();
    }
}
