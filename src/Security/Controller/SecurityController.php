<?php

namespace App\Security\Controller;

use App\Security\Application\AccountService;
use App\Security\Entity\User;
use App\Security\Form\RegistrationForm;
use App\System\Application\MailService;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Contracts\Translation\TranslatorInterface;

class SecurityController extends AbstractController {
    /**
     * This route renders the login form. Any submissions of the form will be handled by the `form_login`
     * authenticator automatically. If the user submits an invalid email or password, that authenticator
     * will store the error and redirect back to this controller.
     */
    #[Route(path: '/login', name: 'app_login')]
    public function login(AuthenticationUtils $authenticationUtils): Response {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', [
            'email' => $lastUsername,
            'error' => $error
        ]);
    }

    #[Route(path: '/register', name: 'app_register')]
    public function register(
        Request             $request,
        TranslatorInterface $trans,
        AccountService      $accountService
    ): Response {
        $form = $this->createForm(RegistrationForm::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $pwPlain = $form->get('password')->getData();
            $pwConfi = $form->get('passwordConfirm')->getData();
            if ($pwPlain === $pwConfi) {
                $accountService->createAccount($form->get('email')->getData(), $pwPlain);
                $this->addFlash('success', $trans->trans('register.successful'));
                return $this->redirectToRoute('index');
            } else $form->get('passwordConfirm')->addError(new FormError($trans->trans('password.nomatch')));
        }

        return $this->render('security/register.html.twig', [
            'form' => $form->createView()
        ]);
    }

    #[Route(path: '/activate/resend', name: 'app_activate_resend')]
    public function activateResend(
        EntityManagerInterface $doctrine,
        TranslatorInterface    $trans,
        MailService            $mail
    ): RedirectResponse {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');

        /** @var User $user */
        $user = $this->getUser();
        if ($user->getActivatedAt() != null) {
            $this->addFlash('info', $trans->trans('account.activate.alreadyactivated'));
            return $this->redirectToRoute('index');
        }
        $now = new DateTimeImmutable();
        if ($now->getTimestamp() - $user->getActivationEmailSentAt()->getTimestamp() <= 10 * 60) {
            return $this->redirectToRoute('index');
        }
        $user->setActivationEmailSentAt($now);
        $doctrine->flush();
        $mail->sendRegisterEmail($user);
        $this->addFlash('success', $trans->trans('account.activate.resend.success'));
        return $this->redirectToRoute('index');
    }

    #[Route(path: '/activate/{user<\d+>}/{token}', name: 'app_activate')]
    public function activate(
        int                    $user,
        string                 $token,
        EntityManagerInterface $doctrine,
        TranslatorInterface    $trans
    ): Response {
        /** @var $user User */
        $user = $doctrine->getRepository(User::class)->find($user);
        if ($user !== null && strcmp($user->getActivationToken(), $token) === 0) {
            if ($user->getActivatedAt() !== null) {
                $this->addFlash('info', $trans->trans('account.activate.alreadyactivated'));
                return $this->redirectToRoute('index');
            }
            $user->setActivatedAt(new DateTimeImmutable());
            $doctrine->persist($user);
            $doctrine->flush();

            $this->addFlash('success', $trans->trans('account.activate.success'));
            return $this->redirectToRoute('index');
        }

        $this->addFlash('danger', $trans->trans('account.activate.error'));
        return $this->redirectToRoute('index');
    }

    #[Route(path: '/switch_user', name: 'app_switch_user')]
    public function switchUser(): Response {
        $this->denyAccessUnlessGranted('security.switch_user');

        return $this->render('security/switch_user.html.twig');
    }
}
