<?php

namespace App\Security\Controller;

use App\Security\Entity\User;
use App\Security\Form\AccountForm;
use App\Security\Repository\UserRepository;
use App\System\Application\ConfigService;
use App\System\Application\MailService;
use App\System\Application\TokenGeneratorService;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route(path: '/account')]
class AccountController extends AbstractController
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly UserRepository $userRepository,
        private readonly UserPasswordHasherInterface $passwordHasher,
        private readonly TranslatorInterface $translator,
        private readonly TokenGeneratorService $tokenGenerator,
        private readonly MailService $mailService,
    ) {}

    #[Route(path: '/', name: 'app_account')]
    public function account(Request $request): Response
    {
        // force the user to log in if they used remember_me
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        /** @var User $user */
        $user = $this->getUser();
        $oldEmail = $user->getEmail();

        $form = $this->createForm(AccountForm::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $password = $form->get('password')->getData();
            if (!empty($password)) {
                if ($password === $form->get('passwordConfirm')->getData()) {
                    $this->addFlash('success', $this->translator->trans('password.reset.success'));
                    $user->setPassword($this->passwordHasher->hashPassword($user, $password));
                } else {
                    $form->get('passwordConfirm')->addError(new FormError($this->translator->trans('password.nomatch')));
                }
            }

            if ($oldEmail !== $user->getEmail()) {
                $user->setActivationToken($this->tokenGenerator->generateToken(16));
                $user->setActivatedAt(null);
                $user->setActivationEmailSentAt(new DateTimeImmutable());
                $this->mailService->sendChangeaddressEmail($user);
            }

            $this->entityManager->persist($user);
            $this->entityManager->flush();
        }

        return $this->render('security/account.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route(path: '/reset', name: 'app_reset')]
    public function reset(Request $request): Response
    {
        $form = $this->createFormBuilder()
            ->add('email', EmailType::class, [
                'label' => 'password.reset.email',
                'invalid_message' => 'email.invalid',
                'required' => true,
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'password.reset.label',
            ])
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var $user User */
            $user = $this->userRepository->findOneBy([
                'email' => $form->get('email')->getData(),
            ]);
            $this->addFlash('success', $this->translator->trans('password.reset.sent'));
            if ($user !== null) {
                $token = $this->tokenGenerator->generateToken(32);
                $token_hash = password_hash($token, PASSWORD_BCRYPT, array('cost' => ConfigService::PASSWORD_HASH_STRENGTH));
                $user->setPasswordReset($token_hash);
                $user->setPasswordResetTimeout(new DateTimeImmutable('+1 hour'));
                $this->entityManager->persist($user);
                $this->entityManager->flush();

                $this->mailService->sendResetEmail($user, $token);
            }
            return $this->redirectToRoute('index');
        }
        return $this->render('security/reset.html.twig', [
            'form' => $form->createView(),
            'reset' => false,
        ]);

    }

    /**
     * Allows a user to reset their password.
     *
     * @param int $userid user to reset
     * @param string $token token for verification
     */
    #[Route(path: '/reset/{userid}/{token}', name: 'app_reset_new')]
    public function resetNew(int $userid, string $token, Request $request): Response
    {
        /** @var $user User */
        $user = $this->userRepository->find($userid);
        if ($user !== null) {
            if ($user->getPasswordResetTimeout() === null || $user->getPasswordResetTimeout()->diff(new DateTimeImmutable())->invert === 0) {
                $this->addFlash('danger', $this->translator->trans('password.reset.timeout'));
                return $this->redirectToRoute('index');
            }

            if (!password_verify($token, $user->getPasswordReset())) {
                $this->addFlash('danger', $this->translator->trans('password.reset.invalid'));
                return $this->redirectToRoute('index');
            }

            $form = $this->createFormBuilder()
                ->add('password', PasswordType::class, [
                    'label' => 'password.new.label',
                    'invalid_message' => 'password.weak',
                    'constraints' => [
                        new Length([
                            'min' => ConfigService::PASSWORD_MIN_LENGTH,
                            'max' => ConfigService::PASSWORD_MAX_LENGTH,
                        ]),
                    ],
                ])
                ->add('passwordConfirm', PasswordType::class, [
                    'label' => 'password.new.confirm',
                ])
                ->add('submit', SubmitType::class, [
                    'label' => 'send',
                ])
                ->getForm();
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $pwPlain = $form->get('password')->getData();
                $pwConfi = $form->get('passwordConfirm')->getData();

                if ($pwPlain === $pwConfi) {
                    $user->setPassword($this->passwordHasher->hashPassword($user, $pwPlain));
                    $user->setPasswordReset(null);
                    $user->setPasswordResetTimeout(null);
                    $this->entityManager->persist($user);
                    $this->entityManager->flush();
                    $this->addFlash('success', $this->translator->trans('password.reset.success'));
                    return $this->redirectToRoute('app_login');
                }

                $form->get('passwordConfirm')->addError(new FormError($this->translator->trans('password.nomatch')));
            }

            return $this->render('security/reset.html.twig', [
                'form' => $form->createView(),
                'reset' => true,
            ]);
        }

        $this->addFlash('danger', $this->translator->trans('password.reset.invalid'));
        return $this->redirectToRoute('index');
    }
}
