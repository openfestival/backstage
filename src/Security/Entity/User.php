<?php

namespace App\Security\Entity;

use App\Band\Entity\ApplicationComment;
use App\Band\Entity\ApplicationFavorite;
use App\Band\Entity\ApplicationHistory;
use App\Band\Entity\ApplicationScore;
use App\Band\Entity\FinalScore;
use App\Guestlist\Entity\GuestlistInviter;
use App\Inventory\Entity\Item;
use App\Security\Repository\UserRepository;
use App\Volunteer\Entity\ShiftArea;
use App\Volunteer\Entity\Volunteer;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[UniqueEntity(fields: ['email'], message: 'email.duplicate')]
class User implements UserInterface, PasswordAuthenticatedUserInterface {
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(type: 'string', length: 180, unique: true)]
    #[Assert\Email]
    #[Assert\NotBlank]
    private ?string $email = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $name = null;

    /**
     * @var string The hashed password
     */
    #[ORM\Column(type: 'string')]
    private string $password;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $activationToken = null;

    #[ORM\Column(type: 'datetime_immutable')]
    private ?DateTimeImmutable $registeredAt = null;

    #[ORM\Column(type: 'datetime_immutable')]
    private ?DateTimeImmutable $activationEmailSentAt = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private ?DateTimeImmutable $activatedAt = null;

    #[ORM\Column(type: 'datetime_immutable')]
    private ?DateTimeImmutable $lastLogin = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $passwordReset = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private ?DateTimeImmutable $passwordResetTimeout = null;

    #[ORM\Column(type: 'boolean')]
    private ?bool $forceReset = null;

    #[ORM\Column(type: 'string', length: 2)]
    private ?string $lang = null;

    #[ORM\OneToOne(mappedBy: 'user', targetEntity: Volunteer::class, cascade: ['persist', 'remove'])]
    private ?Volunteer $volunteer = null;

    #[ORM\OneToOne(mappedBy: 'user', targetEntity: GuestlistInviter::class)]
    private ?GuestlistInviter $guestlistInviter = null;

    #[ORM\OneToMany(mappedBy: 'supervisor', targetEntity: ShiftArea::class)]
    private Collection $supervisingAreas;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: PermissionInheritance::class)]
    private Collection $permissionInheritances;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: ApplicationHistory::class, orphanRemoval: true)]
    private Collection $bandApplicationHistories;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: ApplicationScore::class, orphanRemoval: true)]
    private Collection $bandApplicationScores;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: ApplicationFavorite::class, orphanRemoval: true)]
    private Collection $bandApplicationFavorites;

    #[ORM\OneToMany(mappedBy: 'author', targetEntity: ApplicationComment::class, orphanRemoval: true)]
    private Collection $bandApplicationComments;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: FinalScore::class, orphanRemoval: true)]
    private Collection $bandFinalScores;

    #[ORM\OneToMany(mappedBy: 'borrowedBy', targetEntity: Item::class)]
    private Collection $inventoryItems;

    public function __construct() {
        $this->supervisingAreas = new ArrayCollection();
        $this->permissionInheritances = new ArrayCollection();
        $this->bandApplicationHistories = new ArrayCollection();
        $this->bandApplicationScores = new ArrayCollection();
        $this->bandApplicationFavorites = new ArrayCollection();
        $this->bandApplicationComments = new ArrayCollection();
        $this->bandFinalScores = new ArrayCollection();
        $this->inventoryItems = new ArrayCollection();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getEmail(): ?string {
        return $this->email;
    }

    public function setEmail(string $email): self {
        $this->email = $email;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getUserIdentifier(): string {
        return $this->email;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string {
        return $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string {
        return $this->password;
    }

    public function setPassword(string $password): self {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials() {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getName(): ?string {
        return $this->name;
    }

    public function setName(?string $name): self {
        $this->name = $name;

        return $this;
    }

    public function getDisplayName(): string {
        if ($this->name != null)
            return $this->name;
        return $this->email;
    }

    public function getActivationToken(): ?string {
        return $this->activationToken;
    }

    public function setActivationToken(string $activationToken): self {
        $this->activationToken = $activationToken;

        return $this;
    }

    public function getRegisteredAt(): ?DateTimeImmutable {
        return $this->registeredAt;
    }

    public function setRegisteredAt(DateTimeImmutable $registeredAt): self {
        $this->registeredAt = $registeredAt;

        return $this;
    }

    public function getActivationEmailSentAt(): ?DateTimeImmutable {
        return $this->activationEmailSentAt;
    }

    public function setActivationEmailSentAt(DateTimeImmutable $activationEmailSentAt): self {
        $this->activationEmailSentAt = $activationEmailSentAt;

        return $this;
    }

    public function getActivatedAt(): ?DateTimeImmutable {
        return $this->activatedAt;
    }

    public function setActivatedAt(?DateTimeImmutable $activatedAt): self {
        $this->activatedAt = $activatedAt;

        return $this;
    }

    public function getLastLogin(): ?DateTimeImmutable {
        return $this->lastLogin;
    }

    public function setLastLogin(DateTimeImmutable $lastLogin): self {
        $this->lastLogin = $lastLogin;

        return $this;
    }

    public function getPasswordReset(): ?string {
        return $this->passwordReset;
    }

    public function setPasswordReset(?string $passwordReset): self {
        $this->passwordReset = $passwordReset;

        return $this;
    }

    public function getPasswordResetTimeout(): ?DateTimeImmutable {
        return $this->passwordResetTimeout;
    }

    public function setPasswordResetTimeout(?DateTimeImmutable $passwordResetTimeout): self {
        $this->passwordResetTimeout = $passwordResetTimeout;

        return $this;
    }

    public function getForceReset(): ?bool {
        return $this->forceReset;
    }

    public function setForceReset(bool $forceReset): self {
        $this->forceReset = $forceReset;

        return $this;
    }

    public function getLang(): ?string {
        return $this->lang;
    }

    public function setLang(string $lang): self {
        $this->lang = $lang;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array {
        return ['ROLE_USER'];
    }

    public function getVolunteer(): ?Volunteer {
        return $this->volunteer;
    }

    public function setVolunteer(Volunteer $volunteer): self {
        $this->volunteer = $volunteer;

        // set the owning side of the relation if necessary
        if ($this !== $volunteer->getUser()) {
            $volunteer->setUser($this);
        }

        return $this;
    }

    /**
     * @return Collection|ShiftArea[]
     */
    public function getSupervisingAreas(): Collection {
        return $this->supervisingAreas;
    }

    public function addSupervisingArea(ShiftArea $supervisingArea): self {
        if (!$this->supervisingAreas->contains($supervisingArea)) {
            $this->supervisingAreas[] = $supervisingArea;
            $supervisingArea->setSupervisor($this);
        }

        return $this;
    }

    public function removeSupervisingArea(ShiftArea $supervisingArea): self {
        if ($this->supervisingAreas->contains($supervisingArea)) {
            $this->supervisingAreas->removeElement($supervisingArea);
            // set the owning side to null (unless already changed)
            if ($supervisingArea->getSupervisor() === $this) {
                $supervisingArea->setSupervisor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PermissionInheritance[]
     */
    public function getPermissionInheritances(): Collection {
        return $this->permissionInheritances;
    }

    public function addPermissionInheritance(PermissionInheritance $inheritance): self {
        if (!$this->permissionInheritances->contains($inheritance)) {
            $this->permissionInheritances[] = $inheritance;
            $inheritance->setUser($this);
        }

        return $this;
    }

    public function removePermission(PermissionInheritance $permission): self {
        $this->permissionInheritances->removeElement($permission);

        return $this;
    }

    /**
     * @return Collection|ApplicationHistory[]
     */
    public function getBandApplicationHistories(): Collection {
        return $this->bandApplicationHistories;
    }

    public function addBandApplicationHistory(ApplicationHistory $bandApplicationHistory): self {
        if (!$this->bandApplicationHistories->contains($bandApplicationHistory)) {
            $this->bandApplicationHistories[] = $bandApplicationHistory;
            $bandApplicationHistory->setUser($this);
        }

        return $this;
    }

    public function removeBandApplicationHistory(ApplicationHistory $bandApplicationHistory): self {
        if ($this->bandApplicationHistories->contains($bandApplicationHistory)) {
            $this->bandApplicationHistories->removeElement($bandApplicationHistory);
            // set the owning side to null (unless already changed)
            if ($bandApplicationHistory->getUser() === $this) {
                $bandApplicationHistory->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ApplicationScore[]
     */
    public function getBandApplicationScores(): Collection {
        return $this->bandApplicationScores;
    }

    public function addBandApplicationScore(ApplicationScore $bandApplicationScore): self {
        if (!$this->bandApplicationScores->contains($bandApplicationScore)) {
            $this->bandApplicationScores[] = $bandApplicationScore;
            $bandApplicationScore->setUser($this);
        }

        return $this;
    }

    public function removeBandApplicationScore(ApplicationScore $bandApplicationScore): self {
        if ($this->bandApplicationScores->contains($bandApplicationScore)) {
            $this->bandApplicationScores->removeElement($bandApplicationScore);
            // set the owning side to null (unless already changed)
            if ($bandApplicationScore->getUser() === $this) {
                $bandApplicationScore->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ApplicationFavorite[]
     */
    public function getBandApplicationFavorites(): Collection {
        return $this->bandApplicationFavorites;
    }

    public function addBandApplicationFavorite(ApplicationFavorite $bandApplicationFavorite): self {
        if (!$this->bandApplicationFavorites->contains($bandApplicationFavorite)) {
            $this->bandApplicationFavorites[] = $bandApplicationFavorite;
            $bandApplicationFavorite->setUser($this);
        }

        return $this;
    }

    public function removeBandApplicationFavorite(ApplicationFavorite $bandApplicationFavorite): self {
        if ($this->bandApplicationFavorites->contains($bandApplicationFavorite)) {
            $this->bandApplicationFavorites->removeElement($bandApplicationFavorite);
            // set the owning side to null (unless already changed)
            if ($bandApplicationFavorite->getUser() === $this) {
                $bandApplicationFavorite->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ApplicationComment[]
     */
    public function getBandApplicationComments(): Collection {
        return $this->bandApplicationComments;
    }

    public function addBandApplicationComment(ApplicationComment $bandApplicationComment): self {
        if (!$this->bandApplicationComments->contains($bandApplicationComment)) {
            $this->bandApplicationComments[] = $bandApplicationComment;
            $bandApplicationComment->setAuthor($this);
        }

        return $this;
    }

    public function removeBandApplicationComment(ApplicationComment $bandApplicationComment): self {
        if ($this->bandApplicationComments->contains($bandApplicationComment)) {
            $this->bandApplicationComments->removeElement($bandApplicationComment);
            // set the owning side to null (unless already changed)
            if ($bandApplicationComment->getAuthor() === $this) {
                $bandApplicationComment->setAuthor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|FinalScore[]
     */
    public function getBandFinalScores(): Collection {
        return $this->bandFinalScores;
    }

    public function addScore(FinalScore $score): self {
        if (!$this->bandFinalScores->contains($score)) {
            $this->bandFinalScores[] = $score;
            $score->setUser($this);
        }

        return $this;
    }

    public function removeScore(FinalScore $score): self {
        if ($this->bandFinalScores->contains($score)) {
            $this->bandFinalScores->removeElement($score);
            // set the owning side to null (unless already changed)
            if ($score->getUser() === $this) {
                $score->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Item[]
     */
    public function getInventoryItems(): Collection {
        return $this->inventoryItems;
    }

    public function addInventoryItem(Item $inventoryItem): self {
        if (!$this->inventoryItems->contains($inventoryItem)) {
            $this->inventoryItems[] = $inventoryItem;
            $inventoryItem->setBorrowedBy($this);
        }

        return $this;
    }

    public function removeInventoryItem(Item $inventoryItem): self {
        if ($this->inventoryItems->contains($inventoryItem)) {
            $this->inventoryItems->removeElement($inventoryItem);
            // set the owning side to null (unless already changed)
            if ($inventoryItem->getBorrowedBy() === $this) {
                $inventoryItem->setBorrowedBy(null);
            }
        }

        return $this;
    }
}
