<?php

namespace App\Security\Entity;

use App\Security\Repository\PermissionInheritanceRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PermissionInheritanceRepository::class)]
class PermissionInheritance {
    #[ORM\JoinColumn(nullable: false)]
    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'permissionInheritances')]
    private User $user;

    #[ORM\JoinColumn(nullable: false)]
    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: PermissionGroup::class, inversedBy: 'users')]
    private PermissionGroup $permissionGroup;

    public function getUser(): User {
        return $this->user;
    }

    public function setUser($user): self {
        $this->user = $user;

        return $this;
    }

    public function getPermissionGroup(): PermissionGroup {
        return $this->permissionGroup;
    }

    public function setPermissionGroup($permissionGroup): self {
        $this->permissionGroup = $permissionGroup;

        return $this;
    }
}
