<?php

namespace App\Security\Entity;

use App\Security\Repository\PermissionGroupRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use JsonSerializable;
use ReturnTypeWillChange;

#[ORM\Entity(repositoryClass: PermissionGroupRepository::class)]
class PermissionGroup implements JsonSerializable {
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $name = null;

    #[ORM\ManyToOne(targetEntity: PermissionGroup::class, inversedBy: 'children')]
    private ?PermissionGroup $parent = null;

    #[ORM\OneToMany(mappedBy: 'parent', targetEntity: PermissionGroup::class)]
    private Collection $children;

    #[ORM\OneToMany(mappedBy: 'permissionGroup', targetEntity: Permission::class, fetch: 'EAGER', orphanRemoval: true)]
    private Collection $permissions;

    #[ORM\OneToMany(mappedBy: 'permissionGroup', targetEntity: PermissionInheritance::class)]
    private Collection $users;

    public function __construct() {
        $this->children = new ArrayCollection();
        $this->permissions = new ArrayCollection();
        $this->users = new ArrayCollection();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getName(): ?string {
        return $this->name;
    }

    public function setName(string $name): self {
        $this->name = $name;

        return $this;
    }

    public function getParent(): ?self {
        return $this->parent;
    }

    public function setParent(?self $parent): self {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getChildren(): Collection {
        return $this->children;
    }

    public function addChild(PermissionGroup $child): self {
        if ($child->getId() === $this->getId())
            throw new Exception("Cannot set permission group as a child of itself");
        if (!$this->children->contains($child)) {
            $this->children[] = $child;
            $child->setParent($this);
        }

        return $this;
    }

    public function removeChild(PermissionGroup $child): self {
        if ($this->children->contains($child)) {
            $this->children->removeElement($child);
            // set the owning side to null (unless already changed)
            if ($child->getParent() === $this) {
                $child->setParent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getPermissions(): Collection {
        return $this->permissions;
    }

    public function addPermission(Permission $permission): self {
        if (!$this->permissions->contains($permission)) {
            $this->permissions[] = $permission;
            $permission->setPermissionGroup($this);
        }

        return $this;
    }

    public function removePermission(Permission $permission): self {
        if ($this->permissions->contains($permission)) {
            $this->permissions->removeElement($permission);
            // set the owning side to null (unless already changed)
            if ($permission->getPermissionGroup() === $this) {
                $permission->setPermissionGroup(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PermissionInheritance[]
     */
    public function getUsers(): Collection {
        return $this->users;
    }

    public function addUser(PermissionInheritance $inheritance): self {
        if (!$this->users->contains($inheritance)) {
            $this->users[] = $inheritance;
            $inheritance->setPermissionGroup($this);
        }

        return $this;
    }

    public function removeUser(PermissionInheritance $user): self {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            $user->setPermissionGroup(null);
        }

        return $this;
    }

    #[ReturnTypeWillChange]
    public function jsonSerialize(): array {
        return get_object_vars($this);
    }
}
