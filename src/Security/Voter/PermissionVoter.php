<?php

namespace App\Security\Voter;

use App\Security\Application\AccountService;
use App\Security\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\CacheableVoterInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * Voter for permissions. Permissions are defined as lowercase words (a-z) and underscores (_) separated by period.
 */
class PermissionVoter extends Voter implements CacheableVoterInterface {
    public function __construct(private readonly AccountService $accountService) {}

    protected function supports(string $attribute, $subject): bool {
        return preg_match('/^[a-z_]+(\.[a-z_]+)*$/', $attribute);
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool {
        $user = $token->getUser();
        // if the user is anonymous do not grant access
        if (!$user instanceof User)
            return false;

        $permissions = $this->accountService->getPermissions($user);

        // search for the exact permission string
        if (in_array($attribute, $permissions))
            return true;

        // check for global wildcard
        if (in_array('*', $permissions))
            return true;

        // search for wildcard .* permissions
        $parts = explode('.', $attribute);
        while (sizeof($parts) > 1) {
            // remove last element of array
            array_pop($parts);
            // append .* and search
            if (in_array(implode('.', $parts) . '.*', $permissions))
                return true;
        }
        return false;
    }
}
