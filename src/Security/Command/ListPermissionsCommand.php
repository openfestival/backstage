<?php

namespace App\Security\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ListPermissionsCommand extends Command {
    protected static $defaultName = 'app:listperms';

    protected function configure(): void
    {
        $this->setDescription('Extracts all permissions from all controllers and templates');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int {
        $result = [];

        foreach ($this->getFilesRecursively('src') as $file) {
            foreach ($this->findPermissions($file, '/(denyAccessUnlessGranted|isGranted)\(([^)]*)\)/', 2) as $perm) {
                $result[$perm][] = $file;
            }
        }

        foreach ($this->getFilesRecursively('templates') as $file) {
            foreach ($this->findPermissions($file, '/is_granted\(([^)]*)\)/', 1) as $perm) {
                $result[$perm][] = $file;
            }
        }

        $result = array_map(function ($files) {
            $files = array_unique($files);
            ksort($files);
            return $files;
        }, $result);
        ksort($result);

        foreach ($result as $perm => $files) {
            $output->writeln('<info>' . $perm . '</info>');
            foreach ($files as $file)
                $output->writeln('<comment>    ' . $file . '</comment>');
        }

        return 0;
    }

    private function findPermissions($file, $regex, $group) {
        $content = file_get_contents($file);
        $matches = [];
        preg_match_all($regex, $content, $matches);
        return $matches[$group];
    }

    private function getFilesRecursively($dir, &$results = array()) {
        $files = scandir($dir);
        foreach ($files as $key => $value) {
            $path = realpath($dir . DIRECTORY_SEPARATOR . $value);
            if (!is_dir($path)) {
                $results[] = $path;
            } else if ($value !== '.' && $value !== '..') {
                $this->getFilesRecursively($path, $results);
            }
        }

        return $results;
    }

}
