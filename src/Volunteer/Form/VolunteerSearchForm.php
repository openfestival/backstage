<?php

namespace App\Volunteer\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Form to search for volunteers by shift area and name
 */
class VolunteerSearchForm extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('outdated', CheckboxType::class, [
                'label' => 'volunteer.find_outdated',
                'required' => false
            ])
            ->add('search', SearchType::class, [
                'required' => false
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'search'
            ])
            ->setMethod('GET');
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'csrf_protection' => false
        ]);
    }
}
