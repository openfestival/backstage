<?php

namespace App\Volunteer\Form;

use App\Volunteer\Entity\Shift;
use IntlDateFormatter;
use Locale;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ShiftType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $formatter = new IntlDateFormatter(Locale::getDefault(), IntlDateFormatter::FULL, IntlDateFormatter::NONE);
        /** @var Shift $shift */
        $shift = $options['shift'];
        if ($options['flexible']) {
            $builder
                ->add('date', CheckboxType::class, [
                    'label' => $formatter->format($shift->getStart())
                ])
                ->add('start', TimeType::class, [
                    'hours' => range($shift->getStart()->format('H'), $shift->getEnd()->format('H') - 1),
                    'with_minutes' => false,
                    'input' => 'datetime_immutable',
                ])
                ->add('end', TimeType::class, [
                    'hours' => array_reverse(range((int)$shift->getStart()->format('H') + 1, $shift->getEnd()->format('H'))),
                    'with_minutes' => false,
                    'input' => 'datetime_immutable',
                ]);
        } else {
            $builder->add('date', CheckboxType::class, [
                'label' => $shift->getStart()->format('D, d.m. H:i') . ' - ' . $shift->getEnd()->format('H:i')
            ]);
        }
    }

    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        $view->vars['shift'] = $options['shift'];
        $view->vars['flexible'] = $options['flexible'];
        $view->vars['data'] = $form->getData();
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefined([
            'shift',
            'flexible'
        ]);
        $resolver->setAllowedTypes('shift', Shift::class);
        $resolver->setAllowedTypes('flexible', 'bool');
        $resolver->setRequired('shift');
        $resolver->setDefaults([
            'flexible' => false,
            'error_mapping' => [
                '.' => 'date'
            ]
        ]);
    }

    public function getParent(): string {
        return FormType::class;
    }
}
