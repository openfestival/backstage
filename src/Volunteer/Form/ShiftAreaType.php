<?php

namespace App\Volunteer\Form;

use App\System\Application\ConfigService;
use App\Volunteer\Repository\ShiftRepository;
use App\Volunteer\Validator\Constraints\ShiftTimes;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Festival\Entity\Festival;

class ShiftAreaType extends AbstractType {
    private ShiftRepository $shiftRepository;
    private ConfigService $config;

    public function __construct(ShiftRepository $shiftRepository, ConfigService $config) {
        $this->shiftRepository = $shiftRepository;
        $this->config = $config;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $shifts = $this->shiftRepository->findAllWhereAreaNameAndFestival($options['area'], $options['festival']);

        $builder->setAttribute('shifts', $shifts);
        foreach ($shifts as $shift) {
            $builder->add($shift->getId(), ShiftType::class, [
                'shift' => $shift,
                'flexible' => $options['flexible'],
                'constraints' => new ShiftTimes([
                    'shift' => $shift
                ]),
            ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefined([
            'area',
            'flexible',
            'festival'
        ]);
        $resolver->setAllowedTypes('area', 'string');
        $resolver->setAllowedTypes('flexible', 'bool');
        $resolver->setAllowedTypes('festival', Festival::class);
        $resolver->setRequired('area');
        $resolver->setDefaults([
            'flexible' => false,
            'festival' => $this->config->getCurrentFestival()
        ]);
    }

    public function getParent(): string {
        return FormType::class;
    }
}
