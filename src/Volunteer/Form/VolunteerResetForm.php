<?php

namespace App\Volunteer\Form;

use App\Volunteer\Enum\VolunteerStatus;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class VolunteerResetForm extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('status', ChoiceType::class, [
                'choices' => [
                    'volunteer.reset.accepted' => VolunteerStatus::ACCEPTED,
                    'volunteer.reset.denied' => VolunteerStatus::DENIED,
                    'volunteer.reset.pending' => VolunteerStatus::PENDING,
                ],
                // select accepted status by default
                'data' => [VolunteerStatus::ACCEPTED],
                'multiple' => true,
                'expanded' => true,
                'required' => true,
                'label' => 'volunteer.reset.label'
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'volunteer.reset.submit',
                'attr' => ['class' => 'btn-danger']
            ]);
    }
}
