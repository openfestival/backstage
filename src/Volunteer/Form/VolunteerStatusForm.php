<?php

namespace App\Volunteer\Form;

use App\Volunteer\Enum\VolunteerStatus;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class VolunteerStatusForm extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('status', EnumType::class, [
                'class' => VolunteerStatus::class,
                'choice_label' => function ($choice) {
                    return 'volunteer.status.' . $choice->value;
                }
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'save'
            ]);
    }
}
