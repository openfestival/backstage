<?php

namespace App\Volunteer\Form;

use App\Festival\Entity\FestivalDay;
use App\Security\Entity\User;
use App\System\Application\ConfigService;
use App\Volunteer\Entity\ShiftArea;
use App\Volunteer\Entity\Volunteer;
use App\Volunteer\Enum\Food;
use App\Volunteer\Enum\Gender;
use App\Volunteer\Form\DataMapper\VolunteerMapper;
use Closure;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use IntlDateFormatter;
use Locale;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class VolunteerForm extends AbstractType {
    private EntityManagerInterface $doctrine;
    private ConfigService $config;
    private Security $security;

    public function __construct(EntityManagerInterface $doctrine, ConfigService $config, Security $security) {
        $this->doctrine = $doctrine;
        $this->config = $config;
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $formatter = new IntlDateFormatter(Locale::getDefault(), IntlDateFormatter::FULL, IntlDateFormatter::NONE);
        /** @var User $user */
        $user = $this->security->getUser();
        $builder
            ->add('firstname', TextType::class, [
                'label' => 'form.firstname',
                'constraints' => new NotBlank()
            ])
            ->add('surname', TextType::class, [
                'label' => 'form.surname',
                'constraints' => new NotBlank()
            ])
            ->add('email', EmailType::class, [
                'label' => 'form.email',
                'help' => $user === null ? 'volunteer.email.nouser' : 'volunteer.email.user',
                'data' => $user?->getEmail(),
                'empty_data' => $user?->getEmail()
            ])
            ->add('street', TextType::class, [
                'label' => 'form.address.street',
                'constraints' => new NotBlank()
            ])
            ->add('zip', TextType::class, [
                'label' => 'form.address.zip',
                'constraints' => new NotBlank()
            ])
            ->add('city', TextType::class, [
                'label' => 'form.address.city',
                'constraints' => new NotBlank()
            ])
            ->add('phone', TelType::class, [
                'label' => 'phone',
                'help' => 'optional',
                'required' => false
            ])
            ->add('gender', ChoiceType::class, [
                'choices' => Gender::getAvailableTypes(),
                'choice_label' => function ($value) {
                    return 'gender.' . $value;
                },
                'label' => 'form.gender',
            ])
            ->add('birthday', BirthdayType::class, [
                'label' => 'birthday',
                'format' => 'dd MMM yyyy',
                'years' => range(date('Y') - 18, date('Y') - 99),
                'input' => 'datetime_immutable',
                'help' => 'volunteer.birthday.help'
            ])
            ->add('food', ChoiceType::class, [
                'choices' => Food::getAvailableTypes(),
                'choice_label' => function ($value) {
                    return 'food.' . $value;
                },
                'label' => 'form.food',
            ])
            ->add('construction', ShiftAreaType::class, [
                'area' => 'Aufbau',
                'flexible' => true,
                'required' => false,
                'label' => 'volunteer.construction.label',
                'help' => 'volunteer.construction.help'
            ])
            ->add('dismantling', ShiftAreaType::class, [
                'area' => 'Abbau',
                'flexible' => true,
                'required' => false,
                'label' => 'volunteer.dismantling.label',
                'help' => 'volunteer.construction.help'
            ])
            ->add('days', EntityType::class, [
                'class' => FestivalDay::class,
                'query_builder' => function (EntityRepository $er) {
                    $festival = $this->config->getCurrentFestival();
                    return $er->createQueryBuilder('d')
                        ->where('d.festival = :festival', 'd.date >= :start', 'd.date <= :end')
                        ->setParameters([
                            'festival' => $festival->getId(),
                            'start' => $festival->getStart(),
                            'end' => $festival->getEnd()
                        ])
                        ->orderBy('d.date', 'ASC');
                },
                'choice_label' => fn(FestivalDay $day) => $formatter->format($day->getDate()),
                'multiple' => true,
                'expanded' => true,
                'required' => false,
                'label' => 'volunteer.days'
            ])
            ->add('preferredArea1', EntityType::class, [
                'class' => ShiftArea::class,
                'query_builder' => $this->buildPreferableAreasQuery(),
                'label' => 'volunteer.preferred_areas.label',
                'choice_label' => 'name',
                'required' => false
            ])
            ->add('preferredArea2', EntityType::class, [
                'class' => ShiftArea::class,
                'query_builder' => $this->buildPreferableAreasQuery(),
                'choice_label' => 'name',
                'required' => false
            ])
            ->add('preferredArea3', EntityType::class, [
                'class' => ShiftArea::class,
                'query_builder' => $this->buildPreferableAreasQuery(),
                'help' => 'volunteer.preferred_areas.help',
                'choice_label' => 'name',
                'required' => false
            ])
            ->add('preferredShifts', IntegerType::class, [
                'attr' => [
                    'min' => 1,
                    'max' => 4,
                    'placeholder' => 2
                ],
                'label' => 'volunteer.preferred_shifts.label',
                'help' => 'volunteer.preferred_shifts.help',
                'required' => false,
                'empty_data' => '2',
            ])
            ->add('experience', TextareaType::class, [
                'attr' => ['rows' => 5],
                'required' => false
            ])
            ->add('comments', TextareaType::class, [
                'attr' => ['rows' => 5],
                'required' => false
            ])
            ->add('password', PasswordType::class, [
                'mapped' => false,
                'required' => $user === null,
                'label' => 'form.password',
                'invalid_message' => 'password.weak',
                'constraints' => [
                    new Length([
                        'min' => ConfigService::PASSWORD_MIN_LENGTH,
                        'max' => ConfigService::PASSWORD_MAX_LENGTH
                    ])
                ]
            ])
            ->add('passwordConfirm', PasswordType::class, [
                'mapped' => false,
                'required' => $user === null,
                'label' => 'password.confirm'
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'send'
            ])
            ->setDataMapper(new VolunteerMapper($this->doctrine));
    }

    private function buildPreferableAreasQuery(): Closure {
        return function (EntityRepository $er) {
            return $er->createQueryBuilder('a')
                ->where('a.preferable = 1')
                ->orderBy('a.name', 'ASC');
        };
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => Volunteer::class,
        ]);
    }
}
