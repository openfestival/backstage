<?php

namespace App\Volunteer\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class EmailForm extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('subject', TextType::class, [
                'label' => 'subject',
                'required' => 'true',
                'constraints' => new NotBlank()
            ])
            ->add('text', TextareaType::class, [
                'attr' => ['rows' => 20],
                'required' => 'true',
                'constraints' => new NotBlank()
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'send'
            ]);
    }
}
