<?php

namespace App\Volunteer\Form;

use App\Festival\Entity\FestivalDay;
use App\Festival\Repository\FestivalDayRepository;
use App\Volunteer\Entity\Shift;
use App\Volunteer\Entity\ShiftArea;
use App\Volunteer\Entity\Volunteer;
use App\Volunteer\Form\DataMapper\ShiftMapper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Festival\Entity\Festival;

class ShiftForm extends AbstractType {
    private ShiftMapper $shiftMapper;
    private FestivalDayRepository $festivalDayRepository;

    public function __construct(ShiftMapper $shiftMapper, FestivalDayRepository $festivalDayRepository) {
        $this->shiftMapper = $shiftMapper;
        $this->festivalDayRepository = $festivalDayRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $festival = $options['festival'];

        if (!$options['all_days'])
            $days = $this->festivalDayRepository->findAllDuringFestival($festival);
        else $days = $this->festivalDayRepository->findBy([
            'festival' => $festival
        ]);

        $builder
            ->add('area', ChoiceType::class, [
                'choices' => $options['areas'],
                'choice_label' => function (ShiftArea $area, $key, $value) {
                    return $area->getName();
                },
                'choice_value' => function (ShiftArea $area = null) {
                    return $area ? $area->getId() : '';
                },
                'preferred_choices' => function (ShiftArea $area, $key, $value) use ($options) {
                    return $area->getId() == $options['current_area']->getId();
                },
                'choice_translation_domain' => false,
                'required' => true,
                'label' => 'area'
            ])
            ->add('name', TextType::class, [
                'label' => 'name',
                'required' => false,
                'empty_data' => ''
            ])
            ->add('day', ChoiceType::class, [
                'choices' => $days,
                'choice_label' => function (FestivalDay $day, $key, $value) {
                    return $day->getDate()->format('l, d.m.');
                },
                'choice_value' => function (FestivalDay $day = null) {
                    return $day ? $day->getId() : '';
                },
                'preferred_choices' => function (FestivalDay $day, $key, $value) use ($options, $festival) {
                    if ($options['all_days']) {
                        return $day->getDate() >= $festival->getStart() && $day->getDate() <= $festival->getEnd();
                    }
                    return false;
                },
                'choice_translation_domain' => false,
                'required' => true,
                'label' => 'day'
            ])
            ->add('start', DateTimeType::class, [
                'years' => [$festival->getStart()->format('Y')],
                'months' => array_unique([$festival->getStart()->format('m'), $festival->getEnd()->format('m')]),
                'days' => array_map(function ($day) {
                    return $day->getDate()->format('d');
                }, $festival->getDays()->toArray()),
                'minutes' => [0, 15, 30, 45],
                'choice_translation_domain' => false,
                'input' => 'datetime_immutable',
                'required' => true,
                'label' => 'start'
            ])
            ->add('end', DateTimeType::class, [
                'years' => [$festival->getStart()->format('Y')],
                'months' => array_unique([$festival->getStart()->format('m'), $festival->getEnd()->format('m')]),
                'days' => array_map(function ($day) {
                    return $day->getDate()->format('d');
                }, $festival->getDays()->toArray()),
                'minutes' => [0, 15, 30, 45],
                'choice_translation_domain' => false,
                'input' => 'datetime_immutable',
                'required' => true,
                'label' => 'end'
            ])
            ->add('volunteer', ChoiceType::class, [
                'choices' => $options['volunteers'],
                'choice_label' => function (Volunteer $vol, $key, $value) {
                    return $vol->getFirstname() . ' ' . $vol->getSurname();
                },
                'choice_value' => function (Volunteer $vol = null) {
                    return $vol ? $vol->getId() : '';
                },
                'choice_translation_domain' => false,
                'required' => false,
                'label' => 'volunteer'
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'send'
            ])
            ->setDataMapper($this->shiftMapper);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefined([
            'areas',
            'festival',
            'volunteers',
            'current_area',
            'all_days'
        ]);
        $resolver->setRequired('areas');
        $resolver->setRequired('festival');
        $resolver->setRequired('volunteers');
        $resolver->setRequired('current_area');

        $resolver->setAllowedTypes('areas', 'App\Volunteer\Entity\ShiftArea[]');
        $resolver->setAllowedTypes('festival', Festival::class);
        $resolver->setAllowedTypes('volunteers', 'App\Volunteer\Entity\Volunteer[]');
        $resolver->setAllowedTypes('current_area', ShiftArea::class);
        $resolver->setAllowedTypes('all_days', 'bool');

        $resolver->setDefaults([
            'data_class' => Shift::class,
            'all_days' => false
        ]);
    }
}
