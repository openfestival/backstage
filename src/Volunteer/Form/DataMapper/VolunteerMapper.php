<?php

namespace App\Volunteer\Form\DataMapper;

use App\Volunteer\Entity\Shift;
use App\Volunteer\Entity\ShiftAreaPreference;
use App\Volunteer\Entity\ShiftVolunteer;
use App\Volunteer\Entity\Volunteer;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Form\FormInterface;
use Traversable;

class VolunteerMapper implements DataMapperInterface
{
    private EntityManagerInterface $doctrine;

    public function __construct(EntityManagerInterface $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    /**
     * @var FormInterface[]|Traversable $forms
     */
    public function mapDataToForms($data, $forms): void
    {
        if ($data === null)
            return;

        if (!$data instanceof Volunteer)
            throw new UnexpectedTypeException($data, Volunteer::class);

        $forms = iterator_to_array($forms);

        $forms['firstname']->setData($data->getFirstname());
        $forms['surname']->setData($data->getSurname());
        $forms['street']->setData($data->getStreet());
        $forms['zip']->setData($data->getZip());
        $forms['city']->setData($data->getCity());
        $forms['phone']->setData($data->getPhone());
        $forms['gender']->setData($data->getGender());
        $forms['birthday']->setData($data->getBirthday());
        $forms['food']->setData($data->getFood());
        $forms['days']->setData($data->getDays());
        $i = 0;
        foreach ($data->getPreferredAreas() as $pref) {
            $forms['preferredArea' . $i + 1]->setData($pref->getShiftArea());
            if (++$i == 3)
                break;
        }
        $forms['preferredShifts']->setData($data->getPreferredShifts());
        $forms['experience']->setData($data->getExperience());
        $forms['comments']->setData($data->getComments());

        $construction = [];
        $allConstructions = $forms['construction']->all();
        $dismantling = [];
        $allDismantlings = $forms['dismantling']->all();
        foreach ($data->getShifts() as $shift) {
            $id = $shift->getShift()->getId();
            if (array_key_exists($id, $allConstructions)) {
                $construction[$id] = [
                    'date' => true,
                    'start' => $shift->getStart(),
                    'end' => $shift->getEnd()
                ];
            }
            if (array_key_exists($id, $allDismantlings)) {
                $dismantling[$id] = [
                    'date' => true,
                    'start' => $shift->getStart(),
                    'end' => $shift->getEnd()
                ];
            }
        }
        $forms['construction']->setData($construction);
        $forms['dismantling']->setData($dismantling);
    }

    /**
     * @param Volunteer $data
     */
    public function mapFormsToData($forms, &$data): void
    {
        $forms = iterator_to_array($forms);

        $data->setFirstname($forms['firstname']->getData());
        $data->setSurname($forms['surname']->getData());
        $data->setStreet($forms['street']->getData());
        $data->setZip($forms['zip']->getData());
        $data->setCity($forms['city']->getData());
        $data->setPhone($forms['phone']->getData());
        $data->setGender($forms['gender']->getData());
        $data->setBirthday($forms['birthday']->getData());
        $data->setFood($forms['food']->getData());
        $data->setDays($forms['days']->getData() ?? new ArrayCollection());
        $preferences = new ArrayCollection();
        $duplicates = [];
        for ($i = 1; $i <= 3; $i++) {
            $pref = $forms['preferredArea' . $i]->getData();
            if ($pref != null && !in_array($pref->getId(), $duplicates)) {
                $preferences->add(new ShiftAreaPreference($pref, $data, $i));
                $duplicates[] = $pref->getId();
            }
        }
        $data->setPreferredAreas($preferences);
        $data->setPreferredShifts($forms['preferredShifts']->getData());
        $data->setExperience($forms['experience']->getData());
        $data->setComments($forms['comments']->getData());

        foreach ($forms['construction']->getData() as $id => $entry) {
            $this->mapShift($id, $entry, $data);
        }

        foreach ($forms['dismantling']->getData() as $id => $entry) {
            $this->mapShift($id, $entry, $data);
        }
    }

    /**
     * @param array{'date': bool, 'start': DateTimeImmutable, 'end': DateTimeImmutable} $entry
     */
    private function mapShift(int $id, array $entry, Volunteer $data): void
    {
        /** @var ShiftVolunteer|false $sv */
        $sv = $data->getShifts()
            ->filter(fn(ShiftVolunteer $shift) => $shift->getShift()->getId() == $id)
            ->first();
        /** @var Shift $shift */
        $shift = $this->doctrine->find(Shift::class, $id);
        if ($shift === null)
            throw new TransformationFailedException("Cannot find any shift with id " . $id);

        if (isset($entry['date']) && $entry['date'] === true) {
            $start = $entry['start']
                ->setDate(...explode('-', $shift->getStart()->format('Y-m-d')));
            $end = $entry['end']
                ->setDate(...explode('-', $shift->getStart()->format('Y-m-d')));
            // check if this shift already exists
            if ($sv !== false) {      // update times
                $sv->setStart($start);
                $sv->setEnd($end);
            } else {                  // add new
                $sv = new ShiftVolunteer();
                $sv->setVolunteer($data);
                $sv->setShift($shift);
                $sv->setStart($start);
                $sv->setEnd($end);

                $data->addShift($sv);
            }
        } else {
            if ($sv)
                $data->removeShift($sv);
        }
    }
}
