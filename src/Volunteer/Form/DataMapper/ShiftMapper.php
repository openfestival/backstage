<?php

namespace App\Volunteer\Form\DataMapper;

use App\Volunteer\Entity\Shift;
use App\Volunteer\Entity\ShiftVolunteer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Form\FormInterface;
use Traversable;

class ShiftMapper implements DataMapperInterface {
    private EntityManagerInterface $doctrine;

    public function __construct(EntityManagerInterface $doctrine) {
        $this->doctrine = $doctrine;
    }

    /**
     * @var FormInterface[]|Traversable $forms
     */
    public function mapDataToForms($data, $forms): void
    {
        if ($data === null)
            return;

        if (!$data instanceof Shift)
            throw new UnexpectedTypeException($data, Shift::class);

        $forms = iterator_to_array($forms);

        $forms['area']->setData($data->getArea());
        $forms['name']->setData($data->getName());
        $forms['day']->setData($data->getDay());
        $forms['start']->setData($data->getStart());
        $forms['end']->setData($data->getEnd());
        $forms['volunteer']->setData($data->getVolunteers()->isEmpty() ? null : $data->getVolunteers()[0]->getVolunteer());
    }

    /**
     * @param Shift $data
     */
    public function mapFormsToData($forms, &$data): void
    {
        $forms = iterator_to_array($forms);

        $data->setName('');
        $data->setArea($forms['area']->getData());
        $data->setName($forms['name']->getData());
        $data->setDay($forms['day']->getData());
        $data->setStart($forms['start']->getData());
        $data->setEnd($forms['end']->getData());
        if ($forms['volunteer']->getData() != null) {
            if ($data->getVolunteers()->isEmpty()) {
                $sv = new ShiftVolunteer();
                $sv->setVolunteer($forms['volunteer']->getData());
                $sv->setShift($data);
                $this->doctrine->persist($sv);
                $data->setVolunteer($sv);
            } else {
                $sv = $data->getVolunteers()[0];
                $sv->setConfirmed(false);
                $sv->setVolunteer($forms['volunteer']->getData());
            }
        } else {
            foreach ($data->getVolunteers() as $sv)
                $data->removeVolunteer($sv);
        }
    }
}
