<?php

namespace App\Volunteer\Enum;

class Gender {
    const UNKNOWN = "unknown";
    const MALE = "male";
    const FEMALE = "female";
    const OTHER = "other";

    /**
     * @return array<string>
     */
    public static function getAvailableTypes(): array
    {
        return [
            self::UNKNOWN,
            self::MALE,
            self::FEMALE,
            self::OTHER
        ];
    }
}
