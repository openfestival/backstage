<?php

namespace App\Volunteer\Enum;

enum Food {
    const ALL = "all";
    const VEGETARIAN = "vegetarian";
    const VEGAN = "vegan";
    const ALLERGIES = "allergies";

    /**
     * @return array<string>
     */
    public static function getAvailableTypes() {
        return [
            self::ALL,
            self::VEGETARIAN,
            self::VEGAN,
            self::ALLERGIES
        ];
    }
}
