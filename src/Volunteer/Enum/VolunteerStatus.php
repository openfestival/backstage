<?php

namespace App\Volunteer\Enum;

enum VolunteerStatus: string {
    case PENDING = 'pending';
    case OUTDATED = 'outdated';
    case ACCEPTED = 'accepted';
    case DENIED = 'denied';

    /**
     * Compares this status with another status. Always sorts in order of PENDING, ACCEPTED, DENIED, OUTDATED
     */
    public function compareTo(VolunteerStatus $other): int {
        // return 0 if same
        if ($this == $other)
            return 0;
        // always list outdated last
        if ($this == VolunteerStatus::OUTDATED)
            return -1;
        if ($other == VolunteerStatus::OUTDATED)
            return 1;
        // always list pending first
        if ($this == VolunteerStatus::PENDING)
            return 1;
        if ($other == VolunteerStatus::PENDING)
            return -1;
        // after that list accepted before
        if ($this == VolunteerStatus::ACCEPTED)
            return 1;
        if ($other == VolunteerStatus::ACCEPTED)
            return -1;
        // should never be reached
        return -1;
    }
}
