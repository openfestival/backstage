<?php

namespace App\Volunteer\Validator\Constraints;

use DateTimeImmutable;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ShiftTimesValidator extends ConstraintValidator {

    /**
     * @param array{'date': bool, 'start': DateTimeImmutable, 'end': DateTimeImmutable} $value
     */
    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof ShiftTimes)
            throw new UnexpectedTypeException($constraint, ShiftTimes::class);
        if ($value === null)
            return;

        if (!is_array($value) || !isset($value['date']) || !isset($value['start']) || !isset($value['end'])) {
            $this->addViolation($constraint);
            return;
        }

        if ($value['date'] !== true)
            return;

        $shift = $constraint->shift;

        // start and end times are returned with the date set to '1970-01-01'. To compare them, set the date manually.
        $start = $value['start']
            ->setDate(...explode('-', $shift->getStart()->format('Y-m-d')));
        $end = $value['end']
            ->setDate(...explode('-', $shift->getEnd()->format('Y-m-d')));

        if ($start < $shift->getStart() || $end > $shift->getEnd() || $start >= $end) {
            $this->addViolation($constraint);
        }
    }

    private function addViolation(ShiftTimes $constraint): void
    {
        $this->context
            ->buildViolation($constraint->message)
            ->addViolation();
    }
}
