<?php

namespace App\Volunteer\Repository;

use App\Festival\Entity\Festival;
use App\Volunteer\Entity\Shift;
use App\Volunteer\Entity\ShiftArea;
use App\Volunteer\Entity\Volunteer;
use App\Volunteer\Enum\ShiftAreaType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Shift|null find($id, $lockMode = null, $lockVersion = null)
 * @method Shift|null findOneBy(array $criteria, array $orderBy = null)
 * @method Shift[]    findAll()
 * @method Shift[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShiftRepository extends ServiceEntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Shift::class);
    }

    public function findAllWhereAreaAndFestivalJoinVolunteer(ShiftArea $area, Festival $festival): array {
        return $this->createQueryBuilder('s')
            ->select('s', 'd', 'sv', 'v')
            ->join('s.day', 'd')
            ->leftJoin('s.volunteers', 'sv')
            ->leftJoin('sv.volunteer', 'v')
            ->where('d.festival = :festival', 's.area = :area')
            ->orderBy('s.start')
            ->setParameters([
                'festival' => $festival,
                'area' => $area
            ])
            ->getQuery()
            ->getResult();
    }

    /**
     * @param string $area name of the area
     * @param Festival $festival festival
     * @return Shift[] all shifts of the given area and the given festival
     */
    public function findAllWhereAreaNameAndFestival(string $area, Festival $festival): array {
        return $this->createQueryBuilder('s')
            ->join('s.area', 'a')
            ->join('s.day', 'd')
            ->where('d.festival = :festival', 'a.name = :area')
            ->setParameters([
                'festival' => $festival,
                'area' => $area
            ])
            ->getQuery()
            ->getResult();
    }

    /**
     * @param ShiftAreaType $type type of the area
     * @param Festival $festival festival
     * @return Shift[] all shifts of the given area and the given festival
     */
    public function findAllWhereAreaTypeAndFestival(ShiftAreaType $type, Festival $festival): array {
        return $this->createQueryBuilder('s')
            ->join('s.area', 'a')
            ->join('s.day', 'd')
            ->where('d.festival = :festival', 'a.type = :type')
            ->setParameters([
                'festival' => $festival,
                'type' => $type->value
            ])
            ->getQuery()
            ->getResult();
    }

    /**
     * @param ShiftAreaType $type type of area
     * @param Festival $festival festival
     * @param Volunteer $volunteer volunteer working the shifts
     * @return Shift[] all shifts of the given area and the given festival worked by the given volunteer
     */
    public function findAllWhereAreaTypeAndFestivalAndVolunteer(ShiftAreaType $type, Festival $festival, Volunteer $volunteer): array {
        return $this->createQueryBuilder('s')
            ->select('s')
            ->join('s.area', 'a')
            ->join('s.day', 'd')
            ->leftJoin('s.volunteers', 'v')
            ->where('d.festival = :festival', 'a.type = :type', 'v.volunteer = :volunteer')
            ->orderBy('s.start')
            ->setParameters([
                'festival' => $festival,
                'type' => $type->value,
                'volunteer' => $volunteer
            ])
            ->getQuery()
            ->getResult();
    }
}
