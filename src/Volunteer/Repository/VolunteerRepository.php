<?php

namespace App\Volunteer\Repository;

use App\Festival\Entity\Festival;
use App\Volunteer\Entity\Volunteer;
use App\Volunteer\Entity\VolunteerGroup;
use App\Volunteer\Enum\ShiftAreaType;
use App\Volunteer\Enum\VolunteerStatus;
use DateTimeImmutable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Volunteer|null find($id, $lockMode = null, $lockVersion = null)
 * @method Volunteer|null findOneBy(array $criteria, array $orderBy = null)
 * @method Volunteer[]    findAll()
 * @method Volunteer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VolunteerRepository extends ServiceEntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Volunteer::class);
    }

    /**
     * Finds all volunteers, joined with their groups and shifts. Only joins shifts with area type
     * {@link ShiftAreaType::FESTIVAL}, and for the given festival.
     *
     * @param bool $outdated if set to false, finds only volunteers with status not equal {@link VolunteerStatus::OUTDATED}
     * @param ?string $name if given, only return volunteers with the specified name
     * @param Festival $festival only return shifts from this festival
     * @return array all volunteers
     */
    public function findAllJoinGroupsJoinFestivalShiftsWhereOutdatedAndName(
        bool     $outdated,
        ?string  $name,
        Festival $festival
    ): array {
        // set up query builder and initial query
        $qb = $this->createQueryBuilder('v');
        $query = $qb->select('v', 'u', 'g', 'sv')
            ->join('v.user', 'u')
            ->leftJoin('v.groups', 'g')
            ->leftJoin('v.shifts', 'sv', Join::WITH, $qb->expr()->in(
                'sv.shift',
                // join only shifts matching the following sub query:
                // filter for area type and festival
                $this->getEntityManager()->createQueryBuilder()
                    ->select('s.id')
                    ->from('AppVolunteer:Shift', 's')
                    ->join('s.area', 'a')
                    ->join('s.day', 'd')
                    ->where('d.festival = :festival', 'a.type = :type')
                    ->getDQL()
            ));
        // if outdated is set to false, find only volunteers who are not outdated
        if (!$outdated) {
            $query->andWhere('v.status != :status')
                ->setParameter('status', VolunteerStatus::OUTDATED->value);
        }
        // search for name if not null
        if (!is_null($name)) {
            $query->andWhere($qb->expr()->like(
                $qb->expr()->concat('v.firstname', $qb->expr()->concat('\' \'', 'v.surname')),
                ':name'
            ))
                ->setParameter('name', '%' . $name . '%');
        }

        return $query
            ->setParameter('festival', $festival)
            ->setParameter('type', ShiftAreaType::FESTIVAL->value)
            ->getQuery()
            ->getResult();
    }

    /**
     * Finds the ids of all volunteers with the given status
     *
     * @param string[] $status find volunteers wich have any of these statuses
     * @return int[] user IDs of the found volunteers
     */
    public function findIdsWhereStatusIn(array $status): array {
        return $this->createQueryBuilder('v')
            ->select('v.id')
            ->where('v.status IN (:status)')
            ->setParameters([
                'status' => $status
            ])
            ->getQuery()
            ->getSingleColumnResult();
    }

    /**
     * Finds the ids of all volunteers in the given groups and with the given status
     *
     * @param int[] $groups find volunteers which belong to any of these groups
     * @param string[] $status find volunteers wich have any of these statuses
     * @return int[] user IDs of the found volunteers
     */
    public function findIdsWhereVolunteerGroupInAndStatusIn(array $groups, array $status): array {
        return $this->createQueryBuilder('v')
            ->select('v.id')
            ->distinct()
            ->leftJoin('v.groups', 'g')
            ->where('g.id IN (:groups)', 'v.status IN (:status)')
            ->setParameters([
                'groups' => $groups,
                'status' => $status
            ])
            ->getQuery()
            ->getSingleColumnResult();
    }

    /**
     * @param VolunteerStatus $status find volunteers with this status
     * @return Volunteer[] all volunteers with the given status, sorted by firstname and surname
     */
    public function findAllWhereStatusOrderedByName(VolunteerStatus $status): array {
        return $this->createQueryBuilder('v')
            ->where('v.status = :status')
            ->addOrderBy('v.firstname')
            ->addOrderBy('v.surname')
            ->setParameter('status', $status->value)
            ->getQuery()
            ->getResult();
    }

    /**
     * Finds all volunteers which have applied after the given daten and whose status does not equal "outdated".
     *
     * @param DateTimeImmutable $time
     * @return Volunteer[]
     */
    public function findAllWhereAppliedAtGreaterThanAndStatusNotOutdated(DateTimeImmutable $time): array {
        return $this->createQueryBuilder('v')
            ->where('v.appliedAt >= :time', 'v.status != :status')
            ->setParameters([
                'time' => $time,
                'status' => VolunteerStatus::OUTDATED->value
            ])
            ->getQuery()
            ->getResult();
    }

    /**
     * Sets the status of all volunteers matching one of the given statuses to a new status.
     *
     * @param VolunteerStatus[] $statuses update volunteers with a status from this list
     * @param VolunteerStatus $status new status to set
     */
    public function setStatusWhereStatusIn(array $statuses, VolunteerStatus $status): void
    {
        $this->createQueryBuilder('v')
            ->update()
            ->set('v.status', ':new')
            ->where('v.status IN (:current)')
            ->setParameters([
                'new' => $status->value,
                'current' => array_map(fn(VolunteerStatus $s) => $s->value, $statuses)
            ])
            ->getQuery()
            ->execute();
    }

    /**
     * Deletes all relations between {@link Volunteer}s and {@link VolunteerGroup}s where the status of the volunteers
     * matches one of the given statuses.
     *
     * @param array $statuses delete all groups from volunteers with a status from this list
     */
    public function deleteGroupsWhereStatusIn(array $statuses): void
    {
        $sql = 'DELETE vgv FROM volunteer_group_volunteer vgv LEFT JOIN volunteer v ON vgv.volunteer_id = v.id WHERE v.status IN (:status);';
        $query = $this->getEntityManager()->createNativeQuery($sql, new ResultSetMapping());
        $query->setParameter('status', array_map(fn(VolunteerStatus $s) => $s->value, $statuses));
        $query->execute();
    }
}
