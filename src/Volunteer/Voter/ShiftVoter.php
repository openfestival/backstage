<?php

namespace App\Volunteer\Voter;

use App\Security\Entity\User;
use App\Volunteer\Entity\Shift;
use App\Volunteer\Entity\ShiftArea;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;


/**
 * Voter for shift and shift area subjects. Returns true if the current user is the supervisor of the shift.
 */
class ShiftVoter extends Voter {
    private Security $security;

    public function __construct(Security $security) {
        $this->security = $security;
    }

    protected function supports($attribute, $subject): bool {
        return $subject instanceof Shift || $subject instanceof ShiftArea;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool {
        if (!$this->security->isGranted('IS_AUTHENTICATED_REMEMBERED'))
            return false;
        /** @var User $user */
        $user = $this->security->getUser();
        if ($user === null)
            return false;

        if ($subject instanceof Shift)
            return $subject->getArea()->getSupervisor()->getId() === $user->getId();
        if ($subject instanceof ShiftArea)
            return $subject->getSupervisor()->getId() === $user->getId();
        return false;
    }
}
