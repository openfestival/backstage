<?php

namespace App\Volunteer\Command;

use App\System\Application\ConfigService;
use App\System\Application\MailService;
use App\Volunteer\Entity\VolunteerDigest;
use App\Volunteer\Repository\VolunteerDigestRepository;
use App\Volunteer\Repository\VolunteerRepository;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class VolunteerDigestCommand extends Command {
    protected static $defaultName = 'app:volunteer-digest';

    private EntityManagerInterface $doctrine;
    private MailService $mail;
    private VolunteerDigestRepository $digestRepository;
    private VolunteerRepository $volunteerRepository;

    public function __construct(EntityManagerInterface $doctrine, VolunteerDigestRepository $digestRepository, VolunteerRepository $repository, MailService $mail) {
        $this->doctrine = $doctrine;
        $this->mail = $mail;
        $this->digestRepository = $digestRepository;
        $this->volunteerRepository = $repository;

        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Creates an email digest for all new volunteer applications.')
            ->addOption('all', 'a', InputOption::VALUE_NONE, 'Create digest for all applications')
            ->addArgument('recipient', InputArgument::REQUIRED, 'The receiver of the digest');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int {
        $recipient = $input->getArgument('recipient');
        if (1 !== preg_match("/" . ConfigService::EMAIL_REGEX . "/", $recipient)) {
            $output->writeln("Please specify a valid email address.");
            return Command::INVALID;
        }

        // find digest
        $digest = $this->digestRepository->find($recipient);
        if ($digest === null) {
            $digest = new VolunteerDigest();
            $digest->setRecipient($recipient);
            $digest->setLastMessageTime(new DateTimeImmutable('1970-01-01 00:00:00'));
            $this->doctrine->persist($digest);
        }
        if ($input->getOption('all'))
            $digest->setLastMessageTime(new DateTimeImmutable('1970-01-01 00:00:00'));

        $volunteers = $this->volunteerRepository
            ->findAllWhereAppliedAtGreaterThanAndStatusNotOutdated($digest->getLastMessageTime());
        $newVolunteers = sizeof($volunteers);

        if ($newVolunteers === 0) {
            $output->writeln("No new volunteers since last digest, nothing to do.");
            return Command::SUCCESS;
        }

        $output->writeln($newVolunteers . " new volunteers, sending digest...");
        $this->mail->sendVolunteerDigest($recipient, $volunteers);

        // update digest
        $digest->setLastMessageTime(new DateTimeImmutable());
        $this->doctrine->flush();

        return Command::SUCCESS;
    }
}
