<?php

namespace App\Volunteer\Application;

use App\Festival\Entity\Festival;
use App\Security\Entity\User;
use App\System\Application\MailService;
use App\System\Entity\EmailRecord;
use App\System\Exception\InternalServerErrorHttpException;
use App\Volunteer\Entity\Volunteer;
use App\Volunteer\Entity\VolunteerGroup;
use App\Volunteer\Enum\VolunteerStatus;
use App\Volunteer\Form\EmailForm;
use App\Volunteer\Repository\VolunteerGroupRepository;
use App\Volunteer\Repository\VolunteerRepository;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use mikehaertl\pdftk\Pdf;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class VolunteerService
{
    public function __construct(
        private readonly EntityManagerInterface $doctrine,
        private readonly Security $security,
        private readonly MailService $mailService,
        private readonly VolunteerRepository $volunteerRepository,
        private readonly VolunteerGroupRepository $groupRepository,
        #[Autowire(param: 'kernel.project_dir')]
        private readonly string $projectDir,
        #[Autowire(env: 'MAILER_FROM_STAFF')]
        private readonly string $staffEmail,
    ) {}

    /**
     * Finds all volunteers, joined with their groups and shifts during the festival.
     *
     * @param bool $findOutdated limit search to volunteers which are not outdated if set to false
     * @param ?string $findName limit search to volunteers with the given name
     * @param Festival $festival only return shifts from this festival
     * @return Volunteer[] all volunteers
     */
    public function getVolunteers(bool $findOutdated, ?string $findName, Festival $festival): array
    {
        $findName = preg_replace('/\s+/', ' ', trim($findName));
        $volunteers = $this->volunteerRepository->findAllJoinGroupsJoinFestivalShiftsWhereOutdatedAndName(
            $findOutdated,
            $findName,
            $festival,
        );

        // sort volunteers
        usort($volunteers, function(Volunteer $v, Volunteer $w) {
            // first compare by status (descending)
            $status = $v->getStatus()->compareTo($w->getStatus());
            if ($status != 0)
                return $status * -1;
            // then compare by name
            return strcasecmp($v->getFirstname() . $v->getSurname(), $w->getFirstname() . $w->getSurname());
        });
        return $volunteers;
    }

    /**
     * Finds a single volunteer.
     *
     * @param int $id id of the volunteer
     * @return Volunteer|null
     */
    public function getVolunteer(int $id): ?Volunteer
    {
        return $this->volunteerRepository->find($id);
    }

    /**
     * @return Volunteer[] all accepted volunteers, sorted by name
     */
    public function getAcceptedVolunteersOrderedByName(): array
    {
        return $this->volunteerRepository->findAllWhereStatusOrderedByName(VolunteerStatus::ACCEPTED);
    }

    /**
     * Finds all volunteer groups.
     *
     * @return VolunteerGroup[] all volunteer groups
     */
    public function getVolunteerGroups(): array
    {
        return $this->groupRepository->findAllJoinVolunteers();
    }

    /**
     * Saves a volunteer to the database. Creates a new user account for the volunteer if necessary. Updates the users
     * name field to the firstname and lastname of the volunteer if it was not set with another value yet.
     *
     * @param Volunteer $volunteer The volunteer to save
     * @param User $user The user account of the volunteer
     * @param string $email The email address of the volunteer (used to create a new account)
     */
    public function saveVolunteer(Volunteer $volunteer, User $user): void
    {
        $volunteer->setAppliedAt(new DateTimeImmutable());
        if ($volunteer->getUser() === null)
            $volunteer->setUser($user);
        // set name is user account if it was not set yet
        if (empty($user->getName()))
            $user->setName($volunteer->getFirstname() . ' ' . $volunteer->getSurname());

        $this->doctrine->persist($volunteer);
        $this->doctrine->flush();
    }

    /**
     * Processes an email form and sends the corresponding email to the given volunteer if the form was submitted.
     *
     * @param FormInterface $form the email form. Has to be of type {@link EmailForm}.
     * @param Volunteer $volunteer the volunteer to send the mail to
     * @param VolunteerStatus|null $status sets the volunteer status if given
     */
    public function handleEmailForm(FormInterface $form, Volunteer $volunteer, ?VolunteerStatus $status = null): void
    {
        if (!$form->getConfig()->getType()->getInnerType() instanceof EmailForm)
            throw new InternalServerErrorHttpException(sprintf('Invalid form type %s, expected %s.',
                $form->getConfig()->getType()->getInnerType()::class, EmailForm::class));

        if (!$form->isSubmitted() || !$form->isValid())
            return;

        if (!$this->security->isGranted('staff.edit'))
            throw new AccessDeniedHttpException();
        /** @var User $user */
        $user = $this->security->getUser();

        if ($status !== null)
            $volunteer->setStatus($status);

        $record = (new EmailRecord())
            ->setSender($user)
            ->setFromEmail($this->staffEmail)
            ->setReceiver($volunteer->getUser())
            ->setToEmail($volunteer->getUser()->getEmail())
            ->setSentAt(new DateTimeImmutable())
            ->setSubject($form->get('subject')->getData())
            ->setText($form->get('text')->getData());
        $this->doctrine->persist($record);
        $this->doctrine->flush();

        $this->mailService->sendStaffEmail($record);
    }

    /**
     * Resets all volunteers to the {@link VolunteerStatus::OUTDATED} status that match one of the given statuses.
     * Also deletes associated groups for these volunteers.
     *
     * @param VolunteerStatus[] $statuses reset all volunteers matching a status from this list
     */
    public function resetStatus(array $statuses): void
    {
        $this->volunteerRepository->deleteGroupsWhereStatusIn($statuses);
        $this->volunteerRepository->setStatusWhereStatusIn($statuses, VolunteerStatus::OUTDATED);
        $this->doctrine->flush();
    }

    /**
     * Updates the status of the given volunteer
     *
     * @param Volunteer $volunteer volunteer to update
     * @param VolunteerStatus $status status to set
     */
    public function updateStatus(Volunteer $volunteer, VolunteerStatus $status): void
    {
        $volunteer->setStatus($status);
        $this->doctrine->flush();
    }

    /**
     * Updates the internal notes of the given volunteer
     *
     * @param Volunteer $volunteer volunteer to update
     * @param ?string $notes note to save
     */
    public function updateNotes(Volunteer $volunteer, ?string $notes): void
    {
        $volunteer->setNotes($notes);
        $this->doctrine->flush();
    }

    public function getMemberFormPdf(Volunteer $volunteer): Pdf
    {
        return (new Pdf($this->projectDir . '/assets/pdf/member_form.pdf'))
            ->fillForm([
                'member_active' => 2,
                'member_fee' => '24,00',
                'member_name' => $volunteer->getFirstname() . ' ' . $volunteer->getSurname(),
                'member_address' => $volunteer->getStreet(),
                'member_city' => $volunteer->getZip() . ' ' . $volunteer->getCity(),
                'member_email' => $volunteer->getUser()->getEmail(),
                'member_phone' => $volunteer->getPhone(),
                'member_birthday' => $volunteer->getBirthday()->format('d.m.Y'),
            ])
            ->needAppearances();
    }
}
