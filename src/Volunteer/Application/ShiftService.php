<?php

namespace App\Volunteer\Application;

use App\Festival\Entity\Festival;
use App\Festival\Entity\FestivalDay;
use App\System\Application\ConfigService;
use App\Volunteer\Entity\Shift;
use App\Volunteer\Entity\ShiftArea;
use App\Volunteer\Entity\Volunteer;
use App\Volunteer\Enum\ShiftAreaType;
use App\Volunteer\Repository\ShiftAreaRepository;
use App\Volunteer\Repository\ShiftRepository;
use DateInterval;
use DateTimeImmutable;

class ShiftService {
    private ConfigService $configService;
    private ShiftRepository $shiftRepository;
    private ShiftAreaRepository $areaRepository;

    public function __construct(
        ConfigService       $configService,
        ShiftAreaRepository $areaRepository,
        ShiftRepository     $shiftRepository
    ) {
        $this->configService = $configService;

        $this->shiftRepository = $shiftRepository;
        $this->areaRepository = $areaRepository;
    }

    /**
     * Finds all preferable shift areas sorted by name.
     *
     * @return ShiftArea[] the shift areas sorted by name
     */
    public function getPreferableShiftAreas(): array {
        return $this->areaRepository->findBy(['preferable' => true], ['name' => 'ASC']);
    }

    /**
     * @return Shift[] all construction shifts of the current festival
     */
    public function getConstructionShifts(Festival $festival): array {
        return $this->shiftRepository->findAllWhereAreaTypeAndFestival(ShiftAreaType::CONSTRUCTION, $festival);
    }

    /**
     * @return Shift[] all dismantling shifts of the current festival
     */
    public function getDismantlingShifts(Festival $festival): array {
        return $this->shiftRepository->findAllWhereAreaTypeAndFestival(ShiftAreaType::DISMANTLING, $festival);
    }

    /**
     * Finds all shifts for a given festival and area, sorted by day
     *
     * @param Festival $festival the festival to search for
     * @param ShiftArea $area the area to search for
     * @return array 2D array, containing an array of shifts for each festival day
     */
    public function getAreaShiftsByDay(Festival $festival, ShiftArea $area): array {
        $shifts = $this->shiftRepository->findAllWhereAreaAndFestivalJoinVolunteer($area, $festival);
        return $this->sortShiftByDays($shifts);
    }

    /**
     * Finds all shifts during the current festival worked by a volunteer.
     *
     * @param Volunteer $volunteer volunteer working the shifts
     * @return Shift[] shifts during the festival of the given volunteer
     */
    public function getVolunteerShifts(Volunteer $volunteer): array {
        return $this->shiftRepository->findAllWhereAreaTypeAndFestivalAndVolunteer(ShiftAreaType::FESTIVAL, $this->configService->getCurrentFestival(), $volunteer);
    }

    /**
     * @return ShiftArea[] all shift areas with type {@link ShiftAreaType::FESTIVAL}, sorted by name
     */
    public function getFestivalAreas(): array {
        return $this->areaRepository->findWhereTypeOrderedByName(ShiftAreaType::FESTIVAL);
    }

    /**
     * Finds all shifts of a given festival and shift area and sorts them using the {@link ShiftService::sortShiftsInRows()} function.
     *
     * @param Festival $festival
     * @param ShiftArea $area
     * @return array one entry for each festival day, for value format see {@link ShiftService::sortShiftsInRows()}
     */
    public function getSortedShiftRows(Festival $festival, ShiftArea $area): array {
        $shifts = $this->shiftRepository->findAllWhereAreaAndFestivalJoinVolunteer($area, $festival);
        $sorted = $this->sortShiftByDays($shifts);

        // sort the shifts by start time, put in rows
        foreach ($sorted as $day => /** @var Shift[] $s */ $s) {
            if (empty($s))
                continue;

            // sort by start
            usort($s, function ($a, $b) {
                if ($a->getStart() == $b->getStart())
                    return 0;
                return $a->getStart() < $b->getStart() ? -1 : 1;
            });
            $sorted[$day] = $this->sortShiftsInRows($s, $s[0]->getDay());
        }
        return $sorted;
    }

    /**
     * Sorts shifts by days
     *
     * @param array $shifts list of Shifts
     * @return array 2D array, containing an array of shifts for each festival day
     */
    private function sortShiftByDays(array $shifts): array {
        $sorted = [];
        // order shifts by day
        foreach ($shifts as $shift) {
            $day = $shift->getDay()->getId();
            if (empty($sorted[$day]))
                $sorted[$day] = [];
            $sorted[$day][] = $shift;
        }
        return $sorted;
    }

    /**
     * Sorts shifts in rows to display in a css grid
     *
     * @param Shift[] $shifts the shifts to sort into rows. Need to be sorted by starting time.
     * @param FestivalDay $day the festival day to use for meta information (date formatting)
     * @return array{meta: array{step: int, size: int, start: DateTimeImmutable, end: DateTimeImmutable, day: string}, rows: Shift[]}
     */
    public function sortShiftsInRows(array $shifts, FestivalDay $day): array {
        // sort the shifts by start time, put in rows
        // put shifts in rows
        $rows = [];
        // create first row
        $rows[] = [$shifts[0]];
        for ($i = 1; $i < sizeof($shifts); $i++) {
            $curr = $shifts[$i];
            $row = 0;
            do {
                $found = false;
                if ($row == sizeof($rows)) {    // if row does not exist, create new row
                    $rows[] = [$curr];
                    $found = true;
                } else {    // else search for space in row
                    $prev = $rows[$row][sizeof($rows[$row]) - 1];
                    if ($curr->getStart() >= $prev->getEnd()) {     // if this starts after the last shift in the row, append it
                        $rows[$row][] = $curr;
                        $found = true;
                    } else {    // else check next row
                        $row++;
                    }
                }
            } while (!$found);
        }

        // starting time, set to the full hour before first shift start
        /** @var DateTimeImmutable $start */
        $start = $shifts[0]->getStart();
        if ($start->format('i') != 0)
            $start = $start->sub(new DateInterval('PT' . $start->format('i') . 'M'));

        // ending time, set to the full hour after last shift end
        /** @var DateTimeImmutable $end */
        $end = array_reduce($shifts, function ($end, $shift) {
            if ($shift->getEnd() > $end)
                return $shift->getEnd();
            else return $end;
        }, $shifts[0]->getEnd());
        if ($end->format('i') != 0)
            $end = $end->add(new DateInterval('PT' . (60 - $start->format('i')) . 'M'));

        // gdc of shift minutes
        $gcd = $this->gcd_shifts($shifts);

        return [
            'meta' => [
                'step' => $gcd,                 // minutes per grid cell
                'size' => 60 / $gcd,            // grid cells per hour
                'start' => $start,
                'end' => $end,
                'day' => $day->getDate()->format('l, d.m.')
            ],
            'rows' => $rows
        ];
    }

    /**
     * Calculates the GCD of the minutes of the start and end times of all shifts, including 60, to fit them into hour slots
     *
     * @param Shift[] $shifts
     * @return int
     */
    private function gcd_shifts(array $shifts): int {
        // map shifts to start and end minutes, merge that, remove duplicates
        $a = array_unique(array_merge(...array_map(function (/** @var $shift Shift */ $shift) {
            return [(int)$shift->getStart()->format('i'), (int)$shift->getEnd()->format('i')];
        }, $shifts)));
        // calculate gcd for all values, use 60 as initial
        return array_reduce($a, [$this, 'gcd'], 60);
    }

    private function gcd(int $a, int $b): int {
        if ($b === 0)
            return $a;
        else return $this->gcd($b, $a % $b);
    }
}
