<?php

namespace App\Volunteer\Controller;

use App\LegacyApiController;
use App\Security\Entity\User;
use App\Security\Repository\UserRepository;
use App\System\Application\MailService;
use App\System\Entity\EmailRecord;
use App\Volunteer\Repository\VolunteerRepository;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\TooManyRequestsHttpException;
use Symfony\Component\RateLimiter\RateLimiterFactory;
use Symfony\Component\Routing\Attribute\Route;

#[Route(path: '/api/volunteer')]
class VolunteerApiController extends LegacyApiController {
    private EntityManagerInterface $doctrine;
    private UserRepository $userRepository;
    private VolunteerRepository $volunteerRepository;

    public function __construct(EntityManagerInterface $doctrine, UserRepository $userRepository, VolunteerRepository $volunteerRepository) {
        $this->doctrine = $doctrine;
        $this->userRepository = $userRepository;
        $this->volunteerRepository = $volunteerRepository;
    }

    #[Route(path: '/exists', methods: ['GET'])]
    public function getExists(Request $request, RateLimiterFactory $volunteerApiLimiter): Response {
        // check for rate limit to prevent scrapers checking lots of emails
        $limiter = $volunteerApiLimiter->create($request->getClientIp());
        if (false === $limiter->consume()->isAccepted()) {
            throw new TooManyRequestsHttpException();
        }

        if (!$request->query->has('email'))
            return $this->responseBadRequest();

        $user = $this->userRepository->findOneBy(['email' => $request->query->get('email')]);

        return $this->responseOk(!is_null($user));
    }

    #[Route(path: '/ids', methods: ['GET'])]
    public function getIds(Request $request): Response {
        if (!$this->isGranted('api.volunteeremails'))
            return $this->responseForbidden();

        /** @var int[] $groups */
        $groups = $request->query->all('group');
        /** @var string[] $status */
        $status = $request->query->all('status');
        if (empty($groups)) {
            $volunteers = $this->volunteerRepository->findIdsWhereStatusIn($status);
        } else {
            $volunteers = $this->volunteerRepository->findIdsWhereVolunteerGroupInAndStatusIn($groups, $status);
        }

        return $this->responseOk($volunteers);
    }

    #[Route(path: '/sendmail', methods: ['POST'])]
    public function postSendmail(Request $request, MailService $mail): Response {
        if (!$this->isGranted('api.mailuser'))
            return $this->responseForbidden();

        $data = $this->resolveBody($request, [
            'to' => 'int',
            'subject' => 'string',
            'body' => 'string'
        ]);
        if ($data === false)
            return $this->responseBadRequest();

        $volunteer = $this->volunteerRepository->find($data->to);
        if ($volunteer === null)
            return $this->responseBadRequest();

        /** @var User $sender */
        $sender = $this->getUser();

        $record = (new EmailRecord())
            ->setFromEmail($sender->getEmail())
            ->setSender($sender)
            ->setToEmail($volunteer->getUser()->getEmail())
            ->setReceiver($volunteer->getUser())
            ->setSentAt(new DateTimeImmutable())
            ->setSubject($data->subject)
            ->setText($data->body);

        // send mail first
        $mail->sendStaffEmail($record);
        // save record if mail was sent successfully
        $this->doctrine->persist($record);
        $this->doctrine->flush();

        return $this->responseOk();
    }
}
