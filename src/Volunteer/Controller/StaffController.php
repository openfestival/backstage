<?php

namespace App\Volunteer\Controller;

use App\Festival\Repository\FestivalDayRepository;
use App\System\Application\ConfigService;
use App\System\Repository\EmailRecordRepository;
use App\System\Repository\TemplateRepository;
use App\Volunteer\Application\ShiftService;
use App\Volunteer\Application\VolunteerService;
use App\Volunteer\Entity\Volunteer;
use App\Volunteer\Enum\VolunteerStatus;
use App\Volunteer\Form\EmailForm;
use App\Volunteer\Form\VolunteerNoteForm;
use App\Volunteer\Form\VolunteerResetForm;
use App\Volunteer\Form\VolunteerSearchForm;
use App\Volunteer\Form\VolunteerStatusForm;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route(path: '/staff')]
class StaffController extends AbstractController {
    private ShiftService $shiftService;
    private VolunteerService $volunteerService;
    private ConfigService $configService;

    private FestivalDayRepository $dayRepository;
    private TemplateRepository $templateRepository;
    private EmailRecordRepository $emailRepository;

    public function __construct(
        ShiftService          $shiftService,
        VolunteerService      $volunteerService,
        ConfigService         $configService,
        FestivalDayRepository $dayRepository,
        TemplateRepository    $templateRepository,
        EmailRecordRepository $emailRepository
    ) {
        $this->shiftService = $shiftService;
        $this->volunteerService = $volunteerService;
        $this->configService = $configService;

        $this->dayRepository = $dayRepository;
        $this->templateRepository = $templateRepository;
        $this->emailRepository = $emailRepository;
    }

    #[Route(path: '/', name: 'app_staff')]
    public function index(Request $request): Response {
        $this->denyAccessUnlessGranted('staff.info');

        // apply reset form
        $resetForm = $this->createForm(VolunteerResetForm::class);
        $resetForm->handleRequest($request);
        if ($resetForm->isSubmitted() && $resetForm->isValid()) {
            $this->denyAccessUnlessGranted('staff.reset');
            $this->volunteerService->resetStatus($resetForm->getData()['status']);
            $this->addFlash('success', 'Anmeldungen wurden zurückgesetzt.');
        }

        // apply search form
        $searchForm = $this->createForm(VolunteerSearchForm::class);
        $searchForm->handleRequest($request);
        $findOutdated = false;
        $findName = null;
        if ($searchForm->isSubmitted() && $searchForm->isValid()) {
            $findOutdated = $searchForm->get('outdated')->getData();
            $findName = $searchForm->get('search')->getData();
        }

        // find volunteers
        $volunteers = $this->volunteerService->getVolunteers(
            $findOutdated,
            $findName,
            $this->configService->getCurrentFestival()
        );

        // find areas
        $areas = $this->shiftService->getPreferableShiftAreas();
        $areasAssoc = [];
        foreach ($areas as $area)
            $areasAssoc[$area->getId()] = $area;

        return $this->render('volunteer/staff/index.html.twig', [
            'search_form' => $searchForm->createView(),
            'reset_form' => $resetForm->createView(),
            'volunteers' => $volunteers,
            'status_list' => [
                VolunteerStatus::ACCEPTED->value => true,
                VolunteerStatus::PENDING->value => false,
                VolunteerStatus::DENIED->value => false,
                VolunteerStatus::OUTDATED->value => false
            ],
            'groups' => $this->volunteerService->getVolunteerGroups(),
            'areas' => $areasAssoc
        ]);
    }

    #[Route(path: '/{volunteer<\d+>}', name: 'app_staff_detail')]
    public function volunteer(
        Volunteer            $volunteer,
        Request              $request,
        FormFactoryInterface $formFactory,
        ConfigService        $config
    ): Response {
        $this->denyAccessUnlessGranted('staff.info');

        // handle emails and email forms
        $acceptTemplate = $this->templateRepository->find('staff.accept');
        $denyTemplate = $this->templateRepository->find('staff.deny');
        $emailTemplate = $this->templateRepository->find('staff.email');

        $acceptForm = $formFactory->createNamed('accept', EmailForm::class);
        $acceptForm->handleRequest($request);
        $denyForm = $formFactory->createNamed('deny', EmailForm::class);
        $denyForm->handleRequest($request);
        $emailForm = $formFactory->createNamed('email', EmailForm::class);
        $emailForm->handleRequest($request);

        $this->volunteerService->handleEmailForm($acceptForm, $volunteer, VolunteerStatus::ACCEPTED);
        $this->volunteerService->handleEmailForm($denyForm, $volunteer, VolunteerStatus::DENIED);
        $this->volunteerService->handleEmailForm($emailForm, $volunteer);

        // status change
        $statusForm = $this->createForm(VolunteerStatusForm::class);
        $statusForm->handleRequest($request);
        if ($statusForm->isSubmitted() && $statusForm->isValid()) {
            $this->volunteerService->updateStatus($volunteer, $statusForm->get('status')->getData());
        } else $statusForm->get('status')->setData($volunteer->getStatus());

        // handle internal notes
        $noteForm = $this->createForm(VolunteerNoteForm::class);
        $noteForm->handleRequest($request);
        if ($noteForm->isSubmitted() && $noteForm->isValid()) {
            $this->denyAccessUnlessGranted('staff.edit');
            $this->volunteerService->updateNotes($volunteer, $noteForm->get('text')->getData());
        } else $noteForm->get('text')->setData($volunteer->getNotes());

        // find remaining data
        $festival = $config->getCurrentFestival();

        return $this->render('volunteer/staff/volunteer.html.twig', [
            'volunteer' => $volunteer,
            'festival' => $config->getCurrentFestival(),
            'days' => $this->dayRepository->findAllDuringFestival($festival),
            'construction' => $this->shiftService->getConstructionShifts($festival),
            'dismantling' => $this->shiftService->getDismantlingShifts($festival),
            'groups' => $this->volunteerService->getVolunteerGroups(),
            'form_accept' => $acceptForm->createView(),
            'template_accept' => $acceptTemplate,
            'form_deny' => $denyForm->createView(),
            'template_deny' => $denyTemplate,
            'form_email' => $emailForm->createView(),
            'template_email' => $emailTemplate,
            'emailrecords' => $this->emailRepository->findAllWhereReceivedOrderBySentAtDescending($volunteer->getUser()),
            'form_status' => $statusForm->createView(),
            'form_note' => $noteForm->createView(),
            'shifts' => $this->shiftService->getVolunteerShifts($volunteer)
        ]);
    }

    #[Route(path: '/{volunteer<\d+>}/member_form', name: 'app_staff_member_form')]
    public function getMemberForm(Volunteer $volunteer): void
    {
        $this->denyAccessUnlessGranted('staff.info.personal');

        $file = $this->volunteerService->getMemberFormPdf($volunteer);
        $fileName = iconv('utf-8', 'ascii//TRANSLIT',
            $volunteer->getFirstname() . '_' . $volunteer->getSurname() . '.pdf');
        $file->send($fileName);
    }
}
