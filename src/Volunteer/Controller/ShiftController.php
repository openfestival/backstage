<?php

namespace App\Volunteer\Controller;

use App\Festival\Entity\Festival;
use App\Festival\Repository\FestivalDayRepository;
use App\Festival\Repository\FestivalRepository;
use App\Security\Entity\User;
use App\System\Application\ConfigService;
use App\Volunteer\Application\ShiftService;
use App\Volunteer\Application\VolunteerService;
use App\Volunteer\Entity\Shift;
use App\Volunteer\Entity\ShiftArea;
use App\Volunteer\Enum\ShiftAreaType;
use App\Volunteer\Form\ShiftForm;
use App\Volunteer\Repository\ShiftAreaRepository;
use App\Volunteer\Repository\ShiftRepository;
use App\Volunteer\Repository\ShiftVolunteerRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Knp\Snappy\Pdf;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route(path: '/shift')]
class ShiftController extends AbstractController {
    private EntityManagerInterface $doctrine;
    private ShiftService $shiftService;
    private VolunteerService $volunteerService;

    private FestivalRepository $festivalRepository;
    private FestivalDayRepository $festivalDayRepository;
    private ShiftAreaRepository $areaRepository;
    private ShiftRepository $shiftRepository;
    private ShiftVolunteerRepository $shiftVolunteerRepository;

    public function __construct(
        EntityManagerInterface   $doctrine,
        ShiftService             $service,
        VolunteerService         $volunteerService,
        FestivalRepository       $festivalRepository,
        FestivalDayRepository    $festivalDayRepository,
        ShiftAreaRepository      $areaRepository,
        ShiftRepository          $shiftRepository,
        ShiftVolunteerRepository $shiftVolunteerRepository,
    ) {
        $this->doctrine = $doctrine;
        $this->shiftService = $service;
        $this->volunteerService = $volunteerService;
        $this->festivalRepository = $festivalRepository;
        $this->festivalDayRepository = $festivalDayRepository;
        $this->areaRepository = $areaRepository;
        $this->shiftRepository = $shiftRepository;
        $this->shiftVolunteerRepository = $shiftVolunteerRepository;
    }

    #[Route(path: '/{festival<\d+>?}/{area<\d+>?}', name: 'app_shift')]
    public function index(Festival $festival, ?ShiftArea $area, Request $request): Response {
        $this->denyAccessUnlessGranted('shift.info');

        $areas = $this->shiftService->getFestivalAreas();
        $volunteers = $this->volunteerService->getAcceptedVolunteersOrderedByName();

        if ($area === null && !empty($areas))
            $area = $areas[0];
        if ($area === null) {
            return $this->render('volunteer/shift/index.html.twig', [
                'areas' => $areas,
                'activeArea' => null
            ]);
        }

        $newshift = new Shift();
        $form = $this->createForm(ShiftForm::class, $newshift, [
            'areas' => $areas,
            'festival' => $festival,
            'volunteers' => $volunteers,
            'current_area' => $area,
            'all_days' => true
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($this->isGranted('shift.edit', $newshift)) {
                $this->doctrine->persist($newshift);
                $this->doctrine->flush();
            } else $this->addFlash('danger', 'Du hast nicht nötigen Rechte, um diese Schicht zu bearbeiten.');
        }

        return $this->render('volunteer/shift/index.html.twig', [
            'festival' => $festival,
            'other_festivals' => $this->festivalRepository->findOtherFestivals($festival),
            'areas' => $areas,
            'active' => $area,
            'shifts' => $this->shiftService->getSortedShiftRows($festival, $area),
            'form' => $form->createView(),
            'days' => $festival->getDays(),
            'volunteers' => $volunteers
        ]);
    }

    #[Route(path: '/print/{festival<\d+>}/{area<\d+>}', name: 'app_shift_print')]
    public function print(Festival $festival, ShiftArea $area, Request $request, Pdf $pdf): Response {
        $this->denyAccessUnlessGranted('shift.info');

        $shifts = $this->shiftService->getAreaShiftsByDay($festival, $area);
        $days = $this->festivalDayRepository->findAllDuringFestival($festival);

        // to speed up debugging, render the page in the browser if view parameter is set
        if ($request->query->has('view')) {
            return $this->render('volunteer/shift/print.html.twig', [
                'area' => $area,
                'days' => $days,
                'shifts' => $shifts
            ]);
        }

        // otherwise create a PDF file
        return new PdfResponse(
            $pdf->getOutputFromHtml($this->renderView('volunteer/shift/print.html.twig', [
                'area' => $area,
                'days' => $days,
                'shifts' => $shifts
            ])),
            iconv('utf-8', 'ascii//TRANSLIT', $area->getName()) . '.pdf'
        );
    }

    #[Route(path: '/edit/{shift<\d+>}', name: 'app_shift_edit')]
    public function edit(Shift $shift, Request $request): RedirectResponse|Response {
        $this->denyAccessUnlessGranted('shift.edit', $shift);

        $form = $this->createForm(ShiftForm::class, $shift, [
            'areas' => $this->shiftService->getFestivalAreas(),
            'festival' => $shift->getDay()->getFestival(),
            'volunteers' => $this->volunteerService->getAcceptedVolunteersOrderedByName(),
            'current_area' => $shift->getArea(),
            'all_days' => true
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->doctrine->flush();
            return $this->redirectToRoute('app_shift', [
                'festival' => $shift->getDay()->getFestival()->getId(),
                'area' => $shift->getArea()->getId()
            ]);
        }

        return $this->render('volunteer/shift/edit.html.twig', [
            'shift' => $shift,
            'form' => $form->createView(),
            'days' => $shift->getDay()->getFestival()->getDays()
        ]);
    }

    #[Route(path: '/delete/{shift<\d+>}', name: 'app_shift_delete')]
    public function delete(Shift $shift, Request $request): RedirectResponse|Response {
        $this->denyAccessUnlessGranted('shift.edit', $shift);

        $form = $this->createFormBuilder()
            ->add('submit', SubmitType::class, [
                'label' => 'delete'
            ])
            ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->doctrine->remove($shift);
            $this->doctrine->flush();
            return $this->redirectToRoute('app_shift', [
                'festival' => $shift->getDay()->getFestival()->getId(),
                'area' => $shift->getArea()->getId()
            ]);
        }

        return $this->render('volunteer/shift/delete.html.twig', [
            'shift' => $shift,
            'form' => $form->createView()
        ]);
    }

    #[Route(path: '/self/{vol<\d+>?}', name: 'app_shift_self')]
    public function self($vol, ConfigService $config): Response {
        if ($vol == null || !$this->isGranted('shift.info')) {
            /** @var User $user */
            $user = $this->getUser();
            $volunteer = $user->getVolunteer();
        } else {
            $volunteer = $this->volunteerService->getVolunteer($vol);
            if ($volunteer == null)
                throw $this->createNotFoundException();
        }
        // send empty page if volunteer is not found
        if ($volunteer == null) {
            $this->addFlash('warning', 'Du hast dich noch nicht als Helfer angemeldet.');
            return $this->render('volunteer/shift/self.html.twig', [
                'shifts' => [],
                'areas' => [],
                'volunteer' => $volunteer
            ]);
        }

        $festival = $config->getCurrentFestival();
        // find shifts during festival
        $shifts = $this->shiftVolunteerRepository
            ->findAllWhereAreaTypeAndFestivalAndVolunteer(ShiftAreaType::FESTIVAL, $festival, $volunteer);

        // TODO: move this to service
        $areas = array_reduce($shifts, function ($areas, $sv) {
            $shift = $sv->getShift();
            $area = $shift->getArea()->getId();
            $day = $shift->getDay()->getId();
            if (empty($areas[$area]))
                $areas[$area] = [];
            if (empty($areas[$area][$day])) {
                $shifts = $this->shiftRepository->findBy([
                    'day' => $shift->getDay(),
                    'area' => $shift->getArea()
                ], [
                    'start' => 'ASC'
                ]);
                $areas[$area][$day] = [
                    'area' => $shift->getArea(),
                    'day' => $shift->getDay(),
                    'shifts' => $this->shiftService->sortShiftsInRows($shifts, $shift->getDay())
                ];
            }
            return $areas;
        }, []);

        return $this->render('volunteer/shift/self.html.twig', [
            'shifts' => $shifts,
            'areas' => $areas,
            'volunteer' => $volunteer
        ]);
    }

    #[Route(path: '/construction/{festival<\d+>?}', name: 'app_shift_construction')]
    public function construction(Festival $festival): Response {
        $this->denyAccessUnlessGranted('shift.info');

        $areas = $this->areaRepository->findWhereTypeInOrderedByName([
            ShiftAreaType::CONSTRUCTION,
            ShiftAreaType::DISMANTLING
        ]);
        if (empty($areas))
            throw $this->createNotFoundException('Shift areas not found!');
        $construction = $this->areaRepository->findWhereTypeOrderedByName(ShiftAreaType::CONSTRUCTION);
        if (empty($construction))
            throw $this->createNotFoundException('No construction area found!');

        $shifts = $this->shiftService->getConstructionShifts($festival);

        return $this->returnConstruction($areas, $construction[0], $shifts);
    }

    #[Route(path: '/dismantling/{festival<\d+>?}', name: 'app_shift_dismantling')]
    public function dismantling(Festival $festival): Response {
        $this->denyAccessUnlessGranted('shift.info');

        $areas = $this->areaRepository->findWhereTypeInOrderedByName([
            ShiftAreaType::CONSTRUCTION,
            ShiftAreaType::DISMANTLING
        ]);
        if (empty($areas))
            throw $this->createNotFoundException('Shift areas not found!');
        $dismantling = $this->areaRepository->findWhereTypeOrderedByName(ShiftAreaType::DISMANTLING);
        if (empty($dismantling))
            throw $this->createNotFoundException('No construction area found!');

        $shifts = $this->shiftService->getDismantlingShifts($festival);

        return $this->returnConstruction($areas, $dismantling[0], $shifts);
    }

    /**
     * @param $areas ShiftArea[]
     * @param $area ShiftArea
     * @param $shifts Shift[]
     * @return Response
     */
    private function returnConstruction(array $areas, ShiftArea $area, array $shifts): Response {
        $days = [];
        foreach ($shifts as $shift) {
            $hours = [];
            $start = $shift->getStart()->format('H');
            $end = $shift->getEnd()->format('H');
            for ($i = (int)$start; $i <= $end; $i++)
                $hours[$i] = ['coming' => [], 'going' => []];
            $hours = array_reduce($shift->getVolunteers()->toArray(), function ($h, $sv) {
                $h[(int)$sv->getStart()->format('H')]['coming'][] = $sv;
                $h[(int)$sv->getEnd()->format('H')]['going'][] = $sv;
                return $h;
            }, $hours);
            $days[$shift->getId()] = [
                'shift' => $shift,
                'count' => $shift->getVolunteers()->count(),
                'volunteers' => $hours
            ];
        }

        return $this->render('volunteer/shift/construction.html.twig', [
            'areas' => $areas,
            'active' => $area->getId(),
            'days' => $days
        ]);
    }
}
