<?php

namespace App\Volunteer\Controller;

use App\Festival\Entity\Festival;
use App\Security\Application\AccountService;
use App\Security\Entity\User;
use App\Security\Exception\DuplicateUserException;
use App\Volunteer\Application\VolunteerService;
use App\Volunteer\Entity\Volunteer;
use App\Volunteer\Enum\VolunteerStatus;
use App\Volunteer\Form\VolunteerForm;
use DateTimeImmutable;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route(path: '/volunteer')]
class VolunteerController extends AbstractController {
    private VolunteerService $service;
    private TranslatorInterface $translator;
    private AccountService $accountService;

    public function __construct(VolunteerService $service, TranslatorInterface $translator, AccountService $accountService) {
        $this->service = $service;
        $this->translator = $translator;
        $this->accountService = $accountService;
    }

    #[Route(path: '/', name: 'app_volunteer')]
    public function index(Festival $festival, Request $request): Response {
        // Initialize user and volunteer variables.
        // This page can be displayed while not logged in, so neither have to exit yet.
        /** @var User $user */
        $user = $this->getUser();
        /** @var Volunteer $volunteer */
        $volunteer = $user?->getVolunteer();
        if ($volunteer === null) {
            $volunteer = new Volunteer();
            $volunteer->setStatus(VolunteerStatus::PENDING);
        }

        // if application timespan has not yet elapsed handle form, otherwise disable it
        $end = $festival->getVolunteerApplicationEnd();
        $now = new DateTimeImmutable();
        if ($end > $now) {
            $form = $this->createForm(VolunteerForm::class, $volunteer);
            $form->handleRequest($request);

            if ($form->isSubmitted()) {
                if ($form->isValid()) {
                    // check if we need to create an account
                    if ($user === null) {
                        $pwPlain = $form->get('password')->getData();
                        $pwConfi = $form->get('passwordConfirm')->getData();
                        if ($pwPlain === $pwConfi) {
                            try {
                                $user = $this->accountService->createAccount($form->get('email')->getData(), $pwPlain);
                                $this->addFlash('success', $this->translator->trans('register.successful'));

                                $this->service->saveVolunteer($volunteer, $user);
                                $this->addFlash('success', $this->translator->trans('volunteer.saved'));
                            } catch (DuplicateUserException $e) {
                                $this->addFlash('danger', $this->translator->trans('volunteer.account.exists'));
                            }
                        } else {
                            $this->addFlash('danger', $this->translator->trans('volunteer.error'));
                            $form->get('passwordConfirm')->addError(new FormError($this->translator->trans('password.nomatch')));
                        }
                    } else {
                        // else save volunteer
                        if ($volunteer->getStatus() == VolunteerStatus::OUTDATED)
                            $volunteer->setStatus(VolunteerStatus::PENDING);
                        $this->service->saveVolunteer($volunteer, $user);
                        $this->addFlash('success', $this->translator->trans('volunteer.saved'));
                    }
                } else {
                    $this->addFlash('danger', $this->translator->trans('volunteer.error'));
                }
            }
        } else {
            $form = $this->createForm(VolunteerForm::class, $volunteer, [
                'disabled' => true
            ]);
        }

        return $this->render('volunteer/volunteer.html.twig', [
            'form' => $form->createView(),
            'volunteer' => $volunteer,
            'end' => $end->format('d.m.Y')
        ]);
    }
}
