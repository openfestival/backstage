<?php

namespace App\Volunteer\Entity;

use App\Security\Entity\User;
use App\Volunteer\Enum\ShiftAreaType;
use App\Volunteer\Repository\ShiftAreaRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: ShiftAreaRepository::class)]
class ShiftArea {
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $name = null;

    #[ORM\Column(type: 'string', length: 255, enumType: ShiftAreaType::class)]
    #[Assert\NotBlank]
    private ?ShiftAreaType $type = null;

    #[ORM\JoinColumn(nullable: false)]
    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'supervisingAreas')]
    private ?User $supervisor = null;

    #[ORM\Column(type: 'boolean')]
    private bool $preferable = false;

    #[ORM\OneToMany(mappedBy: 'shiftArea', targetEntity: ShiftAreaPreference::class, cascade: ['persist'], orphanRemoval: true)]
    private Collection $preferences;

    #[ORM\OneToMany(mappedBy: 'area', targetEntity: Shift::class, orphanRemoval: true)]
    private Collection $shifts;

    public function __construct() {
        $this->preferences = new ArrayCollection();
        $this->shifts = new ArrayCollection();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getName(): ?string {
        return $this->name;
    }

    public function setName(string $name): self {
        $this->name = $name;

        return $this;
    }

    public function getType(): ?ShiftAreaType {
        return $this->type;
    }

    public function setType(?ShiftAreaType $type): self {
        $this->type = $type;

        return $this;
    }

    public function getSupervisor(): ?User {
        return $this->supervisor;
    }

    public function setSupervisor(?User $supervisor): self {
        $this->supervisor = $supervisor;

        return $this;
    }

    public function getPreferable(): bool {
        return $this->preferable;
    }

    public function setPreferable(bool $preferable): self {
        $this->preferable = $preferable;

        return $this;
    }

    /**
     * @return Collection|ShiftAreaPreference[]
     */
    public function getPreferences(): Collection {
        return $this->preferences;
    }

    public function setPreferences(Collection $preferences): self {
        $this->preferences = $preferences;
        return $this;
    }

    public function addPreference(ShiftAreaPreference $preference): self {
        if (!$this->preferences->contains($preference)) {
            $this->preferences[] = $preference;
            $preference->setShiftArea($this);
        }

        return $this;
    }

    public function removePreference(ShiftAreaPreference $preference): self {
        if ($this->preferences->contains($preference)) {
            $this->preferences->removeElement($preference);
            // set the owning side to null (unless already changed)
            if ($preference->getShiftArea() === $this) {
                $preference->setShiftArea(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Shift[]
     */
    public function getShifts(): Collection {
        return $this->shifts;
    }

    public function addShift(Shift $shift): self {
        if (!$this->shifts->contains($shift)) {
            $this->shifts[] = $shift;
            $shift->setArea($this);
        }

        return $this;
    }

    public function removeShift(Shift $shift): self {
        if ($this->shifts->contains($shift)) {
            $this->shifts->removeElement($shift);
            // set the owning side to null (unless already changed)
            if ($shift->getArea() === $this) {
                $shift->setArea(null);
            }
        }

        return $this;
    }
}
