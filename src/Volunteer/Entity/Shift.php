<?php

namespace App\Volunteer\Entity;

use App\Festival\Entity\FestivalDay;
use App\Volunteer\Repository\ShiftRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ShiftRepository::class)]
class Shift {
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\JoinColumn(nullable: false, columnDefinition: 'INT DEFAULT 1 NOT NULL')]
    #[ORM\ManyToOne(targetEntity: FestivalDay::class, inversedBy: 'shifts')]
    private ?FestivalDay $day = null;

    #[ORM\Column(type: 'datetime_immutable')]
    private ?DateTimeImmutable $start = null;

    #[ORM\Column(type: 'datetime_immutable')]
    private ?DateTimeImmutable $end = null;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $name = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $description = null;

    #[ORM\JoinColumn(nullable: false)]
    #[ORM\ManyToOne(targetEntity: ShiftArea::class, inversedBy: 'shifts')]
    private ?ShiftArea $area = null;

    #[ORM\OneToMany(mappedBy: 'shift', targetEntity: ShiftVolunteer::class, fetch: 'EAGER', orphanRemoval: true)]
    private Collection $volunteers;

    public function __construct() {
        $this->volunteers = new ArrayCollection();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getDay(): ?FestivalDay {
        return $this->day;
    }

    public function setDay(?FestivalDay $day): self {
        $this->day = $day;

        return $this;
    }

    public function getStart(): ?DateTimeImmutable {
        return $this->start;
    }

    public function setStart(DateTimeImmutable $start): self {
        $this->start = $start;

        return $this;
    }

    public function getEnd(): ?DateTimeImmutable {
        return $this->end;
    }

    public function setEnd(DateTimeImmutable $end): self {
        $this->end = $end;

        return $this;
    }

    public function getName(): ?string {
        return $this->name;
    }

    public function setName(string $name): self {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string {
        return $this->description;
    }

    public function setDescription(?string $description): self {
        $this->description = $description;

        return $this;
    }

    public function getArea(): ?ShiftArea {
        return $this->area;
    }

    public function setArea(?ShiftArea $area): self {
        $this->area = $area;

        return $this;
    }

    /**
     * @return Collection|ShiftVolunteer[]
     */
    public function getVolunteers(): Collection {
        return $this->volunteers;
    }

    public function addVolunteer(ShiftVolunteer $volunteer): self {
        if (!$this->volunteers->contains($volunteer)) {
            $this->volunteers[] = $volunteer;
            $volunteer->setShift($this);
        }

        return $this;
    }

    public function removeVolunteer(ShiftVolunteer $volunteer): self {
        if ($this->volunteers->contains($volunteer)) {
            $this->volunteers->removeElement($volunteer);
            // set the owning side to null (unless already changed)
            if ($volunteer->getShift() === $this) {
                $volunteer->setShift(null);
            }
        }

        return $this;
    }

    public function removeVolunteers(): self {
        foreach ($this->volunteers as $volunteer) {
            if ($volunteer->getShift() === $this)
                $volunteer->setShift(null);
        }
        $this->volunteers = new ArrayCollection();
        return $this;
    }

    public function setVolunteer(ShiftVolunteer $volunteer): self {
        foreach ($this->volunteers as $vol) {
            if ($vol !== $volunteer && $vol->getShift() === $this)
                $vol->setShift(null);
        }
        $volunteer->setShift($this);
        $this->volunteers = new ArrayCollection([$volunteer]);
        return $this;
    }

    /**
     * @param $start DateTimeImmutable start of day, has to be a full hour (no minutes or seconds)
     * @param $step int minutes per grid cell
     * @return int starting grid cell for this shift
     */
    public function getGridStart($start, $step): float|int
    {
        $minutes = ($this->getStart()->getTimestamp() - $start->getTimestamp()) / 60;
        return $minutes === 0 ? 1 : $minutes / $step + 1;
    }

    /**
     * @param $step int minutes per grid cell
     * @return int width in grid cells for this shift
     */
    public function getGridSpan($step): float|int
    {
        $minutes = ($this->getEnd()->getTimestamp() - $this->getStart()->getTimestamp()) / 60;
        return $minutes === 0 ? 0 : $minutes / $step;
    }
}
