<?php

namespace App\Volunteer\Entity;

use App\Volunteer\Repository\ShiftAreaPreferenceRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: ShiftAreaPreferenceRepository::class)]
#[ORM\Table(name: 'volunteer_preferred_areas')]
#[ORM\UniqueConstraint(fields: ['volunteer', 'preference'])]
class ShiftAreaPreference {

    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: ShiftArea::class, inversedBy: 'preferences')]
    #[ORM\JoinColumn(nullable: false)]
    private ?ShiftArea $shiftArea;

    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: Volunteer::class, inversedBy: 'preferredAreas')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Volunteer $volunteer;

    #[ORM\Column(type: 'integer')]
    #[Assert\GreaterThanOrEqual(1)]
    private ?int $preference;

    public function __construct(ShiftArea $shiftArea, Volunteer $volunteer, int $preference) {
        $this->shiftArea = $shiftArea;
        $this->volunteer = $volunteer;
        $this->preference = $preference;
    }

    public function getShiftArea(): ?ShiftArea {
        return $this->shiftArea;
    }

    public function setShiftArea(?ShiftArea $shiftArea): self {
        $this->shiftArea = $shiftArea;

        return $this;
    }

    public function getVolunteer(): ?Volunteer {
        return $this->volunteer;
    }

    public function setVolunteer(?Volunteer $volunteer): self {
        $this->volunteer = $volunteer;

        return $this;
    }

    public function getPreference(): ?int {
        return $this->preference;
    }

    public function setPreference(?int $preference): self {
        $this->preference = $preference;

        return $this;
    }
}
