<?php

namespace App\Volunteer\Entity;

use App\Volunteer\Repository\ShiftVolunteerRepository;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ShiftVolunteerRepository::class)]
class ShiftVolunteer {
    #[ORM\JoinColumn(nullable: false)]
    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: Shift::class, inversedBy: 'volunteers')]
    private ?Shift $shift = null;

    #[ORM\JoinColumn(nullable: false)]
    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: Volunteer::class, inversedBy: 'shifts')]
    private ?Volunteer $volunteer = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private ?DateTimeImmutable $start = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private ?DateTimeImmutable $end = null;

    #[ORM\Column(type: 'boolean', nullable: true)]
    private ?bool $confirmed = null;

    public function getShift(): ?Shift {
        return $this->shift;
    }

    public function setShift(?Shift $shift): self {
        $this->shift = $shift;

        return $this;
    }

    public function getVolunteer(): ?Volunteer {
        return $this->volunteer;
    }

    public function setVolunteer(?Volunteer $volunteer): self {
        $this->volunteer = $volunteer;

        return $this;
    }

    public function getStart(): ?DateTimeImmutable {
        return $this->start;
    }

    public function setStart(?DateTimeImmutable $start): self {
        $this->start = $start;

        return $this;
    }

    public function getEnd(): ?DateTimeImmutable {
        return $this->end;
    }

    public function setEnd(?DateTimeImmutable $end): self {
        $this->end = $end;

        return $this;
    }

    public function getConfirmed(): ?bool {
        return $this->confirmed;
    }

    public function setConfirmed(?bool $confirmed): self {
        $this->confirmed = $confirmed;

        return $this;
    }
}
