<?php

namespace App\Volunteer\Entity;

use App\Volunteer\Repository\VolunteerDigestRepository;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: VolunteerDigestRepository::class)]
class VolunteerDigest {
    #[ORM\Id]
    #[ORM\Column(type: 'string', length: 255)]
    private ?string $recipient;

    #[ORM\Column(type: 'datetime_immutable')]
    private ?DateTimeImmutable $lastMessageTime;

    public function getRecipient(): ?string {
        return $this->recipient;
    }

    public function setRecipient(string $recipient): self {
        $this->recipient = $recipient;

        return $this;
    }

    public function getLastMessageTime(): ?DateTimeImmutable {
        return $this->lastMessageTime;
    }

    public function setLastMessageTime(DateTimeImmutable $lastMessageTime): self {
        $this->lastMessageTime = $lastMessageTime;

        return $this;
    }
}
