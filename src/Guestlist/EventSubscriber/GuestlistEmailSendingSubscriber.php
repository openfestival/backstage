<?php
declare(strict_types=1);

namespace App\Guestlist\EventSubscriber;

use App\Guestlist\Entity\Guestlist;
use App\System\Application\MailService;
use App\System\Application\TemplateService;
use App\System\Application\TokenGeneratorService;
use App\System\Entity\EmailRecord;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Workflow\Attribute\AsEnteredListener;
use Symfony\Component\Workflow\Event\Event;

class GuestlistEmailSendingSubscriber
{
    public const CONFIRM_EMAIL_TEMPLATE_ID = 'guestlist.confirm_email';
    public const CONFIRMED_EMAIL_TEMPLATE_ID = 'guestlist.confirmed_email';
    public const ACCEPTED_EMAIL_TEMPLATE_ID = 'guestlist.accepted_email';
    public const REJECTED_EMAIL_TEMPLATE_ID = 'guestlist.rejected_email';

    public function __construct(
        private readonly TemplateService $templateService,
        private readonly MailService $mailService,
        private readonly TokenGeneratorService $tokenGenerator,
        private readonly UrlGeneratorInterface $urlGenerator,
        private readonly EntityManagerInterface $entityManager,
    ) {}

    #[AsEnteredListener(workflow: 'guestlist', place: 'email_confirmation_pending')]
    public function sendConfirmationEmailForNewGuestlistEntries(Event $event): void
    {
        /** @var Guestlist $guestlist */
        $guestlist = $event->getSubject();
#
        if ($guestlist->getToken() == null) {
            $token = $this->tokenGenerator->generateToken(32);
            $guestlist->setToken($token);
            $this->entityManager->flush();
        } else {
            $token = $guestlist->getToken();
        }
        $confirmationLink = $this->urlGenerator->generate('app_guestlist_confirm_email', [
            'token' => $token,
        ], UrlGeneratorInterface::ABSOLUTE_URL);

        $template = $this->templateService->getTemplate(self::CONFIRM_EMAIL_TEMPLATE_ID);
        if ($template === null) {
            throw new HttpException(
                statusCode: Response::HTTP_INTERNAL_SERVER_ERROR,
                message: 'Cannot find template with id ' . self::CONFIRM_EMAIL_TEMPLATE_ID,
            );
        }
        $emailRecord = (new EmailRecord())
            ->setToEmail($guestlist->getEmail())
            ->setSubject($template->getSubject())
            ->setText($template->renderText([
                'name' => $guestlist->getName(),
                'confirmationLink' => $confirmationLink,
            ]));
        $this->mailService->sendEmail($emailRecord);
    }

    #[AsEnteredListener(workflow: 'guestlist', place: 'email_confirmed')]
    public function sendNotificationEmailForConfirmedGuestlistEntries(Event $event): void {
        /** @var Guestlist $guestlist */
        $guestlist = $event->getSubject();

        $inviter = $guestlist->getInviter()?->getUser();
        if ($inviter === null) {
            return;
        }

        $template = $this->templateService->getTemplate(self::CONFIRMED_EMAIL_TEMPLATE_ID);
        if ($template === null) {
            throw new HttpException(
                statusCode: Response::HTTP_INTERNAL_SERVER_ERROR,
                message: 'Cannot find template with id ' . self::CONFIRMED_EMAIL_TEMPLATE_ID,
            );
        }

        $emailRecord = (new EmailRecord())
            ->setToEmail($inviter->getEmail())
            ->setSubject($template->getSubject())
            ->setText($template->renderText([
                'inviter' => $inviter->getName(),
                'name' => $guestlist->getName(),
                'origin' => $guestlist->getOrigin(),
            ]));
        $this->mailService->sendEmail($emailRecord);
    }

    #[AsEnteredListener(workflow: 'guestlist', place: 'accepted')]
    public function sendGuestlistAcceptedEmail(Event $event): void {
        /** @var Guestlist $guestlist */
        $guestlist = $event->getSubject();

        if ($guestlist->getEmail() == null)
            return;

        $template = $this->templateService->getTemplate(self::ACCEPTED_EMAIL_TEMPLATE_ID);
        if ($template === null) {
            throw new HttpException(
                statusCode: Response::HTTP_INTERNAL_SERVER_ERROR,
                message: 'Cannot find template with id ' . self::ACCEPTED_EMAIL_TEMPLATE_ID,
            );
        }

        $statusLink = $this->urlGenerator->generate('app_guestlist_status', [
            'token' => $guestlist->getToken(),
        ], UrlGeneratorInterface::ABSOLUTE_URL);

        $emailRecord = (new EmailRecord())
            ->setToEmail($guestlist->getEmail())
            ->setSubject($template->getSubject())
            ->setText($template->renderText([
                'name' => $guestlist->getName(),
                'statusLink' => $statusLink,
            ]));
        $this->mailService->sendEmail($emailRecord);
    }

    #[AsEnteredListener(workflow: 'guestlist', place: 'rejected')]
    public function sendGuestlistRejectedEmail(Event $event): void {
        /** @var Guestlist $guestlist */
        $guestlist = $event->getSubject();

        if ($guestlist->getEmail() == null)
            return;

        $template = $this->templateService->getTemplate(self::REJECTED_EMAIL_TEMPLATE_ID);
        if ($template === null) {
            throw new HttpException(
                statusCode: Response::HTTP_INTERNAL_SERVER_ERROR,
                message: 'Cannot find template with id ' . self::REJECTED_EMAIL_TEMPLATE_ID,
            );
        }

        $emailRecord = (new EmailRecord())
            ->setToEmail($guestlist->getEmail())
            ->setSubject($template->getSubject())
            ->setText($template->renderText([
                'name' => $guestlist->getName(),
            ]));
        $this->mailService->sendEmail($emailRecord);
    }
}
