<?php

namespace App\Guestlist\Controller;

use App\Festival\Application\FestivalService;
use App\Festival\Entity\Festival;
use App\Guestlist\Entity\Band;
use App\Guestlist\Entity\TravelParty;
use App\Guestlist\Model\BandDto;
use App\Guestlist\Model\TravelPartyDto;
use App\Guestlist\Repository\BandRepository;
use App\Guestlist\Repository\GuestlistRepository;
use App\Guestlist\Repository\TravelPartyRepository;
use App\System\Application\MercureService;
use DateTimeZone;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Attribute\Route;

#[Route(path: '/api/bandcheckin')]
class BandCheckinApiController extends AbstractController
{
    public function __construct(
        private readonly FestivalService        $festivalService,
        private readonly MercureService         $mercure,
        private readonly GuestlistRepository    $guestlistRepository,
        private readonly BandRepository         $bandRepository,
        private readonly TravelPartyRepository  $travelPartyRepository,
        private readonly EntityManagerInterface $entityManager,
    ) {}

    #[Route(path: '/bands', methods: ['GET'], format: 'json')]
    public function getBands(): Response
    {
        $this->denyAccessUnlessGranted('checkin.bands');

        $bands = $this->bandRepository->findBy([], ['name' => 'ASC']);

        return $this->json($bands);
    }

    #[Route(path: '/bands', methods: ['POST'], format: 'json')]
    public function postBand(#[MapRequestPayload] BandDto $dto): Response
    {
        $this->denyAccessUnlessGranted('checkin.bands.edit');

        $band = new Band();
        $band->setName($dto->name);
        $band->setLocation($dto->location);
        $band->setContactName($dto->contactName);
        $band->setContactEmail($dto->contactEmail);
        $band->setContactPhone($dto->contactPhone);

        $this->entityManager->persist($band);
        $this->entityManager->flush();

        return $this->json($band);
    }

    #[Route(path: '/bands/edit/{band<\d+>}', methods: ['PUT'], format: 'json')]
    public function putBand(Band $band, #[MapRequestPayload] BandDto $dto): Response
    {
        $this->denyAccessUnlessGranted('checkin.bands.edit');

        $band->setName($dto->name);
        $band->setLocation($dto->location);
        $band->setContactName($dto->contactName);
        $band->setContactEmail($dto->contactEmail);
        $band->setContactPhone($dto->contactPhone);

        $this->entityManager->flush();

        return $this->json($band);
    }

    #[Route(path: '/bands/delete/{band<\d+>}', methods: ['DELETE'], format: 'json')]
    public function deleteBand(Band $band): Response
    {
        $this->denyAccessUnlessGranted('checkin.bands.edit');

        $response = $this->json([
            'id' => $band->getId(),
            'delete' => true,
        ]);

        $this->entityManager->remove($band);
        $this->entityManager->flush();

        return $this->json($response);
    }

    #[Route(path: '/parties/{festival<\d+>}', methods: ['GET'], format: 'json')]
    public function getTravelParties(Festival $festival, Request $request): Response
    {
        $this->denyAccessUnlessGranted('checkin.info');

        $parties = $this->travelPartyRepository->findAllWhereFestival($festival);

        $this->mercure->addDiscovery($request);
        $response = $this->json($parties);
        $this->mercure->finalizeResponse($response);

        return $response;
    }

    #[Route(path: '/parties/{festival<\d+>}', methods: ['POST'], format: 'json')]
    public function postTravelParty(Festival $festival, #[MapRequestPayload] TravelPartyDto $dto): Response
    {
        $this->denyAccessUnlessGranted('checkin.edit');

        $party = $this->validateTravelPartyRequest($dto, $festival);
        $this->entityManager->persist($party);
        $this->entityManager->flush();
        $this->mercure->publish('bandcheckin/parties/' . $party->getDay()->getFestival()->getId(), $party);

        return $this->json($party);
    }

    #[Route(path: '/parties/edit/{party<\d+>}', methods: ['PUT'], format: 'json')]
    public function putTravelParty(TravelParty $party, #[MapRequestPayload] TravelPartyDto $dto): Response
    {
        $this->denyAccessUnlessGranted('checkin.edit');

        $party = $this->validateTravelPartyRequest($dto, $party->getDay()->getFestival(), $party);
        $this->entityManager->flush();
        $this->mercure->publish('bandcheckin/parties/' . $party->getDay()->getFestival()->getId(), $party);

        return $this->json($party);
    }

    #[Route(path: '/parties/delete/{party<\d+>}', methods: ['DELETE'], format: 'json')]
    public function deleteTravelParty(TravelParty $party): Response
    {
        $this->denyAccessUnlessGranted('checkin.edit');


        $response = $this->json([
            'id' => $party->getId(),
            'delete' => true,
        ]);

        $this->entityManager->remove($party);
        $this->entityManager->flush();
        $this->mercure->publish('bandcheckin/parties/' . $party->getDay()->getFestival()->getId(), $response);

        return $this->json($response);
    }

    private function validateTravelPartyRequest(TravelPartyDto $dto, Festival $festival, TravelParty $party = null): TravelParty
    {
        $band = $this->bandRepository->find($dto->bandId);
        if ($band == null)
            throw new BadRequestHttpException('Unknown band');

        $day = $this->festivalService->getFestivalDay($dto->dayId);
        if ($day == null)
            throw new BadRequestHttpException('Unknown day');

        if ($day->getFestival()->getId() !== $festival->getId())
            throw new BadRequestHttpException('Day does not belong to current festival');

        $guestlist = $this->guestlistRepository->findAllWhereIdIn($dto->guestlistIds);
        if (sizeof($guestlist) != sizeof($dto->guestlistIds))
            throw new BadRequestHttpException('Could not find all specified guestlist entries');

        if ($party == null)
            $party = new TravelParty();
        $party->setBand($band);
        $party->setDay($day);
        // Datetime is saved without timezone information, so we need to set it to the default timezone :(
        $party->setStageTime($dto->stageTime->setTimezone(new DateTimeZone(date_default_timezone_get())));
        $party->setVehicle($dto->vehicle);
        $party->setCheckedIn($dto->checkedIn);
        foreach ($party->getGuestlist() as $entry) {
            if (!in_array($entry, $guestlist))
                $party->removeGuestlist($entry);
        }
        foreach ($guestlist as $entry)
            $party->addGuestlist($entry);

        return $party;
    }

    #[Route(path: '/checkin/{party<\d+>}', methods: ['PUT'], format: 'json')]
    public function checkinParty(TravelParty $party): Response
    {
        $this->denyAccessUnlessGranted('checkin.checkin');

        $party->setCheckedIn(true);
        $this->entityManager->flush();
        $this->mercure->publish('bandcheckin/parties/' . $party->getDay()->getFestival()->getId(), $party);

        return $this->json($party);
    }
}
