<?php

namespace App\Guestlist\Controller;

use App\Festival\Entity\Festival;
use App\Festival\Repository\FestivalDayRepository;
use App\System\Application\MercureService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route(path: '/bandcheckin')]
class BandCheckinController extends AbstractController {
    private FestivalDayRepository $festivalDayRepository;

    public function __construct(FestivalDayRepository $festivalDayRepository) {
        $this->festivalDayRepository = $festivalDayRepository;
    }

    /**
     * Displays all checkins. Content gets loaded via javascript asynchronously.
     * Authorizes mercure subscriptions for live updates.
     */
    #[Route(path: '/{festival<\d+>?}', name: 'app_checkin')]
    public function index(Festival $festival, MercureService $mercure, Request $request): Response {
        $this->denyAccessUnlessGranted('checkin.info');
        $days = $this->festivalDayRepository->findAllDuringFestival($festival);

        $mercure->authorizeSubscription($request, [
            'guestlist/' . $festival->getId(),
            'bandcheckin/parties/' . $festival->getId()
        ]);

        return $this->render('guestlist/checkin.html.twig', [
            'festival' => $festival,
            'days' => $days
        ]);
    }

    #[Route(path: '/bands', name: 'app_checkin_bands')]
    public function bands(): Response {
        $this->denyAccessUnlessGranted('checkin.bands');

        return $this->render('guestlist/bands.html.twig');
    }
}
