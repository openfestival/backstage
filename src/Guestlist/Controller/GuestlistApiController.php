<?php
declare(strict_types=1);

namespace App\Guestlist\Controller;

use App\Festival\Entity\Festival;
use App\Festival\Entity\FestivalDay;
use App\Festival\Repository\FestivalDayRepository;
use App\Guestlist\Application\GuestlistService;
use App\Guestlist\Entity\Guestlist;
use App\Guestlist\Entity\GuestlistDay;
use App\Guestlist\Entity\GuestlistInviter;
use App\Guestlist\Model\GuestlistDayDto;
use App\Guestlist\Model\GuestlistDto;
use App\Guestlist\Model\GuestlistInviterDto;
use App\Guestlist\Repository\GuestlistInviterRepository;
use App\Guestlist\Repository\GuestlistRepository;
use App\Security\Repository\UserRepository;
use App\System\Application\MercureService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Workflow\Exception\NotEnabledTransitionException;
use Symfony\Component\Workflow\WorkflowInterface;

#[Route(path: '/api/v1/guestlist')]
class GuestlistApiController extends AbstractController
{
    public function __construct(
        private readonly EntityManagerInterface     $entityManager,
        private readonly GuestlistRepository        $guestlistRepository,
        private readonly FestivalDayRepository      $festivalDayRepository,
        private readonly GuestlistInviterRepository $guestlistInviterRepository,
        private readonly UserRepository             $userRepository,
        private readonly WorkflowInterface          $guestlistStateMachine,
        private readonly MercureService             $mercure,
    ) {}

    #[Route(path: '/list/{festival<\d+>}', methods: ['GET'], format: 'json')]
    public function getList(Festival $festival, Request $request): Response
    {
        $this->denyAccessUnlessGranted('guestlist.read');

        $guestlistEntries = $this->guestlistRepository->findAllWhereFestivalOrderedByName($festival);

        $this->mercure->addDiscovery($request);
        $response = $this->json($guestlistEntries);
        $this->mercure->finalizeResponse($response);
        return $response;
    }

    #[Route(path: '/add', methods: ['POST'], format: 'json')]
    public function addEntry(
        #[MapRequestPayload]
        GuestlistDto $dto,
    ): Response
    {
        $this->denyAccessUnlessGranted('guestlist.write');

        $festivalDays = $this->validateFestivalDays($dto);

        $festival = $festivalDays[0]->getFestival();

        $guestlist = new Guestlist();
        $guestlist->setName($dto->name);
        $guestlist->setType($dto->type);
        $guestlist->setOrigin($dto->origin);
        $guestlist->setComment($dto->comment);
        $guestlist->setCamping($dto->camping);
        $guestlist->setPlus($dto->plus);
        $guestlist->setFestival($festival);

        if ($dto->email) {
            $existing = $this->guestlistRepository->findBy([
                'festival' => $festival,
                'email' => $dto->email,
            ]);
            if ($existing) {
                throw new HttpException(
                    statusCode: Response::HTTP_BAD_REQUEST,
                    message: 'There already is a guestlist entry for this email address and festival.',
                );
            }
            $guestlist->setEmail($dto->email);
        }

        if ($dto->inviterId != null) {
            $inviter = $this->guestlistInviterRepository->findOneBy(['user' => $dto->inviterId]);
            if (!$inviter) {
                throw new HttpException(
                    statusCode: Response::HTTP_BAD_REQUEST,
                    message: 'Unknown user id ' . $dto->inviterId,
                );
            }
            $guestlist->setInviter($inviter);
        }

        foreach ($dto->days as $day) {
            $festivalDay = $this->festivalDayRepository->find($day->festivalDayId);
            $gestlistDay = new GuestlistDay();
            $gestlistDay->setDay($festivalDay);
            $gestlistDay->setBackstage($day->backstage);
            $guestlist->addDay($gestlistDay);
        }

        if ($dto->checkedIn != null) {
            $guestlist->setCheckedIn($dto->checkedIn);
        }
        if ($dto->backstageCheckedIn != null) {
            $guestlist->setBackstageCheckedIn($dto->backstageCheckedIn);
        }

        // initialize state
        $this->guestlistStateMachine->getMarking($guestlist);

        $this->entityManager->persist($guestlist);
        $this->entityManager->flush();

        if ($dto->requireEmailConfirmation && $guestlist->getEmail() !== null) {
            $this->guestlistStateMachine->apply($guestlist, 'require_email_confirmation');
        } else {
            $this->guestlistStateMachine->apply($guestlist, 'accept');
        }

        $this->entityManager->flush();
        $this->mercure->publish('guestlist/' . $festival->getId(), $guestlist);

        return $this->json($guestlist);
    }

    #[Route(path: '/edit/{guestlist<\d+>}', methods: ['PUT'], format: 'json')]
    public function editEntry(
        Guestlist    $guestlist,
        #[MapRequestPayload]
        GuestlistDto $dto,
    ): Response
    {
        $this->denyAccessUnlessGranted('guestlist.write');

        $festivalDays = $this->validateFestivalDays($dto);
        $festival = $festivalDays[0]->getFestival();

        if ($dto->inviterId != null) {
            $inviter = $this->guestlistInviterRepository->findOneBy(['user' => $dto->inviterId]);
            if (!$inviter) {
                throw new HttpException(
                    statusCode: Response::HTTP_BAD_REQUEST,
                    message: 'Unknown user id ' . $dto->inviterId,
                );
            }
            $guestlist->setInviter($inviter);
        }

        $guestlist->setName($dto->name);
        $guestlist->setEmail($dto->email);
        $guestlist->setType($dto->type);
        $guestlist->setOrigin($dto->origin);
        $guestlist->setComment($dto->comment);
        $guestlist->setCamping($dto->camping);
        $guestlist->setPlus($dto->plus);
        $guestlist->setFestival($festival);
        $days = [];
        foreach ($dto->days as $day) {
            $festivalDay = $this->festivalDayRepository->find($day->festivalDayId);
            $guestlistDay = new GuestlistDay();
            $guestlistDay->setGuestlist($guestlist);
            $guestlistDay->setDay($festivalDay);
            $guestlistDay->setBackstage($day->backstage);
            $days[] = $guestlistDay;
        }
        $guestlist->setDays(new ArrayCollection($days));
        if (!is_null($dto->checkedIn)) {
            $guestlist->setCheckedIn($dto->checkedIn);
        }
        if (!is_null($dto->backstageCheckedIn)) {
            $guestlist->setBackstageCheckedIn($dto->backstageCheckedIn);
        }
        $this->entityManager->flush();

        $this->mercure->publish('guestlist/' . $festival->getId(), $guestlist);

        return $this->json($guestlist);
    }

    /**
     * @return Collection<FestivalDay>
     */
    private function validateFestivalDays(GuestlistDto $dto): Collection
    {
        $festivalDays = $this->festivalDayRepository->findBy([
            'id' => array_map(
                fn(GuestlistDayDto $day) => $day->festivalDayId,
                $dto->days,
            ),
        ]);
        if (count($festivalDays) !== count($dto->days)) {
            throw new HttpException(
                statusCode: Response::HTTP_BAD_REQUEST,
                message: sprintf(
                    'One or more festival days not found: %s',
                    implode(
                        ', ',
                        array_diff(
                            array_map(fn(GuestlistDayDto $day) => $day->festivalDayId, $dto->days),
                            array_map(fn(FestivalDay $day) => $day->getId(), $festivalDays),
                        )
                    ),
                ),
            );
        }
        if (count(array_unique(array_map(fn(FestivalDay $day) => $day->getFestival()->getId(), $festivalDays))) > 1) {
            throw new HttpException(
                statusCode: Response::HTTP_BAD_REQUEST,
                message: 'Festival days do not belong to the same festival.',
            );
        }

        return new ArrayCollection($festivalDays);
    }

    #[Route(path: '/{guestlist<\d+>}/confirm', methods: ['POST'], format: 'json')]
    public function confirmEntry(Guestlist $guestlist): Response
    {
        $this->denyAccessUnlessGranted('guestlist.confirm');

        try {
            $this->guestlistStateMachine->apply($guestlist, 'confirm');
        } catch (NotEnabledTransitionException) {
            throw new HttpException(
                statusCode: Response::HTTP_BAD_REQUEST,
                message: 'Transition is not available, current state is ' . $guestlist->getState(),
            );
        }

        $this->mercure->publish('guestlist/' . $guestlist->getFestival()->getId(), $guestlist);

        return $this->json($guestlist);
    }

    #[Route(path: '/{guestlist<\d+>}/accept', methods: ['POST'], format: 'json')]
    public function acceptEntry(Guestlist $guestlist): Response
    {
        $this->denyAccessUnlessGranted('guestlist.accept');

        try {
            $this->guestlistStateMachine->apply($guestlist, 'accept');
        } catch (NotEnabledTransitionException) {
            throw new HttpException(
                statusCode: Response::HTTP_BAD_REQUEST,
                message: 'Transition is not available, current state is ' . $guestlist->getState(),
            );
        }

        $this->entityManager->flush();
        $this->mercure->publish('guestlist/' . $guestlist->getFestival()->getId(), $guestlist);

        return $this->json($guestlist);
    }

    #[Route(path: '/{guestlist<\d+>}/reject', methods: ['POST'], format: 'json')]
    public function rejectEntry(Guestlist $guestlist): Response
    {
        $this->denyAccessUnlessGranted('guestlist.accept');

        try {
            $this->guestlistStateMachine->apply($guestlist, 'reject');
        } catch (NotEnabledTransitionException) {
            throw new HttpException(
                statusCode: Response::HTTP_BAD_REQUEST,
                message: 'Transition is not available, current state is ' . $guestlist->getState(),
            );
        }

        $this->entityManager->flush();
        $this->mercure->publish('guestlist/' . $guestlist->getFestival()->getId(), $guestlist);

        return $this->json($guestlist);
    }

    #[Route(path: '/inviters/list', methods: ['GET'], format: 'json')]
    public function getInviters(): Response
    {
        $this->denyAccessUnlessGranted('guestlist.read');

        return $this->json($this->guestlistInviterRepository->findAll());
    }

    #[Route(path: '/inviters/edit', methods: ['POST'], format: 'json')]
    public function addInviter(#[MapRequestPayload] GuestlistInviterDto $dto): Response
    {
        $user = $this->userRepository->find($dto->userId);
        if (!$user) {
            throw new HttpException(
                statusCode: Response::HTTP_BAD_REQUEST,
                message: 'Unknown user ID ' . $dto->userId,
            );
        }

        $inviter = $this->guestlistInviterRepository->find($dto->userId);
        if (!$inviter) {
            $inviter = new GuestlistInviter();
            $inviter->setUser($user);
        }
        $inviter->setName($dto->name);
        $this->entityManager->persist($inviter);
        $this->entityManager->flush();

        return $this->json($inviter);
    }
}
