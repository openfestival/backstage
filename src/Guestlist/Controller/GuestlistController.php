<?php

namespace App\Guestlist\Controller;

use App\Festival\Entity\Festival;
use App\Festival\Entity\FestivalDay;
use App\Guestlist\Entity\Guestlist;
use App\Guestlist\Entity\GuestlistDay;
use App\Guestlist\Form\GuestlistRegisterForm;
use App\Guestlist\Repository\GuestlistRepository;
use App\System\Application\MercureService;
use App\System\Application\TokenGeneratorService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Workflow\WorkflowInterface;

class GuestlistController extends AbstractController
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly GuestlistRepository    $repository,
        private readonly TokenGeneratorService  $tokenGenerator,
        private readonly WorkflowInterface      $guestlistStateMachine,
        private readonly MercureService         $mercure,
    ) {}

    /**
     * Display the guestlist page. Content gets loaded via javascript asynchronously.
     * Authorizes mercure subscriptions for live updates.
     */
    #[Route(path: '/guestlist/{festival<\d+>?}', name: 'app_guestlist')]
    public function index(Festival $festival, MercureService $mercure, Request $request): Response
    {
        $this->denyAccessUnlessGranted('guestlist.info');

        $mercure->authorizeSubscription($request, [
            'guestlist/' . $festival->getId(),
        ]);

        return $this->render('guestlist/index.html.twig', [
            'festival' => $festival,
        ]);
    }

    #[Route(path: '/guestlist/register/{festival<\d+>}', name: 'app_guestlist_register')]
    public function register(Festival $festival, Request $request): Response
    {
        $form = $this->createForm(GuestlistRegisterForm::class, options: [
            'festival' => $festival,
        ]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $email = $form->get('email')->getData();
            $guestlist = $this->repository->findOneBy([
                'festival' => $festival,
                'email' => $email,
            ]);
            if ($guestlist != null) {
                $this->addFlash('danger', 'Du hast dich für dieses Jahr schon angemeldet.');
                return $this->render('guestlist/register.html.twig', [
                    'festival' => $festival,
                    'form' => $form,
                ]);
            }

            $guestlist = new Guestlist();
            $guestlist->setName($form->get('name')->getData());
            $guestlist->setEmail($email);
            $guestlist->setInviter($form->get('inviter')->getData());
            $guestlist->setType($form->get('type')->getData());
            $guestlist->setOrigin($form->get('origin')->getData());
            $guestlist->setComment($form->get('comment')->getData());
            $guestlist->setCamping($form->get('camping')->getData());
            $guestlist->setPlus($form->get('plus')->getData());
            $guestlist->setFestival($festival);
            /** @var FestivalDay $festivalDay */
            foreach ($form->get('days')->getData() as $festivalDay) {
                if ($festivalDay->getFestival()->getId() !== $festival->getId())
                    continue;
                $day = new GuestlistDay();
                $day->setGuestlist($guestlist);
                $day->setDay($festivalDay);
                $day->setBackstage(false);
                $guestlist->addDay($day);
            }
            $token = $this->tokenGenerator->generateToken(32);
            $guestlist->setToken($token);

            // initialize state
            $this->guestlistStateMachine->getMarking($guestlist);

            $this->entityManager->persist($guestlist);
            $this->entityManager->flush();

            $this->guestlistStateMachine->apply($guestlist, 'require_email_confirmation');
            $this->entityManager->flush();

            $this->mercure->publish('guestlist/' . $festival->getId(), $guestlist);
            return $this->redirectToRoute('app_guestlist_status', [
                'token' => $token,
            ]);
        }

        return $this->render('guestlist/register.html.twig', [
            'festival' => $festival,
            'form' => $form,
        ]);
    }

    #[Route(path: '/guestlist/confirm/{token}', name: 'app_guestlist_confirm_email')]
    public function confirmEmail(string $token): Response
    {
        $guestlist = $this->repository->findOneBy([
            'token' => $token,
        ]);
        if (!$guestlist) {
            throw new HttpException(
                statusCode: Response::HTTP_NOT_FOUND,
                message: 'Unknown confirmation token ' . $token,
            );
        }

        if (!$this->guestlistStateMachine->can($guestlist, 'confirm_email')) {
            return $this->redirectToRoute('app_guestlist_status', [
                'token' => $token,
            ]);
        }

        $this->guestlistStateMachine->apply($guestlist, 'confirm_email');
        $this->entityManager->flush();

        $this->mercure->publish('guestlist/' . $guestlist->getFestival()->getId(), $guestlist);

        return $this->render('guestlist/status.html.twig', [
            'guestlist' => $guestlist,
        ]);
    }

    #[Route(path: '/guestlist/status/{token}', name: 'app_guestlist_status')]
    public function getStatus(string $token): Response
    {
        $guestlist = $this->repository->findOneBy([
            'token' => $token,
        ]);

        if (!$guestlist) {
            throw new HttpException(
                statusCode: Response::HTTP_NOT_FOUND,
                message: 'Unknown confirmation token ' . $token,
            );
        }

        return $this->render('guestlist/status.html.twig', [
            'guestlist' => $guestlist,
        ]);
    }
}
