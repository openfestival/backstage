<?php

namespace App\Guestlist\Controller;

use App\LegacyApiController;
use App\Festival\Application\FestivalService;
use App\Festival\Entity\Festival;
use App\Guestlist\Application\GuestlistService;
use App\Guestlist\Entity\Guestlist;
use App\Guestlist\Entity\GuestlistDay;
use App\System\Application\MercureService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Attribute\Route;

/**
 * Handles all API requests for the guestlist. As almost everything is done
 * through javascript this controller does most of the work.
 * @deprecated use {@link GuestlistApiController}
 */
#[Route(path: '/api/guestlist')]
class GuestlistLegacyApiController extends LegacyApiController {
    public function __construct(
        private readonly GuestlistService $guestlistService,
    ) {}

    /**
     * Deletes an existing guestlist entry.
     *
     * @param Guestlist $guestlist id of the guestlist entry
     */
    #[Route(path: '/delete/{guestlist<\d+>}', methods: ['DELETE'])]
    public function deleteGuestlist(Guestlist $guestlist): Response {
        $this->denyAccessUnlessGranted('guestlist.edit');

        $this->guestlistService->deleteGuestlist($guestlist);
        return $this->responseOk();
    }

    /**
     * Checks in a guestlist entry. Only valid if the entry is not already checked in.
     *
     * @param Guestlist $guestlist id of the guestlist entry
     */
    #[Route(path: '/checkin/{guestlist<\d+>}', methods: ['PUT'])]
    public function checkinGuestlist(Guestlist $guestlist): Response {
        $this->denyAccessUnlessGranted('guestlist.checkin');

        if ($guestlist->getCheckedIn())
            return $this->responseBadRequest('Already checked in');

        $this->guestlistService->checkin($guestlist);
        return $this->responseOk();
    }

    /**
     * Checks in a guestlist entry (backstage). Only valid if the entry is not already checked in (backstage).
     *
     * @param Guestlist $guestlist id of the guestlist entry
     */
    #[Route(path: '/checkin/backstage/{guestlist<\d+>}', methods: ['PUT'])]
    public function checkinBackstage(Guestlist $guestlist): Response {
        $this->denyAccessUnlessGranted('guestlist.backstage');

        if ($guestlist->getBackstageCheckedIn())
            return $this->responseBadRequest('Already checked in');

        $this->guestlistService->checkinBackstage($guestlist);
        return $this->responseOk();
    }
}
