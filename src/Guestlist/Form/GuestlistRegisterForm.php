<?php

namespace App\Guestlist\Form;

use App\Festival\Entity\Festival;
use App\Festival\Entity\FestivalDay;
use App\Guestlist\Entity\GuestlistInviter;
use App\Guestlist\Entity\GuestlistType;
use App\Guestlist\Repository\GuestlistInviterRepository;
use Doctrine\ORM\EntityRepository;
use IntlDateFormatter;
use Locale;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class GuestlistRegisterForm extends AbstractType
{

    public function __construct(
        private readonly GuestlistInviterRepository $guestlistInviterRepository,
        #[Autowire(env: 'PRIVACY_URL')]
        private readonly string $privacyUrl,
    ) {}

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $formatter = new IntlDateFormatter(Locale::getDefault(), IntlDateFormatter::FULL, IntlDateFormatter::NONE);
        /** @var Festival $festival */
        $festival = $options['festival'];
        $builder
            ->add('name', TextType::class, [
                'label' => 'guestlist.register.name',
                'constraints' => new NotBlank(),
                'required' => true,
            ])
            ->add('email', EmailType::class, [
                'label' => 'guestlist.register.email',
                'required' => true,
            ])
            ->add('type', EnumType::class, [
                'class' => GuestlistType::class,
                'label' => 'guestlist.register.type',
                'choice_label' => fn(GuestlistType $type) => 'guestlist.register.types.' . $type->value,
                'required' => true,
            ])
            ->add('origin', TextType::class, [
                'label' => 'guestlist.register.origin',
                'required' => true,
            ])
            ->add('days', EntityType::class, [
                'class' => FestivalDay::class,
                'query_builder' => function (EntityRepository $er) use ($festival) {
                    return $er->createQueryBuilder('d')
                        ->where('d.festival = :festival', 'd.date >= :start', 'd.date <= :end')
                        ->setParameters([
                            'festival' => $festival->getId(),
                            'start' => $festival->getStart(),
                            'end' => $festival->getEnd()
                        ])
                        ->orderBy('d.date', 'ASC');
                },
                'choice_label' => fn(FestivalDay $day) => $formatter->format($day->getDate()),
                'multiple' => true,
                'expanded' => true,
                'required' => true,
                'label' => 'guestlist.register.days'
            ])
            ->add('camping', CheckboxType::class, [
                'label' => 'guestlist.register.camping',
                'required' => false,
            ])
            ->add('plus', ChoiceType::class, [
                'label' => 'guestlist.register.plus.label',
                'required' => true,
                'choices' => [0, 1, 2, 3],
                'choice_label' => fn(int $plus) => 'guestlist.register.plus.' . $plus,
            ])
            ->add('inviter', EntityType::class, [
                'label' => 'guestlist.register.inviter',
                'class' => GuestlistInviter::class,
                'choices' => $this->guestlistInviterRepository->findAll(),
                'choice_label' => 'name',
                'required' => true,
            ])
            ->add('comment', TextareaType::class, [
                'label' => 'guestlist.register.comment',
                'required' => false,
            ])
            ->add('privacy', CheckboxType::class, [
                'label' => 'guestlist.register.privacy',
                'label_html' => true,
                'label_translation_parameters' => [
                    '%privacy_url%' => $this->privacyUrl,
                ],
                'required' => true,
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'guestlist.register.submit',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired('festival');
        $resolver->setAllowedTypes('festival', Festival::class);
    }
}