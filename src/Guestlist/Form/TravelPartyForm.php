<?php

namespace App\Guestlist\Form;

use App\Festival\Entity\FestivalDay;
use App\Festival\Repository\FestivalDayRepository;
use App\Guestlist\Entity\Band;
use App\Guestlist\Entity\TravelParty;
use App\System\Application\ConfigService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TravelPartyForm extends AbstractType {
    private EntityManagerInterface $doctrine;
    private ConfigService $config;

    /**
     * TravelPartyForm constructor.
     */
    public function __construct(EntityManagerInterface $doctrine, ConfigService $config) {
        $this->doctrine = $doctrine;
        $this->config = $config;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var FestivalDayRepository $dayRepo */
        $dayRepo = $this->doctrine->getRepository(FestivalDay::class);
        $days = $dayRepo->findAllDuringFestival($this->config->getCurrentFestival());

        $builder
            ->add('band', EntityType::class, [
                'class' => Band::class,
                'choice_label' => 'name'
            ])
            ->add('vehicle', TextType::class, [
                'required' => false
            ])
            ->add('day', ChoiceType::class, [
                'choices' => $days,
                'choice_label' => function (FestivalDay $day) {
                    return strftime('%A, %d.%m.', $day->getDate()->getTimestamp());
                },
                'choice_value' => function (FestivalDay $day = null) {
                    return $day ? $day->getId() : '';
                },
                'choice_translation_domain' => false,
                'required' => true,
                'label' => 'day'
            ])
            ->add('startTime', TimeType::class)
            ->add('members', CollectionType::class, [
                'entry_type' => BandMemberType::class,
                'entry_options' => [
                    'empty_data' => null
                ],
                'allow_add' => true,
                'allow_delete' => true,
                'delete_empty' => true,
                'by_reference' => false
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'save'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => TravelParty::class
        ]);
    }

}
