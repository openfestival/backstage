<?php

namespace App\Guestlist\Form\DataMapper;

use App\Guestlist\Entity\BandMember;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;

class BandMemberMapper implements DataMapperInterface {
    private EntityManagerInterface $doctrine;

    /**
     * BandMemberMapper constructor.
     */
    public function __construct(EntityManagerInterface $doctrine) {
        $this->doctrine = $doctrine;
    }

    public function mapDataToForms($data, $forms): void
    {
        if ($data === null)
            return;

        if (!$data instanceof BandMember)
            throw new UnexpectedTypeException($data, BandMember::class);

        $forms = iterator_to_array($forms);

        $forms['name']->setData($data->getName());
        $forms['comment']->setData($data->getComment());
    }

    /**
     * @param BandMember $data
     */
    public function mapFormsToData($forms, &$data): void
    {
        $forms = iterator_to_array($forms);

        if ($forms['name']->getData() != null) {
            if ($data == null) {
                $data = new BandMember();
                $this->doctrine->persist($data);
            }
            $data->setName($forms['name']->getData());
            $data->setComment($forms['comment']->getData());
            $data->setArrived($forms['arrived']->getData());
        } else if ($data != null) {
            $this->doctrine->remove($data);
        }
    }
}
