<?php

namespace App\Guestlist\Form;

use App\Guestlist\Entity\BandMember;
use App\Guestlist\Form\DataMapper\BandMemberMapper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BandMemberType extends AbstractType
{
    private EntityManagerInterface $doctrine;

    public function __construct(EntityManagerInterface $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class)
            ->add('comment', TextType::class)
            ->add('arrived', CheckboxType::class)
            ->setDataMapper(new BandMemberMapper($this->doctrine));
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => BandMember::class,
            'empty_data' => new BandMember(),
        ]);
    }
}
