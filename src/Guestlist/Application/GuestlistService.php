<?php

namespace App\Guestlist\Application;

use App\Guestlist\Entity\Guestlist;
use App\System\Application\MercureService;
use Doctrine\ORM\EntityManagerInterface;

class GuestlistService {
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly MercureService         $mercure,
    ) {}

    /**
     * Deletes an existing guestlist entry from the database. Also publishes an update via mercure.
     *
     * @param Guestlist $guestlist the entry to delete
     */
    public function deleteGuestlist(Guestlist $guestlist): void {
        $dto = [
            'id' => $guestlist->getId(),
            'delete' => true
        ];
        $this->entityManager->remove($guestlist);
        $this->entityManager->flush();

        $this->mercure->publish('guestlist/' . $guestlist->getFestival()->getId(), $dto);
    }

    /**
     * Sets the checkedIn property of an entry to true. Also publishes an update via mercure.
     *
     * @param Guestlist $guestlist the entry to check in
     */
    public function checkin(Guestlist $guestlist): void {
        $guestlist->setCheckedIn(true);
        $this->entityManager->flush();

        $this->mercure->publish('guestlist/' . $guestlist->getFestival()->getId(), $guestlist);
    }

    /**
     * Sets the checkedIn and backstageCheckedIn property of an entry to true. Also publishes an update via mercure.
     *
     * @param Guestlist $guestlist the entry to check in
     */
    public function checkinBackstage(Guestlist $guestlist): void {
        $guestlist->setCheckedIn(true);
        $guestlist->setBackstageCheckedIn(true);
        $this->entityManager->flush();

        $this->mercure->publish('guestlist/' . $guestlist->getFestival()->getId(), $guestlist);
    }
}
