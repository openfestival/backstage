<?php
declare(strict_types=1);

namespace App\Guestlist\Entity;

use App\Guestlist\Repository\GuestlistRepository;
use App\Security\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: GuestlistRepository::class)]
class GuestlistInviter implements JsonSerializable
{
    #[ORM\Id]
    #[ORM\JoinColumn(nullable: false)]
    #[ORM\OneToOne(inversedBy: 'guestlistInviter', targetEntity: User::class, cascade: ['remove'])]
    private ?User $user = null;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank]
    private ?string $name = null;

    #[ORM\OneToMany(mappedBy: 'inviter', targetEntity: Guestlist::class)]
    private Collection $guestlistEntries;

    public function __construct() {
        $this->guestlistEntries = new ArrayCollection();
    }

    public function getUser(): ?User {
        return $this->user;
    }

    public function setUser(User $user): self {
        $this->user = $user;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Guestlist[]
     */
    public function getGuestlistEntries(): Collection {
        return $this->guestlistEntries;
    }

    public function addGuestlistEntry(Guestlist $party): self {
        if (!$this->guestlistEntries->contains($party)) {
            $this->guestlistEntries[] = $party;
            $party->setInviter($this);
        }

        return $this;
    }

    public function removeGuestlistEntry(Guestlist $party): self {
        if ($this->guestlistEntries->contains($party)) {
            $this->guestlistEntries->removeElement($party);
            // set the owning side to null (unless already changed)
            if ($party->getInviter() === $this)
                $party->setInviter(null);
        }

        return $this;
    }

    public function jsonSerialize(): array
    {
        return [
            'userId' => $this->user->getId(),
            'name' => $this->name,
        ];
    }
}
