<?php

namespace App\Guestlist\Entity;

use App\Festival\Entity\Festival;
use App\Guestlist\Model\GuestlistDto;
use App\Guestlist\Repository\GuestlistRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: GuestlistRepository::class)]
#[ORM\UniqueConstraint(fields: ['festival', 'email'])]
class Guestlist implements JsonSerializable
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $token = null;

    #[ORM\JoinColumn(nullable: false)]
    #[ORM\ManyToOne(targetEntity: Festival::class)]
    private ?Festival $festival = null;

    #[ORM\OneToMany(mappedBy: 'guestlist', targetEntity: GuestlistDay::class, cascade: ['persist'], fetch: 'EAGER', orphanRemoval: true)]
    private Collection $days;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank]
    private ?string $name = null;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\Email]
    private ?string $email = null;

    #[ORM\JoinColumn(referencedColumnName: 'user_id', nullable: true)]
    #[ORM\ManyToOne(targetEntity: GuestlistInviter::class, inversedBy: 'guestlistEntries')]
    private ?GuestlistInviter $inviter = null;

    #[ORM\Column(type: 'string', length: 255, enumType: GuestlistType::class)]
    #[Assert\NotBlank]
    private ?GuestlistType $type;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $comment = null;

    #[ORM\Column(type: 'boolean', nullable: true)]
    private ?bool $camping = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $origin = null;

    #[ORM\Column(type: 'smallint', nullable: true)]
    #[Assert\GreaterThanOrEqual(0)]
    private ?int $plus = null;

    #[ORM\Column(type: 'boolean')]
    private ?bool $checkedIn = false;

    #[ORM\Column(type: 'boolean')]
    private ?bool $backstageCheckedIn = false;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank]
    private ?string $state = null;

    #[ORM\ManyToMany(targetEntity: TravelParty::class, mappedBy: 'guestlist')]
    private Collection $travelParties;

    public function __construct()
    {
        $this->days = new ArrayCollection();
        $this->travelParties = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFestival(): ?Festival
    {
        return $this->festival;
    }

    public function setFestival(?Festival $festival): self
    {
        $this->festival = $festival;

        return $this;
    }

    public function getDays(): ArrayCollection|Collection
    {
        return $this->days;
    }

    public function addDay(GuestlistDay $day): self
    {
        if (!$this->days->contains($day)) {
            $this->days[] = $day;
            $day->setGuestlist($this);
        }

        return $this;
    }

    public function removeDay(GuestlistDay $day): self
    {
        if ($this->days->contains($day)) {
            $this->days->removeElement($day);
            // set the owning side to null (unless already changed)
            if ($day->getGuestlist() === $this) {
                $day->setGuestlist(null);
            }
        }

        return $this;
    }

    /**
     * @param Collection<GuestlistDay> $days
     */
    public function setDays(Collection $days): self
    {
        $this->days = $days;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    public function getEmailAnonymized(): ?string
    {
        if (!$this->email)
            return null;
        return preg_replace('/(.)(.*)@(.*)/', '$1***@$3', $this->email);
    }

    public function getInviter(): ?GuestlistInviter
    {
        return $this->inviter;
    }

    public function setInviter(?GuestlistInviter $inviter): self
    {
        $this->inviter = $inviter;

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(?string $token): self
    {
        $this->token = $token;

        return $this;
    }


    public function getType(): ?GuestlistType
    {
        return $this->type;
    }

    public function setType(?GuestlistType $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getCamping(): ?bool
    {
        return $this->camping;
    }

    public function setCamping(?bool $camping): self
    {
        $this->camping = $camping;

        return $this;
    }

    public function getOrigin(): ?string
    {
        return $this->origin;
    }

    public function setOrigin(?string $origin): self
    {
        $this->origin = $origin;

        return $this;
    }

    public function getPlus(): ?int
    {
        return $this->plus;
    }

    public function setPlus(?int $plus): self
    {
        $this->plus = $plus;

        return $this;
    }

    public function getCheckedIn(): ?bool
    {
        return $this->checkedIn;
    }

    public function setCheckedIn(?bool $checkedIn): self
    {
        $this->checkedIn = $checkedIn;

        return $this;
    }

    function getBackstageCheckedIn(): ?bool
    {
        return $this->backstageCheckedIn;
    }

    public function setBackstageCheckedIn(?bool $backstageCheckedIn): self
    {
        $this->backstageCheckedIn = $backstageCheckedIn;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(?string $state): self
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @return Collection|TravelParty[]
     */
    public function getTravelParties(): Collection
    {
        return $this->travelParties;
    }

    public function addTravelParty(TravelParty $party): self
    {
        if (!$this->travelParties->contains($party)) {
            $this->travelParties[] = $party;
            $party->addGuestlist($this);
        }

        return $this;
    }

    public function removeTravelParty(TravelParty $party): self
    {
        if ($this->travelParties->contains($party)) {
            $this->travelParties->removeElement($party);
            // set the owning side to null (unless already changed)
            if ($party->getGuestlist()->contains($this))
                $party->removeGuestlist($this);
        }

        return $this;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'festivalId' => $this->festival->getId(),
            'name' => $this->name,
            'email' => $this->email,
            'inviterId' => $this->inviter?->getUser()?->getId(),
            'inviterName' => $this->inviter?->getName(),
            'inviterEmail' => $this->inviter?->getUser()?->getEmail(),
            'type' => $this->type,
            'comment' => $this->comment,
            'camping' => $this->camping,
            'origin' => $this->origin,
            'plus' => $this->plus,
            'checkedIn' => $this->checkedIn,
            'backstageCheckedIn' => $this->backstageCheckedIn,
            'state' => $this->state,
            'days' => $this->days->toArray(),
        ];
    }
}
