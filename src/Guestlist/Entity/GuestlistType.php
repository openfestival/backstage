<?php

declare(strict_types=1);

namespace App\Guestlist\Entity;

enum GuestlistType: string
{
    /**
     * Band members, performers
     */
    case Act = 'act';
    /**
     * Volunteers, technicians
     */
    case Crew = 'crew';
    /**
     * Friends of other festivals or clubs
     */
    case Friend = 'friend';
    /**
     * Sponsors
     */
    case Sponsor = 'sponsor';
    /**
     * Local residents
     */
    case Local = 'local';
}
