<?php

namespace App\Guestlist\Entity;

use App\Guestlist\Repository\BandMemberRepository;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Deprecated;

#[Deprecated(reason: "Not used anymore, guestlist entries used instead")]
#[ORM\Entity(repositoryClass: BandMemberRepository::class)]
class BandMember {
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $name = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $comment = null;

    #[ORM\Column(type: 'boolean')]
    private ?bool $arrived = null;

    #[ORM\JoinColumn(nullable: false)]
    #[ORM\ManyToOne(targetEntity: TravelParty::class, inversedBy: 'members')]
    private ?TravelParty $travelParty = null;

    public function getId(): ?int {
        return $this->id;
    }

    public function getName(): ?string {
        return $this->name;
    }

    public function setName(string $name): self {
        $this->name = $name;

        return $this;
    }

    public function getComment(): ?string {
        return $this->comment;
    }

    public function setComment(?string $comment): self {
        $this->comment = $comment;

        return $this;
    }

    public function getArrived(): ?bool {
        return $this->arrived;
    }

    public function setArrived(bool $arrived): self {
        $this->arrived = $arrived;

        return $this;
    }

    public function getTravelParty(): ?TravelParty {
        return $this->travelParty;
    }

    public function setTravelParty(?TravelParty $travelParty): self {
        $this->travelParty = $travelParty;

        return $this;
    }
}
