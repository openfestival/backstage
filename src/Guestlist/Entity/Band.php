<?php

namespace App\Guestlist\Entity;

use App\Guestlist\Repository\BandRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

#[ORM\Entity(repositoryClass: BandRepository::class)]
class Band implements JsonSerializable {
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $name = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $contactName = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $contactEmail = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $contactPhone = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $location = null;

    #[ORM\OneToMany(mappedBy: 'band', targetEntity: TravelParty::class)]
    private Collection $travelParties;

    public function __construct() {
        $this->travelParties = new ArrayCollection();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getName(): ?string {
        return $this->name;
    }

    public function setName(string $name): self {
        $this->name = $name;

        return $this;
    }

    public function getContactName(): ?string {
        return $this->contactName;
    }

    public function setContactName(?string $contactName): self {
        $this->contactName = $contactName;

        return $this;
    }

    public function getContactEmail(): ?string {
        return $this->contactEmail;
    }

    public function setContactEmail(?string $contactEmail): self {
        $this->contactEmail = $contactEmail;

        return $this;
    }

    public function getContactPhone(): ?string {
        return $this->contactPhone;
    }

    public function setContactPhone(?string $contactPhone): self {
        $this->contactPhone = $contactPhone;

        return $this;
    }

    public function getLocation(): ?string {
        return $this->location;
    }

    public function setLocation(?string $location): self {
        $this->location = $location;

        return $this;
    }

    /**
     * @return Collection|TravelParty[]
     */
    public function getTravelParties(): Collection {
        return $this->travelParties;
    }

    public function addTravelParty(TravelParty $party): self {
        if (!$this->travelParties->contains($party)) {
            $this->travelParties[] = $party;
            $party->setBand($this);
        }

        return $this;
    }

    public function removeTravelParty(TravelParty $party): self {
        if ($this->travelParties->contains($party)) {
            $this->travelParties->removeElement($party);
            // set the owning side to null (unless already changed)
            if ($party->getBand() === $this)
                $party->setBand(null);
        }

        return $this;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'contactName' => $this->contactName,
            'contactEmail' => $this->contactEmail,
            'contactPhone' => $this->contactPhone,
            'location' => $this->location,
        ];
    }
}
