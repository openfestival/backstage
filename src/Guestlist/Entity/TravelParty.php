<?php

namespace App\Guestlist\Entity;

use App\Festival\Entity\FestivalDay;
use App\Guestlist\Repository\TravelPartyRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;
use JsonSerializable;

#[ORM\Entity(repositoryClass: TravelPartyRepository::class)]
class TravelParty implements JsonSerializable {
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\JoinColumn(nullable: false)]
    #[ORM\ManyToOne(targetEntity: Band::class, inversedBy: 'travelParties')]
    private ?Band $band = null;

    #[ORM\JoinColumn(nullable: false)]
    #[ORM\ManyToOne(targetEntity: FestivalDay::class, inversedBy: 'travelParties')]
    private ?FestivalDay $day = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $vehicle = null;

    #[ORM\Column(type: 'datetime_immutable')]
    private ?DateTimeImmutable $stageTime = null;

    #[ORM\Column(type: 'boolean')]
    private ?bool $checkedIn = false;

    #[ORM\JoinTable(name: 'travel_party_guestlist')]
    #[ORM\JoinColumn(name: 'travel_party_id', referencedColumnName: 'id', onDelete: 'CASCADE')]
    #[ORM\InverseJoinColumn(name: 'guestlist_id', referencedColumnName: 'id', onDelete: 'CASCADE')]
    #[ORM\ManyToMany(targetEntity: Guestlist::class, inversedBy: 'travelParties')]
    private Collection $guestlist;

    #[Pure]
    public function __construct() {
        $this->guestlist = new ArrayCollection();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getBand(): ?Band {
        return $this->band;
    }

    public function setBand(Band $band): self {
        $this->band = $band;

        return $this;
    }

    public function getDay(): ?FestivalDay {
        return $this->day;
    }

    public function setDay(?FestivalDay $day): self {
        $this->day = $day;

        return $this;
    }

    public function getVehicle(): ?string {
        return $this->vehicle;
    }

    public function setVehicle(?string $vehicle): self {
        $this->vehicle = $vehicle;

        return $this;
    }

    public function getStageTime(): ?DateTimeImmutable {
        return $this->stageTime;
    }

    public function setStageTime(DateTimeImmutable $stageTime): self {
        $this->stageTime = $stageTime;

        return $this;
    }

    public function getCheckedIn(): ?bool {
        return $this->checkedIn;
    }

    public function setCheckedIn(?bool $checkedIn): self {
        $this->checkedIn = $checkedIn;

        return $this;
    }

    /**
     * @return Collection|Guestlist[]
     */
    public function getGuestlist(): Collection {
        return $this->guestlist;
    }

    public function addGuestlist(Guestlist $entry): self {
        if (!$this->guestlist->contains($entry)) {
            $this->guestlist[] = $entry;
            $entry->addTravelParty($this);
        }

        return $this;
    }

    public function removeGuestlist(Guestlist $entry): self {
        if ($this->guestlist->contains($entry)) {
            $this->guestlist->removeElement($entry);
            // set the owning side to null (unless already changed)
            if ($entry->getTravelParties()->contains($this))
                $entry->removeTravelParty($this);
        }

        return $this;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'band' => $this->band,
            'day' => $this->day,
            'vehicle' => $this->vehicle,
            'stageTime' => $this->stageTime->format(DateTimeImmutable::ATOM),
            'checkedIn' => $this->checkedIn,
            'guestlist' => $this->guestlist->map(fn(Guestlist $entry) => $entry->getId())->toArray()
        ];
    }
}
