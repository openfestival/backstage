<?php

namespace App\Guestlist\Entity;

use App\Festival\Entity\FestivalDay;
use App\Guestlist\Repository\GuestlistDayRepository;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

#[ORM\Entity(repositoryClass: GuestlistDayRepository::class)]
class GuestlistDay implements JsonSerializable
{
    #[ORM\JoinColumn(nullable: false)]
    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: Guestlist::class, inversedBy: 'days')]
    private ?Guestlist $guestlist = null;

    #[ORM\JoinColumn(nullable: false)]
    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: FestivalDay::class)]
    private ?FestivalDay $day = null;

    #[ORM\Column(type: 'boolean')]
    private ?bool $backstage = null;

    public function getGuestlist(): ?Guestlist
    {
        return $this->guestlist;
    }

    public function setGuestlist(?Guestlist $guestlist): self
    {
        $this->guestlist = $guestlist;

        return $this;
    }

    public function getDay(): ?FestivalDay
    {
        return $this->day;
    }

    public function setDay(?FestivalDay $day): self
    {
        $this->day = $day;

        return $this;
    }

    public function getBackstage(): ?bool
    {
        return $this->backstage;
    }

    public function setBackstage(?bool $backstage): self
    {
        $this->backstage = $backstage;

        return $this;
    }

    public function jsonSerialize(): mixed
    {
        return [
            'guestlistId' => $this->guestlist->getId(),
            'festivalDayId' => $this->day->getId(),
            'backstage' => $this->backstage,
        ];
    }
}
