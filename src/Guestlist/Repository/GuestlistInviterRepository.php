<?php

namespace App\Guestlist\Repository;

use App\Guestlist\Entity\GuestlistInviter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method GuestlistInviter|null find($id, $lockMode = null, $lockVersion = null)
 * @method GuestlistInviter|null findOneBy(array $criteria, array $orderBy = null)
 * @method GuestlistInviter[]    findAll()
 * @method GuestlistInviter[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GuestlistInviterRepository extends ServiceEntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, GuestlistInviter::class);
    }
}
