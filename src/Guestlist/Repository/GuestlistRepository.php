<?php

namespace App\Guestlist\Repository;

use App\Festival\Entity\Festival;
use App\Guestlist\Entity\Guestlist;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Guestlist|null find($id, $lockMode = null, $lockVersion = null)
 * @method Guestlist|null findOneBy(array $criteria, array $orderBy = null)
 * @method Guestlist[]    findAll()
 * @method Guestlist[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GuestlistRepository extends ServiceEntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Guestlist::class);
    }

    public function findAllWhereFestivalOrderedByName(Festival $festival): array {
        return $this->createQueryBuilder('g')
            ->select('g', 'd')
            ->leftJoin('g.days', 'd')
            ->where('g.festival = :festival')
            ->orderBy('g.name')
            ->setParameters([
                'festival' => $festival
            ])
            ->getQuery()
            ->getResult();
    }

    public function findAllWhereFestivalAndTravelPartyNotNull(Festival $festival): array {
        return $this->createQueryBuilder('g')
            ->select('g', 'd')
            ->leftJoin('g.days', 'd')
            ->leftJoin('g.travelParties', 't')
            ->where('g.festival = :festival', 't.id IS NOT NULL')
            ->setParameters([
                'festival' => $festival
            ])
            ->getQuery()
            ->getResult();
    }

    public function findAllWhereIdIn(array $ids): array {
        return $this->createQueryBuilder('g')
            ->select('g', 'd')
            ->leftJoin('g.days', 'd')
            ->where('g.id IN (:ids)')
            ->setParameter('ids', $ids)
            ->getQuery()
            ->getResult();
    }
}
