<?php

namespace App\Guestlist\Repository;

use App\Guestlist\Entity\GuestlistDay;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method GuestlistDay|null find($id, $lockMode = null, $lockVersion = null)
 * @method GuestlistDay|null findOneBy(array $criteria, array $orderBy = null)
 * @method GuestlistDay[]    findAll()
 * @method GuestlistDay[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GuestlistDayRepository extends ServiceEntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, GuestlistDay::class);
    }

    // /**
    //  * @return GuestlistDay[] Returns an array of GuestlistDay objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?GuestlistDay
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
