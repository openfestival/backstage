<?php

namespace App\Guestlist\Model;

use Symfony\Component\Validator\Constraints as Assert;

readonly class BandDto
{
    public function __construct(
        #[Assert\NotBlank]
        public string $name,
        public ?string $location,
        public ?string $contactName,
        public ?string $contactEmail,
        public ?string $contactPhone,
    ) {}
}