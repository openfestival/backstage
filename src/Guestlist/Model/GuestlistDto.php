<?php
declare(strict_types=1);

namespace App\Guestlist\Model;

use App\Guestlist\Entity\GuestlistType;
use Symfony\Component\Validator\Constraints as Assert;

readonly class GuestlistDto
{
    public function __construct(
        #[Assert\NotBlank]
        public string $name,

        #[Assert\Email]
        public ?string $email,

        public ?int $inviterId,

        public GuestlistType $type,

        #[Assert\NotBlank]
        public string $origin,

        public ?string $comment,

        public bool $camping,

        #[Assert\GreaterThanOrEqual(0)]
        public int $plus,

        #[Assert\Valid]
        #[Assert\All(new Assert\Type(GuestlistDayDto::class))]
        #[Assert\Count(min: 1)]
        /** @var GuestlistDayDto[] $days */
        public array $days,

        public bool $requireEmailConfirmation = true,

        public ?bool $checkedIn = null,

        public ?bool $backstageCheckedIn = null,
    ) {}
}
