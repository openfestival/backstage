<?php

namespace App\Guestlist\Model;

use DateTimeImmutable;
use Symfony\Component\Validator\Constraints as Assert;

readonly class TravelPartyDto
{
    public function __construct(
        public int $bandId,
        public int $dayId,
        public DateTimeImmutable $stageTime,
        public ?string $vehicle,
        public bool $checkedIn,
        #[Assert\Valid]
        #[Assert\All(new Assert\Type('int'))]
        public array $guestlistIds,
    ) {}
}