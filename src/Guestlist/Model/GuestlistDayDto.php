<?php
declare(strict_types=1);

namespace App\Guestlist\Model;

readonly class GuestlistDayDto
{
    public function __construct(
        public int $festivalDayId,
        public bool $backstage,
    ) {}
}
