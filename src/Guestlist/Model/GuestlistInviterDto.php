<?php
declare(strict_types=1);

namespace App\Guestlist\Model;

readonly class GuestlistInviterDto
{
    public function __construct(
        public int $userId,
        public string $name,
    ) {}
}
