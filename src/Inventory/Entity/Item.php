<?php

namespace App\Inventory\Entity;

use App\Inventory\Repository\ItemRepository;
use App\Inventory\Validator as InventoryAssert;
use App\Security\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @InventoryAssert\IsWellFormedItem()
 */
#[ORM\Table(name: 'inventory_item')]
#[ORM\Index(columns: ['borrowed_for'])]
#[ORM\Entity(repositoryClass: ItemRepository::class)]
class Item {
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\JoinColumn(nullable: false, onDelete: 'CASCADE')]
    #[ORM\ManyToOne(targetEntity: ItemType::class, fetch: 'EAGER', inversedBy: 'items')]
    private ?ItemType $type = null;

    #[ORM\Column(type: 'integer', unique: true, nullable: true)]
    #[Assert\GreaterThanOrEqual(1)]
    private ?int $number = null;

    #[ORM\Column(type: 'integer')]
    #[Assert\GreaterThanOrEqual(1)]
    private ?int $count = null;

    #[ORM\JoinColumn(name: 'borrowed_id')]
    #[ORM\ManyToOne(targetEntity: User::class, fetch: 'EAGER', inversedBy: 'inventoryItems')]
    private ?User $borrowedBy = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $borrowedFor = null;

    #[ORM\JoinColumn(nullable: false)]
    #[ORM\ManyToOne(targetEntity: Location::class, fetch: 'EAGER', inversedBy: 'items')]
    private ?Location $location = null;

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $notes = null;

    #[ORM\Column(type: 'boolean')]
    private ?bool $defective = null;

    #[ORM\Column(type: 'boolean')]
    private ?bool $missing = null;

    #[ORM\Column(type: 'boolean')]
    private bool $deleted = false;

    public function getId(): ?int {
        return $this->id;
    }

    public function getType(): ?ItemType {
        return $this->type;
    }

    public function setType(?ItemType $type): self {
        $this->type = $type;

        return $this;
    }

    public function getNumber(): ?int {
        return $this->number;
    }

    public function setNumber(?int $number): self {
        $this->number = $number;

        return $this;
    }

    public function getCount(): ?int {
        return $this->count;
    }

    public function setCount(int $count): self {
        $this->count = $count;

        return $this;
    }

    public function getBorrowedBy(): ?User {
        return $this->borrowedBy;
    }

    public function setBorrowedBy(?User $borrowedBy): self {
        $this->borrowedBy = $borrowedBy;

        return $this;
    }

    public function getBorrowedFor(): ?string {
        return $this->borrowedFor;
    }

    public function setBorrowedFor(?string $borrowedFor): self {
        $this->borrowedFor = $borrowedFor;

        return $this;
    }

    public function getLocation(): ?Location {
        return $this->location;
    }

    public function setLocation(?Location $location): self {
        $this->location = $location;

        return $this;
    }

    public function getNotes(): ?string {
        return $this->notes;
    }

    public function setNotes(?string $notes): self {
        $this->notes = $notes;

        return $this;
    }

    public function getDefective(): ?bool {
        return $this->defective;
    }

    public function setDefective(bool $defective): self {
        $this->defective = $defective;

        return $this;
    }

    public function getMissing(): ?bool {
        return $this->missing;
    }

    public function setMissing(bool $missing): self {
        $this->missing = $missing;

        return $this;
    }

    public function getDeleted(): ?bool {
        return $this->deleted;
    }

    public function setDeleted(bool $deleted): self {
        $this->deleted = $deleted;

        return $this;
    }

    public function getDisplayName(): string {
        $typeName = $this->getType()->getName();
        if ($this->number != null)
            return $typeName . ' #' . $this->number;
        return $typeName . ' (id::' . $this->id . ')';
    }
}
