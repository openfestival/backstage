<?php

namespace App\Inventory\Entity;

use App\Inventory\Repository\InventoryFileRepository;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @Vich\Uploadable
 */
#[ORM\Table(name: 'inventory_item_type_image')]
#[ORM\Entity(repositoryClass: InventoryFileRepository::class)]
class ItemTypeImage extends InventoryFile {
    #[ORM\JoinColumn(nullable: false)]
    #[ORM\ManyToOne(targetEntity: ItemType::class, inversedBy: 'images')]
    private ?ItemType $itemType = null;

    /**
     * @Vich\UploadableField(mapping="inventory_item_type_image", fileNameProperty="fileName")
     */
    private ?File $image = null;

    public function getItemType(): ?ItemType {
        return $this->itemType;
    }

    public function setItemType(?ItemType $itemType): self {
        $this->itemType = $itemType;

        return $this;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|UploadedFile|null $image
     */
    public function setImage(?File $image = null): void
    {
        $this->image = $image;

        if (null !== $image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new DateTimeImmutable();
        }
    }

    public function getImage(): ?File {
        return $this->image;
    }
}
