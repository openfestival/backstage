<?php

namespace App\Inventory\Entity;

use App\Inventory\Repository\ItemTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'inventory_item_type')]
#[ORM\Index(columns: ['name'])]
#[ORM\Index(columns: ['owner'])]
#[ORM\Entity(repositoryClass: ItemTypeRepository::class)]
class ItemType {
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $name = null;

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $description = null;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $owner = null;

    #[ORM\JoinTable(name: 'inventory_item_category')]
    #[ORM\JoinColumn(name: 'inventory_item_id', referencedColumnName: 'id', onDelete: 'CASCADE')]
    #[ORM\InverseJoinColumn(name: 'inventory_category_id', referencedColumnName: 'id', onDelete: 'CASCADE')]
    #[ORM\ManyToMany(targetEntity: Category::class, inversedBy: 'itemTypes')]
    private Collection $categories;

    #[ORM\OneToMany(mappedBy: 'itemType', targetEntity: ItemTypeImage::class, cascade: ['persist', 'remove'], orphanRemoval: true)]
    private Collection $images;

    #[ORM\OneToMany(mappedBy: 'type', targetEntity: Item::class, orphanRemoval: true)]
    private Collection $items;

    #[ORM\OneToMany(mappedBy: 'itemType', targetEntity: Log::class, orphanRemoval: true)]
    private Collection $log;

    public function __construct() {
        $this->categories = new ArrayCollection();
        $this->images = new ArrayCollection();
        $this->items = new ArrayCollection();
        $this->log = new ArrayCollection();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getName(): ?string {
        return $this->name;
    }

    public function setName(string $name): self {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string {
        return $this->description;
    }

    public function setDescription(?string $description): self {
        $this->description = $description;

        return $this;
    }

    public function getOwner(): ?string {
        return $this->owner;
    }

    public function setOwner(string $owner): self {
        $this->owner = $owner;

        return $this;
    }

    /**
     * @return Collection|Category[]
     */
    public function getCategories(): Collection {
        return $this->categories;
    }

    public function addCategory(Category $category): self {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
            $category->addItemType($this);
        }

        return $this;
    }

    public function removeCategory(Category $category): self {
        if ($this->categories->contains($category)) {
            $this->categories->removeElement($category);
            // set the owning side to null (unless already changed)
            if ($category->getItemTypes()->contains($this)) {
                $category->removeItemType($this);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ItemTypeImage[]
     */
    public function getImages(): Collection {
        return $this->images;
    }

    public function addImage(ItemTypeImage $image): self {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->setItemType($this);
        }

        return $this;
    }

    public function removeImage(ItemTypeImage $image): self {
        if ($this->images->contains($image)) {
            $this->images->removeElement($image);
            // set the owning side to null (unless already changed)
            if ($image->getItemType() === $this) {
                $image->setItemType(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Item[]
     */
    public function getItems(): Collection {
        return $this->items;
    }

    public function addItem(Item $item): self {
        if (!$this->items->contains($item)) {
            $this->items[] = $item;
            $item->setType($this);
        }

        return $this;
    }

    public function removeItem(Item $item): self {
        if ($this->items->contains($item)) {
            $this->items->removeElement($item);
            // set the owning side to null (unless already changed)
            if ($item->getType() === $this) {
                $item->setType(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Log[]
     */
    public function getLog(): Collection {
        return $this->log;
    }

    public function addLog(Log $log): self {
        if (!$this->log->contains($log)) {
            $this->log[] = $log;
            $log->setItemType($this);
        }

        return $this;
    }

    public function removeLog(Log $log): self {
        if ($this->log->contains($log)) {
            $this->log->removeElement($log);
            // set the owning side to null (unless already changed)
            if ($log->getItemType() === $this) {
                $log->setItemType(null);
            }
        }

        return $this;
    }
}
