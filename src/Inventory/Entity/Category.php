<?php

namespace App\Inventory\Entity;

use App\Inventory\Repository\CategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'inventory_category')]
#[ORM\Entity(repositoryClass: CategoryRepository::class)]
class Category {
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $name = null;

    #[ORM\ManyToMany(targetEntity: ItemType::class, mappedBy: 'categories')]
    private Collection $itemTypes;

    public function __construct() {
        $this->itemTypes = new ArrayCollection();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getName(): ?string {
        return $this->name;
    }

    public function setName(string $name): self {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|ItemType[]
     */
    public function getItemTypes(): Collection {
        return $this->itemTypes;
    }

    public function addItemType(ItemType $item): self {
        if (!$this->itemTypes->contains($item)) {
            $this->itemTypes[] = $item;
            $item->addCategory($this);
        }

        return $this;
    }

    public function removeItemType(ItemType $item): self {
        if ($this->itemTypes->contains($item)) {
            $this->itemTypes->removeElement($item);
            // set the owning side to null (unless already changed)
            if ($item->getCategories()->contains($this)) {
                $item->removeCategory($this);
            }
        }

        return $this;
    }
}
