<?php

namespace App\Inventory\Entity;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

#[ORM\MappedSuperclass]
abstract class InventoryFile {
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    protected ?int $id = null;

    #[ORM\Column(type: 'string', nullable: true)]
    protected ?string $fileName = null;

    #[ORM\Column(type: 'datetime_immutable')]
    protected ?DateTimeImmutable $updatedAt = null;

    public function getId(): ?int {
        return $this->id;
    }

    public function getFileName(): ?string {
        return $this->fileName;
    }

    public function setFileName(?string $fileName): self {
        $this->fileName = $fileName;

        return $this;
    }

    public function getUpdatedAt(): ?DateTimeImmutable {
        return $this->updatedAt;
    }

    public function setUpdatedAt(DateTimeImmutable $updatedAt): self {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
