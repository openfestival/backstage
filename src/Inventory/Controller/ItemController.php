<?php

namespace App\Inventory\Controller;

use App\Inventory\Application\ItemService;
use App\Inventory\Entity\Item;
use App\Inventory\Entity\ItemType;
use App\Inventory\Form\ItemBorrowForm;
use App\Inventory\Form\ItemForm;
use App\Inventory\Form\ItemReturnForm;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Attribute\Route;

#[Route(path: '/inventory/item')]
class ItemController extends AbstractController {
    private ItemService $itemService;

    public function __construct(ItemService $service) {
        $this->itemService = $service;
    }

    /**
     * Processes {@link ItemBorrowForm} requests to borrow items.
     *
     * @param Item $item item to borrow
     */
    #[Route(path: '/borrow/{item<\d+>}', name: 'app_inventory_item_borrow', methods: ['POST'])]
    public function borrowItem(Item $item, Request $request): RedirectResponse {
        $this->denyAccessUnlessGranted('inventory.borrow');

        $form = $this->createForm(ItemBorrowForm::class);
        $form->handleRequest($request);
        if (!$form->isSubmitted() || !$form->isValid())
            throw new BadRequestHttpException('Form was not submitted or is invalid.');
        if ($item->getBorrowedBy() != null)
            throw new BadRequestHttpException('Item is already borrowed by somebody.');

        $count = $form->get('count')->getData();
        if ($count <= 0 || $count > $item->getCount())
            throw new BadRequestHttpException('Cannot borrow ' . $count . ' of ' . $item->getCount() . ' items.');

        $this->itemService->borrowItem($item, $form->get('for')->getData(), $count, $form->get('notes')->getData());
        $this->addFlash('success', $item->getDisplayName() . ' wurde ausgeliehen.');

        return $this->redirectToRoute('app_inventory_type', ['type' => $item->getType()->getId()]);
    }

    /**
     * Processes {@link ItemReturnForm} requests to return items.
     *
     * @param Item $item item to return
     */
    #[Route(path: '/return/{item<\d+>}', name: 'app_inventory_item_return', methods: ['POST'])]
    public function returnItem(Item $item, Request $request): RedirectResponse {
        $this->denyAccessUnlessGranted('inventory.borrow');

        $form = $this->createForm(ItemReturnForm::class);
        $form->handleRequest($request);
        if (!$form->isSubmitted() || !$form->isValid())
            throw new BadRequestHttpException('Form was not submitted or is invalid.');
        if ($item->getBorrowedBy() == null)
            throw new BadRequestHttpException('Item is not borrowed by anybody.');

        if ($item->getBorrowedBy() !== $this->getUser() && !$this->isGranted('inventory.return.other'))
            throw new AccessDeniedHttpException('Cannot return items for other users.');

        $count = $form->get('count')->getData();
        if ($count <= 0 || $count > $item->getCount())
            throw new BadRequestHttpException('Cannot return ' . $count . ' of ' . $item->getCount() . ' items.');

        $this->itemService->returnItem($item, $count, $form->get('notes')->getData());
        $this->addFlash('success', $item->getDisplayName() . ' wurde ausgeliehen.');

        return $this->redirectToRoute('app_inventory_type', ['type' => $item->getType()->getId()]);
    }

    /**
     * Processes {@link ItemForm} requests to edit items.
     *
     * @param Item $item item to edit
     */
    #[Route(path: '/edit/{item<\d+>}', name: 'app_inventory_item_edit', methods: ['POST'])]
    public function editItem(Item $item, Request $request): RedirectResponse {
        $this->denyAccessUnlessGranted('inventory.edit');

        // save old values
        $number = $item->getNumber();
        $count = $item->getCount();
        $location = $item->getLocation();
        $notes = $item->getNotes();
        $defective = $item->getDefective();
        $missing = $item->getMissing();

        // edit item form
        $form = $this->createForm(ItemForm::class, $item);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                // changing item numbers after the fact requires special permission
                if (!is_null($number) && $number != $item->getNumber() && !$this->isGranted('inventory.edit.number'))
                    throw new AccessDeniedHttpException('Not allowed to change item numbers');

                if ($this->itemService->checkNumberExists($item)) {
                    $this->addFlash('danger', 'Es existiert bereits ein Artikel mit der Nummer #' . $item->getNumber());
                } else {
                    $this->itemService->editItem($item, $number, $count, $location, $notes, $defective, $missing);
                    $this->addFlash('success', $item->getDisplayName() . ' wurde gespeichert.');
                }
            } else {
                foreach ($form->getErrors() as $error)
                    $this->addFlash('danger', $error->getMessage());
            }
        }

        return $this->redirectToRoute('app_inventory_type', ['type' => $item->getType()->getId()]);
    }

    /**
     * Processes {@link ItemForm} requests to create new items for a given type.
     *
     * @param ItemType $type item type to create a new item for
     */
    #[Route(path: '/new/{type<\d+>}', name: 'app_inventory_item_new', methods: ['POST'])]
    public function newItem(ItemType $type, Request $request, SessionInterface $session): RedirectResponse {
        $this->denyAccessUnlessGranted('inventory.edit');

        $item = new Item();
        $form = $this->createForm(ItemForm::class, $item);
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid())
            throw new BadRequestHttpException();

        if ($this->itemService->checkNumberExists($item)) {
            $this->addFlash('danger', 'Es existiert bereits ein Artikel mit der Nummer #' . $item->getNumber());
        } else {
            $this->itemService->addItem($type, $item);
            $this->addFlash('success', $item->getDisplayName() . ' wurde gespeichert.');
        }

        // save location used in session, to speed up creation of multiple items
        $session->set('inventory.item.defaultLocation', $item->getLocation()->getId());

        return $this->redirectToRoute('app_inventory_type', ['type' => $type->getId()]);
    }
}
