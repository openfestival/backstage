<?php

namespace App\Inventory\Controller;

use App\Inventory\Application\InventoryService;
use App\Inventory\Entity\Category;
use App\Inventory\Form\CategoryForm;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Attribute\Route;

#[Route(path: '/inventory/categories')]
class CategoryController extends AbstractController {

    private FormFactoryInterface $formFactory;
    private InventoryService $service;

    public function __construct(FormFactoryInterface $formFactory, InventoryService $service) {
        $this->formFactory = $formFactory;
        $this->service = $service;
    }

    /**
     * Creates a new Category.
     */
    #[Route(path: '/', name: 'app_inventory_categories')]
    public function show(): Response {
        $this->denyAccessUnlessGranted('inventory.info');

        $categories = $this->service->getCategories();
        $newForm = $this->formFactory->createNamed('new_form', CategoryForm::class, null, [
            'action' => $this->generateUrl('app_inventory_categories_new')
        ]);
        $editForm = $this->formFactory->createNamed('edit_form', CategoryForm::class);

        return $this->render('inventory/categories.html.twig', [
            'categories' => $categories,
            'new_form' => $newForm->createView(),
            'edit_form' => $editForm->createView()
        ]);
    }

    #[Route(path: '/edit/{category<\d+>}', name: 'app_inventory_categories_edit')]
    public function edit(Category $category, Request $request, EntityManagerInterface $doctrine): RedirectResponse {
        $this->denyAccessUnlessGranted('inventory.edit');

        $form = $this->formFactory->createNamed('edit_form', CategoryForm::class, $category);
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid())
            throw new BadRequestHttpException();

        $doctrine->flush();
        $this->addFlash('success', $category->getName() . ' wurde gespeichert.');
        return $this->redirectToRoute('app_inventory_categories');
    }

    #[Route(path: '/new', name: 'app_inventory_categories_new')]
    public function new(Request $request): RedirectResponse {
        $this->denyAccessUnlessGranted('inventory.edit');

        $category = new Category();
        $form = $this->formFactory->createNamed('new_form', CategoryForm::class, $category);
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid())
            throw new BadRequestHttpException();

        $this->service->createCategory($category);
        $this->addFlash('success', $category->getName() . ' wurde gespeichert.');
        return $this->redirectToRoute('app_inventory_categories');
    }
}
