<?php

namespace App\Inventory\Controller;

use App\LegacyApiController;
use App\Inventory\Application\ItemService;
use App\Inventory\Entity\Item;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route(path: '/api/inventory')]
class ApiController extends LegacyApiController {
    private ItemService $service;

    public function __construct(ItemService $service) {
        $this->service = $service;
    }

    /**
     * Finds items by their number for the inventory scanner
     *
     * @param int $number number of an item
     */
    #[Route(path: '/item-by-number/{number<\d+>}')]
    public function getItemType(int $number): Response {
        $this->denyAccessUnlessGranted('inventory.info');

        $item = $this->service->getItemByNumber($number);
        if (is_null($item))
            return $this->responseNotFound();

        return $this->responseOk([
            'id' => $item->getId(),
            'type' => $item->getType()->getId(),
            'number' => $item->getNumber(),
            'count' => $item->getCount(),
            'location' => $item->getLocation()->getName(),
            'notes' => $item->getNotes(),
            'defective' => $item->getDefective(),
            'missing' => $item->getMissing(),
            'deleted' => $item->getDeleted(),
            'borrowed' => $item->getBorrowedBy() == null ? null : [
                'by' => $item->getBorrowedBy()->getId(),
                'byName' => $item->getBorrowedBy()->getDisplayName(),
                'for' => $item->getBorrowedFor()
            ],
            'displayName' => $item->getDisplayName(),
            'link' => $this->generateUrl('app_inventory_type', [
                'type' => $item->getType()->getId(),
                'showDeleted' => $item->getDeleted(),
                'highlight' => $item->getId()
            ])
        ]);
    }

    #[Route(path: '/item/borrow/{item<\d+>}', methods: ['POST'])]
    public function borrowItem(Item $item, Request $request): Response {
        $this->denyAccessUnlessGranted('inventory.borrow');

        $data = $this->resolveBody($request, [
            'for' => '?string',
            'notes' => '?string',
        ]);
        if ($data === false)
            return $this->responseBadRequest();

        if ($item->getBorrowedBy() != null)
            return $this->responseBadRequest('Item is already borrowed.');

        $this->service->borrowItem($item, $data->for, $item->getCount(), $data->notes);
        return $this->responseOk();
    }

    #[Route(path: '/item/return/{item<\d+>}', methods: ['POST'])]
    public function returnItem(Item $item, Request $request): Response {
        $this->denyAccessUnlessGranted('inventory.borrow');

        $data = $this->resolveBody($request, [
            'notes' => '?string',
        ]);
        if ($data === false)
            return $this->responseBadRequest();

        if ($item->getBorrowedBy() == null)
            return $this->responseBadRequest('Item is not borrowed.');
        if ($item->getBorrowedBy() !== $this->getUser() && !$this->isGranted('inventory.return.other'))
            return $this->responseForbidden('Item is not borrowed by you.');

        $this->service->returnItem($item, $item->getCount(), $data->notes);
        return $this->responseOk();
    }
}
