<?php

namespace App\Inventory\Controller;

use App\Inventory\Application\ItemService;
use App\Inventory\Application\LocationService;
use App\Inventory\Entity\Item;
use App\Inventory\Entity\ItemType;
use App\Inventory\Entity\ItemTypeImage;
use App\Inventory\Form\ItemBorrowForm;
use App\Inventory\Form\ItemForm;
use App\Inventory\Form\ItemNumberForm;
use App\Inventory\Form\ItemReturnForm;
use App\Inventory\Form\ItemTypeForm;
use App\Inventory\Form\ItemTypeImageForm;
use App\Inventory\ItemNumberDuplicateException;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Attribute\Route;

#[Route(path: '/inventory/type')]
class ItemTypeController extends AbstractController {

    private ItemService $itemService;
    private LocationService $locationService;

    public function __construct(ItemService $service, LocationService $locationService) {
        $this->itemService = $service;
        $this->locationService = $locationService;
    }

    /**
     * Displays an item type and all items belonging to the type.
     *
     * @param ItemType $type the item type
     */
    #[Route(path: '/{type<\d+>}', name: 'app_inventory_type')]
    public function showType(ItemType $type, Request $request, SessionInterface $session): Response {
        $this->denyAccessUnlessGranted('inventory.info');

        $showDeleted = $request->query->getBoolean('showDeleted');

        // check for action
        if ($request->query->has('action')) {
            if (!$this->isGranted('inventory.edit'))
                throw new AccessDeniedHttpException('Insufficient permissions to edit item.');

            $items = $request->query->all('items');
            foreach ($items as $itemId) {
                $item = $this->itemService->getItem((int)$itemId);
                if ($item == null || $item->getType() !== $type)
                    continue;

                $this->performItemAction($item, $request->query->getAlpha('action'));
            }
            return $this->redirectToRoute('app_inventory_type', [
                'type' => $type->getId(),
                'showDeleted' => $showDeleted,
                'items' => $items
            ]);
        }

        // find items to display in table
        $items = $this->itemService->getItemsOfType($type, $showDeleted);
        // count items
        $count = 0;
        foreach ($items as $item)
            $count += $item->getCount();

        // new item form: set initial location to same as first item if possible, otherwise use session
        $item = new Item();
        if (sizeof($items) > 0) {
            $item->setLocation($items[0]->getLocation());
        } else if ($session->has('inventory.item.defaultLocation')) {
            $location = $this->locationService->getLocation($session->get('inventory.item.defaultLocation'));
            if ($location != null)
                $item->setLocation($location);
        }
        $newItemForm = $this->createForm(ItemForm::class, $item);

        return $this->render('inventory/item/show.html.twig', [
            'type' => $type,
            'items' => $items,
            'show_deleted' => $showDeleted,
            'count' => $count,
            'log' => $this->itemService->getRecentActivity($type),
            'number_form' => $this->createForm(ItemNumberForm::class, null, [
                'action' => $this->generateUrl('app_inventory_number_items', ['type' => $type->getId()])
            ])->createView(),
            'borrow_form' => $this->createForm(ItemBorrowForm::class)->createView(),
            'return_form' => $this->createForm(ItemReturnForm::class)->createView(),
            'edit_form' => $this->createForm(ItemTypeForm::class, $type)->createView(),
            'new_item_form' => $newItemForm->createView(),
            'image_form' => $this->createForm(ItemTypeImageForm::class, null, [
                'action' => $this->generateUrl('app_inventory_image_add', ['type' => $type->getId()])
            ])->createView()
        ]);
    }

    /**
     * Performs an action for a given item. Available actions are `borrow`, `return`, `delete` and `restore`.
     *
     * @param Item $item the item to perform the action on
     * @param string $action the action to perform
     */
    private function performItemAction(Item $item, string $action): void {
        switch ($action) {
            // set defective
            case 'defective':
                $defective = $item->getDefective();
                $item->setDefective(!$item->getDefective());
                $this->itemService->editItem($item, $item->getNumber(), $item->getCount(), $item->getLocation(),
                    $item->getNotes(), $defective, $item->getMissing());
                $this->addFlash('success', $item->getDisplayName() . ' wurde als ' . ($item->getDefective() ? 'defekt' : 'nicht defekt') . ' markiert.');
                break;

            // set missing
            case 'missing':
                $missing = $item->getMissing();
                $item->setMissing(!$item->getMissing());
                $this->itemService->editItem($item, $item->getNumber(), $item->getCount(), $item->getLocation(),
                    $item->getNotes(), $item->getDefective(), $missing);
                $this->addFlash('success', $item->getDisplayName() . ' wurde als ' . ($item->getMissing() ? 'fehlend' : 'nicht fehlend') . ' markiert.');
                break;

            // delete item
            case 'delete':
                if ($this->itemService->deleteItem($item)) {
                    $this->addFlash('success', $item->getDisplayName() . ' wurde gelöscht.');
                } else $this->addFlash('danger', $item->getDisplayName() . ' ist schon gelöscht.');
                break;

            // restore item
            case 'restore':
                if ($this->itemService->restoreItem($item)) {
                    $this->addFlash('success', $item->getDisplayName() . ' wurde wiederhergestellt.');
                } else $this->addFlash('danger', $item->getDisplayName() . ' ist nicht gelöscht.');
                break;
        }
    }

    /**
     * Processes {@link ItemTypeForm} requests to edit items types.
     *
     * @param ItemType $type item type to edit
     */
    #[Route(path: '/edit/{type<\d+>}', name: 'app_inventory_type_edit', methods: ['POST'])]
    public function editType(ItemType $type, Request $request): RedirectResponse {
        $this->denyAccessUnlessGranted('inventory.edit');

        // save old values
        $name = $type->getName();
        $description = $type->getDescription();
        $owner = $type->getOwner();
        $categories = new ArrayCollection();
        foreach ($type->getCategories() as $category)
            $categories->add($category);

        $form = $this->createForm(ItemTypeForm::class, $type);
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid())
            throw new BadRequestHttpException();

        $this->itemService->editItemType($type, $name, $description, $owner, $categories);
        $this->addFlash('success', $type->getName() . ' wurde gespeichert.');

        return $this->redirectToRoute('app_inventory_type', ['type' => $type->getId()]);
    }

    /**
     * Processes {@link ItemTypeForm} requests to create new items types.
     */
    #[Route(path: '/new', name: 'app_inventory_type_new', methods: ['POST'])]
    public function newType(Request $request, SessionInterface $session): RedirectResponse {
        $this->denyAccessUnlessGranted('inventory.edit');

        $type = new ItemType();
        $form = $this->createForm(ItemTypeForm::class, $type);
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid())
            throw new BadRequestHttpException();

        // save category used in session, to speed up creation of multiple items
        $session->set('inventory.item.defaultCategory', $type->getCategories()->first()->getId());

        $this->itemService->createItemType($type);

        $this->addFlash('success', $type->getName() . ' wurde gespeichert.');
        return $this->redirectToRoute('app_inventory_type', ['type' => $type->getId()]);
    }

    /**
     * Processes {@link ItemTypeImageForm} requests to add images to item types.
     *
     * @param ItemType $type item type to add an image to
     */
    #[Route(path: '/image/add/{type<\d+>}', name: 'app_inventory_image_add', methods: ['POST'])]
    public function addImage(ItemType $type, Request $request): RedirectResponse {
        $this->denyAccessUnlessGranted('inventory.edit');

        $image = new ItemTypeImage();
        $image->setItemType($type);
        $form = $this->createForm(ItemTypeImageForm::class, $image);
        $form->handleRequest($request);

        if (!$form->isSubmitted())
            throw new BadRequestHttpException();
        if ($form->isValid()) {
            $this->itemService->addImage($type, $image);
            $this->addFlash('success', 'Bild wurde erfolgreich hinzugefügt.');
        } else {
            foreach ($form->get('image')->getErrors() as $error)
                $this->addFlash('danger', $error->getMessage());
        }

        return $this->redirectToRoute('app_inventory_type', ['type' => $type->getId()]);
    }

    /**
     * Deletes item images
     *
     * @param ItemTypeImage $image image to delete
     */
    #[Route(path: '/image/delete/{image<\d+>}', name: 'app_inventory_image_delete')]
    public function delImage(ItemTypeImage $image): RedirectResponse {
        $this->denyAccessUnlessGranted('inventory.edit');

        $type = $image->getItemType();
        $this->itemService->deleteImage($image);
        $this->addFlash('success', 'Bild wurde erfolgreich gelöscht.');

        return $this->redirectToRoute('app_inventory_type', ['type' => $type->getId()]);
    }

    /**
     * Processes {@link ItemNumberForm} requests to add numbers to items
     *
     * @param ItemType $type type to which the items belong to
     * @return RedirectResponse
     */
    #[Route(path: '/number/{type<\d+>}', name: 'app_inventory_number_items', methods: ['POST'])]
    public function numberItems(ItemType $type, Request $request): RedirectResponse {
        $this->denyAccessUnlessGranted('inventory.edit.number');

        $form = $this->createForm(ItemNumberForm::class);
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid())
            throw new BadRequestHttpException();

        $itemIds = explode(',', $form->get('items')->getData());
        if (empty($itemIds))    // @phpstan-ignore-line
            throw new BadRequestHttpException('No items found in request');
        foreach ($itemIds as $id)
            if (!ctype_digit($id))
                throw new BadRequestHttpException('Invalid item id ' . $id);

        try {
            $this->itemService->numberItems($type, $itemIds, $form->get('start')->getData());
        } catch (ItemNumberDuplicateException $e) {
            $this->addFlash('danger', 'Artikel sind schon nummeriert oder Nummer ist nicht frei!');
        }

        return $this->redirectToRoute('app_inventory_type', ['type' => $type->getId()]);
    }
}
