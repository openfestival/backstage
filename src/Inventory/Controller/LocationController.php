<?php

namespace App\Inventory\Controller;

use App\Inventory\Application\InventoryService;
use App\Inventory\Application\LocationService;
use App\Inventory\Entity\ItemType;
use App\Inventory\Entity\Location;
use App\Inventory\Form\ItemTypeForm;
use App\Inventory\Form\LocationForm;
use App\Inventory\Form\SearchForm;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Attribute\Route;

#[Route(path: '/inventory/location')]
class LocationController extends AbstractController {

    private InventoryService $inventoryService;
    private LocationService $locationService;

    public function __construct(
        InventoryService $inventoryService,
        LocationService  $locationService
    ) {
        $this->inventoryService = $inventoryService;
        $this->locationService = $locationService;
    }

    /**
     * Lists all items in a given location.
     *
     * @param Location $location the location
     */
    #[Route(path: '/{location<\d+>}', name: 'app_inventory_location')]
    public function show(Location $location, SessionInterface $session): Response {
        $this->denyAccessUnlessGranted('inventory.info');

        $types = $this->locationService->getItemTypes($location);

        // edit form
        $editForm = $this->createForm(LocationForm::class, $location, [
            'action' => $this->generateUrl('app_inventory_location_edit', ['location' => $location->getId()])
        ]);

        // new item form
        $type = new ItemType();
        if ($session->has('inventory.item.defaultCategory')) {
            $category = $this->inventoryService->getCategory($session->get('inventory.item.defaultCategory'));
            if ($category != null)
                $type->addCategory($category);
        }
        $newItemForm = $this->createForm(ItemTypeForm::class, $type);

        return $this->render('inventory/location/show.html.twig', [
            'location' => $location,
            'types' => $types,
            'search_form' => $this->createForm(SearchForm::class)->createView(),
            'edit_form' => $editForm->createView(),
            'new_item_form' => $newItemForm->createView()
        ]);
    }

    /**
     * Processes {@link LocationForm} requests to edit locations.
     *
     * @param Location $location the location to edit
     */
    #[Route(path: '/edit/{location<\d+>}', name: 'app_inventory_location_edit', methods: ['POST'])]
    public function edit(Location $location, Request $request, EntityManagerInterface $doctrine): RedirectResponse {
        $this->denyAccessUnlessGranted('inventory.edit');

        // edit form
        $editForm = $this->createForm(LocationForm::class, $location);
        $editForm->handleRequest($request);

        if (!$editForm->isSubmitted() || !$editForm->isValid())
            throw new BadRequestHttpException();

        if ($editForm->get('delete')->isClicked()) {
            if ($location->getItems()->isEmpty()) {
                $doctrine->remove($location);
                $doctrine->flush();
                $this->addFlash('success', $location->getName() . ' wurde gelöscht.');
                return $this->redirectToRoute('app_inventory');
            } else {
                $this->addFlash('danger', 'Bitte entferne alle Objekte aus diesem Lagerort bevor du ihn löschst.');
            }
        } else {
            $doctrine->flush();
            $this->addFlash('success', $location->getName() . ' wurde gespeichert.');
        }

        return $this->redirectToRoute('app_inventory_location', ['location' => $location->getId()]);
    }

    /**
     * Processes {@link LocationForm} requests to create new locations.
     */
    #[Route(path: '/new', name: 'app_inventory_location_new', methods: ['POST'])]
    public function newType(Request $request): RedirectResponse {
        $this->denyAccessUnlessGranted('inventory.edit');

        $location = new Location();
        $form = $this->createForm(LocationForm::class, $location);
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid())
            throw new BadRequestHttpException();

        $this->locationService->createLocation($location);
        $this->addFlash('success', $location->getName() . ' wurde gespeichert.');
        return $this->redirectToRoute('app_inventory_location', ['location' => $location->getId()]);
    }

}
