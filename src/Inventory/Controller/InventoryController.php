<?php

namespace App\Inventory\Controller;

use App\Inventory\Application\InventoryService;
use App\Inventory\Application\ItemService;
use App\Inventory\Application\LocationService;
use App\Inventory\Entity\ItemType;
use App\Inventory\Entity\Location;
use App\Inventory\Form\ItemTypeForm;
use App\Inventory\Form\LocationForm;
use App\Inventory\Form\SearchForm;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Attribute\Route;

#[Route(path: '/inventory')]
class InventoryController extends AbstractController {

    private InventoryService $inventoryService;
    private ItemService $itemService;
    private LocationService $locationService;

    public function __construct(
        InventoryService $inventoryService,
        ItemService      $service,
        LocationService  $locationService
    ) {
        $this->inventoryService = $inventoryService;
        $this->itemService = $service;
        $this->locationService = $locationService;
    }

    /**
     * Shows all available locations and the recent activity.
     */
    #[Route(path: '/', name: 'app_inventory')]
    public function index(SessionInterface $session): Response {
        $this->denyAccessUnlessGranted('inventory.info');

        $locations = $this->locationService->getLocations();
        $itemCounts = $this->itemService->getItemCounts();

        // new location form
        $newLocationForm = $this->createForm(LocationForm::class, new Location(), [
            'action' => $this->generateUrl('app_inventory_location_new')
        ]);

        // new item form
        $type = new ItemType();
        if ($session->has('inventory.item.defaultCategory')) {
            $category = $this->inventoryService->getCategory($session->get('inventory.item.defaultCategory'));
            if ($category != null)
                $type->addCategory($category);
        }
        $newItemForm = $this->createForm(ItemTypeForm::class, $type);

        return $this->render('inventory/index.html.twig', [
            'locations' => $locations,
            'itemCounts' => $itemCounts,
            'search_form' => $this->createForm(SearchForm::class)->createView(),
            'new_item_form' => $newItemForm->createView(),
            'new_location_form' => $newLocationForm->createView()
        ]);
    }

    #[Route(path: '/search', name: 'app_inventory_search')]
    public function search(Request $request): Response {
        $this->denyAccessUnlessGranted('inventory.info');

        $form = $this->createForm(SearchForm::class);
        $form->handleRequest($request);

        $types = [];

        if ($form->isSubmitted() && $form->isValid()) {
            $find = $form->get('find')->getData();

            // check for number match
            $matches = [];
            if (preg_match('/^#?(\d+)$/', $find, $matches)) {
                $item = $this->itemService->getItemByNumber($matches[1]);
                if (!is_null($item)) {
                    return $this->redirectToRoute('app_inventory_type', [
                        'type' => $item->getType()->getId(),
                        'highlight' => $item->getId()
                    ]);
                }
            }

            $types = $this->itemService->searchItemTypes($find, $form->get('categories')->getData()->toArray());
        }

        return $this->render('inventory/search.html.twig', [
            'form' => $form->createView(),
            'types' => $types
        ]);
    }

    /**
     * Shows all deleted items.
     */
    #[Route(path: '/trash', name: 'app_inventory_trash')]
    public function trash(): Response {
        $this->denyAccessUnlessGranted('inventory.info');

        $types = $this->itemService->getDeletedItemTypes();

        return $this->render('inventory/trash.html.twig', [
            'types' => $types
        ]);
    }

}
