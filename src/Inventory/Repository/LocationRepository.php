<?php

namespace App\Inventory\Repository;

use App\Inventory\Entity\Location;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Location|null find($id, $lockMode = null, $lockVersion = null)
 * @method Location|null findOneBy(array $criteria, array $orderBy = null)
 * @method Location[]    findAll()
 * @method Location[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LocationRepository extends ServiceEntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Location::class);
    }

    /**
     * Finds the count of not-deleted item types for all locations
     *
     * @return array Array containing the location IDs as keys and the corresponding item counts as values
     */
    public function countItemWhereNotDeletedGroupedByLocation(): array {
        $result = $this->createQueryBuilder('l')
            ->leftJoin('l.items', 'i')
            ->where('i.deleted = false')
            ->select('l.id', 'COUNT(DISTINCT i.type) AS count')
            ->groupBy('l.id')
            ->getQuery()
            ->getResult();
        // reduce result to a one-dimensional array with the location id as key and item count as value
        return array_reduce($result, function ($carry, $item) {
            $carry[$item['id']] = intval($item['count']);
            return $carry;
        }, []);
    }
}
