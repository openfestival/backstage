<?php

namespace App\Inventory\Repository;

use App\Inventory\Entity\ItemType;
use App\Inventory\Entity\Log;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr;
use Doctrine\Persistence\ManagerRegistry;
use App\Security\Entity\User;
use App\Inventory\Entity\Item;

/**
 * @method Log|null find($id, $lockMode = null, $lockVersion = null)
 * @method Log|null findOneBy(array $criteria, array $orderBy = null)
 * @method Log[]    findAll()
 * @method Log[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LogRepository extends ServiceEntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Log::class);
    }

    /**
     * Finds the most recent log entries.
     *
     * @param int $count the amount of entries to return.
     * @param ItemType $type if not null, only returns entries concerning the given item.
     * @return array the found log entries.
     */
    public function findAllOrderedByTimestamp(int $count, ItemType $type = null): array {
        $query = $this->createQueryBuilder('l')
            ->select('l.id', 'l.timestamp', 'u.name AS userName', 'u.email AS userEmail', 't.name AS type', 'i.id AS itemId', 'i.number AS itemNumber', 'l.action', 'l.newValue', 'l.oldValue')
            ->join(ItemType::class, 't', Expr\Join::WITH, 'l.itemType = t')
            ->leftJoin(Item::class, 'i', Expr\Join::WITH, 'l.item = i')
            ->join(User::class, 'u', Expr\Join::WITH, 'l.user = u');
        if ($type != null)
            $query->where('t = :type');
        $query = $query->orderBy('l.timestamp', 'DESC')
            ->setMaxResults($count);
        if ($type != null)
            $query->setParameter('type', $type);
        return $query
            ->getQuery()
            ->getResult();
    }
}
