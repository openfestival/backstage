<?php

namespace App\Inventory\Repository;

use App\Inventory\Entity\ItemType;
use App\Inventory\Entity\Location;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ItemType|null find($id, $lockMode = null, $lockVersion = null)
 * @method ItemType|null findOneBy(array $criteria, array $orderBy = null)
 * @method ItemType[]    findAll()
 * @method ItemType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ItemTypeRepository extends ServiceEntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, ItemType::class);
    }

    public function findAllWhereCategoryIn(array $categories): array {
        return $this->createQueryBuilder('t')
            ->select('t')
            ->join('t.categories', 'c')
            ->where('c IN (:categories)')
            ->groupBy('t.id')
            ->orderBy('t.name')
            ->getQuery()
            ->setParameter('categories', $categories)
            ->getResult();
    }

    public function findAllWhereNameLike(string $name): array {
        return $this->createQueryBuilder('t')
            ->select('t')
            ->where('t.name LIKE :name')
            ->orderBy('t.name')
            ->getQuery()
            ->setParameter('name', $name)
            ->getResult();
    }

    public function findAllWhereNameLikeAndCategoryIn(string $name, array $categories): array {
        return $this->createQueryBuilder('t')
            ->select('t')
            ->join('t.categories', 'c')
            ->where('c IN (:categories)', 't.name LIKE :name')
            ->groupBy('t.id')
            ->orderBy('t.name')
            ->getQuery()
            ->setParameter('categories', $categories)
            ->setParameter('name', $name)
            ->getResult();
    }

    /**
     * Returns all item types found in a given location as well as their item count.
     *
     * @param Location $location find only item types with items in this location
     * @return array[] the item types, ordered by name
     */
    public function findWhereItemLocationEqualsOrderedByName(Location $location): array {
        return $this->createQueryBuilder('t')
            ->select('t.id', 't.name', 't.description', 't.owner', 'SUM(i.count) AS count')
            ->join('t.items', 'i')
            ->where('i.location = :location')
            ->andWhere('i.deleted = false')
            ->groupBy('t.id')
            ->orderBy('t.name', 'ASC')
            ->getQuery()
            ->setParameter('location', $location)
            ->getArrayResult();
    }

    /**
     * Finds all item types having every item associated deleted.
     *
     * @return array[] the item types, ordered by name
     */
    public function findWhereItemsDeletedOrderedByName(): array {
        return $this->createQueryBuilder('t')
            ->select('t.id', 't.name', 't.description', 't.owner', 'SUM(i.count) AS count')
            ->leftJoin('t.items', 'i')
            ->where('i.deleted = true')
            ->orWhere('i IS NULL')
            ->orderBy('t.name')
            ->groupBy('t.id')
            ->getQuery()
            ->getResult();
    }

    /**
     * Finds all owners that exist, ordered by name
     *
     * @return String[] the names of the owners
     */
    public function findAllOwnersDistinct(): array {
        return $this->createQueryBuilder('t')
            ->select('t.owner')
            // don't return empty owners, as old items may have no owner set
            ->where('t.owner <> :empty')
            ->distinct()
            ->orderBy('t.owner')
            ->getQuery()
            ->setParameter('empty', '')
            ->getSingleColumnResult();
    }
}
