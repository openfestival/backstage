<?php

namespace App\Inventory\Application;

use App\Inventory\Entity\Category;
use App\Inventory\Entity\Item;
use App\Inventory\Entity\ItemType;
use App\Inventory\Entity\ItemTypeImage;
use App\Inventory\Entity\Location;
use App\Inventory\Entity\Log;
use App\Inventory\Enum\LogAction;
use App\Inventory\ItemNumberDuplicateException;
use App\Inventory\Model\ItemTypeDto;
use App\Inventory\Repository\ItemRepository;
use App\Inventory\Repository\ItemTypeRepository;
use App\Inventory\Repository\LocationRepository;
use App\Inventory\Repository\LogRepository;
use App\Security\Entity\User;
use App\System\Application\MapperService;
use App\System\Exception\InternalServerErrorHttpException;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\SecurityBundle\Security;

class ItemService {
    private EntityManagerInterface $doctrine;
    private Security $security;
    private MapperService $mapper;

    private ItemTypeRepository $itemTypeRepository;
    private ItemRepository $itemRepository;
    private LocationRepository $locationRepository;
    private LogRepository $logRepository;

    public function __construct(
        EntityManagerInterface $doctrine,
        Security               $security,
        MapperService          $mapper,
        ItemTypeRepository     $itemTypeRepository,
        ItemRepository         $itemRepository,
        LocationRepository     $locationRepository,
        LogRepository          $logRepository
    ) {
        $this->doctrine = $doctrine;
        $this->security = $security;
        $this->mapper = $mapper;

        $this->itemTypeRepository = $itemTypeRepository;
        $this->itemRepository = $itemRepository;
        $this->locationRepository = $locationRepository;
        $this->logRepository = $logRepository;
    }

    /**
     * Finds the count of not-deleted items for all locations.
     *
     * @return array Array containing the location IDs as keys and the corresponding item counts as values
     */
    public function getItemCounts(): array {
        return $this->locationRepository->countItemWhereNotDeletedGroupedByLocation();
    }

    /**
     * Finds a single item.
     *
     * @param int $id the item id
     * @return Item|null the item if it exists
     */
    public function getItem(int $id): ?Item {
        return $this->itemRepository->find($id);
    }

    /**
     * Finds a single item
     *
     * @param int $number the item number
     * @return Item|null the item if it exists
     */
    public function getItemByNumber(int $number): ?Item {
        return $this->itemRepository->findWhereNumber($number);
    }

    /**
     * Finds all item types matching the given name and/or categories.
     * If both arguments are empty, an empty array is returned.
     * If the name is empty, all items having at least one of the given categories are returned.
     * If the categories are empty, any item containing the given name are returned.
     * Otherwise, all item types containing the given name and having at least one of the given categories are returned.
     *
     * @param string|null $name name to search for
     * @param Category[] $categories categories to search for
     * @return ItemType[] item types found
     */
    public function searchItemTypes(?string $name, array $categories): array {
        if (empty($name) && empty($categories))
            return [];

        if (empty($name))
            $types = $this->itemTypeRepository->findAllWhereCategoryIn($categories);
        else if (empty($categories))
            $types = $this->itemTypeRepository->findAllWhereNameLike('%' . $name . '%');
        else $types = $this->itemTypeRepository->findAllWhereNameLikeAndCategoryIn('%' . $name . '%', $categories);

        return $types;
    }

    /**
     * Finds all items belonging to a given type.
     *
     * @param ItemType $type the item type
     * @param bool $includeDeleted set to true if deleted items should be included. Defaults to false.
     * @return Item[] the items belonging to the given type.
     */
    public function getItemsOfType(ItemType $type, bool $includeDeleted = false): array {
        $criteria = ['type' => $type];
        if (!$includeDeleted)
            $criteria['deleted'] = false;
        return $this->itemRepository->findBy($criteria, ['number' => 'ASC']);
    }

    /**
     * Finds all deleted item types.
     *
     * @return ItemTypeDto[] all deleted items, sorted by name.
     */
    public function getDeletedItemTypes(): array {
        $types = $this->itemTypeRepository->findWhereItemsDeletedOrderedByName();
        return $this->mapper->mapToObjects($types, ItemTypeDto::class);
    }

    /**
     * Saves a new item type
     *
     * @param ItemType $type the new item type
     */
    public function createItemType(ItemType $type): void {
        $this->createLog($type, null, LogAction::CREATED);
        $this->doctrine->persist($type);
        $this->doctrine->flush();
    }

    /**
     * Finds all items of the given type with the given ids and adds numbers to them sequentially. Items with a count
     * of more than 1 are split into individual items.
     *
     * @param ItemType $type type of items
     * @param array $ids ids of items
     * @param int $start start of number range
     * @throws ItemNumberDuplicateException if any item is already numbered or a number from the range is not free
     */
    public function numberItems(ItemType $type, array $ids, int $start): void {
        $items = $this->itemRepository->findAllWhereTypeAndIdIn($type, $ids);

        // check that numbers are free
        $end = $start + array_sum(array_map(function ($item) {
                return $item->getCount();
            }, $items)) - 1;
        if ($this->itemRepository->findCountWhereNumberBetween($start, $end) != 0)
            throw new ItemNumberDuplicateException('Number range is not free');

        // loop through items
        $n = $start;
        foreach ($items as $item) {
            if ($item->getNumber() != null)
                throw new ItemNumberDuplicateException('Item is already numbered');

            // set number
            $this->createLog($item->getType(), $item, LogAction::CHANGED_NUMBER, null, $n);
            $item->setNumber($n);
            $n++;

            // if this item has a count greater than one, split it into individual items
            if ($item->getCount() > 1) {
                $itemsToCreate = $item->getCount() - 1;
                // set count of original item to one
                $this->createLog($item->getType(), $item, LogAction::CHANGED_COUNT, $item->getCount(), 1);
                $item->setCount(1);

                // create new items
                for ($i = 0; $i < $itemsToCreate; $i++) {
                    $newItem = new Item();
                    $newItem->setNumber($n);
                    $newItem->setLocation($item->getLocation());
                    $newItem->setCount(1);
                    $newItem->setNotes($item->getNotes());
                    $newItem->setDefective($item->getDefective());
                    $newItem->setMissing($item->getMissing());
                    $item->getType()->addItem($newItem);

                    $this->createLog($item->getType(), $newItem, LogAction::ITEM_ADDED);
                    $this->doctrine->persist($newItem);

                    $n++;
                }
            }
        }

        // flush changes
        $this->doctrine->flush();
    }

    /**
     * Borrows an item. If the count borrowed is smaller than the count of the item, a new item entity is created
     * for the amount remaining after borrowing the given count of this item. The appropriate log entries are created.
     *
     * @param Item $item The item to borrow
     * @param ?string $for Who this item is borrowed for, null if self
     * @param int $count amount of items borrowed
     */
    public function borrowItem(Item $item, ?string $for, int $count, ?string $notes): void {
        if ($item->getBorrowedBy() != null)
            throw new InternalServerErrorHttpException('Item is already borrowed!');

        if ($count != $item->getCount())
            $this->splitItem($item, $count);

        /** @var User $user */
        $user = $this->security->getUser();
        $this->createLog($item->getType(), $item, LogAction::CHANGED_BORROWED, null, $for);
        $item->setBorrowedBy($user);
        $item->setBorrowedFor($for);

        if ($notes != $item->getNotes()) {
            $this->createLog($item->getType(), $item, LogAction::CHANGED_NOTES, $item->getNotes(), $notes);
            $item->setNotes($notes);
        }

        $this->doctrine->flush();
    }

    /**
     * Returns an item. If the count returned is smaller than the count of the item, a new item entity is created
     * for the amount remaining after returning the given count of this item. The appropriate log entries are created.
     *
     * @param Item $item The item being returned
     * @param int $count amount of items returned
     */
    public function returnItem(Item $item, int $count, ?string $notes): void {
        if ($item->getBorrowedBy() == null)
            throw new InternalServerErrorHttpException('Item is not borrowed by anybody!');

        if ($count != $item->getCount())
            $this->splitItem($item, $count);

        $this->createLog($item->getType(), $item, LogAction::CHANGED_BORROWED, $item->getBorrowedFor() ?: $item->getBorrowedBy()->getDisplayName());
        $item->setBorrowedBy(null);
        $item->setBorrowedFor(null);

        if ($notes != $item->getNotes()) {
            $this->createLog($item->getType(), $item, LogAction::CHANGED_NOTES, $item->getNotes(), $notes);
            $item->setNotes($notes);
        }

        $this->doctrine->flush();
    }

    /**
     * Splits an item entity into two entities. The original item will have its count set to the given count and the new
     * item will represent the remaining amount. The appropriate log entries are created.
     *
     * @param Item $item item to split
     * @param int $count amount of items to keep for the original item
     * @throws InternalServerErrorHttpException if the given count is less than zero or greater than or equal to the count of the given item
     */
    private function splitItem(Item $item, int $count): void {
        if ($count <= 0 || $count >= $item->getCount())
            throw new InternalServerErrorHttpException('Cannot split ' . $count . ' of ' . $item->getCount() . ' for item ' . $item->getId());

        $remaining = new Item();
        $remaining->setLocation($item->getLocation());
        $remaining->setCount($item->getCount() - $count);
        $remaining->setNotes($item->getNotes());
        $remaining->setDefective($item->getDefective());
        $remaining->setMissing($item->getMissing());
        $item->getType()->addItem($remaining);

        $this->createLog($item->getType(), $remaining, LogAction::ITEM_ADDED);
        $this->doctrine->persist($remaining);

        $this->createLog($item->getType(), $item, LogAction::CHANGED_COUNT, $item->getCount(), $count);
        $item->setCount($count);
    }

    /**
     * Checks if any item exists that does not have the id of the given item but does have its number. If the given
     * item is new (does not have an id yet) this checks for any item with the given number.
     *
     * @param Item $item item to check for
     * @return bool true, if any item exists that matches the description
     */
    public function checkNumberExists(Item $item): bool {
        if (is_null($item->getNumber()))
            return false;
        return $this->itemRepository->findWhereNotIdAndNumber($item->getId(), $item->getNumber()) != null;
    }

    /**
     * Marks an item as deleted.
     *
     * @param Item $item The item to delete
     * @return bool false if the item was already deleted, true otherwise
     */
    public function deleteItem(Item $item): bool {
        if ($item->getDeleted())
            return false;

        $this->createLog($item->getType(), $item, LogAction::CHANGED_DELETED, false, true);
        $item->setDeleted(true);
        $this->doctrine->flush();
        return true;
    }

    /**
     * Restores an item marked as deleted.
     *
     * @param Item $item The item to restore
     * @return bool false if the item was not deleted, true otherwise
     */
    public function restoreItem(Item $item): bool {
        if (!$item->getDeleted())
            return false;

        $this->createLog($item->getType(), $item, LogAction::CHANGED_DELETED, true, false);
        $item->setDeleted(false);
        $this->doctrine->flush();
        return true;
    }

    /**
     * Adds a new item to an item type
     *
     * @param ItemType $type the type to add the item to
     * @param Item $item the item to create
     */
    public function addItem(ItemType $type, Item $item): void {
        if ($item->getType() != null)
            throw new InternalServerErrorHttpException('Item already has a type associated');
        $this->createLog($type, $item, LogAction::ITEM_ADDED);
        $type->addItem($item);


        $this->doctrine->persist($item);
        $this->doctrine->flush();
    }

    /**
     * Saves an edited item. Checks each attribute for changes and logs them if necessary.
     *
     * @param Item $item the edited item
     * @param int $oldNumber the old number
     * @param int $oldCount the old count
     * @param Location $oldLocation the old location
     * @param bool $oldDefective the old defective flag
     * @param bool $oldMissing the old missing flag
     */
    public function editItem(
        Item     $item,
        ?int     $oldNumber,
        int      $oldCount,
        Location $oldLocation,
        ?string  $oldNotes,
        bool     $oldDefective,
        bool     $oldMissing
    ): void {
        if ($oldNumber != $item->getNumber())
            $this->createLog($item->getType(), $item, LogAction::CHANGED_NUMBER, $oldNumber, $item->getNumber());

        if ($oldCount != $item->getCount())
            $this->createLog($item->getType(), $item, LogAction::CHANGED_COUNT, $oldCount, $item->getCount());

        if ($oldLocation->getId() != $item->getLocation()->getId())
            $this->createLog($item->getType(), $item, LogAction::CHANGED_LOCATION, $oldLocation->getId(), $item->getLocation()->getId());

        if ($oldNotes != $item->getNotes())
            $this->createLog($item->getType(), $item, LogAction::CHANGED_NOTES, $oldNotes, $item->getNotes());

        if ($oldDefective != $item->getDefective())
            $this->createLog($item->getType(), $item, LogAction::CHANGED_DEFECTIVE, $oldDefective, $item->getDefective());

        if ($oldMissing != $item->getMissing())
            $this->createLog($item->getType(), $item, LogAction::CHANGED_MISSING, $oldMissing, $item->getMissing());

        $this->doctrine->flush();
    }

    /**
     * Saves an edited item type. Checks each attribute for changes and logs them if necessary.
     *
     * @param ItemType $type The edited item type
     * @param string $oldName the old name
     * @param string $oldDescription the old description
     * @param string $oldOwner the old owner
     * @param Collection $oldCategories the old categories
     */
    public function editItemType(
        ItemType   $type,
        string     $oldName,
        ?string    $oldDescription,
        string     $oldOwner,
        Collection $oldCategories
    ): void {
        if ($oldName != $type->getName())
            $this->createLog($type, null, LogAction::CHANGED_NAME, $oldName, $type->getName());

        if ($oldDescription != $type->getDescription())
            $this->createLog($type, null, LogAction::CHANGED_DESCRIPTION, $oldDescription, $type->getDescription());

        if ($oldOwner != $type->getOwner())
            $this->createLog($type, null, LogAction::CHANGED_OWNER, $oldOwner, $type->getOwner());

        foreach ($type->getCategories() as $category)
            if (!$oldCategories->contains($category))
                $this->createLog($type, null, LogAction::CATEGORY_ADDED, null, $category->getName());
        foreach ($oldCategories as $category)
            if (!$type->getCategories()->contains($category))
                $this->createLog($type, null, LogAction::CATEGORY_REMOVED, null, $category->getName());

        $this->doctrine->flush();
    }

    /**
     * Uploads an image for a given item type.
     *
     * @param ItemType $type the type to add the image to
     * @param ItemTypeImage $image image to add
     */
    public function addImage(ItemType $type, ItemTypeImage $image): void {
        $this->createLog($type, null, LogAction::IMAGE_ADDED);
        $type->addImage($image);
        $this->doctrine->persist($image);
        $this->doctrine->flush();
    }

    /**
     * Deletes an image
     *
     * @param ItemTypeImage $image image to delete
     */
    public function deleteImage(ItemTypeImage $image): void {
        $this->createLog($image->getItemType(), null, LogAction::IMAGE_DELETED);
        $image->getItemType()->removeImage($image);
        $this->doctrine->remove($image);
        $this->doctrine->flush();
    }

    /**
     * Creates a new Log. Does not commit yet.
     */
    private function createLog(ItemType $type, ?Item $item, LogAction $action, $oldValue = null, $newValue = null): void {
        /** @var User $user */
        $user = $this->security->getUser();
        $log = new Log($type, $item, $user, $action, $oldValue, $newValue);
        $this->doctrine->persist($log);
    }

    /**
     * Finds the most recent log entries.
     *
     * @param int $count the amount of entries to return.
     * @param ItemType $item if not null, only returns entries concerning the given item.
     * @return array the found log entries.
     */
    public function getRecentActivity(ItemType $item = null): array {
        return $this->logRepository->findAllOrderedByTimestamp(50, $item);
    }
}
