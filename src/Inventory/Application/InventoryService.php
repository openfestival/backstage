<?php

namespace App\Inventory\Application;

use App\Inventory\Entity\Category;
use App\Inventory\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;

class InventoryService {
    private EntityManagerInterface $doctrine;

    private CategoryRepository $categoryRepository;

    public function __construct(EntityManagerInterface $doctrine, CategoryRepository $categoryRepository) {
        $this->doctrine = $doctrine;

        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Finds all categories.
     *
     * @return Category[] all categories sorted by name
     */
    public function getCategories(): array {
        return $this->categoryRepository->findBy([], ['name' => 'ASC']);
    }

    /**
     * Finds a single category.
     *
     * @param int $id The id of the category
     * @return Category|null The category if it exists
     */
    public function getCategory(int $id): ?Category {
        return $this->categoryRepository->find($id);
    }

    /**
     * Saves a new Category.
     *
     * @param Category $category the new category
     */
    public function createCategory(Category $category): void {
        $this->doctrine->persist($category);
        $this->doctrine->flush();
    }

}
