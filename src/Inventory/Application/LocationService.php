<?php

namespace App\Inventory\Application;

use App\Inventory\Entity\Location;
use App\Inventory\Model\ItemTypeDto;
use App\Inventory\Repository\ItemTypeRepository;
use App\Inventory\Repository\LocationRepository;
use App\System\Application\MapperService;
use Doctrine\ORM\EntityManagerInterface;

class LocationService {

    private EntityManagerInterface $doctrine;
    private MapperService $mapper;

    private ItemTypeRepository $itemTypeRepository;
    private LocationRepository $locationRepository;

    public function __construct(
        EntityManagerInterface $doctrine,
        MapperService          $mapper,
        ItemTypeRepository     $itemTypeRepository,
        LocationRepository     $locationRepository,
    ) {
        $this->doctrine = $doctrine;
        $this->mapper = $mapper;

        $this->itemTypeRepository = $itemTypeRepository;
        $this->locationRepository = $locationRepository;
    }

    /**
     * Finds a single location.
     *
     * @param int $id The id of the location
     * @return Location|null The location if it exists
     */
    public function getLocation(int $id): ?Location {
        return $this->locationRepository->find($id);
    }

    /**
     * Finds all locations, ordered by name.
     *
     * @return Location[] all locations ordered by name.
     */
    public function getLocations(): array {
        return $this->locationRepository->findBy([], ['name' => 'ASC']);
    }

    /**
     * Saves a new item.
     *
     * @param Location $item the new item.
     */
    public function createLocation(Location $item): void {
        $this->doctrine->persist($item);
        $this->doctrine->flush();
    }

    /**
     * Finds all item types in a location.
     *
     * @param Location $location
     * @return ItemTypeDto[] all item types of the given location
     */
    public function getItemTypes(Location $location): array {
        $items = $this->itemTypeRepository->findWhereItemLocationEqualsOrderedByName($location);
        return $this->mapper->mapToObjects($items, ItemTypeDto::class);
    }

}
