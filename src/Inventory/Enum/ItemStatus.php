<?php

namespace App\Inventory\Enum;

/**
 * Describes the state of an individual item
 */
enum ItemStatus: string {
    case OK = 'ok';
    case DEFECTIVE = 'defective';
    case MISSING = 'missing';
}
