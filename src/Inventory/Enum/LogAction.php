<?php

namespace App\Inventory\Enum;

/**
 * All possible actions that can be applied to an item type
 */
enum LogAction: string {
    // item type changes
    case CREATED = 'created';

    case CHANGED_NAME = 'changed_name';
    case CHANGED_DESCRIPTION = 'changed_description';
    case CHANGED_OWNER = 'changed_owner';
    /**
     * @deprecated use {@link CATEGORY_ADDED} or {@link CATEGORY_REMOVED}
     */
    case CHANGED_CATEGORY = 'changed_category';
    case CATEGORY_ADDED = 'category_added';
    case CATEGORY_REMOVED = 'category_removed';

    case IMAGE_UPDATED = 'image_updated';
    case IMAGE_ADDED = 'image_added';
    case IMAGE_DELETED = 'image_deleted';

    // item changes
    case ITEM_ADDED = 'item_added';
    case CHANGED_BORROWED = 'changed_borrowed';
    case CHANGED_COUNT = 'changed_count';
    case CHANGED_NUMBER = 'changed_number';
    case CHANGED_LOCATION = 'changed_location';
    case CHANGED_NOTES = 'changed_notes';
    case CHANGED_DEFECTIVE = 'changed_defective';
    case CHANGED_MISSING = 'changed_missing';
    case CHANGED_DELETED = 'changed_deleted';
}
