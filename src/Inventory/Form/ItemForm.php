<?php

namespace App\Inventory\Form;

use App\Inventory\Entity\Item;
use App\Inventory\Entity\Location;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ItemForm extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('number', IntegerType::class, [
                'required' => false,
                'attr' => [
                    'min' => 1
                ],
                'label' => 'inventory.number'
            ])
            ->add('count', IntegerType::class, [
                'attr' => [
                    'min' => 1
                ],
                'label' => 'inventory.count'
            ])
            ->add('location', EntityType::class, [
                'class' => Location::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('l')
                        ->orderBy('l.name', 'ASC');
                },
                'choice_label' => 'name',
                'required' => true,
                'label' => 'inventory.location'
            ])
            ->add('notes', TextareaType::class, [
                'required' => false,
                'attr' => [
                    'rows' => 4
                ],
                'label' => 'inventory.notes'
            ])
            ->add('defective', CheckboxType::class, [
                'required' => false,
                'label' => 'inventory.item.defective'
            ])
            ->add('missing', CheckboxType::class, [
                'required' => false,
                'label' => 'inventory.item.missing'
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'save'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefault('data_class', Item::class);
    }
}
