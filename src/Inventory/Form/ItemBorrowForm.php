<?php

namespace App\Inventory\Form;

use App\Inventory\Repository\ItemRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;

class ItemBorrowForm extends AbstractType {
    private ItemRepository $itemRepository;

    public function __construct(ItemRepository $itemRepository) {
        $this->itemRepository = $itemRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('self', CheckboxType::class, [
                'required' => false,
                'label' => 'inventory.borrow.self'
            ])
            ->add('for', DatalistType::class, [
                'required' => false,
                'label' => 'inventory.borrow.for',
                'attr' => [
                    'placeholder' => 'name'
                ],
                'datalist' => $this->itemRepository->findAllBorrowedForDistinct()
            ])
            ->add('count', IntegerType::class, [
                'constraints' => [
                    new GreaterThanOrEqual(1)
                ],
                'attr' => [
                    'min' => 1
                ],
                'label' => 'inventory.count'
            ])
            ->add('notes', TextareaType::class, [
                'required' => false,
                'attr' => [
                    'rows' => 4
                ],
                'label' => 'inventory.notes'
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'inventory.borrow.label'
            ]);
    }
}
