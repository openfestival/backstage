<?php

namespace App\Inventory\Form;

use App\Inventory\Entity\Category;
use App\Inventory\Entity\ItemType;
use App\Inventory\Repository\ItemTypeRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class ItemTypeForm extends AbstractType {
    private ItemTypeRepository $itemTypeRepository;

    public function __construct(ItemTypeRepository $itemTypeRepository) {
        $this->itemTypeRepository = $itemTypeRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'required' => true,
                'constraints' => new NotBlank(),
                'attr' => [
                    'placeholder' => 'name'
                ],
                'label' => 'inventory.name'
            ])
            ->add('description', TextareaType::class, [
                'attr' => ['rows' => 10],
                'label' => 'description',
                'required' => false
            ])
            ->add('owner', DatalistType::class, [
                'label' => 'owner',
                'required' => true,
                'attr' => [
                    'placeholder' => 'owner'
                ],
                'datalist' => $this->itemTypeRepository->findAllOwnersDistinct()
            ])
            ->add('categories', EntityType::class, [
                'label' => 'category',
                'class' => Category::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->orderBy('c.name', 'ASC');
                },
                'choice_label' => 'name',
                'multiple' => true,
                'required' => true
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'save'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefault('data_class', ItemType::class);
    }
}
