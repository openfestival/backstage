<?php

namespace App\Inventory\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;

class ItemReturnForm extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('count', IntegerType::class, [
                'constraints' => [
                    new GreaterThanOrEqual(1)
                ],
                'attr' => [
                    'min' => 1
                ],
                'label' => 'inventory.count'
            ])
            ->add('notes', TextareaType::class, [
                'required' => false,
                'attr' => [
                    'rows' => 4
                ],
                'label' => 'inventory.notes'
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'inventory.return'
            ]);
    }
}
