<?php

namespace App\Inventory\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;

class ItemNumberForm extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('items', HiddenType::class)
            ->add('start', IntegerType::class, [
                'constraints' => [
                    new GreaterThanOrEqual(1)
                ],
                'attr' => [
                    'min' => 1
                ],
                'label' => 'inventory.numbering.start'
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'inventory.numbering.submit'
            ]);
    }
}
