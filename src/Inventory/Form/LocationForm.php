<?php

namespace App\Inventory\Form;

use App\Inventory\Entity\Location;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class LocationForm extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'required' => true,
                'constraints' => new NotBlank(),
                'attr' => [
                    'placeholder' => 'name'
                ]
            ])
            ->add('description', TextareaType::class, [
                'attr' => ['rows' => 10],
                'label' => 'description',
                'required' => true,
                'constraints' => new NotBlank()
            ])
            ->add('delete', SubmitType::class, [
                'label' => 'delete'
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'save'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefault('data_class', Location::class);
    }
}
