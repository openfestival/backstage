<?php

namespace App\Inventory\Form;

use App\Inventory\Entity\ItemTypeImage;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ItemTypeImageForm extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('image', VichImageType::class, [
                'label' => 'image',
                'constraints' => [
                    new Image([
                        'maxSize' => '10M',
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/png',
                        ],
                        'minRatio' => '0.33',
                        'maxRatio' => 3
                    ])
                ],
                'delete_label' => 'delete',
                'download_uri' => false,
                'translation_domain' => 'messages'
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'save'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefined('placeholder');
        $resolver->setDefaults([
            'data_class' => ItemTypeImage::class
        ]);
    }
}
