<?php

namespace App\Inventory\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DatalistType extends AbstractType {
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefined('datalist');
        $resolver->setAllowedTypes('datalist', 'string[]');
        $resolver->setRequired('datalist');
    }

    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        $view->vars['datalist'] = $options['datalist'];
    }

    public function getParent(): string {
        return TextType::class;
    }
}
