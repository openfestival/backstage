<?php

namespace App\Inventory\Validator;

use App\Inventory\Entity\Item;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

/**
 * Checks that item is well-formed (see comments in validate method)
 */
class IsWellFormedItemValidator extends ConstraintValidator {
    public function validate(mixed $value, Constraint $constraint): void
    {
        if (!$constraint instanceof IsWellFormedItem)
            throw new UnexpectedTypeException($constraint, IsWellFormedItem::class);

        if (!$value instanceof Item)
            throw new UnexpectedValueException($value, Item::class);

        // if the item has a number, it must have a count of one
        if ($value->getNumber() != null && $value->getCount() != 1) {
            $this->context->buildViolation('Numbered items must have a count of one!')->addViolation();
        }

        // if the item is deleted, it cannot be borrowed by somebody
        if ($value->getDeleted() && $value->getBorrowedBy() != null) {
            $this->context->buildViolation('Deleted items cannot be borrowed!')->addViolation();
        }
    }
}
