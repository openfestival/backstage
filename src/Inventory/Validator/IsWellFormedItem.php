<?php

namespace App\Inventory\Validator;

use App\Inventory\Entity\Item;
use Symfony\Component\Validator\Constraint;

/**
 * Used to annotate {@link Item}s
 *
 * @Annotation
 */
class IsWellFormedItem extends Constraint {
    public function getTargets(): string {
        return self::CLASS_CONSTRAINT;
    }
}
