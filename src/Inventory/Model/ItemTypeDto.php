<?php

namespace App\Inventory\Model;

/**
 * Represents an item type with the count of all items for this type
 */
class ItemTypeDto {
    private int $id;
    private string $name;
    private ?string $description;
    private ?string $owner;
    private ?int $count;

    public function __construct(int $id, string $name, ?string $description, ?string $owner, ?int $count) {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
        $this->owner = $owner;
        $this->count = $count;
    }

    public function getId(): int {
        return $this->id;
    }

    public function getName(): string {
        return $this->name;
    }

    public function getDescription(): ?string {
        return $this->description;
    }

    public function getOwner(): ?string {
        return $this->owner;
    }

    public function getCount(): ?int {
        return $this->count;
    }
}
