import { put } from './api';
import { Alert } from 'bootstrap';

import '../css/app_sidebar.scss'

// Maximum widths at which the screen is considered inside the given breakpoint
// see https://getbootstrap.com/docs/5.1/layout/breakpoints/#max-width for specifics
const maxGridBreakpoints = {
    sm: '575.98px',
    md: '767.98px',
    lg: '991.98px',
    xl: '1199.98px',
    xxl: '1399.98px'
}

/**
 * Checks if the current screen size is at most the given width.
 *
 * @param size any css width. For example {@link maxGridBreakpoints.md}.
 * @returns {boolean} true, if the screen is the given size or smaller
 */
function matchScreenMaxWidth(size) {
    return window.matchMedia('screen and (max-width: ' + size + ')').matches;
}

/**
 * Closes all dismissible alerts one at a time. Waits for each alert to be closed and CSS transitions to complete before closing the next.
 */
function closeAlerts() {
    const alert = document.querySelector('.alert.alert-dismissible');
    if (alert != null) {
        // close next alert after this has finished closing
        alert.addEventListener('closed.bs.alert', () => closeAlerts());
        Alert.getOrCreateInstance(alert).close();
    }
}

window.addEventListener('DOMContentLoaded', () => {
    // sidebar behaviour
    const sidebar = document.getElementById('sidebar');
    // listen to toggle button clicks
    document.querySelector('button[data-sidebar-toggler]').addEventListener('click', () => {
        let sidebarVisible;
        // if the screen size is small use the toggled class, otherwise use the collapsed class
        if (matchScreenMaxWidth(maxGridBreakpoints.sm)) {
            sidebarVisible = sidebar.classList.toggle('toggled');
        } else {
            sidebarVisible = !sidebar.classList.toggle('collapsed');
            // save in session
            put('session', {
                sidebarCollapsed: !sidebarVisible
            });
        }
        // focus sidebar if it is visible
        if (sidebarVisible)
            sidebar.focus();
    });
    // remove the toggled state when the content is clicked
    document.querySelector('.app-wrapper').addEventListener('click', () => {
        sidebar.classList.remove('toggled');
    });

    // remove preload class from elements after some time
    setTimeout(() => {
        document.querySelectorAll('.preload')
            .forEach(e => e.classList.remove('preload'));
    }, 500);

    // close dismissible alerts after 10 seconds
    setTimeout(() => closeAlerts(), 10 * 1000);

    // stretched table links
    document.querySelectorAll('tr[data-link-target]').forEach(tr => {
        tr.addEventListener('click', event => {
            // if the clicked element was not a link by itself
            if (event.target.tagName !== 'I' && event.target.tagName !== 'A') {
                event.preventDefault();
                // find target link via css selector
                const selector = tr.dataset.linkTarget;
                let target;
                if (selector.startsWith('#'))
                    target = document.getElementById(selector.substring(1));
                else target = document.querySelector(CSS.escape(selector));
                // click link
                target.click();
            }
        });
    });
});
