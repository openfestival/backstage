import {get} from './api';

document.addEventListener('DOMContentLoaded', () => {
    // find all selected shifts and show the times
    document.querySelectorAll('div[data-shift]').forEach(div => {
        const shift = div.getAttribute('data-shift');
        const check = document.querySelector(`input[type="checkbox"][data-shift="${shift}"]`);
        if (check.checked) {
            div.classList.remove('d-none');
        }
    });

    // listen to date select change
    document.querySelectorAll('input[type="checkbox"][data-shift]').forEach(checkbox => {
        checkbox.addEventListener('change', e => {
            const shiftId = e.target.dataset.shift;
            document.querySelectorAll(`div[data-shift="${shiftId}"]`).forEach(div => {
                div.classList.toggle('d-none');
            })
        })
    });

    // check for duplicate email addresses
    const email = document.getElementById('volunteer_form_email');
    const popover = new bootstrap.Popover(email, {
        placement: 'top',
        html: true,
        title: 'Diese E-Mail Adresse existiert bereits!',
        content: 'Bitte <a href="/login">melde dich an</a> bevor du weitermachst.',
        trigger: 'manual',
        template: `
            <div class="popover" role="tooltip">
                <div class="popover-arrow"></div>
                <h3 class="popover-header text-danger"></h3>
                <div class="popover-body"></div>
            </div>`
    });
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    email.addEventListener('change', () => {
        // check for duplicate email addresses only if the current value actually is an email address
        if (!emailRegex.test(email.value)) {
            popover.hide();
            return;
        }
        get('volunteer/exists', {
            email: email.value
        }).then(exists => {
            if (exists)
                popover.show();
            else popover.hide();
        }).catch(() => {
            // hide in case we hit the rate limit to prevent confusion
            popover.hide();
        })
    });

    // open send modal if password was the only wrong form field
    const invalidForms = document.querySelectorAll('input.is-invalid');
    if (invalidForms.length > 0) {
        let passwordError = true;
        invalidForms.forEach(e => passwordError = passwordError && e.type === 'password');
        if (passwordError)
            new bootstrap.Modal('#account-modal').show();
    }
});
