/**
 * Sends an HTTP request to the api.
 *
 * @param method the HTTP method to use
 * @param path the API path to use
 * @param data data to send. Gets converted to search parameters for GET requests and to JSON for all others
 * @returns {Promise<Object>} promise resolving to the response body
 */
function request(method, path, data) {
    const url = new URL(window.location.protocol + '/' + window.location.host + '/api/' + path);
    // convert data to search parameters for GET requests
    if (method === 'GET' && data != null) {
        for (const key in data) {
            const value = data[key]
            if (Array.isArray(value)) {
                // if value is an array, append brackets so symfony understands it as one parameter
                value.forEach(v => url.searchParams.append(key + '[]', v));
            } else url.searchParams.append(key, value);
        }
        data = null;
    }
    return new Promise((resolve, reject) => {
        const req = new XMLHttpRequest();
        req.open(method, url.toString());
        req.setRequestHeader('Content-Type', 'application/json');
        req.onload = () => {
            if (req.status === 200) {
                let response = req.response;
                if (response !== '')
                    response = JSON.parse(response);
                resolve(response);
            } else {
                let response = req.response;
                if (response !== '') {
                    response = JSON.parse(response);
                    console.error(response.detail);
                    reject(Error(response.detail));
                } else {
                    reject(Error(req.statusText));
                }
            }
        };
        req.onerror = () => reject(Error('Network Error'));
        req.send(data == null ? null : JSON.stringify(data));
    });
}

function get(path, data = null) {
    return request('GET', path, data);
}

function post(path, data = null) {
    return request('POST', path, data);
}

function del(path, data = null) {
    return request('DELETE', path, data);
}

function put(path, data = null) {
    return request('PUT', path, data);
}

/**
 * Sends a GET request to the given path, expecting a Link HTTP header in the response pointing to the Mercure Hub.
 * Creates an EventSource with the discovered Mercure Hub and the given topic(s).
 *
 * @param {string} path the path without leading `/api/`
 * @param {string|string[]} topics the topics without leading slash
 * @param {function} onmessage consumer for the first response and future messages
 * @returns {Promise<EventSource>} a promise which resolves with the EventSource
 */
function openSSE(path, topics, onmessage) {
    return new Promise((resolve, reject) => {
        const req = new XMLHttpRequest();
        req.open('GET', '/api/' + path);
        req.setRequestHeader('Accept', 'application/json');
        req.onload = () => {
            if (req.status !== 200)
                reject(Error(req.statusText));

            onmessage(JSON.parse(req.response));

            const hubUrl = req.getResponseHeader('Link').match(/<([^>]+)>;\s+rel=(?:mercure|"[^"]*mercure[^"]*")/)[1];
            const hub = new URL(hubUrl);
            const topicURI = req.getResponseHeader('X-Mercure-Topic-Uri');
            if (Array.isArray(topics))
                topics.forEach(appendTopic);
            else appendTopic(topics);
            const eventSource = new EventSource(hub.toString(), {withCredentials: true});
            eventSource.onmessage = msg => onmessage(JSON.parse(msg.data));
            eventSource.onerror = error => reject(error);
            eventSource.onopen = () => resolve(eventSource);

            function appendTopic(topic) {
                hub.searchParams.append('topic', topicURI + '/' + topic);
            }
        };
        req.onerror = () => reject(Error('Network Error'));
        req.send();
    });
}

export {get, post, del, put, openSSE};
