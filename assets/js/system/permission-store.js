import { defineStore } from 'pinia';
import { get } from '../api.js';
import AsyncLock from 'async-lock';

const lock = new AsyncLock();

export const usePermissionStore = defineStore('permission', {
    state: () => ({
        cache: null,
    }),

    actions: {
        async fetchPermissions() {
            this.cache = await get('v1/security/permissions');
        }
    },

    getters: {
        isGranted: (state) => {
            return (permission) => {
                if (state.cache == null) {
                    return false;
                }

                // search for the exact permission string
                if (state.cache.includes(permission)) {
                    return true;
                }
                // check for global wildcard
                if (state.cache.includes('*')) {
                    return true;
                }

                // search for wildcard .* permissions
                const parts = permission.split('.');
                while (parts.length > 1) {
                    parts.pop();
                    if (state.cache.includes(parts.join('.') + '.*')) {
                        return true;
                    }
                }
                return false;
            };
        },
    }
});
