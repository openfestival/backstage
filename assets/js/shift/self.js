import { post } from '../api.js';

window.addEventListener('DOMContentLoaded', () => {
    document.querySelectorAll('input[name="confirm"]').forEach(input => input.addEventListener('change', e => {
        const input = e.currentTarget;
        const shift = input.dataset.shift;

        const td = input.parentNode.parentNode;
        while (td.firstChild)
            td.removeChild(td.firstChild);
        const spinner = document.createElement('i');
        spinner.classList.add('fas', 'fa-spin', 'fa-spinner');
        td.appendChild(spinner);
        post('confirmshift', {
            shift: parseInt(shift)
        }).then(() => {
            td.removeChild(spinner);
            const lock = document.createElement('i');
            lock.classList.add('fas', 'fa-lock', 'text-success');
            td.appendChild(lock);
            td.appendChild(document.createTextNode(' Bestätigt'));
        })
    }));
});
