import {get, post} from '../api';

const form_day = document.getElementById('shift_form_day');
const form_start_year = document.getElementById('shift_form_start_date_year');
const form_start_month = document.getElementById('shift_form_start_date_month');
const form_start_day = document.getElementById('shift_form_start_date_day');
const form_end_year = document.getElementById('shift_form_end_date_year');
const form_end_month = document.getElementById('shift_form_end_date_month');
const form_end_day = document.getElementById('shift_form_end_date_day');

// listen for changes in shift day and change dates of start and end accordingly
setStartEnd(form_day.value);
form_day.onchange = event => {
    setStartEnd(event.target.value);
};

function setStartEnd(day_id) {
    const day = document.getElementById('festival-day-' + day_id);
    form_start_year.value = day.dataset.year;
    form_end_year.value = day.dataset.year;
    form_start_month.value = day.dataset.month;
    form_end_month.value = day.dataset.month;
    const diff = parseInt(form_end_day.value) - parseInt(form_start_day.value);
    form_start_day.value = day.dataset.day;
    form_end_day.value = parseInt(day.dataset.day) + diff;
}

// listen for changes in start hours and change end hour accordingly
const form_start_hour = document.getElementById('shift_form_start_time_hour');
const form_end_hour = document.getElementById('shift_form_end_time_hour');
let hour_diff = parseInt(form_end_hour.value) - parseInt(form_start_hour.value);
if (hour_diff < 0)
    hour_diff = hour_diff + 24;
if (hour_diff === 0)
    hour_diff = 4;
form_start_hour.onchange = event => {
    if (form_end_hour.dataset.manual === 'true')
        return;

    const start = parseInt(event.target.value);
    const end = (start + hour_diff) % 24;
    form_end_hour.value = end;
    if (end < start) {
        form_end_day.value = parseInt(form_start_day.value) + 1;
    } else {
        form_end_day.value = form_start_day.value;
    }
};
// if end hour was set manually, don't change it automatically anymore
form_end_hour.onchange = () => {
    form_end_hour.dataset.manual = 'true';
};

// listen for changes in start minutes and change end minutes accordingly
const form_start_minute = document.getElementById('shift_form_start_time_minute');
const form_end_minute = document.getElementById('shift_form_end_time_minute');
let minute_diff = parseInt(form_end_minute.value) - parseInt(form_start_minute.value);
if (minute_diff < 0)
    minute_diff = minute_diff + 60;
form_start_minute.onchange = event => {
    if (form_end_minute.dataset.manual === 'true')
        return;

    const v = parseInt(event.target.value);
    form_end_minute.value = (v + minute_diff) % 60;
};
// if end minutes were set manually, don't change them automatically anymore
form_end_minute.onchange = () => {
    form_end_minute.dataset.manual = 'true';
};

document.querySelectorAll('.shift-table-volunteer')
    .forEach(div => div.addEventListener('click', () => {
        const expanded = div.dataset.expanded;
        // if expanded ignore click
        if (expanded)
            return;
        div.dataset.expanded = 'true';

        // clear contents
        while (div.firstChild)
            div.removeChild(div.firstChild);

        const shift = div.dataset.shift;
        const selected = div.dataset.selected;
        const outsideClickListener = getOutsideClickListener(div);
        document.addEventListener('click', outsideClickListener);

        // append select
        const select = document.getElementById('shift-volunteer-select').cloneNode(true);
        for (const o of select.options) {
            if (o.value === selected)
                o.setAttribute('selected', true);
        }
        div.appendChild(select);

        // on select change
        select.addEventListener('change', () => {
            // remove select
            div.removeChild(select);
            // append loading icon
            div.appendChild(createIcon('fa-spin', 'fa-spinner'));
            // send post request
            post('shiftvolunteer', {
                shift: parseInt(shift),
                volunteer: parseInt(select.value) || null
            }).then(response => {
                fillShiftInfo(div, response);
                // update all other shifts
                updateOtherShifts(div, response.volunteerId);
                // update all other shifts of old volunteer
                if (selected !== response.volunteerId)
                    updateOtherShifts(div, selected);
            });

            document.removeEventListener('click', outsideClickListener);
        });
    }));

function updateOtherShifts(div, volunteerId) {
    document.querySelectorAll('.shift-table-volunteer[data-selected="' + volunteerId + '"]').forEach(d => {
        if (d !== div) {
            get('shiftvolunteer/' + d.dataset.shift).then(response => fillShiftInfo(d, response));
        }
    })
}

function getOutsideClickListener(div) {
    const outsideClickListener = function (event) {
        if (div.dataset.expanded && !div.contains(event.target)) {
            while (div.firstChild)
                div.removeChild(div.firstChild);
            const icon = createIcon('fa-spin', 'fa-spinner');
            div.appendChild(icon);
            get('shiftvolunteer/' + div.dataset.shift).then(response => fillShiftInfo(div, response));
            document.removeEventListener('click', outsideClickListener);
        }
    };
    return outsideClickListener;
}

function fillShiftInfo(div, response) {
    // remove contents
    while (div.firstChild)
        div.removeChild(div.firstChild);
    // set contents according to response
    if (response) {
        div.appendChild(document.createTextNode(response.volunteerName));
        const badge = document.createElement('span');
        badge.classList.add('ms-1', 'badge', 'rounded-pill', 'bg-dark');
        badge.innerHTML = response.shifts.length;
        div.appendChild(badge);
        if (response.conflict)
            div.appendChild(createIcon('fa-exclamation-triangle', 'text-danger'));
    } else {
        const i = document.createElement('i');
        i.innerHTML = 'Frei';
        div.appendChild(i);
    }
    // update dataset
    div.dataset.selected = response.volunteerId;
    delete div.dataset.expanded;
}

function createIcon(...icon) {
    const i = document.createElement('i');
    i.classList.add('ms-1', 'fas', ...icon);
    return i;
}
