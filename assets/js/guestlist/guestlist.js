import { createApp } from 'vue';
import { createPinia } from 'pinia';
import GuestListTable from './GuestListTable.vue';

const pinia = createPinia();
createApp(GuestListTable)
    .use(pinia)
    .mount('#vue-guestlist');
