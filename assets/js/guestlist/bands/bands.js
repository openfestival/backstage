import { createApp } from 'vue';
import { createPinia } from 'pinia';
import BandsTable from './BandsTable.vue';

const pinia = createPinia();
createApp(BandsTable)
    .use(pinia)
    .mount('#vue-bands');
