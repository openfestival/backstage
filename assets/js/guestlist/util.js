function getDayLabel(timestamp) {
    return new Date(timestamp * 1000).toLocaleDateString('de-DE', { weekday: 'long' });
}

export { getDayLabel };
