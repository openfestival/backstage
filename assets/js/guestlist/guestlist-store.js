import { defineStore } from 'pinia';
import { del, get, openSSE, post, put } from '../api.js';
import { useFetch } from '@vueuse/core';

export const useGuestListStore = defineStore('guestList', {
    state: () => ({
        guestList: [],
        travelParties: [],
        bands: [],
        inviters: [],
        showAcceptedOnly: true,
        showBackstageOnly: false,
        search: '',
        currentEdit: null,
    }),

    actions: {
        // guestlist
        async fetchAndSubscribeGuestlist(festivalId) {
            await openSSE('v1/guestlist/list/' + festivalId, 'guestlist/' + festivalId, message => {
                if (Array.isArray(message)) {
                    // replace all values if the message was an array (e.g. initial fetch on page load)
                    this.guestList = message;
                } else {
                    // update or add individual entry if message is a single object
                    this._updateEntry(message);
                }
            });
        },

        async fetchInviters() {
            useFetch('api/v1/guestlist/inviters/list').get().json().then(({data}) => {
                this.inviters = data.value;
            });
        },

        _updateEntry(entry) {
            const index = this.guestList.findIndex(e => e.id === entry.id);
            // if entry was not found and is marked to delete, do nothing
            if (index === -1 && entry.delete)
                return;
            // otherwise add entry if not found
            if (index === -1)
                this.guestList.push(entry);
            // or delete
            else if (entry.delete)
                this.guestList.splice(index, 1);
            // or replace
            else this.guestList[index] = entry;
        },

        async deleteEntry(id) {
            await del('guestlist/delete/' + id);
        },

        // bands

        async fetchBands() {
            this.bands = await get('bandcheckin/bands');
        },

        async createBand(data) {
            this.bands.push(await post('bandcheckin/bands', data));
        },

        async updateBand(id, data) {
            const band = await put('bandcheckin/bands/edit/' + id, data);
            const index = this.bands.findIndex((b) => b.id === band.id);
            this.bands[index] = band;
        },

        async deleteBand(id) {
            await del('bandcheckin/bands/delete/' + id);
            const index = this.bands.findIndex((b) => b.id === id);
            this.bands.splice(index, 1);
        },

        // travel parties

        async fetchAndSubscribeTravelParties(festivalId) {
            await openSSE('bandcheckin/parties/' + festivalId, 'bandcheckin/parties/' + festivalId, message => {
                if (Array.isArray(message)) {
                    // replace all values if the message was an array (e.g. initial fetch on page load)
                    this.travelParties = message;
                } else {
                    // update or add individual party if message is a single object
                    this._updateParty(message);
                }
            });
        },

        async createTravelParty(festivalId, data) {
            const party = await post('bandcheckin/parties/' + festivalId, data);
            this._updateParty(party);
        },

        async updateTravelParty(id, data) {
            const party = await put('bandcheckin/parties/edit/' + id, data);
            this._updateParty(party);
        },

        async deleteTravelParty(id){
            await del('bandcheckin/parties/delete/' + id);
            this._updateParty({ id, delete: true })
        },

        _updateParty(party) {
            const index = this.travelParties.findIndex(p => p.id === party.id);
            // if entry was not found and is marked to delete, do nothing
            if (index === -1 && party.delete)
                return;
            // otherwise add entry if not found
            if (index === -1)
                this.travelParties.push(party);
            // or delete
            else if (party.delete)
                this.travelParties.splice(index, 1);
            // or replace
            else this.travelParties[index] = party;
        },
    },

    getters: {
        guestListSorted: (state) => {
            return state.guestList.sort((a, b) => a.name.localeCompare(b.name));
        },

        guestListFiltered: (state) => {
            return state.guestList
                // filter by search criteria
                .filter(gl =>
                    // check if state should be filtered
                    (!state.showAcceptedOnly || gl.state === 'accepted')
                    // check if backstage only is switched on
                    && (!state.showBackstageOnly || gl.days.find(day => day.backstage) !== undefined)
                    // perform search if it is not empty
                    && (
                        !state.search
                        || gl.name.toLowerCase().includes(state.search.toLowerCase())
                        || (gl.origin != null && gl.origin.toLowerCase().includes(state.search.toLowerCase()))
                        || (gl.comment != null && gl.comment.toLowerCase().includes(state.search.toLowerCase()))
                    )
                )
                // sort by name
                .sort((a, b) => a.name.localeCompare(b.name));
        },

        travelPartiesSorted: (state) => {
            return state.travelParties.sort((a, b) => {
                if (a.checkedIn && !b.checkedIn)
                    return 1;
                if (!a.checkedIn && b.checkedIn)
                    return -1;
                return Date.parse(a.stageTime) - Date.parse(b.stageTime);
            })
        },

        travelPartyGuestList: (state) => {
            return (party) => state.guestList
                .filter(entry => party.guestlist.includes(entry.id))
                .sort((a, b) => a.name.localeCompare(b.name));
        },

        bandsSorted: (state) => {
            return state.bands.sort((a, b) => a.name.localeCompare(b.name));
        },
    }
});
