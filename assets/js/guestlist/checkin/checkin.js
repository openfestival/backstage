import { createApp } from 'vue';
import { createPinia } from 'pinia';
import CheckinTable from './CheckinTable.vue';

const pinia = createPinia();
createApp(CheckinTable)
    .use(pinia)
    .mount('#vue-checkin');
