import { Carousel, Toast } from 'bootstrap';

/**
 * Enables or disables all buttons named "action".
 *
 * @param {boolean} enable true to enable the buttons, false to disable the buttons
 */
function enableActionButtons(enable) {
    document.querySelectorAll('button[name="action"]')
        .forEach(b => b.disabled = !enable);
}

window.addEventListener('DOMContentLoaded', () => {
    // decode all items for later use
    const items = [];
    document.querySelectorAll('div[data-item]').forEach(e => {
        const item = JSON.parse(e.dataset.item);
        items[item.id] = item;
    });

    // find all item checkboxes
    const itemCheckboxes = document.querySelectorAll('input[name="items[]"]');

    // watch for select all checkbox
    const itemSelectAll = document.getElementById('item-select-all');
    if (itemSelectAll != null) {
        itemSelectAll.addEventListener('change', () => {
            // select all items or none
            itemCheckboxes.forEach(i => i.checked = itemSelectAll.checked);
            enableActionButtons(itemSelectAll.checked);
        });
    }

    // watch for individual item checkboxes
    itemCheckboxes.forEach(i => i.addEventListener('change', () => {
        const itemCheckboxesArray = Array.from(itemCheckboxes);
        // enable/disable all actions if at least one item is selected
        enableActionButtons(itemCheckboxesArray.some(input => input.checked));
        // set select all checkbox if any individual item is not selected
        itemSelectAll.checked = !itemCheckboxesArray.some(input => !input.checked);
    }));

    // number form management
    const itemNumberStart = document.getElementById('item_number_form_start');
    const itemNumberItems = document.getElementById('item_number_form_items');
    const itemNumberSelected = document.getElementById('item_number_form_selected');
    let itemNumberCount = 0;
    const itemNumberRange = document.getElementById('item_number_form_range');

    function updateNumberRange(start, count) {
        if (isNaN(start))
            itemNumberRange.textContent = '';
        else itemNumberRange.textContent = '#' + start + ' bis #' + (start + count - 1);
    }

    document.getElementById('modal-number').addEventListener('show.bs.modal', () => {
        const selected = Array.from(itemCheckboxes)
            .filter(input => input.checked)
            .map(input => parseInt(input.value));

        // sum up count of all selected items
        itemNumberCount = selected.map(id => items[id].count).reduce((sum, count) => sum + count, 0);
        itemNumberSelected.textContent = itemNumberCount;
        itemNumberItems.value = selected.join(',');
        updateNumberRange(parseInt(itemNumberStart.value), itemNumberCount)
    });
    itemNumberStart.addEventListener('input', () => updateNumberRange(parseInt(itemNumberStart.value), itemNumberCount));

    // item form management
    const itemForm = document.querySelector('form[name="item_form"]');
    const itemFormNumberToggle = document.getElementById('item_form_number_toggle');
    const itemFormHeader = document.getElementById('item_form_header');
    const itemFormNumber = document.getElementById('item_form_number');
    const itemFormCount = document.getElementById('item_form_count');
    const itemFormLocation = document.getElementById('item_form_location');
    const itemFormNotes = document.getElementById('item_form_notes');
    const itemFormDefective = document.getElementById('item_form_defective');
    const itemFormMissing = document.getElementById('item_form_missing');
    // set form values on modal open
    document.getElementById('modal-item').addEventListener('show.bs.modal', e => {
        const button = e.relatedTarget;
        itemForm.setAttribute('action', button.dataset.action);
        if ('item' in button.dataset) {
            const item = items[button.dataset.item];
            // show item number input toggle if the item has a number
            if (itemFormNumberToggle != null)
                itemFormNumberToggle.classList.toggle('d-none', item.number == null)

            // fill in current values
            itemFormHeader.textContent = item.displayName + ' bearbeiten';
            itemFormNumber.value = item.number;
            itemFormNumber.toggleAttribute('readonly', item.number != null || item.count !== 1);
            itemFormCount.value = item.count;
            itemFormCount.toggleAttribute('readonly', item.number != null);
            itemFormLocation.value = item.location;
            itemFormNotes.value = item.notes;
            itemFormDefective.checked = item.defective;
            itemFormMissing.checked = item.missing;
        } else {
            // hide item number input toggle
            if (itemFormNumberToggle != null)
                itemFormNumberToggle.classList.add('d-none');

            // reset inputs
            itemFormHeader.textContent = 'Neuen Artikel hinzufügen';
            itemFormNumber.value = '';
            itemFormNumber.removeAttribute('readonly');
            itemFormCount.value = 1;
            itemFormCount.removeAttribute('readonly');
            itemFormNotes.value = '';
            itemFormDefective.checked = false;
            itemFormMissing.checked = false;
        }
    });
    // toggle count and number fields readonly attributes
    if (itemFormNumberToggle != null) {
        itemFormNumberToggle.addEventListener('click', () => {
            itemFormNumber.toggleAttribute('readonly');
        });
    }
    itemFormNumber.addEventListener('input', () => {
        // disable count field if any number is set
        itemFormCount.toggleAttribute('readonly', itemFormNumber.value !== '');
    });
    itemFormCount.addEventListener('input', () => {
        // disable number field if count is greater than 1
        itemFormNumber.toggleAttribute('readonly', itemFormCount.value > 1);
    });
    // we always have at least one item
    itemFormCount.addEventListener('change', () => {
        // set count to 1 if it is empty
        if (itemFormCount.value === '')
            itemFormCount.value = 1;
    });

    // borrow form management
    const borrowForm = document.querySelector('form[name="item_borrow_form"]');
    const borrowFormName = document.getElementById('item_borrow_form_name');
    const borrowFormSelf = document.getElementById('item_borrow_form_self');
    const borrowFormFor = document.getElementById('item_borrow_form_for');
    const borrowFormCount = document.getElementById('item_borrow_form_count');
    const borrowFormNotes = document.getElementById('item_borrow_form_notes');
    const borrowFormToast = Toast.getOrCreateInstance(document.getElementById('item_borrow_form_toast'));
    // set form values on modal open
    document.getElementById('modal-borrow').addEventListener('show.bs.modal', e => {
        const button = e.relatedTarget;
        const item = items[button.dataset.item];
        borrowForm.setAttribute('action', button.dataset.action);
        borrowFormName.textContent = item.displayName;
        borrowFormCount.value = item.count;
        borrowFormCount.setAttribute('max', item.count);
        borrowFormCount.toggleAttribute('readonly', item.count === 1)
        borrowFormNotes.value = item.notes;
    });
    // toggle self and for fields
    borrowFormSelf.addEventListener('change', () => {
        borrowFormFor.toggleAttribute('disabled', borrowFormSelf.checked);
        if (borrowFormSelf.checked)
            borrowFormFor.value = '';
    });
    // prevent form submit if self is not checked and for is empty
    borrowForm.addEventListener('submit', event => {
        if (!borrowFormSelf.checked && /^\s*$/.test(borrowFormFor.value)) {
            borrowFormToast.show();
            borrowFormSelf.focus();
            event.preventDefault();
        }
    });

    // return form management
    const returnForm = document.querySelector('form[name="item_return_form"]');
    const returnFormName = document.getElementById('item_return_form_name');
    const returnFormCount = document.getElementById('item_return_form_count');
    const returnFormNotes = document.getElementById('item_return_form_notes');
    // set form values on modal open
    document.getElementById('modal-return').addEventListener('show.bs.modal', e => {
        const button = e.relatedTarget;
        const item = items[button.dataset.item];
        returnForm.setAttribute('action', button.dataset.action);
        returnFormName.textContent = item.displayName;
        returnFormCount.value = item.count;
        returnFormCount.setAttribute('max', item.count);
        returnFormCount.toggleAttribute('readonly', item.count === 1)
        returnFormNotes.value = item.notes;
    });

    // item notes
    document.querySelectorAll('button[data-toggle="notes"]')
        .forEach(button => button.addEventListener('click', () => {
            document.getElementById(`item-notes-${button.dataset.item}`).classList.toggle('collapse');
        }));

    // images
    // scroll to correct image when gallery modal is opened
    const imageCarousel = Carousel.getOrCreateInstance(document.getElementById('galleryCarousel'));
    document.getElementById('modal-gallery').addEventListener('show.bs.modal', e => {
        imageCarousel.to(e.relatedTarget.dataset.index);
    });
    // set correct image delete link when delete modal is opened
    const imageDeleteLink = document.getElementById('item_action_image_delete');
    document.getElementById('modal-image-delete').addEventListener('show.bs.modal', e => {
        imageDeleteLink.setAttribute('href', e.relatedTarget.dataset.action);
    });
});
