import Vue from 'vue';
import ItemScanner from './item-scanner.vue';

new Vue({
   el: '#vue-scanner',
   components: {
       ItemScanner
   }
});
