import { defineStore } from 'pinia';
import { useFetch } from '@vueuse/core';

export const useFestivalStore = defineStore('festival', {
    state: () => {
        return {
            festivals: {},
            currentFestival: null,
        }
    },

    actions: {
        async fetchCurrentFestival() {
            const {data} = await useFetch(`/api/v1/festival/current`).get().json();
            const festival = data.value;
            this.festivals[festival.id] = festival;
            this.currentFestival = festival;
            return festival;
        },

        async fetchFestival(festivalId) {
            const {data} = await useFetch(`/api/v1/festival/${festivalId}`).get().json();
            const festival = data.value;
            this.festivals[festival.id] = festival;
            return festival;
        },
    },

    getters: {
        currentFestivalDays: (state) => {
            if (!state.currentFestival) {
                return [];
            }
            const festival = state.currentFestival;
            return festival.days
                .filter(day => day.date >= festival.start && day.date <= festival.end)
                .sort((a, b) => a.date - b.date);
        },
    }
});
