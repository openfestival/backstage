import {createApp} from 'vue';
import DayList from './DayList.vue';

createApp(DayList)
    .mount('#vue-days');
