import {get, post} from '../api';
import {Modal} from 'bootstrap';

let volunteers = [];
let status = 'writing';

window.addEventListener('DOMContentLoaded', () => {
    // on send disable form, query all recipients and show confirm button
    document.getElementById('email-send').onclick = () => {
        const recStatus = Array.from(document.querySelectorAll('input[name="status"]'));
        const recGroups = Array.from(document.querySelectorAll('input[name="groups"]'));
        const subject = document.getElementById('email-subject');
        const body = document.getElementById('email-body');
        const send = document.getElementById('email-send');

        recStatus.forEach(btn => btn.setAttribute('disabled', ''));
        recGroups.forEach(btn => btn.setAttribute('disabled', ''));
        subject.setAttribute('disabled', '');
        body.setAttribute('disabled', '');
        send.setAttribute('disabled', '');

        status = 'preparing';

        // query recipients
        get('volunteer/ids', {
            status: recStatus.filter(input => input.checked).map(input => input.value),
            group: recGroups.filter(input => input.checked).map(input => input.value)
        }).then(ids => {
            volunteers = ids;

            // hide send button and display confirm button
            send.style.display = 'none';
            const confirm = document.getElementById('email-send-confirm');
            confirm.style.display = 'inherit';
            const persons = document.getElementById('email-persons');
            persons.innerText = `${ids.length}`;
        });
    };

    // on confirm send emails, update progress
    document.getElementById('email-send-confirm').onclick = () => {
        const subject = document.getElementById('email-subject');
        const body = document.getElementById('email-body');
        const confirm = document.getElementById('email-send-confirm');
        const cancel = document.getElementById('email-cancel');
        const close = document.getElementById('email-close');

        // hide confirm, show progress bar
        confirm.style.display = 'none';
        const max = volunteers.length;

        const progressDiv = document.getElementById('email-progress-div');
        progressDiv.style.display = 'inherit';
        const progressBar = document.getElementById('email-progress');
        progressBar.setAttribute('area-valuemin', '0');
        progressBar.setAttribute('area-valuemax', `${max}`);
        progressBar.classList.remove('bg-danger');
        progressBar.classList.add('bg-success', 'progress-bar-animated');
        progressBar.textContent = `0/${max}`;

        status = 'sending';

        let i = 0;
        // send emails one after the other
        volunteers.reduce((p, volunteer) => {
            // resolve next email
            return p.then(() => {
                // update progress bar
                progressBar.setAttribute('area-valuenow', `${i}`);
                progressBar.style.width = (i / max) * 100 + '%';
                progressBar.textContent = `${i}/${max}`;

                // if status is still sending, send the next email
                if (status === 'sending') {
                    i++;
                    return new Promise(resolve => {
                        // pause
                        setTimeout(() => resolve(), 500)
                    }).then(() => {
                        // send mail
                        return post('volunteer/sendmail', {
                            to: volunteer,
                            body: body.value,
                            subject: subject.value
                        })
                    });
                } else {    // else abort
                    return Promise.reject(`Aborted ${i}/${max}`)
                }
            }).catch(() => Promise.reject(`Error while sending email to volunteer ${volunteer}`));   // catch exceptions and pass them on
        }, Promise.resolve())
            // after sending all emails
            .then(() => {
                // fill progressbar
                progressBar.setAttribute('area-valuenow', `${i}`);
                progressBar.style.width = '100%';
                progressBar.textContent = `${i}/${max}`;
                progressBar.classList.remove('progress-bar-animated');
                // hide cancel and conform buttons, show close button
                cancel.style.display = 'none';
                close.style.display = 'inherit';
            })
            // on error stop animation
            .catch(error => {
                // fill progressbar with error
                progressBar.classList.remove('bg-success', 'progress-bar-animated');
                progressBar.classList.add('bg-danger');
                progressBar.textContent = error;

                // hide cancel and confirm buttons, show close button
                cancel.style.display = 'none';
                close.style.display = 'inherit';
            });
    };

    // on cancel close modal or abort preparation/sending
    document.getElementById('email-cancel').onclick = () => {
        const recStatus = Array.from(document.querySelectorAll('input[name="status"]'));
        const recGroups = Array.from(document.querySelectorAll('input[name="groups"]'));
        const subject = document.getElementById('email-subject');
        const body = document.getElementById('email-body');
        const send = document.getElementById('email-send');
        const confirm = document.getElementById('email-send-confirm');
        const cancel = document.getElementById('email-send-cancel');
        const close = document.getElementById('email-send-close');
        const modal = Modal.getOrCreateInstance('#modal-groupemail');

        switch (status) {
            case 'writing':     // close modal
                modal.hide();
                break;
            case 'preparing':   // open form, show send button again, hide confirm
                status = 'writing';

                recStatus.forEach(btn => btn.removeAttribute('disabled'));
                recGroups.forEach(btn => btn.removeAttribute('disabled'));
                subject.removeAttribute('disabled');
                body.removeAttribute('disabled');
                send.removeAttribute('disabled');
                send.style.display = 'inherit';
                confirm.style.display = 'none';
                break;
            case 'sending':     // abort sending, hide cancel, show close button
                status = 'aborted';

                recStatus.forEach(btn => btn.removeAttribute('disabled'));
                recGroups.forEach(btn => btn.removeAttribute('disabled'));
                subject.removeAttribute('disabled');
                body.removeAttribute('disabled');
                cancel.style.display = 'none';
                close.style.display = 'inherit';
                break;
        }
    };

    document.getElementById('modal-groupemail').addEventListener('show.bs.modal', () => {
        status = 'writing';

        const recStatus = Array.from(document.querySelectorAll('input[name="status"]'));
        const recGroups = Array.from(document.querySelectorAll('input[name="groups"]'));
        const to = Array.from(document.querySelectorAll('input[name="to"]'));
        const subject = document.getElementById('email-subject');
        const body = document.getElementById('email-body');
        const send = document.getElementById('email-send');
        const cancel = document.getElementById('email-cancel');
        const close = document.getElementById('email-close');
        const progressDiv = document.getElementById('email-progress-div');

        recStatus.forEach(btn => btn.removeAttribute('disabled'));
        recGroups.forEach(btn => btn.removeAttribute('disabled'));
        to.forEach(btn => btn.removeAttribute('disabled'));
        subject.removeAttribute('disabled');
        body.removeAttribute('disabled');
        send.removeAttribute('disabled');
        send.style.display = 'inherit';
        cancel.style.display = 'inherit';
        close.style.display = 'none';
        progressDiv.style.display = 'none';
    })
});
