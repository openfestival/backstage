import {del, post} from '../api';

window.addEventListener('DOMContentLoaded', () => {
    document.querySelectorAll('input[name="volunteer-group"]').forEach(
        input => input.addEventListener('change', e => {
            const checkbox = e.target;
            checkbox.setAttribute('disabled', true);

            if (checkbox.checked) {
                post('volunteergroup', {
                    volunteer: parseInt(checkbox.dataset.volunteer),
                    group: parseInt(checkbox.dataset.group)
                }).then(() => {
                    checkbox.removeAttribute('disabled');
                }).catch(error => {
                    console.error(error);
                    checkbox.removeAttribute('disabled');
                })
            } else {
                del('volunteergroup', {
                    volunteer: parseInt(checkbox.dataset.volunteer),
                    group: parseInt(checkbox.dataset.group)
                }).then(() => {
                    checkbox.removeAttribute('disabled');
                }).catch(error => {
                    console.error(error);
                    checkbox.removeAttribute('disabled');
                })
            }
        })
    );
});
