document.querySelectorAll('th.sortable').forEach(th => {
    th.addEventListener('click', () => {
        if (!th.classList.contains('asc')) {
            document.querySelectorAll('th.sortable').forEach(x => x.classList.remove('asc', 'desc'));
            th.classList.add('asc');
            setLocation(th.dataset.sort, 'ASC');
        } else {
            document.querySelectorAll('th.sortable').forEach(x => x.classList.remove('asc', 'desc'));
            th.classList.add('desc');
            setLocation(th.dataset.sort, 'DESC');
        }
    });
});

function setLocation(sort, order) {
    const params = new URLSearchParams(window.location.search);
    params.set('sort', sort);
    params.set('order', order);
    params.delete('page');
    window.location.search = params.toString();
}

// scroll to last entry
const params = new URLSearchParams(window.location.search);
if (params.has('last')) {
    const last = params.get('last');
    const row = document.querySelector(`tr[data-application="${last}"]`);
    if (row != null) {
        row.scrollIntoView({
            behavior: 'auto',
            block: 'center',
            inline: 'nearest'
        });
    }
}

// stretch links
document.querySelectorAll('tr[data-application]').forEach(tr => {
    tr.addEventListener('click', event => {
        // if the clicked element was not a link by itself
        if (event.target.tagName !== 'I' && event.target.tagName !== 'A') {
            event.preventDefault();
            document.getElementById(`a-${tr.dataset.application}`).click();
        }
    });
});
