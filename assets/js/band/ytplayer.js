// load and insert YouTube iframe API
var tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// This function is called by the YT iframe API
global.onYouTubeIframeAPIReady = () => {
    $(".ytplayer").each(function () {
        new YT.Player($(this).attr('id'), {
            events: {
                'onStateChange': onPlayerStateChange
            }
        });
    });
};

// The API calls this function when the player's state changes.
function onPlayerStateChange(event) {
    if (event.data === 1) {
        $(".carousel-control").hide();
    } else {
        $(".carousel-control").show();
    }
}
