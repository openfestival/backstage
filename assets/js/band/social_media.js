document.querySelectorAll('input[data-socialmedia]').forEach(input => {
    // validate all inputs on page load
    validate(input);
    // on focus insert the url (if value is valid)
    input.onfocus = () => {
        if (isValidated(input)) {
            input.value = getUrl(input.value, input.dataset.socialmedia);
            input.select();
        } else {
            validate(input);
        }
    };
    // on change validate
    input.onchange = () => {
        validate(input);
    };
    // on blur remove url (if value is valid)
    input.onblur = () => {
        if (isValidated(input)) {
            input.value = getValue(input);
        }
    };
});

document.querySelectorAll('a[data-socialmedia]').forEach(a => {
    a.href = getUrl(a.dataset.value, a.dataset.socialmedia);
});

function isValidated(input) {
    return input.dataset.validated === 'true';
}

function setValidated(input, validated) {
    input.dataset.validated = validated;
}

function validate(input) {
    if (!input.value) {
        setValidated(input, false);
        clearError(input);
        clearLink(input);
        return;
    }
    const value = getValue(input);
    if (value == null) {
        setValidated(input, false);
        setError(input);
        clearLink(input);
    } else {
        setValidated(input, true);
        clearError(input);
        setLink(input, value);
    }
}

function getUrl(value, socialmedia) {
    switch (socialmedia) {
        case 'facebook':
            return 'https://www.facebook.com/' + value;
        case 'instagram':
            return 'https://www.instagram.com/' + value;
        case 'youtube':
            if (value.match(/^[a-zA-Z0-9_-]{24}$/) == null)
                return 'https://youtube.com/' + value;
            return 'https://youtube.com/channel/' + value;
        case 'spotify':
            return 'https://open.spotify.com/artist/' + value;
        case 'twitter':
            return 'https://x.com/' + value;
        default:
            return value;
    }
}

function getValue(input) {
    const value = input.value;
    let match;
    switch (input.dataset.socialmedia) {
        case 'website':
            match = value.match(/(^https?:\/\/.+\.[a-z]{2,4}.*$)/i);
            if (match == null)
                return null;
            return match[0];
        case 'facebook':
            match = value.match(/^((https?:\/\/)?([a-z-]+\.)?facebook\.(com|de)\/)?([a-z0-9.]+)\/?(\?.*)?$/i);
            if (match == null)
                return null;
            return match[5];
        case 'instagram':
            match = value.match(/^((https?:\/\/)?(www\.)?instagram\.com\/)?([a-z0-9_.]+)\/?(\?.*)?$/i);
            if (match == null)
                return null;
            return match[4];
        case 'youtube':
            match = value.match(/^((https?:\/\/)?(www\.|m\.)?youtube\.com\/)?(.+)$/i);
            if (match == null)
                return null;
            return match[4];
        case 'spotify':
            match = value.match(/^((https?:\/\/)?open\.spotify\.com(\/.+)?\/artist\/)?([a-z0-9]+)(\?.*)?$/i);
            if (match == null)
                return null;
            return match[4];
        case 'twitter':
            match = value.match(/^((https?:\/\/)?(www\.)?(twitter|x)\.com\/)?([a-z0-9_]+)$/i);
            if (match == null)
                return null;
            return match[5];
        default:
            return value;
    }
}

function setError(input) {
    const a = input.previousElementSibling.firstElementChild;
    const icon = a.firstElementChild;
    if (!icon.classList.contains('text-danger')) {
        icon.classList.add('text-danger');
        const error = document.createElement('small');
        error.classList.add('text-danger');
        error.innerHTML = input.dataset.error;
        input.parentElement.parentElement.appendChild(error);
    }

    a.href = '#';
    a.classList.remove('icon');
    a.classList.add('disabled');
}

function clearError(input) {
    const icon = input.previousElementSibling.firstElementChild.firstElementChild;
    if (icon.classList.contains('text-danger')) {
        icon.classList.remove('text-danger');
        const error = input.parentElement.parentElement.lastElementChild;
        error.remove();
    }
}

function setLink(input, value) {
    const a = input.previousElementSibling.firstElementChild;
    a.href = getUrl(value, input.dataset.socialmedia);
    a.classList.remove('disabled');
    a.classList.add('icon');
}

function clearLink(input) {
    const a = input.previousElementSibling.firstElementChild;
    a.href = '#';
    a.classList.add('disabled');
    a.classList.remove('icon')
}
