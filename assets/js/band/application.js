import {del, get, post, put} from '../api';

const header = document.querySelector('.score-header');
const headerParent = header.parentElement;
window.addEventListener('scroll', () => {
    if (!header.classList.contains('fixed')) {
        if (headerParent.getBoundingClientRect().top < 0)
            header.classList.add('fixed');
    } else {
        if (headerParent.getBoundingClientRect().top > 0)
            header.classList.remove('fixed');
    }
});

document.querySelectorAll('input[name="score"]').forEach(input => {
    input.addEventListener('change', event => {
        put('band/score', {
            application: parseInt(input.dataset.application),
            score: parseInt(input.value)
        }).then(result => {
            // set buttons to selected color
            document.querySelectorAll('label[data-score]')
                .forEach(label => {
                    label.dataset.score = input.value;
                    if (document.getElementById(label.htmlFor).checked)
                        label.classList.add('active');
                    else label.classList.remove('active');
                });
            // set overall score
            document.querySelectorAll('span[data-score]')
                .forEach(span => {
                    span.dataset.score = String(Math.floor(result.score));
                    span.innerHTML = result.score.toFixed(2);
                });
            document.getElementById('score-count').innerHTML = `(${result.count})`;
            input.blur();
        }).catch(err => {
            console.log(err)
        });
    });
});

const favorite = document.getElementById('favorite');
if (favorite != null) {
    favorite.onclick = () => {
        const icon = favorite.firstElementChild;
        icon.classList.add('fa-spin');
        if (favorite.dataset.selected) {
            del('band/favorite/' + favorite.dataset.application)
                .then(() => {
                    delete favorite.dataset.selected;
                    favorite.classList.add('btn-light');
                    favorite.classList.remove('btn-warning');
                    icon.classList.remove('fa-spin');
                    icon.classList.add('text-muted');
                    favorite.blur();
                })
                .catch(err => {
                    console.log(err);
                });
        } else {
            put('band/favorite/' + favorite.dataset.application)
                .then(() => {
                    favorite.dataset.selected = 'true';
                    favorite.classList.remove('btn-light');
                    favorite.classList.add('btn-warning');
                    icon.classList.remove('fa-spin', 'text-muted');
                    favorite.blur();
                })
                .catch(err => {
                    console.log(err);
                });
        }
    };
}

// media preview clicks
document.querySelectorAll('a[data-slide-to]').forEach(a => {
    a.onclick = () => {
        const target = document.getElementById(a.getAttribute('href').substring(1));
        if (!isElementInViewport(target)) {
            target.scrollIntoView({
                behavior: 'smooth',
                block: 'center'
            });
        }
        const carousel = bootstrap.Carousel.getOrCreateInstance(target);
        carousel.to(a.dataset.slideTo);
    }
});

document.querySelectorAll('a[href="#spotify-preview"]').forEach(a => {
    a.onclick = event => {
        event.preventDefault();
        const target = document.getElementById('spotify-preview');
        if (!isElementInViewport(target)) {
            target.scrollIntoView({
                behavior: 'smooth',
                block: 'center'
            })
        }
    }
});

function isElementInViewport(el) {
    var rect = el.getBoundingClientRect();

    return (
        rect.top >= 0 &&
        rect.left >= 0 &&
        rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /*or $(window).height() */
        rect.right <= (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */
    );
}

// load discussion
const discussion = document.getElementById('discussion');
get('band/comments/' + discussion.dataset.application)
    .then(comments => {
        document.getElementById('comment-loader').remove();
        document.getElementById('comment-action').style.display = 'inherit';
        if (comments.length === 0) {
            const div = document.createElement('div');
            div.id = 'comment-empty';
            div.innerHTML = 'Hier scheint nichts zu sein...';
            discussion.appendChild(div);
        } else {
            for (const comment of comments) {
                const div = createComment(comment);
                discussion.appendChild(div);
            }
        }
    })
    .catch(err => {
        console.log(err)
    });

// send and cancel behaviour
const input = document.getElementById('comment-input');
const send = document.getElementById('comment-send');
const cancel = document.getElementById('comment-cancel');
send.onclick = () => {
    if (!input.value)
        return;
    input.setAttribute('disabled', '');
    send.setAttribute('disabled', '');
    cancel.setAttribute('disabled', '');
    if (input.dataset.edit) {
        // edit comment
        put('band/comments/' + input.dataset.edit, {
            comment: input.value
        }).then(() => {
            const div = document.getElementById('comment-' + input.dataset.edit);
            div.classList.remove('edit');
            div.childNodes[1].innerHTML = nl2br(input.value);
            input.value = '';
            delete input.dataset.edit;
            input.removeAttribute('disabled');
            send.removeAttribute('disabled');
            cancel.removeAttribute('disabled');
        }).catch(err => {
            console.log(err);
        });
    } else {
        // create new comment
        post('band/comments', {
            application: parseInt(discussion.dataset.application),
            comment: input.value
        }).then(response => {
            const empty = document.getElementById('comment-empty');
            if (empty != null)
                empty.remove();
            const div = createComment({
                id: response.id,
                author_id: parseInt(discussion.dataset.authorid),
                author_name: discussion.dataset.authorname,
                author_email: discussion.dataset.authoremail,
                comment: input.value,
                created_at: 'gerade eben',
                edited_at: null
            });
            discussion.appendChild(div);
            input.value = '';
            input.removeAttribute('disabled');
            send.removeAttribute('disabled');
            cancel.removeAttribute('disabled');
        }).catch(err => {
            console.log(err);
        });
    }
};

cancel.onclick = () => {
    if (input.dataset.edit) {
        const div = document.getElementById('comment-' + input.dataset.edit);
        div.classList.remove('edit');
        delete input.dataset.edit;
    }
    input.value = '';
};

// Helper function for newline conversion
function nl2br(text) {
    return text.replace(/\n/g, '<br />');
}

function br2nl(text) {
    return text.replace(/<br\s*\/?>/g, '\n');
}

function createComment(comment) {
    // outer div
    const div = document.createElement('div');
    div.id = 'comment-' + comment.id;
    div.dataset.id = comment.id;
    div.classList.add('comment');
    if (comment.author_id === parseInt(discussion.dataset.authorid))
        div.classList.add('self');
    // name
    const name = document.createElement('span');
    name.classList.add('comment-name');
    name.innerHTML = comment.author_name || comment.author_email;
    div.appendChild(name);
    // edit button
    if (comment.author_id === parseInt(discussion.dataset.authorid)) {
        const edit = document.createElement('i');
        edit.classList.add('fas', 'fa-pen', 'comment-edit');
        edit.addEventListener('click', event => {
            // remove all other edit classes
            document.querySelectorAll('.comment.edit').forEach(c => c.classList.remove('edit'));
            const comment = event.currentTarget.parentElement.parentElement;
            input.value = br2nl(comment.childNodes[1].innerHTML);
            comment.classList.add('edit');
            input.dataset.edit = comment.dataset.id;
            input.focus();
        });
        name.appendChild(edit);
    }
    // text with newlines converted to <br>
    const text = document.createElement('div');
    text.innerHTML = nl2br(comment.comment);
    div.appendChild(text);
    // timestamp
    const time = document.createElement('span');
    time.classList.add('comment-time');
    time.innerHTML = comment.created_at;
    if (comment.edited_at != null)
        time.innerHTML += ' (' + comment.edited_at + ')';
    div.appendChild(time);

    return div;
}
