import {openSSE, put} from "../api";
import {appendHeader, appendTable, appendText, clearContent, setLoading} from "./modules";
import {Modal} from "bootstrap";

const content = document.getElementById('content');
const festival = parseInt(document.getElementById('festival-info').dataset.festival);
const voteTitle = document.getElementById('voteTitle');

// the band currently being voted on. Gets updated by the renderOpen method.
let currentVote = null;

// find score buttons, add event listener
const scoreButtons = document.querySelectorAll('button[data-score]');
scoreButtons.forEach(button => button.addEventListener('click', () => {
    scoreButtons.forEach(btn => {
        btn.disabled = true;
        btn.classList.remove('active');
    });
    setLoading(button);
    put('band/finals/' + festival + '/score', {
        application: currentVote.id,
        score: parseInt(button.dataset.score)
    }).then(() => {
        button.classList.add('active');
        button.innerHTML = button.dataset.score;
        scoreButtons.forEach(btn => {
            if (btn !== button)
                btn.disabled = false;
        });
        // enable action buttons after scoring
        actionButtons.forEach(btn => btn.disabled = false);
    });
}));

// find action buttons, add event listener
const actionButtons = document.querySelectorAll('button[data-action]');
actionButtons.forEach(button => button.addEventListener('click', () => {
    button.disabled = true;
    put('band/finals/' + festival + '/action', {
        application: currentVote.id,
        action: button.dataset.action,
        state: !button.classList.contains('active')
    }).then(() => {
        button.classList.toggle('active')
        button.disabled = false;
    })
}));

// wait for page to load completely
window.addEventListener('load', () => {
    // get status, discover mercure
    openSSE('band/finals/' + festival + '/status', 'band/finals/' + festival,
        message => {
            clearContent(content);
            switch (message.status) {
                case 'open':
                    renderOpen(message);
                    break;
                case 'finished':
                    renderFinished(message);
                    break;
                case 'closed':
                default:
                    renderClosed();
                    break;
            }
        })
        .catch(err => {
            console.error(err);
            content.innerText = 'An error occurred';
        });
});

function renderClosed() {
    appendHeader(content, 'Das Band Finale ist geschlossen.');
}

function renderOpen(message) {
    appendHeader(content, 'Das Band Finale ist geöffnet.');

    if (message.currentVote == null) {
        appendText(content, 'Momentan ist keine Band zur Abstimmung verfügbar.');
        // close vote modal
        const modal = Modal.getOrCreateInstance(document.getElementById('voteModal'));
        modal.hide();
    } else {
        currentVote = message.currentVote;
        // update titles
        appendText(content, 'Abstimmung über ' + message.currentVote.name + ' läuft.');
        voteTitle.innerText = "Abstimmung: " + message.currentVote.name;
        // clear action buttons
        actionButtons.forEach(btn => btn.classList.remove('active'));
        // open vote modal
        const modal = Modal.getOrCreateInstance(document.getElementById('voteModal'));
        modal.show();
    }

    appendTable(content, message.scores)
}

function renderFinished(message) {
    appendHeader(content, 'Das Band Finale ist beendet.');
    appendTable(content, message.scores)
}
