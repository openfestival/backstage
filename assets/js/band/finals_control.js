import {openSSE, put} from "../api";
import {appendButton, appendHeader, appendTable, appendText, clearContent, setLoading, updateTableRow} from "./modules";

const content = document.getElementById('content');
const festival = document.getElementById('festival-info').dataset.festival;

// init silent vote checkbox
const silentVoteInput = document.getElementById('silent-vote');
let silentVote = silentVoteInput.checked;
silentVoteInput.addEventListener('change', event => {
    silentVote = event.target.checked;
    // reload page with query parameter set to new value
    const url = new URL(document.location);
    url.searchParams.set('silent', silentVote);
    window.location = url.toString();
});

// wait for page to load completely
window.addEventListener('load', () => {
    // remove parameters from URL
    const url = new URL(document.location);
    url.searchParams.delete('vote');
    url.searchParams.delete('silent');
    window.history.replaceState({}, '', url.toString());

    // get status, discover mercure
    openSSE('band/finals/' + festival + '/status', [
        'band/finals/' + festival,
        'band/finals/control/' + festival
    ], message => {
        switch (message.status) {
            case 'open':
                renderOpen(message);
                break;
            case 'finished':
                renderFinished(message);
                break;
            case 'closed':
            default:
                renderClosed();
                break;
        }
    }).catch(err => {
        console.error(err);
        content.innerText = 'An error occurred';
    });
});

function renderClosed() {
    clearContent(content);
    appendHeader(content, 'Das Band Finale ist geschlossen.');

    // controls
    const button = appendButton(content, 'Start', () => update(festival, button, 'open'), 'btn-warning');
}

function renderOpen(message) {
    if (message.hasOwnProperty('scores')) {
        // render all scores
        clearContent(content);
        appendHeader(content, 'Das Band Finale ist geöffnet.');

        if (message.currentVote == null) {
            appendText(content, 'Momentan ist keine Band zur Abstimmung verfügbar.');
        } else {
            appendText(content, 'Abstimmung über ' + message.currentVote.name + ' läuft.');
        }

        appendTable(content, message.scores, message.currentVote);

        // controls
        if (message.currentVote == null) {
            const list = document.createElement('a');
            list.href = document.getElementById('list-link').dataset.href;
            list.classList.add('btn', 'btn-info');
            list.innerText = 'Bewerbungen';
            content.appendChild(list);

            const stop = appendButton(content, 'Stop', () => update(festival, stop, 'closed'), 'btn-warning', 'ms-2');
            const end = appendButton(content, 'Beenden', () => update(festival, end, 'finished'), 'btn-danger', 'ms-2');
        } else {
            const end = appendButton(content, 'Abstimmung beenden', () => update(festival, end, 'open'), 'btn-info')
        }
    } else if (message.hasOwnProperty('score')) {
        // only update if silent vote is not selected
        if (silentVote)
            return;
        // update single score in existing table.
        const table = content.querySelector('table');
        updateTableRow(table, message.currentVote, message.score);
    }
}

function renderFinished(message) {
    clearContent(content);
    appendHeader(content, 'Das Band Finale ist beendet.');

    appendTable(content, message.scores)

    // controls
    const button = appendButton(content, 'Start', () => update(festival, button, 'open'), 'btn-warning');
}

function update(festival, button, status) {
    setLoading(button);
    put('band/finals/' + festival + '/update', {
        'status': status
    });
}
