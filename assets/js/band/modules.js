function clearContent(node) {
    while (node.firstChild)
        node.removeChild(node.firstChild);
}

function appendText(node, text) {
    const p = document.createElement('p');
    p.innerText = text;
    node.appendChild(p);
    return p;
}

function appendHeader(node, text, ...classes) {
    const header = document.createElement('h5');
    if (classes.length > 0)
        header.classList.add(...classes);
    header.innerText = text;
    node.appendChild(header);
    return header;
}

function appendButton(node, text, callback, ...classes) {
    const button = document.createElement('button');
    button.type = 'button';
    button.classList.add('btn', ...classes);
    button.innerText = text;
    button.onclick = callback;
    node.appendChild(button);
    return button;
}

function setLoading(button) {
    button.disabled = true;
    const spinner = document.createElement('span');
    spinner.classList.add('spinner-border', 'spinner-border-sm');
    button.replaceChild(spinner, button.firstChild);
}

function appendTable(content, scores, currentVote = null) {
    const table = document.createElement('table');
    table.classList.add('table', 'table-light');

    let i = 1;
    for (const score of scores)
        appendTableRow(table, score, i++, currentVote);
    content.appendChild(table);
}

function updateTableRow(table, currentVote, score) {
    const tr = table.querySelector('tr[data-app="' + currentVote.id + '"]');

    if (tr == null) {
        appendTableRow(table, score, 0, currentVote);
    } else {
        let inserted = false;
        // find first row with bigger score, insert before that
        for (const row of table.childNodes) {
            if (row !== tr && parseFloat(score.score) < parseFloat(row.dataset.score)) {
                table.insertBefore(tr, row);
                inserted = true;
                break;
            }
        }
        // if no row is bigger, set as last
        if (!inserted)
            table.insertBefore(tr, undefined);

        // update content
        const bar = tr.querySelector('div.progress-bar');
        bar.innerText = score.score + ' (' + score.count + ')';
        bar.style.width = calcProgbarWidth(score.score) + '%';
        updateInfoColumn(tr.lastChild, score);
    }

    // update places
    let place = 1;
    for (const row of table.childNodes) {
        if (parseInt(row.dataset.app) === currentVote.id)
            row.classList.add('table-active');
        else row.classList.remove('table-active');
        row.firstChild.innerText = (place++) + '.';
    }
}

function appendTableRow(table, score, place, currentVote) {
    // create new row
    const tr = document.createElement('tr');
    tr.dataset.app = score.id;
    tr.dataset.score = score.score;
    if (currentVote != null && score.id === currentVote.id)
        tr.classList.add('table-active');

    // place column
    const c1 = document.createElement('th');
    c1.classList.add('align-middle');
    c1.scope = 'row';
    c1.style.width = '1%';
    c1.style.fontSize = '130%';
    c1.innerText = place + '.';

    // band column
    const c2 = document.createElement('td');
    c2.classList.add('text-left');

    // name, location, genres, fee
    const clearfix = document.createElement('div');
    clearfix.classList.add('clearfix');

    const name = document.createElement('a');
    name.classList.add('fw-bold');
    name.href = '/band/application/' + score.id;
    name.innerText = score.name;
    clearfix.appendChild(name);

    const location = document.createElement('span')
    location.classList.add('text-muted')
    location.innerText = ' (' + score.location + ')';
    clearfix.appendChild(location);

    for (const genre of score.genres) {
        const badge = document.createElement('span');
        badge.classList.add('badge', 'bg-secondary', 'ms-1');
        badge.textContent = genre;
        clearfix.appendChild(badge);
    }

    if (currentVote == null) {
        const startButton = document.createElement('a');
        startButton.href = '/band/finals/control?vote=' + score.id;
        startButton.classList.add('me-1', 'float-end');
        const startButtonIcon = document.createElement('i');
        startButtonIcon.classList.add('fas', 'fa-play-circle', 'text-info');
        startButton.appendChild(startButtonIcon);
        clearfix.appendChild(startButton);
    }

    c2.appendChild(clearfix);

    // progress bar
    const progress = document.createElement('div');
    progress.classList.add('progress', 'mt-1', 'p-0');

    const bar = document.createElement('div');
    bar.classList.add('progress-bar', 'bg-primary');
    bar.role = 'progressbar';
    bar.style.width = calcProgbarWidth(score.score) + '%';
    bar.style.transition = 'width 0.5s';
    bar.innerText = score.score + ' (' + score.count + ')';
    progress.appendChild(bar);
    c2.appendChild(progress);

    // info column
    const c3 = document.createElement('td');
    c3.style.width = '8%';
    updateInfoColumn(c3, score);

    // finish table
    tr.appendChild(c1);
    tr.appendChild(c2);
    tr.appendChild(c3);
    table.appendChild(tr);
}

function updateInfoColumn(column, score) {
    clearContent(column);
    if (parseInt(score.opendoor) > 0) {
        const opendoor = document.createElement('i')
        opendoor.classList.add('fas', 'fa-sm', 'fa-door-open')
        column.appendChild(opendoor);
        column.appendChild(document.createTextNode(' x' + score.opendoor));
        column.appendChild(document.createElement('br'))
    }
    if (parseInt(score.aftershow) > 0) {
        const aftershow = document.createElement('i')
        aftershow.classList.add('fas', 'fa-sm', 'fa-cocktail')
        column.appendChild(aftershow);
        column.appendChild(document.createTextNode(' x' + score.aftershow));
        column.appendChild(document.createElement('br'))
    }
    if (parseInt(score.budget) > 0) {
        const budget = document.createElement('i')
        budget.classList.add('fas', 'fa-sm', 'fa-euro-sign')
        column.appendChild(budget);
        column.appendChild(document.createTextNode(' x' + score.budget));
    }
}

function calcProgbarWidth(score) {
    return Math.round(((7 - score) / 6) * 100);
}

export {clearContent, appendHeader, appendText, appendButton, setLoading, appendTable, updateTableRow};