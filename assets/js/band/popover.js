import { Popover } from 'bootstrap'

document.addEventListener('DOMContentLoaded', function() {
    document.querySelectorAll('[data-bs-toggle="popover"]').forEach(element => {
        Popover.getOrCreateInstance(element, {
            html: true,
            trigger: 'focus'
        });
    });
});
