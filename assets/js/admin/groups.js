import { createApp } from 'vue';
import GroupsTable from './GroupsTable.vue';

createApp(GroupsTable)
    .mount('#vue-groups');
