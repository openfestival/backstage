import { createApp } from 'vue';
import InheritanceList from './InheritanceList.vue';

const appRoot = document.getElementById('vue-inheritances')
createApp(InheritanceList, appRoot.dataset)
    .mount(appRoot);
