## Namespace Structure

The `src` directory contains all PHP files. For each functionality of the backstage tool a dedicated namespace exists. For example all inventory related files can be found in the `src/Inventory` directory. The individual namespaces are structured like usual symfony applications:

* `Application`: Contains the actual application logic. Classes in this namespace are used by the controllers.
* `Command`: Commands related to the namespace
* `Controller`: Contains all route definitions. Handles HTTP requests and uses the application classes to interact with data.
* `Entity`: Contains all doctrine entities.
* `EventSubscriber`: Contains all event subscribers.
* `Exception`: Contains all exceptions.
* `Enum`: Contains enums used in the namespace.
* `Form`: Contains all Forms and Form Types used in the namespace.
* `Repository`: Manages all interactions with the database.
* `Request`: Request related helpers like parameter converters
* `Validator`: Form validators
* `Voter`: Security voters

TODO...

### Adding new Namespaces

When adding new namespaces you need to configure the following packages:
* New controllers need to be configured in `config/routes/annotations.yaml`
* New entities need to be added in `config/packages/doctrine.yaml`

## Permissions
* Permissions should always be named by what they are for, not by the group or user who will be granted the permission
* Permissions should be grouped by topic, using a period `.` as delimiter

## Routing and Controllers
* Controller names should indicate the function of the controller. If a controller matches routes for many unrelated functions this is a hint to split up the controller into multiple controllers.
* Wherever possible the last named part of the route path should match the function name in the controller
  * e.g. The route `/shift/edit/42` is handled by the `edit` function in the `ShiftController`.
* API routes should always be handled by a separate controller ending with `ApiController`.
* Any route which does not render an HTML page but fulfills some kind of API purpose must be located under the `/api/` path.
* Administrative pages should always be located under the `/admin/` path, then followed by their namespace.

## Twig Templates
* Templates which are rendered by controllers should be named like the method rendering the template
* Files should be organized into directories matching the route that renders them
* Files containing components that are included by other templates using embed or include shoud always start with an underscore
  * e.g. `_sidebar_drawer.html.twig` contains a component that is never rendered by itself but always included in another template

## Forms
* Custom forms that are rendered directly should always be named `Form`.
  * These forms often include a Submit type and other fields not related to the entity but only the HTML rendering 
* Custom forms that are included in other forms and represent entities or other data types should be named `Type`.

## Mercure

Mercure is used to push data to clients. The mercure hub is an independent server. It's parameters are defined in the environment files:

* `MERCURE_URL`: The URL of the Mercure hub, used by the app to publish updates (can be a local URL)
* `MERCURE_PUBLIC_URL`: The public URL of the Mercure hub, used by the browser to connect. Must be available from the internet.
* `MERCURE_JWT_SECRET`: the key to sign the JWT. Choose a strong random value. 
* `MERCURE_TOPIC_URI`: the URI (including scheme) used to create topics. Can be the same as the web server domain.

The secret used to sign the JWT must be the same value as the `subscriber_jwt` and `publisher_jwt` from the mercure hub configuration (typically in `/etc/mercure/Caddyfile`).

### Using mercure

Typically mercure is used when data is continuously updated while a page is loaded by the browser. There are three steps required to enable a browser to subscribe to updates:

* The correct authorization cookie needs to be sent to the browser. This cookie contains all topics the client should be allowed to receive updates for. Normally it is set once when the page is loaded (in the route rendering the template).
  * Use the `authorizeSubscription(Request, Topics)` method from the `MercureService` class.
* The mercure hub needs to be discovered. This is done in the first request for "live" data.
  * Use the `addDiscovery(Request)` method from the `MercureService` class.
* The topic URI used to subscribe to all topics needs to be known by the client. This is also done in the first request for "live" data.
  * Use the `finalizeResponse(Response)` method from the `MercureService` class.

# ===== OUTDATED! =====
## Controllers and Routes
#### HTML Pages
* Try keeping all routes related to the same topic in one controller
* e.g. `/band/apply` and `/band/applications`
  
#### API Pages
* API routes are all routes returning non text/html content
* They should still be defined in the same controller as their topic
* paths should be prefixed with `/api`
* e.g. `/api/band/score`
