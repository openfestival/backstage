import { defineConfig } from 'vite';
import symfonyPlugin from 'vite-plugin-symfony';
import vuePlugin from '@vitejs/plugin-vue';

const path = require('path');

export default defineConfig({
    plugins: [
        symfonyPlugin(),
        vuePlugin(),
    ],
    build: {
        rollupOptions: {
            input: {
                'app': './assets/js/app.js',
                'app_sidebar': './assets/js/app_sidebar.js',

                'theme': './assets/css/app.scss',
                'theme_sidebar': './assets/css/app_sidebar.scss',
                'theme_shift': './assets/css/shift.scss',
                'theme_inventory': './assets/css/inventory.scss',

                'admin/groups': './assets/js/admin/groups.js',
                'admin/users': './assets/js/admin/users.js',
                'band/application': './assets/js/band/application.js',
                'band/applications': './assets/js/band/applications.js',
                'band/finals': './assets/js/band/finals.js',
                'band/finals_control': './assets/js/band/finals_control.js',
                'band/popover': './assets/js/band/popover.js',
                'band/social_media': './assets/js/band/social_media.js',
                'band/ytplayer': './assets/js/band/ytplayer.js',
                'festival/festival': './assets/js/festival/festival.js',
                'guestlist/bands': './assets/js/guestlist/bands/bands.js',
                'guestlist/checkin': './assets/js/guestlist/checkin/checkin.js',
                'guestlist/guestlist': './assets/js/guestlist/guestlist.js',
                'inventory/categories': './assets/js/inventory/categories.js',
                'inventory/item': './assets/js/inventory/item.js',
                //'inventory/scan': './assets/js/inventory/scan.js',
                'shift/self': './assets/js/shift/self.js',
                'shift/shift': './assets/js/shift/shift.js',
                'staff/staff': './assets/js/staff/staff.js',
                'staff/volunteer': './assets/js/staff/volunteer.js',
                'templates/templates': './assets/js/templates/templates.js',
                'volunteer': './assets/js/volunteer.js',
            },
        }
    },
    resolve: {
        alias: {
            '~bootstrap': path.resolve(__dirname, 'node_modules/bootstrap'),
            '~bootstrap-select': path.resolve(__dirname, 'node_modules/bootstrap-select'),
            '~fontawesome': path.resolve(__dirname, 'node_modules/@fortawesome/fontawesome-free'),
        }
    },
    server: {
        host: 'localhost',
    }
});
